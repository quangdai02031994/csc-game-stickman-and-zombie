
up_player.png
format: RGBA8888
filter: Linear,Linear
repeat: none
aim
  rotate: false
  xy: 587, 497
  size: 18, 28
  orig: 18, 28
  offset: 0, 0
  index: -1
ak47
  rotate: false
  xy: 834, 456
  size: 132, 61
  orig: 132, 61
  offset: 0, 0
  index: -1
arm1
  rotate: true
  xy: 916, 62
  size: 22, 30
  orig: 22, 30
  offset: 0, 0
  index: -1
arm2
  rotate: false
  xy: 795, 488
  size: 37, 29
  orig: 37, 29
  offset: 0, 0
  index: -1
arm2_1
  rotate: true
  xy: 834, 232
  size: 47, 93
  orig: 47, 93
  offset: 0, 0
  index: -1
arm3
  rotate: false
  xy: 607, 584
  size: 13, 16
  orig: 13, 16
  offset: 0, 0
  index: -1
arrow
  rotate: false
  xy: 398, 16
  size: 76, 9
  orig: 76, 9
  offset: 0, 0
  index: -1
awp
  rotate: false
  xy: 607, 530
  size: 206, 52
  orig: 206, 52
  offset: 0, 0
  index: -1
bazoka
  rotate: false
  xy: 393, 527
  size: 212, 73
  orig: 212, 73
  offset: 0, 0
  index: -1
body1
  rotate: true
  xy: 806, 99
  size: 19, 65
  orig: 19, 65
  offset: 0, 0
  index: -1
bow
  rotate: true
  xy: 795, 65
  size: 32, 62
  orig: 32, 62
  offset: 0, 0
  index: -1
bow_down
  rotate: true
  xy: 873, 91
  size: 27, 41
  orig: 27, 41
  offset: 0, 0
  index: -1
bow_thin
  rotate: false
  xy: 279, 206
  size: 2, 54
  orig: 2, 54
  offset: 0, 0
  index: -1
bow_up
  rotate: true
  xy: 877, 120
  size: 38, 64
  orig: 38, 64
  offset: 0, 0
  index: -1
eye
  rotate: false
  xy: 795, 465
  size: 34, 21
  orig: 34, 21
  offset: 0, 0
  index: -1
eye1
  rotate: false
  xy: 916, 86
  size: 29, 32
  orig: 29, 32
  offset: 0, 0
  index: -1
eye2
  rotate: false
  xy: 921, 160
  size: 33, 36
  orig: 33, 36
  offset: 0, 0
  index: -1
eye3
  rotate: false
  xy: 921, 198
  size: 45, 32
  orig: 45, 32
  offset: 0, 0
  index: -1
eye4
  rotate: true
  xy: 878, 26
  size: 30, 35
  orig: 44, 39
  offset: 14, 4
  index: -1
eye5
  rotate: false
  xy: 929, 243
  size: 31, 36
  orig: 31, 36
  offset: 0, 0
  index: -1
eye6
  rotate: false
  xy: 776, 8
  size: 30, 50
  orig: 30, 50
  offset: 0, 0
  index: -1
eye7
  rotate: false
  xy: 845, 27
  size: 31, 36
  orig: 42, 40
  offset: 11, 4
  index: -1
eye8
  rotate: true
  xy: 878, 58
  size: 31, 36
  orig: 31, 36
  offset: 0, 0
  index: -1
firegun
  rotate: false
  xy: 633, 186
  size: 102, 50
  orig: 102, 50
  offset: 0, 0
  index: -1
firegun_thin
  rotate: true
  xy: 446, 181
  size: 15, 16
  orig: 15, 16
  offset: 0, 0
  index: -1
flash1
  rotate: false
  xy: 832, 160
  size: 87, 70
  orig: 94, 81
  offset: 1, 3
  index: -1
flash2
  rotate: false
  xy: 737, 166
  size: 93, 75
  orig: 94, 81
  offset: 1, 2
  index: -1
flash3
  rotate: true
  xy: 623, 2
  size: 87, 80
  orig: 94, 81
  offset: 0, 1
  index: -1
gas
  rotate: false
  xy: 932, 591
  size: 32, 77
  orig: 32, 77
  offset: 0, 0
  index: -1
gatlinggun
  rotate: true
  xy: 562, 183
  size: 182, 69
  orig: 182, 69
  offset: 0, 0
  index: -1
gatlinggun1
  rotate: true
  xy: 623, 91
  size: 90, 31
  orig: 90, 31
  offset: 0, 0
  index: -1
head
  rotate: false
  xy: 733, 60
  size: 60, 55
  orig: 60, 55
  offset: 0, 0
  index: -1
hip
  rotate: false
  xy: 734, 240
  size: 1, 1
  orig: 1, 1
  offset: 0, 0
  index: -1
hit
  rotate: false
  xy: 2, 2
  size: 275, 258
  orig: 280, 258
  offset: 0, 0
  index: -1
hit1
  rotate: false
  xy: 393, 602
  size: 279, 258
  orig: 280, 258
  offset: 1, 0
  index: -1
hit2
  rotate: false
  xy: 2, 262
  size: 279, 258
  orig: 280, 258
  offset: 1, 0
  index: -1
hit3
  rotate: true
  xy: 674, 584
  size: 276, 256
  orig: 280, 258
  offset: 1, 0
  index: -1
lazer
  rotate: false
  xy: 815, 519
  size: 150, 63
  orig: 150, 63
  offset: 0, 0
  index: -1
lazer_bullet
  rotate: true
  xy: 656, 97
  size: 87, 40
  orig: 104, 40
  offset: 17, 0
  index: -1
lazer_bullet_item
  rotate: false
  xy: 808, 23
  size: 35, 40
  orig: 35, 40
  offset: 0, 0
  index: -1
lazer_item1
  rotate: true
  xy: 698, 94
  size: 90, 30
  orig: 90, 30
  offset: 0, 0
  index: -1
lazer_item2
  rotate: false
  xy: 806, 120
  size: 69, 38
  orig: 69, 38
  offset: 0, 0
  index: -1
light1
  rotate: false
  xy: 283, 181
  size: 161, 157
  orig: 167, 169
  offset: 6, 11
  index: -1
light2
  rotate: false
  xy: 424, 367
  size: 161, 158
  orig: 167, 169
  offset: 5, 11
  index: -1
light3
  rotate: false
  xy: 793, 866
  size: 164, 164
  orig: 167, 169
  offset: 0, 0
  index: -1
light4
  rotate: true
  xy: 398, 27
  size: 152, 123
  orig: 167, 169
  offset: 6, 25
  index: -1
phone
  rotate: true
  xy: 730, 117
  size: 47, 74
  orig: 47, 74
  offset: 0, 0
  index: -1
pistol1
  rotate: true
  xy: 733, 2
  size: 56, 41
  orig: 56, 41
  offset: 0, 0
  index: -1
shortgun
  rotate: false
  xy: 607, 465
  size: 186, 63
  orig: 186, 63
  offset: 0, 0
  index: -1
shortgun1
  rotate: true
  xy: 705, 12
  size: 80, 26
  orig: 80, 26
  offset: 0, 0
  index: -1
slash1
  rotate: false
  xy: 2, 862
  size: 398, 168
  orig: 398, 168
  offset: 0, 0
  index: -1
slash2
  rotate: false
  xy: 2, 692
  size: 389, 168
  orig: 398, 168
  offset: 0, 0
  index: -1
slash3
  rotate: false
  xy: 402, 862
  size: 389, 168
  orig: 398, 168
  offset: 0, 0
  index: -1
slash4
  rotate: false
  xy: 2, 522
  size: 389, 168
  orig: 398, 168
  offset: 0, 0
  index: -1
smoke
  rotate: false
  xy: 834, 281
  size: 112, 120
  orig: 112, 120
  offset: 0, 0
  index: -1
sw1
  rotate: true
  xy: 446, 198
  size: 167, 114
  orig: 184, 139
  offset: 6, 17
  index: -1
sw2
  rotate: true
  xy: 283, 340
  size: 180, 139
  orig: 184, 139
  offset: 4, 0
  index: -1
sw3
  rotate: true
  xy: 279, 9
  size: 170, 117
  orig: 184, 139
  offset: 3, 13
  index: -1
sw4
  rotate: true
  xy: 523, 2
  size: 179, 98
  orig: 184, 139
  offset: 0, 27
  index: -1
sword
  rotate: false
  xy: 932, 670
  size: 18, 194
  orig: 18, 194
  offset: 0, 0
  index: -1
thundergun
  rotate: false
  xy: 826, 403
  size: 135, 51
  orig: 135, 51
  offset: 0, 0
  index: -1
wp1_e1
  rotate: false
  xy: 722, 367
  size: 102, 96
  orig: 133, 99
  offset: 17, 1
  index: -1
wp1_e2
  rotate: true
  xy: 734, 243
  size: 122, 98
  orig: 133, 99
  offset: 6, 0
  index: -1
wp1_e3
  rotate: true
  xy: 633, 238
  size: 127, 99
  orig: 133, 99
  offset: 4, 0
  index: -1
wp1_e4
  rotate: false
  xy: 587, 367
  size: 133, 96
  orig: 133, 99
  offset: 0, 1
  index: -1
