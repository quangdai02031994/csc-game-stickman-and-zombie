
die_effect.png
format: RGBA8888
filter: Linear,Linear
repeat: none
blood
  rotate: false
  xy: 2, 46
  size: 119, 31
  orig: 119, 31
  offset: 0, 0
  index: -1
die1
  rotate: false
  xy: 54, 6
  size: 44, 38
  orig: 44, 38
  offset: 0, 0
  index: -1
die2
  rotate: true
  xy: 100, 2
  size: 42, 20
  orig: 42, 20
  offset: 0, 0
  index: -1
die3
  rotate: true
  xy: 2, 2
  size: 42, 50
  orig: 42, 50
  offset: 0, 0
  index: -1
die4
  rotate: true
  xy: 122, 14
  size: 30, 27
  orig: 30, 27
  offset: 0, 0
  index: -1
die5
  rotate: false
  xy: 123, 53
  size: 24, 24
  orig: 24, 24
  offset: 0, 0
  index: -1
