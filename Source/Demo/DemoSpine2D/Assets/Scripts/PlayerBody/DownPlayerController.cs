﻿using UnityEngine;
using System.Collections;
using Spine.Unity;
using Spine;
using GameEnum;

public class DownPlayerController : MonoBehaviour
{

    [SerializeField]
    private SkeletonAnimation skeletonAnimation;

    [SerializeField]
    private Skeleton skeleton;

    [SerializeField]
    private Spine.AnimationState state;

    [SpineAnimation]
    public string idle = "idle";

    [SpineAnimation]
    public string run = "run";

    [SpineAnimation]
    public string stand = "stand";

    public ANIMATION_STATE animationState = ANIMATION_STATE.IDLE_LEFT;

    #region Behaviour
    private void Start()
    {
        skeleton = skeletonAnimation.Skeleton;
        state = skeletonAnimation.state;
        onPlayAnimation(idle, false);
    }


    private void Update()
    {
#if UNITY_EDITOR

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            if (animationState != ANIMATION_STATE.RUN_LEFT)
            {
                animationState = ANIMATION_STATE.RUN_LEFT;
                onPlayAnimation(run, true);
            }
        }
        else if (Input.GetKey(KeyCode.RightArrow))
        {
            if (animationState != ANIMATION_STATE.RUN_RIGHT)
            {
                animationState = ANIMATION_STATE.RUN_RIGHT;
                onPlayAnimation(run, false);
            }
        }

        if (Input.GetKeyUp(KeyCode.LeftArrow))
        {
            if (animationState != ANIMATION_STATE.IDLE_LEFT)
            {
                onPlayAnimation(idle, true);
                animationState = ANIMATION_STATE.IDLE_LEFT;
            }
        }
        else if (Input.GetKeyUp(KeyCode.RightArrow))
        {
            if (animationState != ANIMATION_STATE.IDLE_RIGHT)
            {
                onPlayAnimation(idle, false);
                animationState = ANIMATION_STATE.IDLE_RIGHT;
            }
        }
#endif

    }

    #endregion

    private void onPlayAnimation(string animationName, bool FlipX)
    {
        state.SetAnimation(1, animationName, true);
        skeleton.FlipX = FlipX;
    }


}
