﻿using UnityEngine;
using System.Collections;
using Spine.Unity;
using Spine;
using GameEnum;

public class UpPlayerController : MonoBehaviour
{

    [SerializeField]
    private SkeletonAnimation skeletonAnimation;

    [SerializeField]
    private Skeleton skeleton;

    [SerializeField]
    private Spine.AnimationState state;

    [SpineAnimation]
    public string idle = "idle";

    [SpineAnimation]
    public string attack = "wp";

    [SpineAnimation]
    public string shooting = "wp1";

    public ANIMATION_STATE animationState = ANIMATION_STATE.IDLE_LEFT;

    private bool isAttacking = false;

    #region Behaviour
    private void Awake()
    {
        skeleton = skeletonAnimation.Skeleton;
        state = skeletonAnimation.state;
        onPlayAnimation(idle, false, true);
    }

    private void OnEnable()
    {
        state.Complete += onComplete;
    }

    private void OnDisable()
    {
        state.Complete -= onComplete;
    }


    private void Update()
    {
#if UNITY_EDITOR

        if (Input.GetKey(KeyCode.A))
        {
            if (animationState != ANIMATION_STATE.ATTACK_LEFT)
            {
                animationState = ANIMATION_STATE.ATTACK_LEFT;
                onPlayAnimation(attack, true);
                isAttacking = true;
            }
        }
        else if (Input.GetKey(KeyCode.D))
        {
            if (animationState != ANIMATION_STATE.ATTACK_RIGHT)
            {
                animationState = ANIMATION_STATE.ATTACK_RIGHT;
                onPlayAnimation(attack, false);
                isAttacking = true;
            }
        }

        if (Input.GetKeyUp(KeyCode.A))
        {
            isAttacking = false;
        }
        else if (Input.GetKeyUp(KeyCode.D))
        {
            isAttacking = false;
        }




#endif

    }

    #endregion

    private void onComplete(TrackEntry trackEntry)
    {
        if (isAttacking)
        {
            switch (animationState)
            {
                case ANIMATION_STATE.ATTACK_LEFT:
                    onPlayAnimation(attack, true);
                    break;
                case ANIMATION_STATE.ATTACK_RIGHT:
                    onPlayAnimation(attack, false);
                    break;
            }

        }
        else
        {
            if (trackEntry.Animation.name != idle)
            {
                bool isFlipX = skeleton.FlipX;
                if (isFlipX)
                {
                    animationState = ANIMATION_STATE.IDLE_LEFT;
                }
                else
                {
                    animationState = ANIMATION_STATE.IDLE_RIGHT;
                }

                onPlayAnimation(idle, isFlipX, true);
            }
        }




    }

    private void onPlayAnimation(string animationName, bool FlipX, bool loop = false)
    {
        state.SetAnimation(1, animationName, loop);
        skeleton.FlipX = FlipX;
    }


}
