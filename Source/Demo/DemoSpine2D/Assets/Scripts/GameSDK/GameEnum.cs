﻿using UnityEngine;
using System.Collections;
namespace GameEnum
{
    public enum ANIMATION_STATE
    {
        IDLE_LEFT,
        IDLE_RIGHT,

        RUN_LEFT,
        RUN_RIGHT,

        ATTACK_LEFT,
        ATTACK_RIGHT,

        SHOOTING_LEFT,
        SHOOTING_RIGHT
    }

}