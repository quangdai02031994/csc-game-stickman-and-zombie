
down_player.png
format: RGBA8888
filter: Linear,Linear
repeat: none
hip
  rotate: false
  xy: 230, 53
  size: 1, 1
  orig: 1, 1
  offset: 0, 0
  index: -1
leg1
  rotate: true
  xy: 184, 22
  size: 32, 44
  orig: 32, 44
  offset: 0, 0
  index: -1
leg2
  rotate: true
  xy: 2, 12
  size: 10, 71
  orig: 10, 71
  offset: 0, 0
  index: -1
leg3
  rotate: false
  xy: 2, 2
  size: 20, 8
  orig: 20, 8
  offset: 0, 0
  index: -1
shadow
  rotate: false
  xy: 2, 24
  size: 180, 30
  orig: 180, 30
  offset: 0, 0
  index: -1
