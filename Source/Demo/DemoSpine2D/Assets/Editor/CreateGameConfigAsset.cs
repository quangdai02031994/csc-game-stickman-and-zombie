﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class CreateGameConfigAsset
{
    [MenuItem("CSCFramework/Create Game Config")]
    public static void CreateMyAsset()
    {
        GameConfig asset = new GameConfig();  //scriptable object 
        AssetDatabase.CreateAsset(asset, "Assets/Resources/GameSetting/GameConfig.asset");
        AssetDatabase.SaveAssets();
        EditorUtility.FocusProjectWindow();
        Selection.activeObject = asset;
    }
}