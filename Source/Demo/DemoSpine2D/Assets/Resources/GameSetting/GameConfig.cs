﻿using System;
using UnityEngine;


[Serializable]
public class GameConfig : ScriptableObject
{
    public float TimeScale = 1;
    public int gold = 0;
    public int muteBGM = 0;
    public int muteSFX = 0;
}
