using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace GameSupport
{
    public enum LEVEL_TYPE
    {
        /// <summary>
        /// Sống sót trong khoảng thời gian
        /// </summary>
        COUNT_TIME,
        /// <summary>
        /// Đếm số lượng zombie giết
        /// </summary>
        COUNT_MONSTER,
        /// <summary>
        /// Đếm số lượng zombie đặc biệt
        /// </summary>
        KILL_MONSTER
    }

    public enum HOME_PANEL_OPEN
    {
        NONE,
        HOME,
        UPGRADE_PLAYER,
        UPGRADE_ARMS
    }

    public enum MAP_TYPES
    {
        NONE,
        GRAY_1,
        YELLOW_2,
        GREEN_3,
        WHILE_4,
        RED_5,
    }


    public enum PLAYER_LEG_STATE
    {
        IDE_LEFT,
        IDE_RIGHT,
        RUN_LEFT,
        RUN_RIGHT,
        ATTACK_LEFT,
        ATTACK_RIGHT,
        APPEAR,
        DEAD
    }

    public enum PLAYER_BODY_STATE
    {
        IDE_LEFT,
        IDE_RIGHT,
        ATTACK_LEFT,
        ATTACK_RIGHT,
        CHANGE_ARM,
        APPEAR,
        DEAD
    }

    public enum ITEM_TYPE
    {
        NONE,

        /// <summary>
        /// Hộp đạn, làm đầy số lượng đạn của vũ khí đang sử dụng.
        /// </summary>
        CARTRIDGE_BOX,

        /// <summary>
        /// Giày tăng tốc, tăng 50% speed của player trong 1 phút
        /// </summary>
        SHOE_SPEED,

        /// <summary>
        /// Nước tăng lực, tăng 50% strong của player trong 1 phút
        /// </summary>
        POWER_DRINK,

        /// <summary>
        /// Hộp máu, làm đầy máu của player
        /// </summary>
        BLOOD_BOX
    }


    public enum PLAYER_ARMS
    {
        SWORD,
        BOW,
        PISTOL,
        DUAL_PISTOL,
        AK47,
        SHOTGUN,
        GATLING_GUN,
        AWM_GUN,
        BAZOKA,
        FIRE_GUN,
        THUNDER_GUN,
        NUCLEAR_GUN
    }

    public enum BULLET_TYPE
    {
        GUN,
        AWM_GUN,
        BAZOKA_GUN,
        //        NUCLEAR_GUN,
        STONE,
        SPEAR,
        BOW,
        FIRE
    }

    public enum MONSTER_TYPE
    {
        HAND,
        STONE,
        SPEAR,
        SALIVA,
        BRAVE
    }

    public enum MONSTER_STATE
    {
        IDLE_LEFT,
        IDLE_RIGHT,

        WALK_LEFT,
        WALK_RIGHT,

        HIT_LEFT,
        HIT_RIGHT,

        HIT_LEFT_THUNDER,
        HIT_RIGHT_THUNDER,

        APPEAR_LEFT,
        APPEAR_RIGHT,

        ATTACK_LEFT,
        ATTACK_RIGHT,

        DEAD,
        DEAD_THUNDER,
        DEAD_FIRE
    }

    public enum GAME_STATE
    {
        START,
        WIN,
        PLAYING,
        PAUSE,
        GAMEOVER
    }

    public class ScenesName
    {
        public const string Loading = "1.Loading";
        public const string Home = "2.Home";
        public const string GamePlay = "4.GamePlay";
        public const string Map = "3.Map";
    }

    public class MonsterAnimationName
    {

        #region Zombie thường

        public const string hand_idle = "idle";
        public const string hand_walk = "walk";
        public const string hand_run = "run";
        public const string hand_attack = "atk";
        public const string hand_attack1 = "atk1";
        public const string hand_appear = "income";

        public const string hand_dead = "die";
        public const string hand_dead_fire = "die_fire";
        public const string hand_dead_thunder = "die_thunder";

        public const string hand_hit = "hit";
        public const string hand_hit_fire = "hit_fire";
        public const string hand_hit_thunder = "hit_thunder";

        #endregion

        #region Zombie ném gạch

        public const string stone_idle = "idle_stone";
        public const string stone_run = "run_stone";
        public const string stone_walk = "walk_stone";
        public const string stone_attack = "atk_stone";
        public const string stone_appear = "income_stone";

        public const string stone_hit = "hit_stone";
        public const string stone_hit_thunder = "hit_stone_thunder";


        public const string stone_dead = "die_stone";
        public const string stone_dead_thunder = "die_stone_thunder";
        public const string stone_dead_fire = "die_stone_fire";

        #endregion

        #region Zombie ném lao

        public const string spear_idle = "idle_spear";
        public const string spear_run = "run_spear";
        public const string spear_walk = "walk_spear";
        public const string spear_attack = "atk_spear";
        public const string spear_appear = "income_spear";

        public const string spear_hit = "hit_spear";
        public const string spear_hit_thunder = "hit_spear_thunder";

        public const string spear_dead = "die_spear";
        public const string spear_dead_fire = "die_spear_fire";
        public const string spear_dead_thunder = "die_spear_thunder";

        #endregion

        #region Zombie nhổ nước bọt

        public const string saliva_idle = "idle";
        public const string saliva_walk = "walk";
        public const string saliva_run = "run";
        public const string saliva_attack = "split";
        public const string saliva_appear = "income";

        public const string saliva_hit = "hit";
        public const string saliva_hit_thunder = "hit_thunder";
        public const string saliva_hit_fire = "hit_fire";

        public const string saliva_dead = "die";
        public const string saliva_dead_thunder = "die_thunder";
        public const string saliva_dead_fire = "die_fire";

        #endregion

        #region Zommbie tự sát

        public const string brave_attack = "atk";
        public const string brave_hit = "hit";
        public const string brave_idle = "idle";
        public const string brave_appear = "income";
        public const string brave_walk = "run";

        public const string brave_dead = "die";
        public const string brave_dead_thunder = "die_thunder";
        public const string brave_dead_fire = "die_fire";
        public const string brave_hit_thunder = "hit_thunder";


        #endregion
    }

    public class MonsterSkin
    {
        public const string skin_z1 = "z1";

        public const string skin_z2 = "z2";

        public const string skin_z3 = "z3";

        public const string skin_z4 = "z4";

        public const string skin_default = "default";
    }

    public class PlayerAnimationName
    {
        #region Chân Stickman

        public const string leg_idle = "idle";
        public const string leg_run = "run";
        public const string leg_hit1_sword = "hit1";
        public const string leg_hit2_sword = "hit2";
        public const string leg_hit3_sword = "hit3";
        public const string leg_win = "win";
        public const string leg_dead = "die";

        public const string leg_appear = "incom";

        #endregion

        #region Thân Stickman

        public const string body_win = "win";
        public const string body_dead = "die";
        public const string body_appear = "incom";

        #endregion

        #region Thân Stickman dùng kiếm

        public const string body_idle_sword = "idle";
        public const string body_use_sword = "use";
        public const string body_drop_sword = "drop";
        public const string body_hit1_sword = "hit1";
        public const string body_hit2_sword = "hit2";
        public const string body_hit3_sword = "hit3";

        #endregion

        #region Thân Stickman dùng súng lục

        public const string body_idle_pistol = "idle1";
        public const string body_drop_pistol = "drop1";
        public const string body_use_pistol = "use1";
        public const string body_hit_pistol = "atk1";

        #endregion

        #region Thân Stickman dùng cung

        public const string body_idle_bow = "idle_bow";
        public const string body_drop_bow = "drop_bow";
        public const string body_use_bow = "use_bow";
        public const string body_hit_bow = "atk_bow";

        #endregion

        #region Thân Stickman dùng súng lửa

        public const string body_idle_firegun = "idle_firegun";
        public const string body_drop_firegun = "drop_firegun";
        public const string body_use_firegun = "use_firegun";
        public const string body_hit_firegun = "atk_firegun";

        #endregion

        #region Thân Stickman dùng súng điện

        public const string body_idle_thundergun = "idle_thundergun";
        public const string body_drop_thundergun = "drop_thundergun";
        public const string body_use_thundergun = "use_thundergun";
        public const string body_hit_thundergun = "atk_thundergun";

        #endregion

        #region Thân Stickman dùng AK47

        public const string body_idle_ak47 = "idle_ak47";
        public const string body_drop_ak47 = "drop_ak47";
        public const string body_use_ak47 = "use_ak47";
        public const string body_hit_ak47 = "atk_ak47";

        #endregion

        #region Thân Stickman dùng súng 6 nòng

        public const string body_use_gatling_gun = "use_gatling";
        public const string body_idle_gatling_gun = "idle_gatling";
        public const string body_hit_gatling_gun = "atk_gatling";
        public const string body_drop_gatling_gun = "drop_gatling";

        #endregion

        #region Thân Stickman dùng súng hoa cải

        public const string body_idle_shotgun = "idle_shotgun";
        public const string body_drop_shotgun = "drop_shotgun";
        public const string body_hit_shotgun = "atk_shotgun";
        public const string body_use_shotgun = "use_shortgun";

        #endregion

        #region Thân Stickman dùng súng lục đôi

        public const string body_idle_two_pistol = "idle_2pistol";
        public const string body_drop_two_pistol = "drop_2pistol";
        public const string body_use_two_pistol = "use_2pistol";
        public const string body_hit_two_pistol_up = "atk_2pistol_up";
        public const string body_hit_two_pistol_down = "atk_2pistol_down";

        #endregion

        #region Thân Stickman dùng súng bắn tăng

        public const string body_idle_bazoka = "idle_bazoka";
        public const string body_hit_bazoka = "atk_bazoka";
        public const string body_use_bazoka = "use_bazoka";
        public const string body_drop_bazoka = "drop_bazoka";

        #endregion

        #region Thân Stickman dùng súng ngắm

        public const string body_idle_awm_gun = "idle_awp";
        public const string body_hit_awm_gun = "atk_awp";
        public const string body_use_awm_gun = "use_awp";
        public const string body_drop_awm_gun = "drop_awp";
        public const string body_aim_awm_gun = "aim_awp";

        #endregion

        #region Thân Stickman dùng súng hạt nhân

        public const string body_idle_nuclear_gun = "idle_laser";
        public const string body_hit_nuclear_gun = "atk_laser";
        public const string body_use_nuclear_gun = "use_laser";
        public const string body_drop_nuclear_gun = "drop_laser";

        #endregion
    }


    public class GameSupportUI
    {
        public static Color HexToColor(string hex)
        {
            hex = hex.Replace("0x", "");//in case the string is formatted 0xFFFFFF
            hex = hex.Replace("#", "");//in case the string is formatted #FFFFFF
            byte a = 255;//assume fully visible unless specified in hex
            byte r = byte.Parse(hex.Substring(0, 2), System.Globalization.NumberStyles.HexNumber);
            byte g = byte.Parse(hex.Substring(2, 2), System.Globalization.NumberStyles.HexNumber);
            byte b = byte.Parse(hex.Substring(4, 2), System.Globalization.NumberStyles.HexNumber);
            //Only use alpha if the string has enough characters
            if (hex.Length == 8)
            {
                a = byte.Parse(hex.Substring(4, 2), System.Globalization.NumberStyles.HexNumber);
            }
            return new Color32(r, g, b, a);
        }
    }

}
