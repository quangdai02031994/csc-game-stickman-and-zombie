﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using GameSupport;
using System;

public class SaveAndLoadData
{
    public static ModelPlayerArm LoadDataPlayerGun()
    {
        string _dataFolderPath = Application.persistentDataPath + Path.DirectorySeparatorChar + "Data";
        string _armFolderPath = Application.persistentDataPath + Path.DirectorySeparatorChar + "Data" + Path.DirectorySeparatorChar + "Arms";
        string _gunDataPath = Application.persistentDataPath + Path.DirectorySeparatorChar + "Data" + Path.DirectorySeparatorChar + "Arms"
                              + Path.DirectorySeparatorChar + "player_gun.dat";
        if (!Directory.Exists(_dataFolderPath))
        {
            Directory.CreateDirectory(_dataFolderPath);
            Directory.CreateDirectory(_armFolderPath);
        }
        else
        {
            if (!Directory.Exists(_armFolderPath))
            {
                Directory.CreateDirectory(_armFolderPath);
            }
        }

        if (File.Exists(_gunDataPath))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = new FileStream(_gunDataPath, FileMode.Open);
            ModelPlayerArm _model = bf.Deserialize(file)as ModelPlayerArm;
            file.Close();
            return _model;
        }
        else
        {
            ModelPlayerArm _modelArm = new ModelPlayerArm();
            SaveDataPlayerGun(_modelArm);
            return _modelArm;
        }
           
    }

    public static ModelSword LoadSwordData()
    {
        string _dataFolderPath = Application.persistentDataPath + Path.DirectorySeparatorChar + "Data";
        string _armFolderPath = Application.persistentDataPath + Path.DirectorySeparatorChar + "Data" + Path.DirectorySeparatorChar + "Arms";
        string _swordDataPath = Application.persistentDataPath + Path.DirectorySeparatorChar + "Data" + Path.DirectorySeparatorChar + "Arms"
                                + Path.DirectorySeparatorChar + "sword.dat";
        if (!Directory.Exists(_dataFolderPath))
        {
            Directory.CreateDirectory(_dataFolderPath);
            Directory.CreateDirectory(_armFolderPath);
        }
        else
        {
            if (!Directory.Exists(_armFolderPath))
            {
                Directory.CreateDirectory(_armFolderPath);
            }
        }

        if (File.Exists(_swordDataPath))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = new FileStream(_swordDataPath, FileMode.Open);
            ModelSword _model = bf.Deserialize(file) as ModelSword;
            file.Close();
            return _model;
        }
        else
        {
            ModelSword _model = new ModelSword();
            _model.name = "SWORD";
            _model.damage = 100;
            _model.levelDamage = 0;
            _model.description = "* Multi-Targets \n* Strong of Stickman affects to damage";
            SaveSwordData(_model);
            return _model;
        }
    }

    public static ModelGameData LoadGameData()
    {
        string _dataFolderPath = Application.persistentDataPath + Path.DirectorySeparatorChar + "Data";
        string _gameDataPath = Application.persistentDataPath + Path.DirectorySeparatorChar + "Data" + Path.DirectorySeparatorChar + "game.dat";
        if (!Directory.Exists(_dataFolderPath))
            Directory.CreateDirectory(_dataFolderPath);
        if (File.Exists(_gameDataPath))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = new FileStream(_gameDataPath, FileMode.Open);
            ModelGameData _model = bf.Deserialize(file) as ModelGameData;
            file.Close();
            return _model;   
        }
        else
        {
            ModelGameData _model = new ModelGameData();
            _model.gold = 2000;
            SaveGameData(_model);
            return _model;
        }
    }

    public static ModelPlayerItem LoadPlayerItemData()
    {
        string _dataFolderPath = Application.persistentDataPath + Path.DirectorySeparatorChar + "Data";
        string _gameDataPath = Application.persistentDataPath + Path.DirectorySeparatorChar + "Data" + Path.DirectorySeparatorChar + "item.dat";
        if (!Directory.Exists(_dataFolderPath))
            Directory.CreateDirectory(_dataFolderPath);
        if (File.Exists(_gameDataPath))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = new FileStream(_gameDataPath, FileMode.Open);
            ModelPlayerItem _model = bf.Deserialize(file) as ModelPlayerItem;
            file.Close();
            return _model;   
        }
        else
        {
            ModelPlayerItem _model = new ModelPlayerItem();
            SavePlayerItemData(_model);
            return _model;
        }
    }

    public static ModelGameMap LoadDataGameMap()
    {
        string _dataFolderPath = Application.persistentDataPath + Path.DirectorySeparatorChar + "Data";
        string _mapDataPath = Application.persistentDataPath + Path.DirectorySeparatorChar + "Data" + Path.DirectorySeparatorChar + "map.dat";
        if (!Directory.Exists(_dataFolderPath))
            Directory.CreateDirectory(_dataFolderPath);
        if (File.Exists(_mapDataPath))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = new FileStream(_mapDataPath, FileMode.Open);
            ModelGameMap _model = bf.Deserialize(file) as ModelGameMap;
            file.Close();
            return _model;   
        }
        else
        {
            ModelGameMap _model = new ModelGameMap();
            SaveDataMap(_model);
            return _model;
        }

    }

    public static ModelPlayer LoadPlayerData()
    {
        string _dataFolderPath = Application.persistentDataPath + Path.DirectorySeparatorChar + "Data";
        string _playerDataPath = Application.persistentDataPath + Path.DirectorySeparatorChar + "Data" + Path.DirectorySeparatorChar + "player.dat";
        if (!Directory.Exists(_dataFolderPath))
            Directory.CreateDirectory(_dataFolderPath);
        
        if (File.Exists(_playerDataPath))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = new FileStream(_playerDataPath, FileMode.Open);
            ModelPlayer _model = bf.Deserialize(file) as ModelPlayer;
            file.Close();
            return _model;
        }
        else
        {
            ModelPlayer _model = new ModelPlayer();
            _model.hp = 100;
            _model.name = "Player";
            _model.speed = 2;
            _model.strong = 10;
            SavePlayerData(_model);
            return _model;
        }
    }

    public static void SaveGameData(ModelGameData _model)
    {
        string _gameDataPath = Application.persistentDataPath + Path.DirectorySeparatorChar + "Data" + Path.DirectorySeparatorChar + "game.dat";
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = new FileStream(_gameDataPath, FileMode.Create);
        bf.Serialize(file, _model);
        file.Close();
    }

    public static void SaveSwordData(ModelSword _model)
    {
        string _swordDataPath = Application.persistentDataPath + Path.DirectorySeparatorChar + "Data" + Path.DirectorySeparatorChar + "Arms"
                                + Path.DirectorySeparatorChar + "sword.dat";
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = new FileStream(_swordDataPath, FileMode.Create);
        bf.Serialize(file, _model);
        file.Close();
    }

    public static void SavePlayerItemData(ModelPlayerItem model)
    {
        string _itemDataPath = Application.persistentDataPath + Path.DirectorySeparatorChar + "Data" + Path.DirectorySeparatorChar + "item.dat";
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = new FileStream(_itemDataPath, FileMode.Create);
        bf.Serialize(file, model);
        file.Close();
    }

    public static void SavePlayerData(ModelPlayer _model)
    {
        string _playerDataPath = Application.persistentDataPath + Path.DirectorySeparatorChar + "Data" + Path.DirectorySeparatorChar + "player.dat";
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = new FileStream(_playerDataPath, FileMode.Create);
        bf.Serialize(file, _model);
        file.Close();
    }

    public static void SaveDataMap(ModelGameMap _model)
    {
        string _mapDataPath = Application.persistentDataPath + Path.DirectorySeparatorChar + "Data" + Path.DirectorySeparatorChar + "map.dat";
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = new FileStream(_mapDataPath, FileMode.Create);
        bf.Serialize(file, _model);
        file.Close();
    }

    public static void SaveDataPlayerGun(ModelPlayerArm _model)
    {
        string _gunDataPath = Application.persistentDataPath + Path.DirectorySeparatorChar + "Data" + Path.DirectorySeparatorChar + "Arms"
                              + Path.DirectorySeparatorChar + "player_gun.dat";
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = new FileStream(_gunDataPath, FileMode.Create);
        bf.Serialize(file, _model);
        file.Close();
    }



    /// <summary>
    /// Lấy giá trị thông tin súng mặc định
    /// </summary>
    /// <param name="type"> Loại súng.</param>
    public static ModelGun OnGetModelGunDefault(PLAYER_ARMS type)
    {
        ModelGun _model = new ModelGun();
        switch (type)
        {
            case PLAYER_ARMS.BOW:
                _model.modelBullet = onCreateModelBullet(type);
                _model.numberBullet = 50;
                _model.description = "* Multi-Targets\n* Strong of Stickman affects to damage";
                _model.name = type.ToString();
                return _model;
            case PLAYER_ARMS.PISTOL:
                _model.modelBullet = onCreateModelBullet(type);
                _model.numberBullet = 100;
                _model.description = "* Single-Target";
                _model.name = type.ToString();
                return _model;
            case PLAYER_ARMS.DUAL_PISTOL:
                _model.modelBullet = onCreateModelBullet(type);
                _model.numberBullet = 150;
                _model.description = "* Single-Target";
                _model.name = type.ToString();
                return _model;
            case PLAYER_ARMS.AK47:
                _model.modelBullet = onCreateModelBullet(type);
                _model.numberBullet = 200;
                _model.description = "* Single-Target\n-5% speed";
                _model.name = type.ToString();
                return _model;
            case PLAYER_ARMS.SHOTGUN:
                _model.modelBullet = onCreateModelBullet(type);
                _model.numberBullet = 50;
                _model.description = "* Multi-Targets\n-5% speed";
                _model.name = type.ToString();
                return _model;
            case PLAYER_ARMS.GATLING_GUN:
                _model.modelBullet = onCreateModelBullet(type);
                _model.numberBullet = 300;
                _model.description = "* Single-Target\n-10% speed";
                _model.name = type.ToString();
                return _model;
            case PLAYER_ARMS.AWM_GUN:
                _model.modelBullet = onCreateModelBullet(type);
                _model.numberBullet = 50;
                _model.description = "* Multi-Targets\n-15% speed";
                _model.name = type.ToString();
                return _model;
            case PLAYER_ARMS.BAZOKA:
                _model.modelBullet = onCreateModelBullet(type);
                _model.numberBullet = 30;
                _model.description = "* Multi-Targets\n-20% speed";
                _model.name = type.ToString();
                return _model;
            case PLAYER_ARMS.FIRE_GUN:
                _model.modelBullet = onCreateModelBullet(type);
                _model.numberBullet = 30;
                _model.description = "* Multi-Targets\n-5% speed";
                _model.name = type.ToString();
                return _model;
            case PLAYER_ARMS.THUNDER_GUN:
                _model.modelBullet = onCreateModelBullet(type);
                _model.numberBullet = 50;
                _model.description = "* Multi-Targets\n-5% speed";
                _model.name = type.ToString();
                return _model;
            case PLAYER_ARMS.NUCLEAR_GUN:
                _model.modelBullet = onCreateModelBullet(type);
                _model.numberBullet = 50;
                _model.description = "* Multi-Targets\n-5% speed";
                _model.name = type.ToString();
                return _model;
            default:
                Debug.LogError("Type gun fail: " + type);
                return null;
        }
    }

    /// <summary>
    /// Lấy giá trị thông tin của item mặc định
    /// </summary>
    /// <param name="type">Type.</param>
    public static ModelItem OnGetModelItemDefault(ITEM_TYPE type, int state = -1)
    {
        switch (type)
        {
            case ITEM_TYPE.BLOOD_BOX:
                return new ModelItem(type, "First Aid", 300, "Recover full health", state);
            case ITEM_TYPE.CARTRIDGE_BOX:
                return new ModelItem(type, "Ammo Pack", 200, "Reload 100% max ammo current weapon", state);
            case ITEM_TYPE.POWER_DRINK:
                return new ModelItem(type, "Energy Drink", 300, "Increase 50% strong for 1 minute", state);
            case ITEM_TYPE.SHOE_SPEED:
                return new ModelItem(type, "Speed Boost", 200, "Increase 50% speed for 1 minute", state);
            default:
                return null;
        }
    }

    public static ModelMonster OnGetModelMonsterHand()
    {
        ModelMonster model = new ModelMonster();
        model.type = MONSTER_TYPE.HAND;
        model.deltaY = UnityEngine.Random.Range(0.1f, 0.2f);
        model.deltaX = UnityEngine.Random.Range(0.2f, 0.35f);
        model.distance = UnityEngine.Random.Range(0.5f, 1);
        model.hp = UnityEngine.Random.Range(100, 300);
        model.name = "Zombie_HP: " + model.hp + " " + model.type;
        model.speed = UnityEngine.Random.Range(0.5f, 1);
        model.timeBeginFind = UnityEngine.Random.Range(5, 8);
        model.timeDelayAttack = UnityEngine.Random.Range(1.0f, 3.0f);
        model.skinName = onGetSkin();
        return model;
    }

    public static ModelMonster OnGetModelMonsterBrave()
    {
        ModelMonster model = new ModelMonster();
        model.type = MONSTER_TYPE.BRAVE;
        model.deltaY = 0.25f;
        model.distance = 1;
        model.deltaX = 0.75f;
        model.hp = UnityEngine.Random.Range(150, 300);
        model.name = "Zombie_HP: " + model.hp + " " + model.type;
        model.speed = UnityEngine.Random.Range(1.5f, 2f);
        model.timeBeginFind = UnityEngine.Random.Range(2, 5);
        model.timeDelayAttack = UnityEngine.Random.Range(1.0f, 3.0f);
        model.skinName = MonsterSkin.skin_default;
        return model;
    }

    private static string onGetSkin()
    {
        int rand = UnityEngine.Random.Range(0, 4);
        switch (rand)
        {
            case 0:
                return MonsterSkin.skin_z1;
            case 1:
                return MonsterSkin.skin_z2;
            case 2:
                return MonsterSkin.skin_z3;
            case 3:
                return MonsterSkin.skin_z4;
            default:
                return MonsterSkin.skin_z1;
        }
    }


    /// <summary>
    /// Lấy thông tin đạn theo từng loại vũ khí. Null nếu vũ khí là kiếm
    /// </summary>
    /// <param name="type">Loại vũ khí.</param>
    private static ModelBullet onCreateModelBullet(PLAYER_ARMS type)
    {
        switch (type)
        {
            case PLAYER_ARMS.BOW:
                return new ModelBullet(20, 50, 20, 1);
            case PLAYER_ARMS.PISTOL:
                return new ModelBullet(20, 150, 30, 1);
            case PLAYER_ARMS.DUAL_PISTOL:
                return new ModelBullet(20, 150, 30, 1);
            case PLAYER_ARMS.AK47:
                return new ModelBullet(20, 150, 35, 1);
            case PLAYER_ARMS.SHOTGUN:
                return new ModelBullet(20, 50, 35, 1);
            case PLAYER_ARMS.GATLING_GUN:
                return new ModelBullet(20, 150, 35, 1);
            case PLAYER_ARMS.AWM_GUN:
                return new ModelBullet(20, 150, 40, 3);
            case PLAYER_ARMS.BAZOKA:
                return new ModelBullet(20, 100, 40, 1);
            case PLAYER_ARMS.FIRE_GUN:
                return new ModelBullet(20, 20, 0, 1);
            case PLAYER_ARMS.THUNDER_GUN:
                return new ModelBullet(20, 20, 0, 1);
            case PLAYER_ARMS.NUCLEAR_GUN:
                return new ModelBullet(20, 200, 10, 100);
            default:
                return null;
        }
    }

        
}
