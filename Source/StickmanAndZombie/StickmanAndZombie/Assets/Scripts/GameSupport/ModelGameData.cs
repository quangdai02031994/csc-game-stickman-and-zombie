﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using GameSupport;

[Serializable]
public class ModelGameData
{
    public int gold;

    public ModelGameData()
    {
        gold = 0;
    }
}