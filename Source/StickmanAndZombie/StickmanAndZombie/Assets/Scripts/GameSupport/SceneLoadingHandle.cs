﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.SceneManagement;
using System;
using UnityEngine.UI;


public class SceneLoadingHandle : MonoBehaviour
{

    public static SceneLoadingHandle Instance{ get; private set; }

    public GameObject Camera;

    [Header("UI Loading")]
    public GameObject UILoadingRoot;
    public RectTransform rectProgressBar;


    [Header("UI Fading Root")]
    public GameObject UIFadingRoot;
    public Image panelFading;

    private float startAnchorPosX;
    private AsyncOperation loadingScene;
    private float timeTween = 1;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Destroy(this.gameObject);
        }
    }


    void Start()
    {
        onResetLoadingState();
    }

    public void OnFadingSceneAsync(string sceneName, Action onComplete = null)
    {
        onResetLoadingState();
        Camera.SetActive(true);
        UILoadingRoot.SetActive(false);
        UIFadingRoot.SetActive(true);
        StartCoroutine(onFadingScene(sceneName, onComplete));
    }

    public void OnLoadingSceneAsync(string sceneName, Action onComplete = null)
    {
        onResetLoadingState();
        Camera.SetActive(true);
        UILoadingRoot.SetActive(true);
        UIFadingRoot.SetActive(false);
        StartCoroutine(onLoadingScene(sceneName, onComplete));
    }

    public void OnSwitchScene()
    {
        if (loadingScene != null && loadingScene.progress >= 0.9f)
        {
            loadingScene.allowSceneActivation = true;
            StartCoroutine(onSwitchSceneDone());
        }
    }

    private IEnumerator onFadingScene(string sceneName, Action onComplete)
    {
        //        Debug.Log("Fading scene: " + sceneName);
        loadingScene = SceneManager.LoadSceneAsync(sceneName);
        loadingScene.allowSceneActivation = false;
        while (loadingScene.progress < 0.9f)
        {
            yield return null;
        }
        //        Debug.Log("Fading Scene done");
        onFadingSceneDone(onComplete);
    }

    private IEnumerator onLoadingScene(string sceneName, Action onComplete)
    {

//        Debug.Log("Loading scene: " + sceneName);
        loadingScene = SceneManager.LoadSceneAsync(sceneName);
        loadingScene.allowSceneActivation = false;
        while (loadingScene.progress < 0.9f)
        {
            yield return null;
        }
//        Debug.Log("Loading done");
        onLoadingSceneDone(onComplete);
    }

    private IEnumerator onSwitchSceneDone()
    {
        yield return new WaitForEndOfFrame();
        Camera.SetActive(false);
        UILoadingRoot.SetActive(false);
        UIFadingRoot.SetActive(false);
        loadingScene = null;
    }

    private void onResetLoadingState()
    {
        rectProgressBar.anchoredPosition = new Vector2(-rectProgressBar.sizeDelta.x, rectProgressBar.anchoredPosition.y);
        Color _color = panelFading.color;
        _color.a = 0;
        panelFading.color = _color;
    }

    private void onFadingSceneDone(Action onComplete = null)
    {
        panelFading.DOFade(1, timeTween).OnComplete(() =>
            {
                if (onComplete != null)
                {
                    onComplete();
                }
                OnSwitchScene();
            });
    }

    private void onLoadingSceneDone(Action onComplete = null)
    {
        rectProgressBar.DOAnchorPosX(0, timeTween).OnComplete(() =>
            {
                if (onComplete == null)
                {
                    OnSwitchScene();
                }
                else
                {
                    Camera.SetActive(false);
                    UILoadingRoot.SetActive(false);
                    onComplete();
                }
            });
    }
}
