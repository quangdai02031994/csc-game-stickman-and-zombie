﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;

public class PlayerBulletEmpty : MonoBehaviour
{
    [SpineAnimation]
    public string spineAnimation;

    private SkeletonAnimation skeletonAnimation;
    private MeshRenderer meshRenderer;

    #region Behaviour

    void Awake()
    {
        skeletonAnimation = GetComponent<SkeletonAnimation>();
        meshRenderer = GetComponent<MeshRenderer>();
    }

    void OnDisable()
    {
        skeletonAnimation.AnimationState.ClearTrack(0);
    }

    #endregion

    #region Publish Methods

    public void OnRescale(int orderLayer)
    {
        if (this.gameObject.activeSelf)
        {
            meshRenderer.sortingOrder = orderLayer + 1;
        }
    }

    public void OnShowEffectEmptyBullet()
    {
        this.gameObject.SetActive(true);
        skeletonAnimation.AnimationState.SetAnimation(0, spineAnimation, true);
    }

    public void OnHideEffectAnimation()
    {
        this.gameObject.SetActive(false);
    }

    #endregion

}
