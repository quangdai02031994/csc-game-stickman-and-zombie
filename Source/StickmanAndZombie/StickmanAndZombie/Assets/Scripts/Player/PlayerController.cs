﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSupport;
using System;

public class PlayerController : MonoBehaviour
{
    public delegate void GameEvent();

    public event GameEvent playerGameOver;

    public event GameEvent playerGameWin;

    public static PlayerController Instance{ get; private set; }

    public bool isAttackRight = false;

    public bool isAttackLeft = false;

    public bool canRunning = true;

    public int countAttackLeft = 0;

    public int countAttackRight = 0;

    public float deltaSpeedArm = 0;

    public bool isAttacking{ get { return (isAttackLeft || isAttackRight); } }

    public ModelPlayer modelPlayer;

    public PlayerBulletEmpty playerBulletEmpty;

    public PLAYER_LEG_STATE playerLegState;

    public PLAYER_BODY_STATE playerBodyState;

    public PLAYER_ARMS playerArms = PLAYER_ARMS.SWORD;

    public bool isBodyLeft     { get { return playerAnimation.isLeft; } }

    public bool isSaliva = false;

    public GameObject prefabBullet;

    public Transform BulletParent;

    public bool isMinOrMax{ get { return playerMove.isMinOrMax; } }

    [SerializeField]
    private List<BulletHandle> ListBullets = new List<BulletHandle>();


    [SerializeField]
    private PlayerSword playerSword;

    [SerializeField]
    private PlayerFireGun playerFireGun;

    [SerializeField]
    private PlayerThunderGun playerThunderGun;

    [SerializeField]
    private PlayerUseItemAnimation playerUseItemAnimation;

    private PlayerAnimation playerAnimation;
    private PlayerItemAnimation playerItemAnimation;
    private MovePlayer playerMove;

    private float maxPlayerHP;

    private float countSaliva = 5;

    private float countTime;

    private float countPowerDrink = 0;

    private float countShoesSpeed = 0;

    private bool isShoesSpeed = false;

    private bool isPowerDrink = false;

    #region Behaviour

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        playerAnimation = GetComponent<PlayerAnimation>();
        playerMove = GetComponent<MovePlayer>();
        playerItemAnimation = GetComponent<PlayerItemAnimation>();
    }

    void Update()
    {
        // Kiểm tra trạng thái trúng độc của stickman
        #region Kiểm tra trạng thái trúng độc
        if (isSaliva)
        {
            countSaliva -= Time.deltaTime;
            countTime += Time.deltaTime;
            if (countTime > 1)
            {
                modelPlayer.hp -= 10;
                if (modelPlayer.hp <= 0)
                {
                    onSetStateGameOver();
                }
                GamePlayUIHandle.Instance.OnUpdatePlayerHP(modelPlayer.hp / maxPlayerHP, modelPlayer.hp, maxPlayerHP);
                countTime = 0;
            }
            if (countSaliva < 0)
            {
                isSaliva = false;
                modelPlayer.deltaSpeedSaliva = 0;
            }
        }
        else
        {
            countSaliva = 5;
            countTime = 1;
        }
        #endregion

        #region Kiểm tra trạng thái sử dụng nước tăng lực

        if (isPowerDrink)
        {
            countPowerDrink -= Time.deltaTime;
            GamePlayUIHandle.Instance.OnShowItemStrong((int)countPowerDrink);
            if (countPowerDrink < 0)
            {
                isPowerDrink = false;
                playerItemAnimation.OnCancelAnimationStrong();
                GamePlayUIHandle.Instance.OnHideItemStrong();
            }
        }
        else
        {
            modelPlayer.deltaStrong = 0;
            countPowerDrink = 0;
        }

        #endregion

        #region Kiểm tra trạng thái dùng giày tăng tốc

        if (isShoesSpeed)
        {
            countShoesSpeed -= Time.deltaTime;
            GamePlayUIHandle.Instance.OnShowItemSpeed((int)countShoesSpeed);
            if (countShoesSpeed < 0)
            {
                isShoesSpeed = false;
                playerItemAnimation.OnCancelAnimationSpeed();
                GamePlayUIHandle.Instance.OnHideItemSpeed();
            }
        }
        else
        {
            countShoesSpeed = 0;
            modelPlayer.deltaSpeedItem = 0;
        }

        #endregion
    }


    #endregion


    #region Publish Method

    public void OnShowBulletEmpty()
    {
        playerBulletEmpty.OnShowEffectEmptyBullet();
    }

    public void OnHideBulletEmpty()
    {
        playerBulletEmpty.OnHideEffectAnimation();
    }

    public int OnGetSortingLayerBody()
    {
        return playerAnimation.OnGetSortingLayerBody();
    }

    public void OnGameOver()
    {
        if (playerGameOver != null)
        {
            playerGameOver();
        }
    }

    public IEnumerator OnGameWin()
    {
        yield return new WaitForSeconds(2);
        if (playerGameWin != null)
        {
            playerGameWin();
        }
        GameAnalys.Instance.OnGameWin(modelPlayer.hp, maxPlayerHP);
        playerAnimation.OnGameWin();
        playerBulletEmpty.OnHideEffectAnimation();
        playerItemAnimation.OnCancelAnimationSpeed();
        playerItemAnimation.OnCancelAnimationStrong();
        OnCancelBurningMonster();
        OnCancelDisChargingMonster();
    }

    public void OnPauseGame()
    {
        switch (playerBodyState)
        {
            case PLAYER_BODY_STATE.ATTACK_LEFT:
                SetAnimationLeg(PLAYER_LEG_STATE.IDE_LEFT);
                SetAnimationBody(PLAYER_BODY_STATE.IDE_LEFT);
                break;
            case PLAYER_BODY_STATE.ATTACK_RIGHT:
                SetAnimationLeg(PLAYER_LEG_STATE.IDE_LEFT);
                SetAnimationBody(PLAYER_BODY_STATE.IDE_LEFT);
                break;
        }
    }

    public void OnUseItem(ITEM_TYPE type)
    {
        switch (type)
        {
            case ITEM_TYPE.BLOOD_BOX:
                modelPlayer.hp = maxPlayerHP;
                GamePlayUIHandle.Instance.OnUpdatePlayerHP(1, maxPlayerHP, maxPlayerHP);
                playerUseItemAnimation.OnSetLayer(playerAnimation.OnGetSortingLayerBody());
                playerUseItemAnimation.OnPlayUseHP();
                break;
            case ITEM_TYPE.CARTRIDGE_BOX:
                GamePlayUIHandle.Instance.OnUpdateNumberBullet();
                playerUseItemAnimation.OnSetLayer(playerAnimation.OnGetSortingLayerBody());
                playerUseItemAnimation.OnPlayUseBullet();
                OnHideBulletEmpty();
                break;
            case ITEM_TYPE.POWER_DRINK:
                countPowerDrink = 60;
                modelPlayer.deltaStrong = modelPlayer.strong * 0.5f;
                isPowerDrink = true;
                playerUseItemAnimation.OnSetLayer(playerAnimation.OnGetSortingLayerBody());
                playerUseItemAnimation.OnPlayUseStrong();
                playerItemAnimation.OnPlayAnimationStrong(playerAnimation.OnGetSortingLayerLeg());
                break;
            case ITEM_TYPE.SHOE_SPEED:
                isShoesSpeed = true;
                modelPlayer.deltaSpeedItem = modelPlayer.speed * 0.5f;
                countShoesSpeed = 60;
                playerUseItemAnimation.OnSetLayer(playerAnimation.OnGetSortingLayerBody());
                playerUseItemAnimation.OnPlayUseSpeed();
                playerItemAnimation.OnPlayAnimationSpeed(playerAnimation.OnGetSortingLayerLeg());
                break;
        }
    }


    public void OnStartGame()
    {
        modelPlayer = new ModelPlayer();
        modelPlayer.hp = GameManager.Instance.OnGetPlayerHP();
        modelPlayer.speed = GameManager.Instance.OnGetPlayerSpeed();
        modelPlayer.strong = GameManager.Instance.OnGetPlayerStrong();

        maxPlayerHP = modelPlayer.hp;
        GamePlayUIHandle.Instance.OnUpdatePlayerHP(modelPlayer.hp / maxPlayerHP, modelPlayer.hp, maxPlayerHP);
        GamePlayUIHandle.Instance.OnShowReady();
    }

    public void OnSaliva()
    {
        GamePlayUIHandle.Instance.OnShowSalivaUI();
        if (!isSaliva)
        {
            isSaliva = true;
            modelPlayer.deltaSpeedSaliva = modelPlayer.speed * -0.2f;
        }
    }

    public void OnHit(float hp)
    {
        modelPlayer.hp -= hp;
        if (modelPlayer.hp > 0)
        {
            GamePlayUIHandle.Instance.OnUpdatePlayerHP(modelPlayer.hp / maxPlayerHP, modelPlayer.hp, maxPlayerHP);
            GamePlayUIHandle.Instance.OnShowBloodUI();

            MonsterBloodEffect _effect = EffectManager.Instance.OnGetEffectBlood();
            _effect.OnSetData(transform.position, playerAnimation.OnGetSortingLayerBody());
            if (playerAnimation.isLeft)
            {
                _effect.OnPlay(true);
            }
            else
            {
                _effect.OnPlay(false);
            }
        }
        else
        {
            modelPlayer.hp = 0;
            onSetStateGameOver();
            GamePlayUIHandle.Instance.OnUpdatePlayerHP(0, 0, maxPlayerHP);
        }
    }


    public void OnCancelBurningMonster()
    {
        playerFireGun.OnCancleAttack();
    }

    public void OnDisChargingMonster(Vector3 startPos)
    {
        ModelBullet model = onCreateModelBullet();
        playerThunderGun.OnSetData(model);
        if (playerBodyState == PLAYER_BODY_STATE.ATTACK_LEFT)
        {
            playerThunderGun.OnAttack(true, startPos);
        }
        else if (playerBodyState == PLAYER_BODY_STATE.ATTACK_RIGHT)
        {
            playerThunderGun.OnAttack(false, startPos);
        }
    }

    public void OnCancelDisChargingMonster()
    {
        playerThunderGun.OnCancelAttack();
    }

    public void OnBurningMonster(Vector3 startPos)
    {
        ModelBullet model = onCreateModelBullet();
        playerFireGun.OnSetData(model);
        if (playerBodyState == PLAYER_BODY_STATE.ATTACK_LEFT)
        {
            playerFireGun.OnAttack(true, startPos);
        }
        else if (playerBodyState == PLAYER_BODY_STATE.ATTACK_RIGHT)
        {
            playerFireGun.OnAttack(false, startPos);
        }
    }

    public void OnSetLayer(int bodyLayer, int legLayer)
    {
        playerAnimation.OnSetLayer(bodyLayer, legLayer);
        playerBulletEmpty.OnRescale(bodyLayer);
        playerItemAnimation.OnSetLayer(legLayer);
    }

    public void OnCreateBullet(Vector3 startPos)
    {
        BulletHandle bullet;
        switch (playerArms)
        {
            case PLAYER_ARMS.PISTOL:
                bullet = onCreateBullet(startPos, onCreateModelBullet(), BULLET_TYPE.GUN);
                if (playerBodyState == PLAYER_BODY_STATE.ATTACK_LEFT)
                {
                    bullet.ShootingBullet(false);
                }
                else if (playerBodyState == PLAYER_BODY_STATE.ATTACK_RIGHT)
                {
                    bullet.ShootingBullet(true);
                }
                break;
            case PLAYER_ARMS.AK47:
                bullet = onCreateBullet(startPos, onCreateModelBullet(), BULLET_TYPE.GUN);
                if (playerBodyState == PLAYER_BODY_STATE.ATTACK_LEFT)
                {
                    bullet.ShootingBullet(false);
                }
                else if (playerBodyState == PLAYER_BODY_STATE.ATTACK_RIGHT)
                {
                    bullet.ShootingBullet(true);
                }
                break;
            case PLAYER_ARMS.BOW:
                bullet = onCreateBullet(startPos, onCreateModelBullet(), BULLET_TYPE.BOW);
                if (playerBodyState == PLAYER_BODY_STATE.ATTACK_LEFT)
                {
                    bullet.ShootingBullet(false);
                }
                else if (playerBodyState == PLAYER_BODY_STATE.ATTACK_RIGHT)
                {
                    bullet.ShootingBullet(true);
                }
                break;
            case PLAYER_ARMS.GATLING_GUN:
                bullet = onCreateBullet(startPos, onCreateModelBullet(), BULLET_TYPE.GUN);
                if (playerBodyState == PLAYER_BODY_STATE.ATTACK_LEFT)
                {
                    bullet.ShootingBullet(false);
                }
                else if (playerBodyState == PLAYER_BODY_STATE.ATTACK_RIGHT)
                {
                    bullet.ShootingBullet(true);
                }
                break;
            case PLAYER_ARMS.DUAL_PISTOL:
                bullet = onCreateBullet(startPos, onCreateModelBullet(), BULLET_TYPE.GUN);
                if (playerBodyState == PLAYER_BODY_STATE.ATTACK_LEFT)
                {
                    bullet.ShootingBullet(false);
                }
                else if (playerBodyState == PLAYER_BODY_STATE.ATTACK_RIGHT)
                {
                    bullet.ShootingBullet(true);
                }
                break;
            case PLAYER_ARMS.SHOTGUN:
                float[] _angles = new float[]{ -5, 0, 5 };
                for (int i = 0; i < _angles.Length; i++)
                {
                    bullet = onCreateBullet(startPos, onCreateModelBullet(), BULLET_TYPE.GUN, _angles[i]);
                    if (playerBodyState == PLAYER_BODY_STATE.ATTACK_LEFT)
                    {
                        bullet.ShootingBullet(false);
                    }
                    else if (playerBodyState == PLAYER_BODY_STATE.ATTACK_RIGHT)
                    {
                        bullet.ShootingBullet(true);
                    }
                }
                break;
            case PLAYER_ARMS.BAZOKA:
                bullet = onCreateBullet(startPos, onCreateModelBullet(), BULLET_TYPE.BAZOKA_GUN);
                if (playerBodyState == PLAYER_BODY_STATE.ATTACK_LEFT)
                {
                    bullet.ShootingBullet(false);
                }
                else if (playerBodyState == PLAYER_BODY_STATE.ATTACK_RIGHT)
                {
                    bullet.ShootingBullet(true);
                }
                break;
            case PLAYER_ARMS.AWM_GUN:
                bullet = onCreateBullet(startPos, onCreateModelBullet(), BULLET_TYPE.AWM_GUN);
                if (playerBodyState == PLAYER_BODY_STATE.ATTACK_LEFT)
                {
                    bullet.ShootingBullet(false);
                }
                else if (playerBodyState == PLAYER_BODY_STATE.ATTACK_RIGHT)
                {
                    bullet.ShootingBullet(true);
                }
                break;
//            case PLAYER_ARMS.NUCLEAR_GUN:
//                bullet = onCreateBullet(startPos, onCreateModelBullet(), BULLET_TYPE.NUCLEAR_GUN);
//                if (playerBodyState == PLAYER_BODY_STATE.ATTACK_LEFT)
//                {
//                    bullet.ShootingBullet(false);
//                }
//                else if (playerBodyState == PLAYER_BODY_STATE.ATTACK_RIGHT)
//                {
//                    bullet.ShootingBullet(true);
//                }
//                break;
        }
    }



    public void OnShootingAwmGun()
    {
        playerAnimation.OnShootingAwmGun();
    }

    public void OnDetectMonster()
    {
        switch (playerArms)
        {
            case PLAYER_ARMS.SWORD:
                playerSword.gameObject.SetActive(true);
                break;
        }
    }

    public void OnMoveSword()
    {
        playerMove.OnMoveSword();
    }


    public void SetAnimationBody(PLAYER_BODY_STATE state)
    {
        playerAnimation.OnSetAnimationBody(state);
    }

    public void SetAnimationLeg(PLAYER_LEG_STATE state)
    {
        playerAnimation.OnSetAnimationLeg(state);
    }

    public void OnChangePlayerArms(PLAYER_ARMS nextArms)
    {
        if (playerArms != nextArms)
        {
            onDropArm(playerArms);
            playerArms = nextArms;

            isAttackLeft = false;
            isAttackRight = false;
            countAttackLeft = 0;
            countAttackRight = 0;
            GamePlayUIHandle.Instance.OnUpdatePlayerArm(playerArms);
            if (nextArms == PLAYER_ARMS.SWORD)
            {
                playerBulletEmpty.OnHideEffectAnimation();
            }
            else
            {
                if (GamePlayUIHandle.Instance.OnGetNumberBullet(nextArms) > 0)
                {
                    playerBulletEmpty.OnHideEffectAnimation();
                }
                else
                {
                    playerBulletEmpty.OnShowEffectEmptyBullet();
                }
            } 

            if (playerFireGun.isBurning)
            {
                playerFireGun.OnCancleAttack();
            }
            if (playerThunderGun.isDischarging)
            {
                playerThunderGun.OnCancelAttack();
            }

        }
    }

    public void OnUseNewArmSuccess(bool isLeft)
    {
        if (isLeft)
        {
            SetAnimationBody(PLAYER_BODY_STATE.IDE_LEFT);
//            SetAnimationLeg(PLAYER_LEG_STATE.IDE_LEFT);
            playerBodyState = PLAYER_BODY_STATE.IDE_LEFT;
        }
        else
        {
            SetAnimationBody(PLAYER_BODY_STATE.IDE_RIGHT);
//            SetAnimationLeg(PLAYER_LEG_STATE.IDE_RIGHT);
            playerBodyState = PLAYER_BODY_STATE.IDE_RIGHT;
        }
        GamePlayUIHandle.Instance.OnUnLockControl();
        onSetSpeedArm(playerArms);
            
    }


    #endregion

    #region Private Methods

    private void onSetSpeedArm(PLAYER_ARMS type)
    {
        switch (type)
        {
            case PLAYER_ARMS.AK47:
                deltaSpeedArm = modelPlayer.speed * -0.05f;
                break;
            case PLAYER_ARMS.SHOTGUN:
                deltaSpeedArm = modelPlayer.speed * -0.05f;
                break;
            case PLAYER_ARMS.GATLING_GUN:
                deltaSpeedArm = modelPlayer.speed * -0.1f;
                break;
            case PLAYER_ARMS.AWM_GUN:
                deltaSpeedArm = modelPlayer.speed * -0.15f;
                break;
            case PLAYER_ARMS.BAZOKA:
                deltaSpeedArm = modelPlayer.speed * -0.2f;
                break;
            case PLAYER_ARMS.FIRE_GUN:
                deltaSpeedArm = modelPlayer.speed * -0.05f;
                break;
            case PLAYER_ARMS.THUNDER_GUN:
                deltaSpeedArm = modelPlayer.speed * -0.05f;
                break;
            case PLAYER_ARMS.NUCLEAR_GUN:
                deltaSpeedArm = modelPlayer.speed * -0.05f;
                break;
            default:
                deltaSpeedArm = 0;
                break;
        }
    }

    private void onClearData()
    {
        ListBullets.Clear();
        foreach (Transform item in BulletParent)
        {
            Destroy(item.gameObject);
        }
    }

    private void onSetStateGameOver()
    {
        isSaliva = false;
//        GameManager.Instance.OnSetGameState(GAME_STATE.GAMEOVER);
        playerLegState = PLAYER_LEG_STATE.DEAD;
        playerBodyState = PLAYER_BODY_STATE.DEAD;
        OnCancelBurningMonster();
        OnCancelDisChargingMonster();
        SetAnimationBody(PLAYER_BODY_STATE.DEAD);
        SetAnimationLeg(PLAYER_LEG_STATE.DEAD);
        onClearData();
        playerItemAnimation.OnCancelAnimationSpeed();
        playerItemAnimation.OnCancelAnimationStrong();
        playerBulletEmpty.OnHideEffectAnimation();
    }

    private void onDropArm(PLAYER_ARMS playerArm)
    {
        playerBodyState = PLAYER_BODY_STATE.CHANGE_ARM;
        playerAnimation.OnDropArm(playerArm);
    }

    private ModelBullet onCreateModelBullet()
    {
        return GameManager.Instance.OnGetModelBullet(playerArms);
    }

    private BulletHandle onCreateBullet(Vector3 startPos, ModelBullet model, BULLET_TYPE type, float eulerAngle = 0)
    {
        if (ListBullets.Count == 0)
        {
            return onSpawBullet(startPos, model, type, eulerAngle);
        }
        else
        {
            for (int i = 0; i < ListBullets.Count; i++)
            {
                if (!ListBullets[i].gameObject.activeSelf)
                {
                    ListBullets[i].OnSetBullet(startPos, model, type, eulerAngle);
                    BulletHandle bullet = ListBullets[i];
                    return bullet;
                }
            }
            return onSpawBullet(startPos, model, type, eulerAngle);
        }
    }

    private BulletHandle onSpawBullet(Vector3 startPos, ModelBullet model, BULLET_TYPE type, float eulerAngles = 0)
    {
        GameObject obj = Instantiate(prefabBullet, startPos, Quaternion.identity, BulletParent) as GameObject;
        BulletHandle _bulletHandler = obj.GetComponent<BulletHandle>();
        _bulletHandler.OnSetBullet(startPos, model, type, eulerAngles);
        ListBullets.Add(_bulletHandler);
        return _bulletHandler;
    }

    #endregion

}
