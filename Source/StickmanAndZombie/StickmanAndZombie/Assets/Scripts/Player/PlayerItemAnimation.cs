﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;

public class PlayerItemAnimation : MonoBehaviour
{

    public SkeletonAnimation skeletonAnimationStrong;
    public SkeletonAnimation skeletonAnimationSpeed;

    public MeshRenderer meshRendererStrong;
    public MeshRenderer meshRendererSpeed;

    public bool isPlayingAnimationSpeed = false;
    public bool isPlayingAnimationStrong = false;

    private Spine.AnimationState animationStateStrong;
    private Spine.AnimationState animationStateSpeed;

    private string animationSpeed = "effect_speed";
    private string animationStrong = "effect_strong";

    #region Behaviour

    private void Start()
    {
        animationStateSpeed = skeletonAnimationSpeed.AnimationState;
        animationStateStrong = skeletonAnimationStrong.AnimationState;

        skeletonAnimationSpeed.gameObject.SetActive(false);
        skeletonAnimationStrong.gameObject.SetActive(false);
    }

    #endregion

    #region Publish Methods

    public void OnPlayAnimationSpeed(int orderLayer)
    {
        skeletonAnimationSpeed.gameObject.SetActive(true);
        meshRendererSpeed.sortingOrder = orderLayer - 1;
        if (!isPlayingAnimationSpeed)
        {
            skeletonAnimationSpeed.AnimationState.SetAnimation(0, animationSpeed, true);
            isPlayingAnimationSpeed = true;
        }
    }

    public void OnCancelAnimationSpeed()
    {
        if (isPlayingAnimationSpeed)
        {
            skeletonAnimationSpeed.AnimationState.ClearTracks();
            skeletonAnimationSpeed.gameObject.SetActive(false);
            isPlayingAnimationSpeed = false;
        }
    }

    public void OnPlayAnimationStrong(int orderLayer)
    {
        skeletonAnimationStrong.gameObject.SetActive(true);
        meshRendererStrong.sortingOrder = orderLayer + 1;
        if (!isPlayingAnimationStrong)
        {
            skeletonAnimationStrong.AnimationState.SetAnimation(0, animationStrong, true);
            isPlayingAnimationStrong = true;
        }
    }


    public void OnCancelAnimationStrong()
    {
        if (isPlayingAnimationStrong)
        {
            skeletonAnimationStrong.AnimationState.ClearTracks();
            skeletonAnimationStrong.gameObject.SetActive(false);
            isPlayingAnimationStrong = false;
        }
    }

    public void OnSetLayer(int legSortingOrder)
    {
        if (skeletonAnimationSpeed.gameObject.activeSelf)
        {
            meshRendererSpeed.sortingOrder = legSortingOrder - 1;
        }
        if (skeletonAnimationStrong.gameObject.activeSelf)
        {
            meshRendererStrong.sortingOrder = legSortingOrder + 1;
        }
    }

    #endregion



}
