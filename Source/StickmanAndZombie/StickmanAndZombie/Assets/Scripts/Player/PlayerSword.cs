﻿using UnityEngine;
using GameSupport;

public class PlayerSword : MonoBehaviour
{
    public LayerMask layerCast;

    private const float RAY_LENGHT = 2;
    private const float SPEED_ROTATION = 9f;
    private Ray2D ray;
    private float degree = 0;
    private const float MAX_DEGREE = 90;
    private bool isAttackRight = true;


    #region Behaviour

    void OnEnable()
    {
        if (PlayerController.Instance.playerBodyState == PLAYER_BODY_STATE.ATTACK_LEFT)
        {
            isAttackRight = false;
        }
        else if (PlayerController.Instance.playerBodyState == PLAYER_BODY_STATE.ATTACK_RIGHT)
        {
            isAttackRight = true;
        }
    }

    void Update()
    {
        if (GameManager.Instance.GameState == GAME_STATE.PLAYING)
        {
            ray = new Ray2D(transform.position, Vector3.up);
            if (isAttackRight)
            {
                degree += SPEED_ROTATION;
            }
            else
            {
                degree += SPEED_ROTATION * -1;
            }
            ray.direction = Quaternion.AngleAxis(degree, Vector3.back) * ray.direction;
            Debug.DrawRay(ray.origin, ray.direction * RAY_LENGHT, Color.red);
            if (Mathf.Abs(degree) > MAX_DEGREE)
            {
                degree = 0;
                gameObject.SetActive(false);
            }
            onCastObject(ray);
        }
    }

    #endregion

    #region Private Methods

    private void onCastObject(Ray2D ray)
    {
        RaycastHit2D[] hits;
        hits = Physics2D.RaycastAll(ray.origin, ray.direction * RAY_LENGHT, RAY_LENGHT, layerCast);
        if (hits.Length > 0)
        {
            for (int i = 0; i < hits.Length; i++)
            {
                if (hits[i].collider != null)
                {
                    if (Mathf.Abs(hits[i].transform.position.y - transform.position.y) < 0.3f)
                    {
                        MonsterHandle _monster = hits[i].transform.GetComponent<MonsterHandle>();
                        if (_monster != null && _monster.modelMonster.hp > 0)
                        {
                            if (PlayerController.Instance.isAttackLeft)
                            {
                                int _damage = (int)(GameManager.Instance.OnGetModelSword().damage
                                              + PlayerController.Instance.modelPlayer.strong
                                              + PlayerController.Instance.modelPlayer.deltaStrong);
                                _monster.OnHit(_damage, false);
                                _monster.transform.position = _monster.transform.position + Vector3.left * 0.25f;
                                EffectManager.Instance.OnExplosion(BULLET_TYPE.BOW, _monster.transform.position, PlayerController.Instance.OnGetSortingLayerBody());
                            }
                            else
                            {
                                int _damage = (int)(GameManager.Instance.OnGetModelSword().damage
                                              + PlayerController.Instance.modelPlayer.strong
                                              + PlayerController.Instance.modelPlayer.deltaStrong);
                                _monster.OnHit(_damage, true);
                                _monster.transform.position = _monster.transform.position + Vector3.right * 0.25f;
                                EffectManager.Instance.OnExplosion(BULLET_TYPE.BOW, _monster.transform.position, PlayerController.Instance.OnGetSortingLayerBody());
                            }
                        }
                    }
                }
            }

            this.gameObject.SetActive(false);
        }
    }

    #endregion


}
