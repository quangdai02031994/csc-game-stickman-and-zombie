﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;
using Spine;
using GameSupport;

public class PlayerAnimation : MonoBehaviour
{
    public bool isLeft{ get { return skeletonAnimationBody.Skeleton.FlipX; } }

    public LayerMask layerCast;

    private MeshRenderer meshRendererBody;
    private MeshRenderer meshRendererLeg;

    private SkeletonAnimation skeletonAnimationBody;
    private Spine.AnimationState animationStateBody;
    private Skeleton skeletonBody;

    private SkeletonAnimation skeletonAnimationLeg;
    private Spine.AnimationState animationStateLeg;
    private Skeleton skeletonLeg;

    private const string SpineEventAttack = "onatk";

    private const string BoneAttackPositionGun = "bone18";
    private const string BoneAttackPositionThunderGun = "bone47";

    private Vector3 localBowRight = new Vector3(0.61f, 0.45f, 0);
    private Vector3 localBowLeft = new Vector3(-0.61f, 0.45f, 0);

    private Vector3 localAK47Right = new Vector3(1, 0.3f, 0);
    private Vector3 localAK47Left = new Vector3(-1, 0.3f, 0);

    private Vector3 localGatlingGunLeft = new Vector3(-1.3f, 0, 0);
    private Vector3 localGatlingGunRight = new Vector3(1.3f, 0, 0);

    private Vector3 localNuclearGunRight = new Vector3(0.9f, 0.35f, 0);
    private Vector3 localNuclearGunLeft = new Vector3(-0.9f, 0.35f, 0);

    private Vector3 localFireGunRight = new Vector3(0.7f, 0.15f, 0);
    private Vector3 localFireGunLeft = new Vector3(-0.7f, 0.15f, 0);

    #region Behaviour

    void Awake()
    {
        skeletonAnimationBody = transform.FindChild("PlayerBodyAnimation").GetComponent<SkeletonAnimation>();
        skeletonAnimationLeg = transform.FindChild("PlayerLegAnimation").GetComponent<SkeletonAnimation>();

        meshRendererBody = skeletonAnimationBody.transform.GetComponent<MeshRenderer>();
        meshRendererLeg = skeletonAnimationLeg.transform.GetComponent<MeshRenderer>();
    }

    void Start()
    {
        animationStateBody = skeletonAnimationBody.state;
        skeletonBody = skeletonAnimationBody.skeleton;

        animationStateLeg = skeletonAnimationLeg.state;
        skeletonLeg = skeletonAnimationLeg.skeleton;

        animationStateBody.Complete += onCompleteSpineEventPlayerBody;
        animationStateBody.Start += onStartSpineEventPlayerBody;
        animationStateBody.Event += onHandleSpineEventPlayerBody;

        animationStateLeg.Complete += onCompleteSpineEventPlayerLeg;

        PlayerController.Instance.playerLegState = PLAYER_LEG_STATE.APPEAR;
        PlayerController.Instance.playerBodyState = PLAYER_BODY_STATE.APPEAR;
        PlayerController.Instance.SetAnimationBody(PLAYER_BODY_STATE.APPEAR);
        PlayerController.Instance.SetAnimationLeg(PLAYER_LEG_STATE.APPEAR);

    }

    void OnDestroy()
    {
        skeletonAnimationLeg.state.Complete -= onCompleteSpineEventPlayerLeg;
        skeletonAnimationBody.state.Complete -= onCompleteSpineEventPlayerBody;
        skeletonAnimationBody.state.Start -= onStartSpineEventPlayerBody;
        skeletonAnimationBody.state.Event -= onHandleSpineEventPlayerBody;
    }



    #endregion


    #region Publish Methods

    public void OnGameWin()
    {
        onPlayAnimationBody(PlayerAnimationName.body_win, skeletonBody.FlipX, false);
        onPlayAnimationLeg(PlayerAnimationName.leg_win, skeletonBody.FlipX, false);
    }

    public int OnGetSortingLayerBody()
    {
        return meshRendererBody.sortingOrder;
    }

    public int OnGetSortingLayerLeg()
    {
        return meshRendererLeg.sortingOrder;
    }

    public void OnSetLayer(int bodyLayer, int legLayer)
    {
        if (meshRendererBody.sortingOrder != bodyLayer && meshRendererLeg.sortingOrder != legLayer)
        {
            meshRendererBody.sortingOrder = bodyLayer;
            meshRendererLeg.sortingOrder = legLayer;
        }
    }

    public void OnDropArm(PLAYER_ARMS playerArm)
    {
        switch (playerArm)
        {
            case PLAYER_ARMS.SWORD:
                onPlayAnimationBody(PlayerAnimationName.body_drop_sword, skeletonBody.FlipX, false);
                break;
            case PLAYER_ARMS.PISTOL:
                onPlayAnimationBody(PlayerAnimationName.body_drop_pistol, skeletonBody.FlipX, false);
                break;
            case PLAYER_ARMS.BOW:
                onPlayAnimationBody(PlayerAnimationName.body_drop_bow, skeletonBody.FlipX, false);
                break;
            case PLAYER_ARMS.AK47:
                onPlayAnimationBody(PlayerAnimationName.body_drop_ak47, skeletonBody.FlipX, false);
                break;
            case PLAYER_ARMS.FIRE_GUN:
                onPlayAnimationBody(PlayerAnimationName.body_drop_firegun, skeletonBody.FlipX, false);
                break;
            case PLAYER_ARMS.THUNDER_GUN:
                onPlayAnimationBody(PlayerAnimationName.body_drop_thundergun, skeletonBody.FlipX, false);
                break;
            case PLAYER_ARMS.GATLING_GUN:
                onPlayAnimationBody(PlayerAnimationName.body_drop_gatling_gun, skeletonBody.FlipX, false);
                break;
            case PLAYER_ARMS.DUAL_PISTOL:
                onPlayAnimationBody(PlayerAnimationName.body_drop_two_pistol, skeletonBody.FlipX, false);
                break;
            case PLAYER_ARMS.SHOTGUN:
                onPlayAnimationBody(PlayerAnimationName.body_drop_shotgun, skeletonBody.FlipX, false);
                break;
            case PLAYER_ARMS.BAZOKA:
                onPlayAnimationBody(PlayerAnimationName.body_drop_bazoka, skeletonBody.FlipX, false);
                break;
            case PLAYER_ARMS.AWM_GUN:
                onPlayAnimationBody(PlayerAnimationName.body_drop_awm_gun, skeletonBody.FlipX, false);
                break;
            case PLAYER_ARMS.NUCLEAR_GUN:
                onPlayAnimationBody(PlayerAnimationName.body_drop_nuclear_gun, skeletonBody.FlipX, false);
                break;
        }
    }

    public void OnShootingAwmGun()
    {
        if (GamePlayUIHandle.Instance.OnGetNumberBullet(PlayerController.Instance.playerArms) > 0)
        {
            onPlayAnimationBody(PlayerAnimationName.body_hit_awm_gun, skeletonBody.FlipX, false);
        }
    }

    public void OnSetAnimationLeg(PLAYER_LEG_STATE state)
    {
        switch (state)
        {
            case PLAYER_LEG_STATE.RUN_LEFT:
                onPlayAnimationLeg(PlayerAnimationName.leg_run, true);
                break;
            case PLAYER_LEG_STATE.RUN_RIGHT:
                onPlayAnimationLeg(PlayerAnimationName.leg_run, false);
                break;
            case PLAYER_LEG_STATE.IDE_LEFT:
                onPlayAnimationLeg(PlayerAnimationName.leg_idle, true);
                break;
            case PLAYER_LEG_STATE.IDE_RIGHT:
                onPlayAnimationLeg(PlayerAnimationName.leg_idle, false);
                break;
            case PLAYER_LEG_STATE.DEAD:
                if (isLeft)
                {
                    onPlayAnimationLeg(PlayerAnimationName.leg_dead, true, false);
                }
                else
                {
                    onPlayAnimationLeg(PlayerAnimationName.leg_dead, false, false);
                }
                break;
        }
    }

    public void OnSetAnimationBody(PLAYER_BODY_STATE state, bool loop = true)
    {
        if (state == PLAYER_BODY_STATE.DEAD)
        {
            if (isLeft)
            {
                onPlayAnimationBody(PlayerAnimationName.body_dead, true, false);   
            }
            else
            {
                onPlayAnimationBody(PlayerAnimationName.body_dead, false, false);
            }
            GameManager.Instance.OnSetGameState(GAME_STATE.GAMEOVER);
            return;
        }
        switch (PlayerController.Instance.playerArms)
        {
            case PLAYER_ARMS.SWORD:
                switch (state)
                {
                    case PLAYER_BODY_STATE.IDE_LEFT:
                        onPlayAnimationBody(PlayerAnimationName.body_idle_sword, true);
                        break;
                    case PLAYER_BODY_STATE.IDE_RIGHT:
                        onPlayAnimationBody(PlayerAnimationName.body_idle_sword, false);
                        break;
                    case PLAYER_BODY_STATE.ATTACK_LEFT:
                        onPlayAnimationBody(PlayerAnimationName.body_hit1_sword, true, false);
                        onPlayAnimationLeg(PlayerAnimationName.leg_hit1_sword, true, false);
                        break;
                    case PLAYER_BODY_STATE.ATTACK_RIGHT:
                        onPlayAnimationBody(PlayerAnimationName.body_hit1_sword, false, false);
                        onPlayAnimationLeg(PlayerAnimationName.leg_hit1_sword, false, false);
                        break;
                    case PLAYER_BODY_STATE.APPEAR:
                        onPlayAnimationBody(PlayerAnimationName.body_appear, false, false);
                        onPlayAnimationLeg(PlayerAnimationName.leg_appear, false, false);
                        break;
                    default:
                        Debug.LogError("No state: " + state);
                        break;
                }
                break;
            case PLAYER_ARMS.PISTOL:
                switch (state)
                {
                    case PLAYER_BODY_STATE.IDE_LEFT:
                        onPlayAnimationBody(PlayerAnimationName.body_idle_pistol, true);
                        break;
                    case PLAYER_BODY_STATE.IDE_RIGHT:
                        onPlayAnimationBody(PlayerAnimationName.body_idle_pistol, false);
                        break;
                    case PLAYER_BODY_STATE.ATTACK_LEFT:
                        if (GamePlayUIHandle.Instance.OnGetNumberBullet(PlayerController.Instance.playerArms) > 0)
                        {
                            onPlayAnimationBody(PlayerAnimationName.body_hit_pistol, true);
                            onPlayAnimationLeg(PlayerAnimationName.leg_idle, true);
                        }
                        else
                        {
                            onResetLeg();
                            onResetBody();
//                            PlayerController.Instance.OnShowBulletEmpty();
                        }
                        break;
                    case PLAYER_BODY_STATE.ATTACK_RIGHT:
                        if (GamePlayUIHandle.Instance.OnGetNumberBullet(PlayerController.Instance.playerArms) > 0)
                        {
                            onPlayAnimationBody(PlayerAnimationName.body_hit_pistol, false);
                            onPlayAnimationLeg(PlayerAnimationName.leg_idle, false);
                        }
                        else
                        {
                            onResetLeg();
                            onResetBody();
//                            PlayerController.Instance.OnShowBulletEmpty();
                        }
                        break;
                }
                break;
            case PLAYER_ARMS.BOW:
                switch (state)
                {
                    case PLAYER_BODY_STATE.IDE_LEFT:
                        onPlayAnimationBody(PlayerAnimationName.body_idle_bow, true);
                        break;
                    case PLAYER_BODY_STATE.IDE_RIGHT:
                        onPlayAnimationBody(PlayerAnimationName.body_idle_bow, false);
                        break;
                    case PLAYER_BODY_STATE.ATTACK_LEFT:
                        if (GamePlayUIHandle.Instance.OnGetNumberBullet(PlayerController.Instance.playerArms) > 0)
                        {
                            onPlayAnimationBody(PlayerAnimationName.body_hit_bow, true, false);
                            onPlayAnimationLeg(PlayerAnimationName.leg_idle, true);
                        }
                        else
                        {
                            onResetLeg();
                            onResetBody();
//                            PlayerController.Instance.OnShowBulletEmpty();
                        }
                        break;
                    case PLAYER_BODY_STATE.ATTACK_RIGHT:
                        if (GamePlayUIHandle.Instance.OnGetNumberBullet(PlayerController.Instance.playerArms) > 0)
                        {
                            onPlayAnimationBody(PlayerAnimationName.body_hit_bow, false, false);
                            onPlayAnimationLeg(PlayerAnimationName.leg_idle, false);
                        }
                        else
                        {
                            onResetLeg();
                            onResetBody();
//                            PlayerController.Instance.OnShowBulletEmpty();
                        }
                        break;
                }
                break;
            case PLAYER_ARMS.AK47:
                switch (state)
                {
                    case PLAYER_BODY_STATE.IDE_LEFT:
                        onPlayAnimationBody(PlayerAnimationName.body_idle_ak47, true);
                        break;
                    case PLAYER_BODY_STATE.IDE_RIGHT:
                        onPlayAnimationBody(PlayerAnimationName.body_idle_ak47, false);
                        break;
                    case PLAYER_BODY_STATE.ATTACK_LEFT:
                        if (GamePlayUIHandle.Instance.OnGetNumberBullet(PlayerController.Instance.playerArms) > 0)
                        {
                            onPlayAnimationBody(PlayerAnimationName.body_hit_ak47, true, true);
                            onPlayAnimationLeg(PlayerAnimationName.leg_idle, true);
                        }
                        else
                        {
                            onResetLeg();
                            onResetBody();
//                            PlayerController.Instance.OnShowBulletEmpty();
                        }
                        break;
                    case PLAYER_BODY_STATE.ATTACK_RIGHT:
                        if (GamePlayUIHandle.Instance.OnGetNumberBullet(PlayerController.Instance.playerArms) > 0)
                        {
                            onPlayAnimationBody(PlayerAnimationName.body_hit_ak47, false, true);
                            onPlayAnimationLeg(PlayerAnimationName.leg_idle, false);
                        }
                        else
                        {
                            onResetLeg();
                            onResetBody();
//                            PlayerController.Instance.OnShowBulletEmpty();
                        }
                        break;
                }
                break;
            case PLAYER_ARMS.GATLING_GUN:
                switch (state)
                {
                    case PLAYER_BODY_STATE.IDE_LEFT:
                        onPlayAnimationBody(PlayerAnimationName.body_idle_gatling_gun, true, true);
                        break;
                    case PLAYER_BODY_STATE.IDE_RIGHT:
                        onPlayAnimationBody(PlayerAnimationName.body_idle_gatling_gun, false, true);
                        break;
                    case PLAYER_BODY_STATE.ATTACK_LEFT:
                        if (GamePlayUIHandle.Instance.OnGetNumberBullet(PlayerController.Instance.playerArms) > 0)
                        {
                            onPlayAnimationBody(PlayerAnimationName.body_hit_gatling_gun, true, true);
                            onPlayAnimationLeg(PlayerAnimationName.leg_idle, true);
                        }
                        else
                        {
                            onResetLeg();
                            onResetBody();
//                            PlayerController.Instance.OnShowBulletEmpty();
                        }

                        break;
                    case PLAYER_BODY_STATE.ATTACK_RIGHT:
                        if (GamePlayUIHandle.Instance.OnGetNumberBullet(PlayerController.Instance.playerArms) > 0)
                        {
                            onPlayAnimationBody(PlayerAnimationName.body_hit_gatling_gun, false, true);
                            onPlayAnimationLeg(PlayerAnimationName.leg_idle, false);
                        }
                        else
                        {
                            onResetLeg();
                            onResetBody();
//                            PlayerController.Instance.OnShowBulletEmpty();
                        }
                        break;
                }
                break;
            case PLAYER_ARMS.NUCLEAR_GUN:
                switch (state)
                {
                    case PLAYER_BODY_STATE.IDE_LEFT:
                        onPlayAnimationBody(PlayerAnimationName.body_idle_nuclear_gun, true, true);
                        break;
                    case PLAYER_BODY_STATE.IDE_RIGHT:
                        onPlayAnimationBody(PlayerAnimationName.body_idle_nuclear_gun, false, true);
                        break;
                    case PLAYER_BODY_STATE.ATTACK_LEFT:
                        if (GamePlayUIHandle.Instance.OnGetNumberBullet(PlayerController.Instance.playerArms) > 0)
                        {
                            onPlayAnimationBody(PlayerAnimationName.body_hit_nuclear_gun, true, true);
                            onPlayAnimationLeg(PlayerAnimationName.leg_idle, true);
                        }
                        else
                        {
                            onResetLeg();
                            onResetBody();
//                            PlayerController.Instance.OnShowBulletEmpty();
                        }
                        break;
                    case PLAYER_BODY_STATE.ATTACK_RIGHT:
                        if (GamePlayUIHandle.Instance.OnGetNumberBullet(PlayerController.Instance.playerArms) > 0)
                        {
                            onPlayAnimationBody(PlayerAnimationName.body_hit_nuclear_gun, false, true);
                            onPlayAnimationLeg(PlayerAnimationName.leg_idle, false);
                        }
                        else
                        {
                            onResetLeg();
                            onResetBody();
//                            PlayerController.Instance.OnShowBulletEmpty();
                        }
                        break;
                }
                break;
            case PLAYER_ARMS.FIRE_GUN:
                switch (state)
                {
                    case PLAYER_BODY_STATE.IDE_LEFT:
                        onPlayAnimationBody(PlayerAnimationName.body_idle_firegun, true);
                        break;
                    case PLAYER_BODY_STATE.IDE_RIGHT:
                        onPlayAnimationBody(PlayerAnimationName.body_idle_firegun, false);
                        break;
                    case PLAYER_BODY_STATE.ATTACK_LEFT:
                        if (GamePlayUIHandle.Instance.OnGetNumberBullet(PlayerController.Instance.playerArms) > 0)
                        {
                            onPlayAnimationBody(PlayerAnimationName.body_hit_firegun, true, true);
                            onPlayAnimationLeg(PlayerAnimationName.leg_idle, true);
                        }
                        else
                        {
                            onResetLeg();
                            onResetBody();
                            PlayerController.Instance.OnCancelBurningMonster();
//                            PlayerController.Instance.OnShowBulletEmpty();
                        }
                        break;
                    case PLAYER_BODY_STATE.ATTACK_RIGHT:
                        if (GamePlayUIHandle.Instance.OnGetNumberBullet(PlayerController.Instance.playerArms) > 0)
                        {
                            onPlayAnimationBody(PlayerAnimationName.body_hit_firegun, false, true);
                            onPlayAnimationLeg(PlayerAnimationName.leg_idle, false);
                        }
                        else
                        {
                            onResetLeg();
                            onResetBody();
                            PlayerController.Instance.OnCancelBurningMonster();
//                            PlayerController.Instance.OnShowBulletEmpty();
                        }
                        break;
                }
                break;
            case PLAYER_ARMS.THUNDER_GUN:
                switch (state)
                {
                    case PLAYER_BODY_STATE.IDE_LEFT:
                        onPlayAnimationBody(PlayerAnimationName.body_idle_thundergun, true);
                        break;
                    case PLAYER_BODY_STATE.IDE_RIGHT:
                        onPlayAnimationBody(PlayerAnimationName.body_idle_thundergun, false);
                        break;
                    case PLAYER_BODY_STATE.ATTACK_LEFT:
                        if (GamePlayUIHandle.Instance.OnGetNumberBullet(PlayerController.Instance.playerArms) > 0)
                        {
                            onPlayAnimationBody(PlayerAnimationName.body_hit_thundergun, true, false);
                            onPlayAnimationLeg(PlayerAnimationName.leg_idle, true);
                        }
                        else
                        {
                            onResetLeg();
                            onResetBody();
                            PlayerController.Instance.OnCancelDisChargingMonster();
//                            PlayerController.Instance.OnShowBulletEmpty();
                        }
                        break;
                    case PLAYER_BODY_STATE.ATTACK_RIGHT:
                        if (GamePlayUIHandle.Instance.OnGetNumberBullet(PlayerController.Instance.playerArms) > 0)
                        {
                            onPlayAnimationBody(PlayerAnimationName.body_hit_thundergun, false, false);
                            onPlayAnimationLeg(PlayerAnimationName.leg_idle, false);
                        }
                        else
                        {
                            onResetLeg();
                            onResetBody();
                            PlayerController.Instance.OnCancelDisChargingMonster();
//                            PlayerController.Instance.OnShowBulletEmpty();
                        }
                        break;
                }
                break;
            case PLAYER_ARMS.DUAL_PISTOL:
                switch (state)
                {
                    case PLAYER_BODY_STATE.IDE_LEFT:
                        onPlayAnimationBody(PlayerAnimationName.body_idle_two_pistol, true, true);
                        break;
                    case PLAYER_BODY_STATE.IDE_RIGHT:
                        onPlayAnimationBody(PlayerAnimationName.body_idle_two_pistol, false, true);
                        break;
                    case PLAYER_BODY_STATE.ATTACK_LEFT:
                        if (GamePlayUIHandle.Instance.OnGetNumberBullet(PlayerController.Instance.playerArms) > 0)
                        {
                            onPlayAnimationBody(PlayerAnimationName.body_hit_two_pistol_down, true);
                            onPlayAnimationLeg(PlayerAnimationName.leg_idle, true);
                        }
                        else
                        {
                            onResetLeg();
                            onResetBody();
//                            PlayerController.Instance.OnShowBulletEmpty();
                        }
                        break;
                    case PLAYER_BODY_STATE.ATTACK_RIGHT:
                        if (GamePlayUIHandle.Instance.OnGetNumberBullet(PlayerController.Instance.playerArms) > 0)
                        {
                            onPlayAnimationBody(PlayerAnimationName.body_hit_two_pistol_down, false);
                            onPlayAnimationLeg(PlayerAnimationName.leg_idle, false);
                        }
                        else
                        {
                            onResetLeg();
                            onResetBody();
//                            PlayerController.Instance.OnShowBulletEmpty();
                        }
                        break;
                }
                break;
            case PLAYER_ARMS.SHOTGUN:
                switch (state)
                {
                    case PLAYER_BODY_STATE.IDE_LEFT:
                        onPlayAnimationBody(PlayerAnimationName.body_idle_shotgun, true, true);
                        break;
                    case PLAYER_BODY_STATE.IDE_RIGHT:
                        onPlayAnimationBody(PlayerAnimationName.body_idle_shotgun, false, true);
                        break;
                    case PLAYER_BODY_STATE.ATTACK_LEFT:
                        if (GamePlayUIHandle.Instance.OnGetNumberBullet(PlayerController.Instance.playerArms) > 0)
                        {
                            onPlayAnimationBody(PlayerAnimationName.body_hit_shotgun, true, false);
                            onPlayAnimationLeg(PlayerAnimationName.leg_idle, true);
                        }
                        else
                        {
                            onResetLeg();
                            onResetBody();
//                            PlayerController.Instance.OnShowBulletEmpty();
                        }
                        break;
                    case PLAYER_BODY_STATE.ATTACK_RIGHT:
                        if (GamePlayUIHandle.Instance.OnGetNumberBullet(PlayerController.Instance.playerArms) > 0)
                        {
                            onPlayAnimationBody(PlayerAnimationName.body_hit_shotgun, false, false);
                            onPlayAnimationLeg(PlayerAnimationName.leg_idle, false);
                        }
                        else
                        {
                            onResetLeg();
                            onResetBody();
//                            PlayerController.Instance.OnShowBulletEmpty();
                        }
                        break;
                    default:
                        Debug.LogError("No state: " + state);
                        break;
                }
                break;
            case PLAYER_ARMS.AWM_GUN:
                switch (state)
                {
                    case PLAYER_BODY_STATE.IDE_LEFT:
                        onPlayAnimationBody(PlayerAnimationName.body_idle_awm_gun, true, true);
                        break;
                    case PLAYER_BODY_STATE.IDE_RIGHT:
                        onPlayAnimationBody(PlayerAnimationName.body_idle_awm_gun, false, true);
                        break;
                    case PLAYER_BODY_STATE.ATTACK_RIGHT:
                        if (GamePlayUIHandle.Instance.OnGetNumberBullet(PlayerController.Instance.playerArms) > 0)
                        {
                            onPlayAnimationBody(PlayerAnimationName.body_aim_awm_gun, false, false);
                            onPlayAnimationLeg(PlayerAnimationName.leg_idle, false);
                        }
                        else
                        {
                            onResetLeg();
                            onResetBody();
//                            PlayerController.Instance.OnShowBulletEmpty();
                        }
                        break;
                    case PLAYER_BODY_STATE.ATTACK_LEFT:
                        if (GamePlayUIHandle.Instance.OnGetNumberBullet(PlayerController.Instance.playerArms) > 0)
                        {
                            onPlayAnimationBody(PlayerAnimationName.body_aim_awm_gun, true, false);
                            onPlayAnimationLeg(PlayerAnimationName.leg_idle, true);
                        }
                        else
                        {
                            onResetLeg();
                            onResetBody();
//                            PlayerController.Instance.OnShowBulletEmpty();
                        }
                        break;
                    default:
                        Debug.LogError("No state: " + state);
                        break;
                }
                break;
            case PLAYER_ARMS.BAZOKA:
                switch (state)
                {
                    case PLAYER_BODY_STATE.IDE_LEFT:
                        onPlayAnimationBody(PlayerAnimationName.body_idle_bazoka, true, true);
                        break;
                    case PLAYER_BODY_STATE.IDE_RIGHT:
                        onPlayAnimationBody(PlayerAnimationName.body_idle_bazoka, false, true);
                        break;
                    case PLAYER_BODY_STATE.ATTACK_LEFT:
                        if (GamePlayUIHandle.Instance.OnGetNumberBullet(PlayerController.Instance.playerArms) > 0)
                        {
                            onPlayAnimationBody(PlayerAnimationName.body_hit_bazoka, true, false);
                            onPlayAnimationLeg(PlayerAnimationName.leg_idle, true);
                        }
                        else
                        {
                            onResetLeg();
                            onResetBody();
                        }
                        break;
                    case PLAYER_BODY_STATE.ATTACK_RIGHT:
                        if (GamePlayUIHandle.Instance.OnGetNumberBullet(PlayerController.Instance.playerArms) > 0)
                        {
                            onPlayAnimationBody(PlayerAnimationName.body_hit_bazoka, false, false);
                            onPlayAnimationLeg(PlayerAnimationName.leg_idle, false);
                        }
                        else
                        {
                            onResetLeg();
                            onResetBody();
                        }
                        break;
                    default:
                        Debug.LogError("No state: ");
                        break;
                }
                break;
            default:
                Debug.LogError("No arm : " + PlayerController.Instance.playerArms);
                break;
        }
    }

    #endregion

    #region Spine Events

    private void onHandleSpineEventPlayerBody(TrackEntry trackEntry, Spine.Event evt)
    {
        //        Debug.Log("Event name: " + evt.Data.Name + " Animation name: " + trackEntry.Animation.Name);
        switch (evt.Data.Name)
        {
            case SpineEventAttack:
                Bone bone;
                Vector3 startPos;
                switch (PlayerController.Instance.playerArms)
                {
                    case PLAYER_ARMS.PISTOL:
                        if (GamePlayUIHandle.Instance.OnGetNumberBullet(PLAYER_ARMS.PISTOL) > 0)
                        {
                            bone = skeletonBody.FindBone(BoneAttackPositionGun);
                            startPos = skeletonAnimationBody.transform.TransformPoint(new Vector3(bone.WorldX, bone.WorldY, 0f));
                            PlayerController.Instance.OnCreateBullet(startPos);
                            onCreateBuletCartouche(skeletonBody.FlipX);
                        }
                        else
                        {
                            onResetLeg();
                            onResetBody();
                        }
                        break;
                    case PLAYER_ARMS.BOW:
                        if (skeletonBody.FlipX)
                        {
                            startPos = transform.TransformPoint(localBowLeft);
                        }
                        else
                        {
                            startPos = transform.TransformPoint(localBowRight);
                        }
                        PlayerController.Instance.OnCreateBullet(startPos);
                        break;
                    case PLAYER_ARMS.SWORD:
                        PlayerController.Instance.OnMoveSword();
                        PlayerController.Instance.OnDetectMonster();
                        break;
                    case PLAYER_ARMS.AK47:
                        if (GamePlayUIHandle.Instance.OnGetNumberBullet(PLAYER_ARMS.AK47) > 0)
                        {
                            if (skeletonBody.FlipX)
                            {
                                startPos = transform.TransformPoint(localAK47Left);
                            }
                            else
                            {
                                startPos = transform.TransformPoint(localAK47Right);
                            }
                            PlayerController.Instance.OnCreateBullet(startPos);
                            onCreateBuletCartouche(skeletonBody.FlipX);
                        }
                        else
                        {
                            onResetLeg();
                            onResetBody();
//                            PlayerController.Instance.OnShowBulletEmpty();
                        }
                        break;
                    case PLAYER_ARMS.GATLING_GUN:
                        if (GamePlayUIHandle.Instance.OnGetNumberBullet(PLAYER_ARMS.GATLING_GUN) > 0)
                        {
                            if (skeletonBody.FlipX)
                            {
                                startPos = transform.TransformPoint(localGatlingGunLeft);
                            }
                            else
                            {
                                startPos = transform.TransformPoint(localGatlingGunRight);
                            }
                            PlayerController.Instance.OnCreateBullet(startPos);
                            onCreateBuletCartouche(skeletonBody.FlipX);
                        }
                        else
                        {
                            onResetLeg();
                            onResetBody();
//                            PlayerController.Instance.OnShowBulletEmpty();
                        }
                        break;
                    case PLAYER_ARMS.DUAL_PISTOL:
                        if (GamePlayUIHandle.Instance.OnGetNumberBullet(PLAYER_ARMS.DUAL_PISTOL) > 0)
                        {
                            bone = skeletonBody.FindBone(BoneAttackPositionGun);
                            startPos = skeletonAnimationBody.transform.TransformPoint(new Vector3(bone.WorldX, bone.WorldY, 0f));
                            PlayerController.Instance.OnCreateBullet(startPos);
                            onCreateBuletCartouche(skeletonBody.FlipX);
                        }
                        else
                        {
                            onResetLeg();
                            onResetBody();
                        }
                        break;
                    case PLAYER_ARMS.SHOTGUN:
                        bone = skeletonBody.FindBone(BoneAttackPositionGun);
                        startPos = skeletonAnimationBody.transform.TransformPoint(new Vector3(bone.WorldX, bone.WorldY, 0f));
                        PlayerController.Instance.OnCreateBullet(startPos);
                        onCreateBuletCartouche(skeletonBody.FlipX);
                        break;
                    case PLAYER_ARMS.BAZOKA:
                        bone = skeletonBody.FindBone(BoneAttackPositionGun);
                        startPos = skeletonAnimationBody.transform.TransformPoint(new Vector3(bone.WorldX, bone.WorldY, 0f));
                        PlayerController.Instance.OnCreateBullet(startPos);
                        break;
                    case PLAYER_ARMS.AWM_GUN:
                        bone = skeletonBody.FindBone(BoneAttackPositionGun);
                        startPos = skeletonAnimationBody.transform.TransformPoint(new Vector3(bone.WorldX, bone.WorldY, 0f));
                        PlayerController.Instance.OnCreateBullet(startPos);
                        onCreateBuletCartouche(skeletonBody.FlipX);
                        break;
                    case PLAYER_ARMS.FIRE_GUN:
                        if (GamePlayUIHandle.Instance.OnGetNumberBullet(PLAYER_ARMS.FIRE_GUN) > 0)
                        {
                            if (skeletonBody.FlipX)
                            {
                                startPos = transform.TransformPoint(localFireGunLeft);
                            }
                            else
                            {
                                startPos = transform.TransformPoint(localFireGunRight);
                            }
                            PlayerController.Instance.OnBurningMonster(startPos);
                        }
                        else
                        {
                            onResetLeg();
                            onResetBody();
                            PlayerController.Instance.OnCancelBurningMonster();
                        }
                        break;
                    case PLAYER_ARMS.THUNDER_GUN:
                        bone = skeletonBody.FindBone(BoneAttackPositionThunderGun);
                        startPos = skeletonAnimationBody.transform.TransformPoint(new Vector3(bone.WorldX, bone.WorldY, 0f));
                        PlayerController.Instance.OnDisChargingMonster(startPos);
                        break;
                    case PLAYER_ARMS.NUCLEAR_GUN:
                        if (skeletonBody.FlipX)
                        {
                            startPos = transform.TransformPoint(localNuclearGunLeft);
                            onAttackNuclearGun(true);
                        }
                        else
                        {
                            startPos = transform.TransformPoint(localNuclearGunRight);
                            onAttackNuclearGun(false);
                        }
                        break;
                    default:
                        Debug.LogError("No state attack: " + PlayerController.Instance.playerArms);
                        break;
                }
                GamePlayUIHandle.Instance.OnAttack(PlayerController.Instance.playerArms);
                break;
        }
    }

    private void onStartSpineEventPlayerBody(TrackEntry trackEntry)
    {

    }

    private void onCompleteSpineEventPlayerBody(TrackEntry trackEntry)
    {
        if (trackEntry.Animation.Name.IndexOf("drop") < 0)
        {
            if (trackEntry.Animation.Name == PlayerAnimationName.body_dead)
            {
                
                PlayerController.Instance.OnGameOver();
                return;
            }
            else if (trackEntry.Animation.Name == PlayerAnimationName.body_win)
            {
                GamePlayUIHandle.Instance.OnGameWin(GameAnalys.Instance.rank);
                return;
            }
            switch (PlayerController.Instance.playerArms)
            {
                case PLAYER_ARMS.AK47:
                    onCompleteSpineEventPlayerAK47(trackEntry);
                    break;
                case PLAYER_ARMS.BAZOKA:
                    onCompleteSpineEventPlayerBazoka(trackEntry);
                    break;
                case PLAYER_ARMS.BOW:
                    onCompleteSpineEventPlayerBow(trackEntry);
                    break;
                case PLAYER_ARMS.FIRE_GUN:
                    onCompleteSpineEventPlayerFireGun(trackEntry);
                    break;
                case PLAYER_ARMS.GATLING_GUN:
                    onCompleteSpineEventGatlingGun(trackEntry);
                    break;
                case PLAYER_ARMS.PISTOL:
                    onCompleteSpineEventPlayerPistol(trackEntry);
                    break;
                case PLAYER_ARMS.SHOTGUN:
                    onCompleteSpineEventPlayerShotGun(trackEntry);
                    break;
                case PLAYER_ARMS.AWM_GUN:
                    onCompleteSpineEventPlayerSnipeGun(trackEntry);
                    break;
                case PLAYER_ARMS.SWORD:
                    onCompleteSpineEventPlayerSword(trackEntry);
                    break;
                case PLAYER_ARMS.THUNDER_GUN:
                    onCompleteSpineEventPlayerThunderGun(trackEntry);
                    break;
                case PLAYER_ARMS.DUAL_PISTOL:
                    onCompleteSpineEventTwoPistol(trackEntry);
                    break;
                case PLAYER_ARMS.NUCLEAR_GUN:
                    onCompleSpineEventNuclearGun(trackEntry);
                    break;
            }
        }
        else
        {
            onUseNewArm();
        }

    }

    private void onCompleteSpineEventPlayerSnipeGun(TrackEntry trackEntry)
    {
        //        Debug.Log("Animation Name: " + trackEntry.Animation.Name);
        switch (trackEntry.Animation.Name)
        {
            case PlayerAnimationName.body_use_awm_gun:
                PlayerController.Instance.OnUseNewArmSuccess(skeletonBody.FlipX);
                break;
            case PlayerAnimationName.body_hit_awm_gun:
                PlayerController.Instance.isAttackLeft = false;
                PlayerController.Instance.isAttackRight = false;
                onResetLeg();
                onResetBody();
                break;
        }

    }

    private void onCompleteSpineEventPlayerShotGun(TrackEntry trackEntry)
    {
        //        Debug.Log("Animation name: " + trackEntry.Animation.Name);
        switch (trackEntry.Animation.Name)
        {
            case PlayerAnimationName.body_use_shotgun:
                PlayerController.Instance.OnUseNewArmSuccess(skeletonBody.FlipX);
                break;
            case PlayerAnimationName.body_hit_shotgun:
                PlayerController.Instance.isAttackLeft = false;
                PlayerController.Instance.isAttackRight = false;
                onResetLeg();
                onResetBody();
                break;
        }
    }

    private void onCompleteSpineEventPlayerBazoka(TrackEntry trackEntry)
    {
        Debug.Log("Animation name: " + trackEntry.Animation.Name);
        switch (trackEntry.Animation.Name)
        {
            case PlayerAnimationName.body_use_bazoka:
                PlayerController.Instance.OnUseNewArmSuccess(skeletonBody.FlipX);
                break;
            case PlayerAnimationName.body_hit_bazoka:
                PlayerController.Instance.isAttackLeft = false;
                PlayerController.Instance.isAttackRight = false;
                onResetLeg();
                onResetBody();
                break;
        }
    }

    private void onCompleSpineEventNuclearGun(TrackEntry trackEntry)
    {
        switch (trackEntry.Animation.Name)
        {
            case PlayerAnimationName.body_use_nuclear_gun:
                PlayerController.Instance.OnUseNewArmSuccess(skeletonBody.FlipX);
                break;
            case PlayerAnimationName.body_hit_nuclear_gun:
                PlayerController.Instance.isAttackLeft = false;
                PlayerController.Instance.isAttackRight = false;
                onResetLeg();
                onResetBody();
                break;
        }
    }

    private void onCompleteSpineEventTwoPistol(TrackEntry trackEntry)
    {
        switch (trackEntry.Animation.Name)
        {
            case PlayerAnimationName.body_use_two_pistol:
                PlayerController.Instance.OnUseNewArmSuccess(skeletonBody.FlipX);
                break;
            case PlayerAnimationName.body_hit_two_pistol_down:
                if (PlayerController.Instance.countAttackLeft > 0 || PlayerController.Instance.countAttackRight > 0)
                {
                    PlayerController.Instance.countAttackLeft = 0;
                    PlayerController.Instance.countAttackRight = 0;
                    onPlayAnimationBody(PlayerAnimationName.body_hit_two_pistol_up, skeletonBody.FlipX, true);
                }
                else
                {
                    PlayerController.Instance.isAttackLeft = false;
                    PlayerController.Instance.isAttackRight = false;
                    onResetLeg();
                    onResetBody();
                }
                break;
            case PlayerAnimationName.body_hit_two_pistol_up:
                if (PlayerController.Instance.countAttackLeft > 0 || PlayerController.Instance.countAttackRight > 0)
                {
                    PlayerController.Instance.countAttackLeft = 0;
                    PlayerController.Instance.countAttackRight = 0;
                    onPlayAnimationBody(PlayerAnimationName.body_hit_two_pistol_down, skeletonBody.FlipX, true);
                }
                else
                {
                    PlayerController.Instance.isAttackLeft = false;
                    PlayerController.Instance.isAttackRight = false;
                    onResetLeg();
                    onResetBody();
                }
                break;
        }
    }

    private void onCompleteSpineEventGatlingGun(TrackEntry trackEntry)
    {
        switch (trackEntry.Animation.Name)
        {
            case PlayerAnimationName.body_use_gatling_gun:
                PlayerController.Instance.OnUseNewArmSuccess(skeletonBody.FlipX);
                break;
            case PlayerAnimationName.body_hit_gatling_gun:
                if (!PlayerController.Instance.isAttacking)
                {
                    onResetLeg();
                    onResetBody();
                }
                break;
        }
    }

    private void onCompleteSpineEventPlayerThunderGun(TrackEntry trackEntry)
    {
        switch (trackEntry.Animation.Name)
        {
            case PlayerAnimationName.body_use_thundergun:
                PlayerController.Instance.OnUseNewArmSuccess(skeletonBody.FlipX);                    
                break;
            case PlayerAnimationName.body_hit_thundergun:
                PlayerController.Instance.isAttackLeft = false;
                PlayerController.Instance.isAttackRight = false;
                onResetLeg();
                onResetBody();
                PlayerController.Instance.OnCancelDisChargingMonster();
                break;
        }
    }

    private void onCompleteSpineEventPlayerFireGun(TrackEntry trackEntry)
    {
        switch (trackEntry.Animation.Name)
        {
            case PlayerAnimationName.body_use_firegun:
                PlayerController.Instance.OnUseNewArmSuccess(skeletonBody.FlipX);
                break;
            case PlayerAnimationName.body_hit_firegun:
                if (!PlayerController.Instance.isAttacking)
                {
                    onResetLeg();
                    onResetBody();
                    PlayerController.Instance.OnCancelBurningMonster();
                }
                break;
        }
    }

    private void onCompleteSpineEventPlayerAK47(TrackEntry trackEntry)
    {
        switch (trackEntry.Animation.Name)
        {
            case PlayerAnimationName.body_use_ak47:
                PlayerController.Instance.OnUseNewArmSuccess(skeletonBody.FlipX);
                break;
            case PlayerAnimationName.body_hit_ak47:
                if (!PlayerController.Instance.isAttacking)
                {
                    onResetLeg();
                    onResetBody();
                }
                else
                {
                    if (GamePlayUIHandle.Instance.OnGetNumberBullet(PLAYER_ARMS.AK47) <= 0)
                    {
                        PlayerController.Instance.isAttackLeft = false;
                        PlayerController.Instance.isAttackLeft = true;
                        onResetLeg();
                        onResetBody();
//                        PlayerController.Instance.OnShowBulletEmpty();
                    }
                }
                break;
        }
    }

    private void onCompleteSpineEventPlayerBow(TrackEntry trackEntry)
    {
        switch (trackEntry.Animation.Name)
        {
            case PlayerAnimationName.body_use_bow:
                PlayerController.Instance.OnUseNewArmSuccess(skeletonBody.FlipX);
                break;
            case PlayerAnimationName.body_hit_bow:
                PlayerController.Instance.isAttackLeft = false;
                PlayerController.Instance.isAttackRight = false;
                onResetLeg();
                onResetBody();
                break;

        }
    }

    private void onCompleteSpineEventPlayerSword(TrackEntry trackEntry)
    {
        switch (trackEntry.Animation.Name)
        {
            case PlayerAnimationName.body_hit1_sword:
                if (PlayerController.Instance.countAttackLeft > 0 || PlayerController.Instance.countAttackRight > 0)
                {
                    if (PlayerController.Instance.playerBodyState == PLAYER_BODY_STATE.ATTACK_LEFT)
                    {
                        onPlayAnimationBody(PlayerAnimationName.body_hit2_sword, true, false);
                    }
                    else if (PlayerController.Instance.playerBodyState == PLAYER_BODY_STATE.ATTACK_RIGHT)
                    {
                        onPlayAnimationBody(PlayerAnimationName.body_hit2_sword, false, false);
                    }
                }
                else
                {
                    onResetLeg();
                    onResetBody();
                }
                break;

            case PlayerAnimationName.body_hit2_sword:
                if (PlayerController.Instance.countAttackLeft > 1 || PlayerController.Instance.countAttackRight > 1)
                {
                    if (PlayerController.Instance.playerBodyState == PLAYER_BODY_STATE.ATTACK_LEFT)
                    {
                        onPlayAnimationBody(PlayerAnimationName.body_hit3_sword, true, false);
                    }
                    else if (PlayerController.Instance.playerBodyState == PLAYER_BODY_STATE.ATTACK_RIGHT)
                    {
                        onPlayAnimationBody(PlayerAnimationName.body_hit3_sword, false, false);
                    }
                }
                else
                {
                    onResetLeg();
                    onResetBody();
                }
                break;
            case PlayerAnimationName.body_hit3_sword:
                onResetLeg();
                onResetBody();
                break;
            case PlayerAnimationName.body_use_sword:
                PlayerController.Instance.OnUseNewArmSuccess(skeletonBody.FlipX);
                break;
            case PlayerAnimationName.body_appear:
                PlayerController.Instance.playerLegState = PLAYER_LEG_STATE.IDE_RIGHT;
                PlayerController.Instance.playerBodyState = PLAYER_BODY_STATE.IDE_RIGHT;
                onResetLeg();
                onResetBody();
                PlayerController.Instance.OnStartGame();
                break;
        }
    }

    private void onCompleteSpineEventPlayerPistol(TrackEntry trackEntry)
    {
        switch (trackEntry.Animation.Name)
        {
            case PlayerAnimationName.body_use_pistol:
                PlayerController.Instance.OnUseNewArmSuccess(skeletonBody.FlipX);
                break;
            case PlayerAnimationName.body_hit_pistol:
                if (PlayerController.Instance.countAttackLeft > 0 || PlayerController.Instance.countAttackRight > 0)
                {
                    PlayerController.Instance.countAttackLeft = 0;
                    PlayerController.Instance.countAttackRight = 0;
                }
                else
                {
                    PlayerController.Instance.isAttackLeft = false;
                    PlayerController.Instance.isAttackRight = false;
                    onResetLeg();
                    onResetBody();
                }
                break;
        }
    }

    private void onCompleteSpineEventPlayerLeg(TrackEntry trackEntry)
    {
        switch (trackEntry.Animation.Name)
        {
            case PlayerAnimationName.leg_hit1_sword:
                if (PlayerController.Instance.countAttackLeft > 0 || PlayerController.Instance.countAttackRight > 0)
                {
                    if (PlayerController.Instance.playerLegState == PLAYER_LEG_STATE.ATTACK_LEFT)
                    {
                        onPlayAnimationLeg(PlayerAnimationName.leg_hit2_sword, true, false);
                    }
                    else if (PlayerController.Instance.playerLegState == PLAYER_LEG_STATE.ATTACK_RIGHT)
                    {
                        onPlayAnimationLeg(PlayerAnimationName.leg_hit2_sword, false, false);
                    }
                }
                else
                {
                    onResetLeg();
                }
                break;

            case PlayerAnimationName.leg_hit2_sword:
                if (PlayerController.Instance.countAttackLeft > 1 || PlayerController.Instance.countAttackRight > 1)
                {
                    if (PlayerController.Instance.playerLegState == PLAYER_LEG_STATE.ATTACK_LEFT)
                    {
                        onPlayAnimationLeg(PlayerAnimationName.leg_hit3_sword, true, false);
                    }
                    else if (PlayerController.Instance.playerLegState == PLAYER_LEG_STATE.ATTACK_RIGHT)
                    {
                        onPlayAnimationLeg(PlayerAnimationName.leg_hit3_sword, false, false);
                    }
                }
                else
                {
                    onResetLeg();
                }
                break;
            case PlayerAnimationName.leg_hit3_sword:
                onResetLeg();
                break;
            default:
                break;
        }
    }

    #endregion

    #region Private Methods

    private void onAttackNuclearGun(bool isLeft)
    {
        ModelBullet bullet = GameManager.Instance.OnGetModelBullet(PLAYER_ARMS.NUCLEAR_GUN);
        if (isLeft)
        {
            Ray2D ray = new Ray2D(transform.TransformPoint(localNuclearGunLeft), Vector3.left);
            Debug.DrawRay(ray.origin, ray.direction * 1000, Color.red);
            RaycastHit2D[] hits = Physics2D.RaycastAll(ray.origin, Vector2.left, 1000, layerCast);
            if (hits.Length > 0)
            {
                for (int i = 0; i < hits.Length; i++)
                {
                    if (hits[i].collider != null
                        && hits[i].transform.position.x < CameraHandle.Instance.maxCameraX
                        && hits[i].transform.position.x > CameraHandle.Instance.minCameraX)
                    {
                        MonsterHandle _handler = hits[i].transform.GetComponent<MonsterHandle>();
                        _handler.OnHitNuclearGun(bullet.damage, false);
                    }
                }
            }
        }
        else
        {
            Ray2D ray = new Ray2D(transform.TransformPoint(localNuclearGunRight), Vector3.right);
            Debug.DrawRay(ray.origin, ray.direction * 1000, Color.red);
            RaycastHit2D[] hits = Physics2D.RaycastAll(ray.origin, Vector2.right, 1000, layerCast);
            if (hits.Length > 0)
            {
                for (int i = 0; i < hits.Length; i++)
                {
                    if (hits[i].collider != null
                        && hits[i].transform.position.x < CameraHandle.Instance.maxCameraX
                        && hits[i].transform.position.x > CameraHandle.Instance.minCameraX)
                    {
                        MonsterHandle _handler = hits[i].transform.GetComponent<MonsterHandle>();
                        _handler.OnHitNuclearGun(bullet.damage, true);
                    }
                }
            }
        }
    }

    private void onCreateBuletCartouche(bool isRight)
    {
        BulletCartoucheEffect _cartouche = EffectManager.Instance.OnGetBulletCartouche(new Vector3(transform.position.x, transform.position.y - 0.5f, transform.position.z));
        _cartouche.OnSetData(new Vector3(transform.position.x, transform.position.y - 0.5f, transform.position.z), meshRendererBody.sortingOrder);
        _cartouche.OnPlay(isRight);
    }

    private void onUseNewArm()
    {
        switch (PlayerController.Instance.playerArms)
        {
            case PLAYER_ARMS.SWORD:
                onPlayAnimationBody(PlayerAnimationName.body_use_sword, skeletonBody.FlipX, false);
                break;
            case PLAYER_ARMS.PISTOL:
                onPlayAnimationBody(PlayerAnimationName.body_use_pistol, skeletonBody.FlipX, false);
                break;
            case PLAYER_ARMS.BOW:
                onPlayAnimationBody(PlayerAnimationName.body_use_bow, skeletonBody.FlipX, false);
                break;
            case PLAYER_ARMS.AK47:
                onPlayAnimationBody(PlayerAnimationName.body_use_ak47, skeletonBody.FlipX, false);
                break;
            case PLAYER_ARMS.FIRE_GUN:
                onPlayAnimationBody(PlayerAnimationName.body_use_firegun, skeletonBody.FlipX, false);
                break;
            case PLAYER_ARMS.THUNDER_GUN:
                onPlayAnimationBody(PlayerAnimationName.body_use_thundergun, skeletonBody.FlipX, false);
                break;
            case PLAYER_ARMS.GATLING_GUN:
                onPlayAnimationBody(PlayerAnimationName.body_use_gatling_gun, skeletonBody.FlipX, false);
                break;
            case PLAYER_ARMS.DUAL_PISTOL:
                onPlayAnimationBody(PlayerAnimationName.body_use_two_pistol, skeletonBody.FlipX, false);
                break;
            case PLAYER_ARMS.SHOTGUN:
                onPlayAnimationBody(PlayerAnimationName.body_use_shotgun, skeletonBody.FlipX, false);
                break;
            case PLAYER_ARMS.AWM_GUN:
                onPlayAnimationBody(PlayerAnimationName.body_use_awm_gun, skeletonBody.FlipX, false);
                break;
            case PLAYER_ARMS.BAZOKA:
                onPlayAnimationBody(PlayerAnimationName.body_use_bazoka, skeletonBody.FlipX, false);
                break;
            case PLAYER_ARMS.NUCLEAR_GUN:
                onPlayAnimationBody(PlayerAnimationName.body_use_nuclear_gun, skeletonBody.FlipX, false);
                break;
            default:
                Debug.LogError("No animation of player arm: " + PlayerController.Instance.playerArms);
                break;
        }
    }

    private void onResetBody()
    {
        switch (PlayerController.Instance.playerBodyState)
        {
            case PLAYER_BODY_STATE.ATTACK_LEFT:
                PlayerController.Instance.countAttackLeft = 0;
                PlayerController.Instance.isAttackLeft = false;
                break;
            case PLAYER_BODY_STATE.ATTACK_RIGHT:
                PlayerController.Instance.countAttackRight = 0;
                PlayerController.Instance.isAttackRight = false;
                PlayerController.Instance.playerBodyState = PLAYER_BODY_STATE.IDE_RIGHT;
                break;
        }

        if (PlayerController.Instance.playerLegState == PLAYER_LEG_STATE.RUN_LEFT || PlayerController.Instance.playerLegState == PLAYER_LEG_STATE.IDE_LEFT)
        {
            PlayerController.Instance.playerBodyState = PLAYER_BODY_STATE.IDE_LEFT;
        }
        else if (PlayerController.Instance.playerLegState == PLAYER_LEG_STATE.RUN_RIGHT || PlayerController.Instance.playerLegState == PLAYER_LEG_STATE.IDE_RIGHT)
        {
            PlayerController.Instance.playerBodyState = PLAYER_BODY_STATE.IDE_RIGHT;
        }

        OnSetAnimationBody(PlayerController.Instance.playerBodyState);
    }

    private void onResetLeg()
    {
        if (PlayerController.Instance.isAttacking)
        {
            switch (PlayerController.Instance.playerLegState)
            {
                case PLAYER_LEG_STATE.ATTACK_LEFT:
                    PlayerController.Instance.playerLegState = PLAYER_LEG_STATE.IDE_LEFT;
                    break;
                case PLAYER_LEG_STATE.ATTACK_RIGHT:
                    PlayerController.Instance.playerLegState = PLAYER_LEG_STATE.IDE_RIGHT;
                    break;
            }
            OnSetAnimationLeg(PlayerController.Instance.playerLegState);
        }
        else
        {
            if (skeletonAnimationBody.Skeleton.FlipX)
            {
                PlayerController.Instance.playerLegState = PLAYER_LEG_STATE.IDE_LEFT;
            }
            else
            {
                PlayerController.Instance.playerLegState = PLAYER_LEG_STATE.IDE_RIGHT;
            }
            OnSetAnimationLeg(PlayerController.Instance.playerLegState);
        }
    }


    //flipx = false => animation quay sang phai
    private void onPlayAnimationBody(string animationName, bool FlipX, bool loop = true)
    {
        //        GameConsole.Log("Current Body: " + skeletonAnimationBody.AnimationName + " Next: " + animationName);
        if (skeletonAnimationBody.AnimationName != animationName)
        {
            animationStateBody.SetAnimation(0, animationName, loop);
        }
        skeletonBody.FlipX = FlipX;
    }

    //flipx = false => animation quay sang phai
    private void onPlayAnimationLeg(string animationName, bool FlipX, bool loop = true)
    {
        if (skeletonAnimationLeg.AnimationName != animationName)
        {
            animationStateLeg.SetAnimation(0, animationName, loop);
        }
        if (skeletonLeg.FlipX != FlipX)
            skeletonLeg.FlipX = FlipX;
    }

    #endregion

}
