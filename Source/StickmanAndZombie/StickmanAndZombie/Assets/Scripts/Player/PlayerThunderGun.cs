﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSupport;

public class PlayerThunderGun : MonoBehaviour
{

    public ThunderBulletEffect bulletEffect;

    public ModelGun modelGun;

    public bool isDischarging{ get { return bulletEffect.isDischarging; } }

    public LayerMask layerCast;

    private Vector3 startPosRight = new Vector3(1, 0.3f, 0);

    private Vector3 startPosLeft = new Vector3(-1, 0.3f, 0);

    private bool isAttackLeft;
    private float distance = 4;

    #region Behaviour

    //    void OnDisable()
    //    {
    //        hit = new RaycastHit2D();
    //    }

    #if UNITY_EDITOR
    
    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        if (isAttackLeft)
        {
            Ray2D ray = new Ray2D(transform.TransformPoint(startPosLeft), Vector2.left);
            Gizmos.DrawRay(ray.origin, ray.direction * distance);
        }
        else
        {
            Ray2D ray = new Ray2D(transform.TransformPoint(startPosRight), Vector2.right);
            Gizmos.DrawRay(ray.origin, ray.direction * distance);
        }
    }
    
    #endif

    void Start()
    {
        modelGun = GameManager.Instance.OnGetPlayerGun(PLAYER_ARMS.THUNDER_GUN);   
    }

    //    void Update()
    //    {
    //        if (isDischarging)
    //        {
    //            if (isAttackLeft)
    //            {
    //                Ray2D ray = new Ray2D(transform.TransformPoint(startPosLeft), Vector2.left);
    //                RaycastHit2D _hit = Physics2D.Raycast(ray.origin, ray.direction, distance, layerCast);
    //                if (_hit.collider != null && _hit != hit)
    //                {
    //                    MonsterHandle _handle = _hit.transform.GetComponent<MonsterHandle>();
    //                    _handle.OnElectricShocking(modelGun.modelBullet.damage, true);
    //                    hit = _hit;
    //                }
    //            }
    //            else
    //            {
    //                Ray2D ray = new Ray2D(transform.TransformPoint(startPosRight), Vector2.right);
    //                RaycastHit2D _hit = Physics2D.Raycast(ray.origin, ray.direction, distance, layerCast);
    //                if (_hit.collider != null && _hit != hit)
    //                {
    //                    MonsterHandle _handle = _hit.transform.GetComponent<MonsterHandle>();
    //                    _handle.OnElectricShocking(modelGun.modelBullet.damage, true);
    //                    hit = _hit;
    //                }
    //            }
    //        }
    //    }

    #endregion

    #region Publish Methods

    public void OnSetData(ModelBullet modelBullet)
    {
        bulletEffect.OnSetData(modelBullet);
    }

    public void OnAttack(bool isLeft, Vector3 startPos)
    {
        isAttackLeft = isLeft;
        this.gameObject.SetActive(true);
        bulletEffect.gameObject.SetActive(true);
        bulletEffect.OnAttack(isLeft, startPos);
        if (isLeft)
        {
            Ray2D ray = new Ray2D(transform.TransformPoint(startPosLeft), Vector2.left);
            RaycastHit2D _hit = Physics2D.Raycast(ray.origin, ray.direction, distance, layerCast);
            if (_hit.collider != null)
            {
                MonsterHandle _handle = _hit.transform.GetComponent<MonsterHandle>();
                _handle.OnElectricShocking(modelGun.modelBullet.damage, true);
            }
        }
        else
        {
            Ray2D ray = new Ray2D(transform.TransformPoint(startPosRight), Vector2.right);
            RaycastHit2D _hit = Physics2D.Raycast(ray.origin, ray.direction, distance, layerCast);
            if (_hit.collider != null)
            {
                MonsterHandle _handle = _hit.transform.GetComponent<MonsterHandle>();
                _handle.OnElectricShocking(modelGun.modelBullet.damage, true);
            }
        }
    }

    public void OnCancelAttack()
    {
        bulletEffect.gameObject.SetActive(false);
        this.gameObject.SetActive(false);
    }



    #endregion

}
