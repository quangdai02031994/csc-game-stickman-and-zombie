﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFireGun : MonoBehaviour
{

    public FireBulletEffect bulletEffect;

    public LayerMask layerCast;

    public bool isBurning{ get { return bulletEffect.isBurning; } }

    private Vector3 startPosRight = new Vector3(0.65f, 0.2f, 0);

    private Vector3 startPosLeft = new Vector3(-0.65f, 0.2f, 0);

    public bool isAttackLeft;
    private float distance = 3;

    #region Behaviour


    void Update()
    {
        if (isAttackLeft)
        {
            Ray2D ray = new Ray2D(transform.TransformPoint(startPosLeft), Vector2.left);
            RaycastHit2D[] hit = Physics2D.RaycastAll(ray.origin, ray.direction, distance, layerCast);
            if (hit.Length > 0)
            {
                for (int i = 0; i < hit.Length; i++)
                {
                    if (hit[i].collider != null)
                    {
                        MonsterHandle handler = hit[i].transform.GetComponent<MonsterHandle>();
                        if (!handler.isBurning)
                        {
                            handler.OnBurning(bulletEffect.modelBullet.damage);
                        }
                    }
                }
            }
        }
        else
        {
            Ray2D ray = new Ray2D(transform.TransformPoint(startPosRight), Vector2.right);
            RaycastHit2D[] hit = Physics2D.RaycastAll(ray.origin, ray.direction, distance, layerCast);
            if (hit.Length > 0)
            {
                for (int i = 0; i < hit.Length; i++)
                {
                    if (hit[i].collider != null)
                    {
                        MonsterHandle handler = hit[i].transform.GetComponent<MonsterHandle>();
                        if (!handler.isBurning)
                        {
                            handler.OnBurning(bulletEffect.modelBullet.damage);
                        }
                    }
                }
            }
        }
    }

    #if UNITY_EDITOR

    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        if (isAttackLeft)
        {
            Ray2D ray = new Ray2D(transform.TransformPoint(startPosLeft), Vector2.left);
            Gizmos.DrawRay(ray.origin, ray.direction * distance);
        }
        else
        {
            Ray2D ray = new Ray2D(transform.TransformPoint(startPosRight), Vector2.right);
            Gizmos.DrawRay(ray.origin, ray.direction * distance);
        }
    }
    #endif

    #endregion

    public void OnSetData(ModelBullet modelBullet)
    {
        bulletEffect.OnSetData(modelBullet);
    }

    public void OnAttack(bool isLeft, Vector3 startPos)
    {
        this.gameObject.SetActive(true);
        bulletEffect.gameObject.SetActive(true);
        bulletEffect.OnAttack(isLeft, startPos);
        isAttackLeft = isLeft;
    }

    public void OnCancleAttack()
    {
        bulletEffect.gameObject.SetActive(false);
        this.gameObject.SetActive(false);
    }



}
