﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;
using Spine;

public class PlayerUseItemAnimation : MonoBehaviour
{
    [SpineAnimation]
    public string animationNameUseHP;

    [SpineAnimation]
    public string animationNameUseSpeed;

    [SpineAnimation]
    public string animationNameUseStrong;

    [SpineAnimation]
    public string animationNameUseBullet;

    [SpineAnimation]
    public string animationNameNone;

    private SkeletonAnimation skeletonAnimation;
    private MeshRenderer meshRenderer;


    #region Behaviour

    void Awake()
    {
        skeletonAnimation = GetComponent<SkeletonAnimation>();
        meshRenderer = GetComponent<MeshRenderer>();
    }

    void Start()
    {
        skeletonAnimation.state.Complete += onCompleteEvent;  
    }

    #endregion

    #region Publish Methods

    public void OnSetLayer(int _sortingOrder)
    {
//        Debug.Log("Play Animation");
        this.gameObject.SetActive(true);
        meshRenderer.sortingOrder = _sortingOrder + 1;
    }

    public void OnPlayUseBullet()
    {
        onPlayAnimation(animationNameUseBullet);
    }

    public void OnPlayUseHP()
    {
        onPlayAnimation(animationNameUseHP);
    }

    public void OnPlayUseSpeed()
    {
        onPlayAnimation(animationNameUseSpeed);
    }

    public void OnPlayUseStrong()
    {
        onPlayAnimation(animationNameUseStrong);
    }

    #endregion

    #region Private Methods

    private void onCompleteEvent(TrackEntry trackEntry)
    {
        if (trackEntry.Next == null)
        {
            this.gameObject.SetActive(false);
        }
    }

    private void onPlayAnimation(string animationName)
    {

        skeletonAnimation.AnimationState.SetAnimation(0, animationName, false);

    }

    #endregion

}
