﻿using System.Collections;

using System.Collections.Generic;
using UnityEngine;
using GameSupport;

public class MovePlayer : MonoBehaviour
{



    private float maxHorizontal;
    private float maxVertical;
    private float minVertical;

    private float heightPositonY;
    private const int MAX_RENDER = 1000;

    public bool isMinOrMax
    {
        get
        { 
            if (transform.position.y <= minVertical || transform.position.y >= maxVertical)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    #region Behaviour

    IEnumerator Start()
    {
        yield return new WaitForEndOfFrame();
        maxHorizontal = CameraHandle.Instance.maxBGPositionX - 1.3f;
        maxVertical = CameraHandle.Instance.maxY - 2.8f;
        minVertical = CameraHandle.Instance.minY + 2.5f;
        heightPositonY = Mathf.Abs(minVertical - maxVertical);
        onReScale();
    }

    void Update()
    {
        if (GameManager.Instance.GameState == GAME_STATE.PLAYING)
        {
            if (PlayerController.Instance.playerLegState == PLAYER_LEG_STATE.RUN_LEFT
                || PlayerController.Instance.playerLegState == PLAYER_LEG_STATE.RUN_RIGHT)
            {
                onMove();
                onReScale();
            }
            else
            {
                if (PlayerController.Instance.isMinOrMax)
                {
                    onMove();
                    onReScale();
                }
            }
        }
    }

    #endregion

    #region Publish Methods

    public void OnMoveSword()
    {
        switch (PlayerController.Instance.playerBodyState)
        {
            case PLAYER_BODY_STATE.ATTACK_LEFT:
                if (GamePlayUIHandle.Instance.xMove > 0)
                {
                    Vector3 dir = new Vector3(transform.localPosition.x + 0.15f * -GamePlayUIHandle.Instance.xMove, transform.localPosition.y + 0.15f * GamePlayUIHandle.Instance.yMove);
                    transform.localPosition = new Vector3(dir.x, dir.y, transform.localPosition.z);
                }
                else
                {
                    Vector3 dir = new Vector3(transform.localPosition.x + 0.15f * GamePlayUIHandle.Instance.xMove, transform.localPosition.y + 0.15f * GamePlayUIHandle.Instance.yMove);
                    transform.localPosition = new Vector3(dir.x, dir.y, transform.localPosition.z);
                }
                break;
            case PLAYER_BODY_STATE.ATTACK_RIGHT:
                if (GamePlayUIHandle.Instance.xMove < 0)
                {
                    Vector3 dir = new Vector3(transform.localPosition.x + 0.15f * -GamePlayUIHandle.Instance.xMove, transform.localPosition.y + 0.15f * GamePlayUIHandle.Instance.yMove);
                    transform.localPosition = new Vector3(dir.x, dir.y, transform.localPosition.z);
                }
                else
                {
                    Vector3 dir = new Vector3(transform.localPosition.x + 0.15f * GamePlayUIHandle.Instance.xMove, transform.localPosition.y + 0.15f * GamePlayUIHandle.Instance.yMove);
                    transform.localPosition = new Vector3(dir.x, dir.y, transform.localPosition.z);
                }
                break;
        }
        onCheckPlayerPosition();
    }


    #endregion

    #region Private Methods

    private void onReScale()
    {
        float delta = Mathf.Abs(transform.localPosition.y - minVertical) / heightPositonY;
        transform.localScale = Vector3.one - new Vector3(0.4f, 0.4f, 0.4f) * delta;
        int bodyLayer = MAX_RENDER - (int)(delta * 900);
        int legLayer = bodyLayer - 1;
        PlayerController.Instance.OnSetLayer(bodyLayer, legLayer);
    }

    private void onMove()
    {
//        Debug.Log("XMove: " + GamePlayUIHandle.Instance.xMove + " YMove: " + GamePlayUIHandle.Instance.yMove);
        if (transform.position.y < maxVertical && transform.position.y > minVertical)
        {
            Vector3 dir = new Vector3(GamePlayUIHandle.Instance.xMove, GamePlayUIHandle.Instance.yMove, 0);
            float speed = PlayerController.Instance.modelPlayer.deltaSpeedSaliva
                          + PlayerController.Instance.modelPlayer.speed
                          + PlayerController.Instance.deltaSpeedArm
                          + PlayerController.Instance.modelPlayer.deltaSpeedItem;
            transform.localPosition += dir * Time.deltaTime * speed;
            onCheckPlayerPosition();
        }
        else if (transform.position.y <= minVertical)
        {
            if (GamePlayUIHandle.Instance.yMove > -0.8f)
            {
                Vector3 dir = new Vector3(GamePlayUIHandle.Instance.xMove, GamePlayUIHandle.Instance.yMove, 0);
                float speed = PlayerController.Instance.modelPlayer.deltaSpeedSaliva
                              + PlayerController.Instance.modelPlayer.speed
                              + PlayerController.Instance.deltaSpeedArm
                              + PlayerController.Instance.modelPlayer.deltaSpeedItem;
                transform.localPosition += dir * Time.deltaTime * speed;
                onCheckPlayerPosition();
            }
            else
            {
                if (PlayerController.Instance.isBodyLeft)
                {
                    if (PlayerController.Instance.playerLegState == PLAYER_LEG_STATE.IDE_LEFT)
                        return;
                    PlayerController.Instance.playerLegState = PLAYER_LEG_STATE.IDE_LEFT;
                    PlayerController.Instance.SetAnimationLeg(PLAYER_LEG_STATE.IDE_LEFT);
                }
                else
                {
                    if (PlayerController.Instance.playerLegState == PLAYER_LEG_STATE.IDE_RIGHT)
                        return;
                    PlayerController.Instance.playerLegState = PLAYER_LEG_STATE.IDE_RIGHT;
                    PlayerController.Instance.SetAnimationLeg(PLAYER_LEG_STATE.IDE_RIGHT);
                }
            }
        }
        else if (transform.position.y >= maxVertical)
        {
            if (GamePlayUIHandle.Instance.yMove < 0.8f)
            {
                Vector3 dir = new Vector3(GamePlayUIHandle.Instance.xMove, GamePlayUIHandle.Instance.yMove, 0);
                float speed = PlayerController.Instance.modelPlayer.deltaSpeedSaliva
                              + PlayerController.Instance.modelPlayer.speed
                              + PlayerController.Instance.deltaSpeedArm
                              + PlayerController.Instance.modelPlayer.deltaSpeedItem;
                transform.localPosition += dir * Time.deltaTime * speed;
                onCheckPlayerPosition();
            }
            else
            {
                if (PlayerController.Instance.isBodyLeft)
                {
                    if (PlayerController.Instance.playerLegState == PLAYER_LEG_STATE.IDE_LEFT)
                        return;
                    PlayerController.Instance.playerLegState = PLAYER_LEG_STATE.IDE_LEFT;
                    PlayerController.Instance.SetAnimationLeg(PLAYER_LEG_STATE.IDE_LEFT);
                }
                else
                {
                    if (PlayerController.Instance.playerLegState == PLAYER_LEG_STATE.IDE_RIGHT)
                        return;
                    PlayerController.Instance.playerLegState = PLAYER_LEG_STATE.IDE_RIGHT;
                    PlayerController.Instance.SetAnimationLeg(PLAYER_LEG_STATE.IDE_RIGHT);
                }
            }
        }
    }

    private void onCheckPlayerPosition()
    {
        if (transform.localPosition.x > maxHorizontal)
        {
            transform.localPosition = new Vector3(maxHorizontal, transform.localPosition.y);
        }
        else if (transform.localPosition.x < -maxHorizontal)
        {
            transform.localPosition = new Vector3(-maxHorizontal, transform.localPosition.y);
        }

        if (transform.localPosition.y > maxVertical)
        {
            transform.localPosition = new Vector3(transform.localPosition.x, maxVertical - 0.01f);
        }
        else if (transform.localPosition.y < minVertical)
        {
            transform.localPosition = new Vector3(transform.localPosition.x, minVertical + 0.01f);
        }
    }


    #endregion
}
