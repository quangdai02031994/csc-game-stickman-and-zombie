﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightningBoltManager : MonoBehaviour
{
    public static LightningBoltManager Instance{ get; private set; }

    [SerializeField]
    private GameObject prefabLightningBolt;

    [SerializeField]
    private List<LightningBoltHandle> ListLightningBolt;


    #region Behaviour

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    void Start()
    {
        ListLightningBolt = new List<LightningBoltHandle>();    
    }


    #endregion

    #region Publish Methods

    public LightningBoltHandle OnCreateLightning(Vector3 startPos, Vector3 endPos, int orderInLayer)
    {
        if (ListLightningBolt.Count == 0)
        {
            return onCreateLightningBolt(startPos, endPos, orderInLayer);
        }
        else
        {
            for (int i = 0; i < ListLightningBolt.Count; i++)
            {
                if (!ListLightningBolt[i].gameObject.activeSelf)
                {
                    ListLightningBolt[i].OnSetData(startPos, endPos, orderInLayer);
                    return ListLightningBolt[i];
                }
            }

            return onCreateLightningBolt(startPos, endPos, orderInLayer);
        }
    }

    #endregion

    #region Private Methods

    private LightningBoltHandle onCreateLightningBolt(Vector3 startPos, Vector3 endPos, int orderInLayer)
    {
        GameObject _obj = Instantiate(prefabLightningBolt, Vector3.zero, Quaternion.identity, this.transform);
        _obj.transform.position = startPos;
        _obj.name = "LightningBolt_Item";
        _obj.transform.localScale = Vector3.one;
        LightningBoltHandle _handler = _obj.GetComponent<LightningBoltHandle>();
        _handler.OnSetData(startPos, endPos, orderInLayer);
        ListLightningBolt.Add(_handler);
        return _handler;
    }

    #endregion


}
