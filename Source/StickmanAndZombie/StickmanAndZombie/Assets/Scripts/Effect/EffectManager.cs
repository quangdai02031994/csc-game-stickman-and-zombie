﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSupport;

public class EffectManager : MonoBehaviour
{

    public static EffectManager Instance{ get; private set; }

    public List<MonsterSalivaEffect> ListEffectSaliva;

    public List<MonsterBloodEffect> ListEffectBlood;

    public List<MonsterDeadEffect> ListEffectDead;

    public List<BulletEffectHandle> ListEffectExposion;

    public List<BulletCartoucheEffect> ListEffectCartouche;

    public List<UIGoldItemHandle> ListGoldItemReward;


    #region Behaviour

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    void Start()
    {
        onDestroyObjects();
        PlayerController.Instance.playerGameOver += onGameOver;
        PlayerController.Instance.playerGameWin += onGameWin;
    }

    void OnDestroy()
    {
        PlayerController.Instance.playerGameOver -= onGameOver;
        PlayerController.Instance.playerGameWin += onGameWin;
    }

    #endregion

    #region Publish Methods

    public UIGoldItemHandle OnGetGoldItemReward(Vector3 startPos)
    {
        if (ListGoldItemReward.Count == 0)
        {
            return onCreateGoldItemReward(startPos);
        }
        else
        {
            for (int i = 0; i < ListGoldItemReward.Count; i++)
            {
                if (!ListGoldItemReward[i].gameObject.activeSelf)
                {
                    return ListGoldItemReward[i];
                }
            }
            return onCreateGoldItemReward(startPos);
        }
    }

    public BulletCartoucheEffect OnGetBulletCartouche(Vector3 startPos)
    {
        if (ListEffectCartouche.Count == 0)
        {
            return onCreateEffectCartouche(startPos);
        }
        else
        {
            for (int i = 0; i < ListEffectCartouche.Count; i++)
            {
                if (!ListEffectCartouche[i].gameObject.activeSelf)
                {
                    return ListEffectCartouche[i];
                }
            }
            return onCreateEffectCartouche(startPos);
        }
    }

    public void OnExplosion(BULLET_TYPE type, Vector3 startPos, int orderInLayer)
    {
        BulletEffectHandle _handler = onGetEffectExplosion();
        _handler.OnSetData(startPos, orderInLayer);
        _handler.OnPlay(type);
    }

    public MonsterSalivaEffect OnGetEffectSaliva(Vector3 startPos)
    {
        if (ListEffectSaliva.Count == 0)
        {
            return onCreateEffectSaliva(startPos);
        }
        else
        {
            for (int i = 0; i < ListEffectSaliva.Count; i++)
            {
                if (!ListEffectSaliva[i].gameObject.activeSelf)
                {
                    return ListEffectSaliva[i];
                }
            }
            return onCreateEffectSaliva(startPos);
        }
    }

    public MonsterDeadEffect OnGetEffectDead()
    {
        if (ListEffectDead.Count == 0)
        {
            return onCreateEffectDead();
        }
        else
        {
            for (int i = 0; i < ListEffectDead.Count; i++)
            {
                if (!ListEffectDead[i].gameObject.activeSelf)
                {
                    return ListEffectDead[i];
                }
            }            
            return onCreateEffectDead();
        }
    }

    public MonsterBloodEffect OnGetEffectBlood()
    {
        if (ListEffectBlood.Count == 0)
        {
            return onCreateEffectBlood();
        }
        else
        {
            for (int i = 0; i < ListEffectBlood.Count; i++)
            {
                if (!ListEffectBlood[i].gameObject.activeSelf)
                {
                    return ListEffectBlood[i];
                }
            }
            return onCreateEffectBlood();
        }
    }

    #endregion

    #region Private Methods

   

    private BulletEffectHandle onGetEffectExplosion()
    {
        if (ListEffectExposion.Count == 0)
        {
            return onCreateEffectExplosion();
        }
        else
        {
            for (int i = 0; i < ListEffectExposion.Count; i++)
            {
                if (!ListEffectExposion[i].gameObject.activeSelf)
                {
                    return ListEffectExposion[i];
                }
            }
            return onCreateEffectExplosion();
        }
    }

    private UIGoldItemHandle onCreateGoldItemReward(Vector3 startPos)
    {
        GameObject _obj = Instantiate(GamePlayResources.Instance.objGold, Vector3.zero, Quaternion.identity, this.transform)as GameObject;
        _obj.name = "gold_item";
        UIGoldItemHandle _goldItem = _obj.GetComponent<UIGoldItemHandle>();
        ListGoldItemReward.Add(_goldItem);
        return _goldItem;
    }

    private BulletCartoucheEffect onCreateEffectCartouche(Vector3 startPos)
    {
        GameObject _obj = Instantiate(GamePlayResources.Instance.objEffectCartouche, Vector3.zero, Quaternion.identity, this.transform) as GameObject;
        _obj.name = "effect_cartouche";
        BulletCartoucheEffect _cartouche = _obj.GetComponent<BulletCartoucheEffect>();
        ListEffectCartouche.Add(_cartouche);
        return _cartouche;
    }

    private BulletEffectHandle onCreateEffectExplosion()
    {
        GameObject _obj = Instantiate(GamePlayResources.Instance.objEffectExplosion, Vector3.zero, Quaternion.identity, this.transform) as GameObject;
        _obj.name = "effect_explosiond";
        BulletEffectHandle _explosion = _obj.GetComponent<BulletEffectHandle>();
        ListEffectExposion.Add(_explosion);
        return _explosion;
    }

    private MonsterDeadEffect onCreateEffectDead()
    {
        GameObject _obj = Instantiate(GamePlayResources.Instance.objEffectDead, Vector3.zero, Quaternion.identity, this.transform) as GameObject;
        _obj.name = "effect_dead";
        MonsterDeadEffect _dead = _obj.GetComponent<MonsterDeadEffect>();
        ListEffectDead.Add(_dead);
        return _dead;
    }

    private MonsterSalivaEffect onCreateEffectSaliva(Vector3 startPos)
    {
        GameObject obj = Instantiate(GamePlayResources.Instance.objEffectSaliva, startPos, Quaternion.identity, this.transform) as GameObject;
        obj.name = "bullet_saliva";
        MonsterSalivaEffect _saliva = obj.GetComponent<MonsterSalivaEffect>();
        ListEffectSaliva.Add(_saliva);
        return _saliva;
    }

    private MonsterBloodEffect onCreateEffectBlood()
    {
        GameObject _obj = Instantiate(GamePlayResources.Instance.objEffectBlood, Vector3.zero, Quaternion.identity, this.transform) as GameObject;
        _obj.name = "effect_blood";
        MonsterBloodEffect _blood = _obj.GetComponent<MonsterBloodEffect>();
        ListEffectBlood.Add(_blood);
        return _blood;
    }

    private void onGameOver()
    {
        onDestroyObjects();
    }

    private void onGameWin()
    {
        onDestroyObjects();
    }

    private void onDestroyObjects()
    {
        ListEffectBlood = new List<MonsterBloodEffect>();
        ListEffectCartouche = new List<BulletCartoucheEffect>();
        ListEffectDead = new List<MonsterDeadEffect>();
        ListEffectExposion = new List<BulletEffectHandle>();
        ListEffectSaliva = new List<MonsterSalivaEffect>();


        if (transform.childCount > 0)
        {
            foreach (Transform item in this.transform)
            {
                Destroy(item.gameObject);
            }
        }
    }

    #endregion

}
