﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class LightningBoltHandle : MonoBehaviour
{
    private Vector3 StartPosition;
    private Vector3 EndPosition;
    private bool isStopping = true;

    private Renderer renderLine;

    private System.Random RandomGenerator = new System.Random();
    private int Generations = 6;
    private float Duration = 0.05f;
    private float timer;
    private float ChaosFactor = 0.15f;
    private LineRenderer lineRenderer;
    private List<KeyValuePair<Vector3, Vector3>> segments = new List<KeyValuePair<Vector3, Vector3>>();
    private int startIndex;
    private Vector2 size;
    private Vector2[] offsets;
    private int animationOffsetIndex;
    private bool orthographic;


    #region Behaviour

    private void OnDisable()
    {
        isStopping = true;
    }

    private void Awake()
    {
        orthographic = (Camera.main != null && Camera.main.orthographic);
        lineRenderer = GetComponent<LineRenderer>();
        renderLine = GetComponent<Renderer>();
        renderLine.sortingLayerName = "Player";
        renderLine.sortingOrder = 0;
        lineRenderer.numPositions = 0;
    }

    private void Update()
    {
        orthographic = (Camera.main != null && Camera.main.orthographic);
        if (timer <= 0.0f)
        {
            if (isStopping)
            {
                timer = Duration;
                lineRenderer.numPositions = 0;
            }
            else
            {
                onTrigger();
            }
        }
        timer -= Time.deltaTime;
    }

    #endregion

    #region Publish Methods

    public void OnSetData(Vector3 startPos, Vector3 endPos, int orderInLayer)
    {
        this.gameObject.SetActive(true);
        StartPosition = startPos;
        EndPosition = endPos;
        renderLine.sortingOrder = orderInLayer;
    }

    public void OnPlay()
    {
        isStopping = false;
        StartCoroutine(onDisableObject(0.2f));
    }

    #endregion

    #region Private Methods

    private IEnumerator onDisableObject(float second)
    {
        yield return new WaitForSeconds(second);
        this.gameObject.SetActive(false);
    }


    private void onTrigger()
    {
        Vector3 start, end;
        timer = Duration + Mathf.Min(0.0f, timer);
        start = StartPosition;
        end = EndPosition;
        startIndex = 0;
        onGenerateLightningBolt(start, end, Generations, Generations, 0.0f);
        onUpdateLineRenderer();
    }


    private void onGetPerpendicularVector(ref Vector3 directionNormalized, out Vector3 side)
    {
        if (directionNormalized == Vector3.zero)
        {
            side = Vector3.right;
        }
        else
        {
            // use cross product to find any perpendicular vector around directionNormalized:
            // 0 = x * px + y * py + z * pz
            // => pz = -(x * px + y * py) / z
            // for computational stability use the component farthest from 0 to divide by
            float x = directionNormalized.x;
            float y = directionNormalized.y;
            float z = directionNormalized.z;
            float px, py, pz;
            float ax = Mathf.Abs(x), ay = Mathf.Abs(y), az = Mathf.Abs(z);
            if (ax >= ay && ay >= az)
            {
                // x is the max, so we can pick (py, pz) arbitrarily at (1, 1):
                py = 1.0f;
                pz = 1.0f;
                px = -(y * py + z * pz) / x;
            }
            else if (ay >= az)
            {
                // y is the max, so we can pick (px, pz) arbitrarily at (1, 1):
                px = 1.0f;
                pz = 1.0f;
                py = -(x * px + z * pz) / y;
            }
            else
            {
                // z is the max, so we can pick (px, py) arbitrarily at (1, 1):
                px = 1.0f;
                py = 1.0f;
                pz = -(x * px + y * py) / z;
            }
            side = new Vector3(px, py, pz).normalized;
        }
    }

    private void onGenerateLightningBolt(Vector3 start, Vector3 end, int _generation, int _totalGenerations, float _offsetAmount)
    {
        if (_generation < 0 || _generation > 8)
        {
            return;
        }
        else if (orthographic)
        {
            start.z = end.z = Mathf.Min(start.z, end.z);
        }

        segments.Add(new KeyValuePair<Vector3, Vector3>(start, end));
        if (_generation == 0)
        {
            return;
        }

        Vector3 randomVector;
        if (_offsetAmount <= 0.0f)
        {
            _offsetAmount = (end - start).magnitude * ChaosFactor;
        }

        while (_generation-- > 0)
        {
            int previousStartIndex = startIndex;
            startIndex = segments.Count;
            for (int i = previousStartIndex; i < startIndex; i++)
            {
                start = segments[i].Key;
                end = segments[i].Value;

                // determine a new direction for the split
                Vector3 midPoint = (start + end) * 0.5f;

                // adjust the mid point to be the new location
                onRandomVector(ref start, ref end, _offsetAmount, out randomVector);
                midPoint += randomVector;

                // add two new segments
                segments.Add(new KeyValuePair<Vector3, Vector3>(start, midPoint));
                segments.Add(new KeyValuePair<Vector3, Vector3>(midPoint, end));
            }

            // halve the distance the lightning can deviate for each generation down
            _offsetAmount *= 0.5f;
        }
    }

    private void onRandomVector(ref Vector3 start, ref Vector3 end, float offsetAmount, out Vector3 result)
    {
        if (orthographic)
        {
            Vector3 directionNormalized = (end - start).normalized;
            Vector3 side = new Vector3(-directionNormalized.y, directionNormalized.x, directionNormalized.z);
            float distance = ((float)RandomGenerator.NextDouble() * offsetAmount * 2.0f) - offsetAmount;
            result = side * distance;
        }
        else
        {
            Vector3 directionNormalized = (end - start).normalized;
            Vector3 side;
            onGetPerpendicularVector(ref directionNormalized, out side);

            // generate random distance
            float distance = (((float)RandomGenerator.NextDouble() + 0.1f) * offsetAmount);

            // get random rotation angle to rotate around the current direction
            float rotationAngle = ((float)RandomGenerator.NextDouble() * 360.0f);

            // rotate around the direction and then offset by the perpendicular vector
            result = Quaternion.AngleAxis(rotationAngle, directionNormalized) * side * distance;
        }
    }

    private void onUpdateLineRenderer()
    {
        int segmentCount = (segments.Count - startIndex) + 1;
        lineRenderer.numPositions = segmentCount;

        if (segmentCount < 1)
        {
            return;
        }

        int index = 0;
        lineRenderer.SetPosition(index++, segments[startIndex].Key);

        for (int i = startIndex; i < segments.Count; i++)
        {
            lineRenderer.SetPosition(index++, segments[i].Value);
        }

        segments.Clear();
    }

    #endregion
}
