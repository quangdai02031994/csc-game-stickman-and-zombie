﻿
using System;
using GameSupport;
using System.Collections.Generic;

[Serializable]
public class ModelLevel
{
    /// <summary>
    /// ID của level
    /// </summary>
    public int id;

    /// <summary>
    /// Tên map chứa level
    /// </summary>
    public MAP_TYPES name;

    /// <summary>
    /// Xếp hạng khi qua level,0-3
    /// </summary>
    public int rank;

    /// <summary>
    /// Loại nhiệm vụ
    /// </summary>
    public LEVEL_TYPE type;

    /// <summary>
    /// Thời gian vượt qua level.
    /// Với nhiệm vụ sống sót trong khoảng thời gian
    /// </summary>
    public float timePass;

    /// <summary>
    /// Số lượng zombie trong level
    /// </summary>
    public int countMonster;

    /// <summary>
    /// Loại zombie cần vượt qua
    /// </summary>
    public int idMonster;

    /// <summary>
    /// Danh sách các zombie
    /// </summary>
    public List<ModelMonster> ListMonster;

    public ModelLevel()
    {
        name = MAP_TYPES.NONE;
        rank = 0;
        ListMonster = new List<ModelMonster>();
    }

}