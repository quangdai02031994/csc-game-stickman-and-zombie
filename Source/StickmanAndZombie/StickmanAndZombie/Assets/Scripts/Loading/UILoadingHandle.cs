﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using GameSupport;
using DG.Tweening;

public class UILoadingHandle : MonoBehaviour
{
    public static UILoadingHandle Instance{ get; private set; }


    public Button btnTouchToStart;

    public Image imgTouchToStart;


    #region Behaviour

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }

    }

    void Start()
    {
        imgTouchToStart.gameObject.SetActive(false);
        btnTouchToStart.onClick.AddListener(onClickButtonStart);
        btnTouchToStart.gameObject.SetActive(false);
        SceneLoadingHandle.Instance.OnLoadingSceneAsync(ScenesName.Home, () =>
            {
                onEnableButton();
            });
    }


    #endregion

    private void onEnableButton()
    {
        imgTouchToStart.gameObject.SetActive(true);
        btnTouchToStart.gameObject.SetActive(true);
    }

    public void onClickButtonStart()
    {
        Debug.Log("onClickButtonStart");
        SceneLoadingHandle.Instance.OnSwitchScene();
    }



}
