﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSupport;

public class MonsterBulletHandler : BaseBulletHandler
{
    private float sizeBulletStone = 0.15f;

    private float sizeBulletSpear = 0.3f;

    private float speedRotation = 0;

    private bool isAttackRight = false;

    private RaycastHit2D hitObject;

    #region Behaviour

    protected override void Awake()
    {
        base.Awake();
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        speedRotation = 0;
    }

    void Update()
    {
        if (transform.position.x < CameraHandle.Instance.minCameraX || transform.position.x > CameraHandle.Instance.maxCameraX)
        {
            this.gameObject.SetActive(false);
            return;
        }

        switch (type)
        {
            case BULLET_TYPE.SPEAR:
                if (isAttackRight)
                {
                    speedRotation -= Time.deltaTime * 2000;
                }
                else
                {
                    speedRotation += Time.deltaTime * 2000;
                }
                transform.localEulerAngles = new Vector3(0, 0, speedRotation);
                RaycastHit2D hitSpear = Physics2D.CircleCast(transform.position, sizeBulletSpear, Vector2.right, sizeBulletStone, layerCast);
                if (hitSpear.collider != null)
                {
                    PlayerController.Instance.OnHit(modelBullet.damage);
                    this.gameObject.SetActive(false);
                }
                break;
            case BULLET_TYPE.STONE:
                RaycastHit2D hitStone = Physics2D.CircleCast(transform.position, sizeBulletStone, Vector2.right, sizeBulletStone, layerCast);
                if (hitStone.collider != null)
                {
                    PlayerController.Instance.OnHit(modelBullet.damage);
                    this.gameObject.SetActive(false);
                    return;
                }
                break;
        }

    }


    #if UNITY_EDITOR
    
    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        switch (type)
        {
            case BULLET_TYPE.STONE:
                Gizmos.DrawWireSphere(transform.position, sizeBulletStone);
                break;
            case BULLET_TYPE.SPEAR:
                Gizmos.DrawWireSphere(transform.position, sizeBulletSpear);
                break;
        }
    }
    
    #endif

    #endregion

    public override void OnSetBullet(Vector3 startPos, ModelBullet model, BULLET_TYPE type, float eulerAngles = 0)
    {
        base.OnSetBullet(startPos, model, type, eulerAngles);
        transform.position = startPos;
        if (heightPositonY > 0)
        {
            onReLayerBullet(startPos);
        }
    }

    public override void ShootingBullet(bool isRight)
    {
        base.ShootingBullet(isRight);
        switch (type)
        {
            case BULLET_TYPE.SPEAR:
                if (isRight)
                    spriteRenderer.flipX = false;
                else
                    spriteRenderer.flipX = true;
                break;
        }
        if (isRight)
        {
            rig.velocity = Vector2.right * modelBullet.speed;
        }
        else
        {
            rig.velocity = Vector2.left * modelBullet.speed;
        }
        isAttackRight = isRight;
    }
}
