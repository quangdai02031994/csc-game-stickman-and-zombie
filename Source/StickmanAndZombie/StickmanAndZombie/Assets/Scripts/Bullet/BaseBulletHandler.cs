﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSupport;

[RequireComponent(typeof(Rigidbody2D))]
public class BaseBulletHandler : MonoBehaviour
{

    public BULLET_TYPE type;
    public ModelBullet modelBullet;
    public LayerMask layerCast;

    protected const int MAX_RENDER = 1000;

    protected float maxVertical;
    protected float minVertical;
    protected float heightPositonY;

    protected Vector3 startPos;
    protected SpriteRenderer spriteRenderer;
    protected Rigidbody2D rig;


    protected float eulerAngles = 0;

    protected virtual void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        rig = GetComponent<Rigidbody2D>();
        maxVertical = CameraHandle.Instance.maxY - 2.8f;
        minVertical = CameraHandle.Instance.minY + 2.5f;
        heightPositonY = Mathf.Abs(minVertical - maxVertical);
        rig.isKinematic = true;
    }

    protected virtual void OnDisable()
    {
        this.transform.localEulerAngles = Vector3.zero;
        eulerAngles = 0;
    }

    public virtual void OnSetBullet(Vector3 startPos, ModelBullet model, BULLET_TYPE type, float eulerAngles = 0)
    {
        this.startPos = startPos;
        this.eulerAngles = eulerAngles;
        transform.position = startPos;
        this.modelBullet = model;
        this.type = type;

    }

    public virtual void ShootingBullet(bool isRight)
    {
        transform.position = this.startPos;
        this.gameObject.SetActive(true);
    }

    protected void onReLayerBullet(Vector3 startPos)
    {
        float delta = Mathf.Abs(startPos.y - minVertical) / heightPositonY;
        int layerOrder = MAX_RENDER - (int)(delta * 900);
        spriteRenderer.sortingOrder = layerOrder;
    }

}
