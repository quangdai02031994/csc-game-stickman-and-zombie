﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSupport;

public class BulletHandle : BaseBulletHandler
{
    private float bulletRadiusBazoka = 1.5f;
    private float bulletRadius = 0.15f;
    private Vector2 sizeArrow = new Vector2(0.75f, 0.05f);
    private Vector2 sizeBazokaGun = new Vector2(0.75f, 0.25f);

    private Vector2 sizeNuclearGun = new Vector2(0.75f, 0.4f);

    private float countHits;
    private Transform hitMonster;

    private SpriteRenderer trailBulletRender;

    #region Behaviour

    protected override void Awake()
    {
        base.Awake();
        trailBulletRender = transform.GetChild(0).GetComponent<SpriteRenderer>();
        trailBulletRender.gameObject.SetActive(false);
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        hitMonster = null;
        countHits = 0;
        trailBulletRender.gameObject.SetActive(false);
    }

    #if UNITY_EDITOR

    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        switch (type)
        {
            case BULLET_TYPE.GUN:
                Gizmos.DrawWireSphere(transform.position, bulletRadius);
                break;
            case BULLET_TYPE.AWM_GUN:
                Gizmos.DrawWireSphere(transform.position, bulletRadius);
                break;
            case BULLET_TYPE.BAZOKA_GUN:
                Gizmos.DrawWireCube(transform.position, sizeBazokaGun);
                Gizmos.DrawWireSphere(transform.position, bulletRadiusBazoka);
                break;
            case BULLET_TYPE.BOW:
                Gizmos.DrawWireCube(transform.position, sizeArrow);
                break;
        }   
    }

    #endif

    void Update()
    {
        if (GameManager.Instance.GameState == GAME_STATE.PLAYING)
        {
            onCastMonster(type);
            if (transform.position.x > CameraHandle.Instance.maxCameraX || transform.position.x < CameraHandle.Instance.minCameraX)
            {
                this.gameObject.SetActive(false);
            }
        }
       
    }

    #endregion

    #region Publish Methods

    public override void OnSetBullet(Vector3 startPos, ModelBullet model, BULLET_TYPE type, float eulerAngles = 0)
    {
        base.OnSetBullet(startPos, model, type, eulerAngles);
        onLoadTexture(type);
        this.name = "bullet_player_" + type;
        if (heightPositonY > 0)
        {
            onReLayerBullet(startPos);
        }
    }


    public override void ShootingBullet(bool isRight)
    {
        base.ShootingBullet(isRight);
        if (isRight)
        {
            spriteRenderer.flipX = false;
            rig.velocity = new Vector2(1, Mathf.Sin(eulerAngles * Mathf.PI / 180)) * modelBullet.speed;
        }
        else
        {
            spriteRenderer.flipX = true;
            rig.velocity = new Vector2(-1, Mathf.Sin(eulerAngles * Mathf.PI / 180)) * modelBullet.speed;
        }
        StartCoroutine(onCheckEnableTrail());
    }

    #endregion


    #region Private Methods

    private IEnumerator onCheckEnableTrail()
    {
        yield return new WaitForEndOfFrame();
        switch (type)
        {
            case BULLET_TYPE.GUN:
                trailBulletRender.gameObject.SetActive(true);
                break;
            case BULLET_TYPE.BAZOKA_GUN:
                trailBulletRender.gameObject.SetActive(true);
                break;
            case BULLET_TYPE.AWM_GUN:
                trailBulletRender.gameObject.SetActive(true);
                break;
            default:
                trailBulletRender.gameObject.SetActive(false);
                break;
        }
        trailBulletRender.flipX = spriteRenderer.flipX;
    }

    private void onCastMonster(BULLET_TYPE type)
    {
        switch (type)
        {
            case BULLET_TYPE.GUN:
                onCastMonsterGun();
                break;
            case BULLET_TYPE.AWM_GUN:
                onCastMonsterAwmGun();
                break;
            case BULLET_TYPE.BOW:
                onCastMonsterBow();
                break;
            case BULLET_TYPE.BAZOKA_GUN:
                onCastMonsterBazoka();
                break;
        }
    }


    private void onCastMonsterAwmGun()
    {
        RaycastHit2D _hitAwmGun = Physics2D.CircleCast(transform.position, bulletRadius, Vector2.right, bulletRadius, layerCast);
        if (_hitAwmGun.collider != null)
        {
            if (_hitAwmGun.transform != hitMonster)
            {
                MonsterHandle _hand = _hitAwmGun.transform.GetComponent<MonsterHandle>();
                if (_hand.monsterState != MONSTER_STATE.DEAD && _hand.monsterState != MONSTER_STATE.DEAD_FIRE && _hand.monsterState != MONSTER_STATE.DEAD_THUNDER
                    && _hand.monsterState != MONSTER_STATE.APPEAR_LEFT && _hand.monsterState != MONSTER_STATE.APPEAR_RIGHT)
                {
                    if (transform.position.x > startPos.x)
                    {
                        _hand.OnHit(modelBullet.damage, true);
                    }
                    else
                    {
                        _hand.OnHit(modelBullet.damage, false);
                    }
                    countHits++;
                    if (countHits >= modelBullet.countHit)
                    {
                        this.gameObject.SetActive(false);
                    }
                    hitMonster = _hitAwmGun.transform;
                    EffectManager.Instance.OnExplosion(type, transform.position, spriteRenderer.sortingOrder);
                }
            }
        }
    }

    private void onCastMonsterGun()
    {
        RaycastHit2D _hitGun = Physics2D.CircleCast(transform.position, bulletRadius, Vector2.right, bulletRadius, layerCast);
        if (_hitGun.collider != null
            && _hitGun.transform.position.x < CameraHandle.Instance.maxCameraX
            && _hitGun.transform.position.x > CameraHandle.Instance.minCameraX)
        {
            MonsterHandle _hand = _hitGun.transform.GetComponent<MonsterHandle>();
            if (_hand.monsterState != MONSTER_STATE.DEAD && _hand.monsterState != MONSTER_STATE.DEAD_FIRE && _hand.monsterState != MONSTER_STATE.DEAD_THUNDER
                && _hand.monsterState != MONSTER_STATE.APPEAR_LEFT && _hand.monsterState != MONSTER_STATE.APPEAR_RIGHT)
            {
                if (transform.position.x > startPos.x)
                {
                    _hand.OnHit(modelBullet.damage, true);
                }
                else
                {
                    _hand.OnHit(modelBullet.damage, false);
                }

                EffectManager.Instance.OnExplosion(type, transform.position, spriteRenderer.sortingOrder);
                this.gameObject.SetActive(false);
            }
        }
    }


    private void onCastMonsterBazoka()
    {
        RaycastHit2D _bazokaHit = Physics2D.BoxCast(transform.position, sizeBazokaGun, 0, Vector2.right, 0, layerCast);
        if (_bazokaHit.collider != null)
        {
            RaycastHit2D[] _hits = Physics2D.CircleCastAll(transform.position, bulletRadiusBazoka, Vector2.zero, bulletRadiusBazoka, layerCast);
            if (_hits.Length > 0)
            {
                for (int i = 0; i < _hits.Length; i++)
                {
                    if (_hits[i].collider != null
                        && _hits[i].transform.position.x < CameraHandle.Instance.maxCameraX
                        && _hits[i].transform.position.x > CameraHandle.Instance.minCameraX)
                    {
                        MonsterHandle _hand = _hits[i].transform.GetComponent<MonsterHandle>();
                        if (_hand.monsterState != MONSTER_STATE.DEAD && _hand.monsterState != MONSTER_STATE.DEAD_FIRE && _hand.monsterState != MONSTER_STATE.DEAD_THUNDER
                            && _hand.monsterState != MONSTER_STATE.APPEAR_LEFT && _hand.monsterState != MONSTER_STATE.APPEAR_RIGHT)
                        {
                            if (transform.position.x > startPos.x)
                            {
                                _hand.OnHit(modelBullet.damage, true);
                            }
                            else
                            {
                                _hand.OnHit(modelBullet.damage, false);
                            }
                            EffectManager.Instance.OnExplosion(type, transform.position, spriteRenderer.sortingOrder);
                        }
                    }
                }
                this.gameObject.SetActive(false);
            }

        }
    }

    private void onCastMonsterBow()
    {
        RaycastHit2D _hitBow = Physics2D.BoxCast(transform.position, sizeArrow, 0, Vector2.right, 0, layerCast);
        if (_hitBow.collider != null
            && _hitBow.transform.position.x < CameraHandle.Instance.maxCameraX
            && _hitBow.transform.position.x > CameraHandle.Instance.minCameraX)
        {
            MonsterHandle _hand = _hitBow.transform.GetComponent<MonsterHandle>();
            if (_hand.monsterState != MONSTER_STATE.DEAD && _hand.monsterState != MONSTER_STATE.DEAD_FIRE && _hand.monsterState != MONSTER_STATE.DEAD_THUNDER
                && _hand.monsterState != MONSTER_STATE.APPEAR_LEFT && _hand.monsterState != MONSTER_STATE.APPEAR_RIGHT)
            {
                if (transform.position.x > startPos.x)
                {
                    int _damage = (int)(modelBullet.damage
                                  + PlayerController.Instance.modelPlayer.strong
                                  + PlayerController.Instance.modelPlayer.deltaStrong);

                    _hand.OnHit(_damage, true);
                }
                else
                {
                    int _damage = (int)(modelBullet.damage
                                  + PlayerController.Instance.modelPlayer.strong
                                  + PlayerController.Instance.modelPlayer.deltaStrong);

                    _hand.OnHit(_damage, false);
                }
                EffectManager.Instance.OnExplosion(type, transform.position, spriteRenderer.sortingOrder);
                this.gameObject.SetActive(false);
            }
        }
    }

    private void onLoadTexture(BULLET_TYPE type)
    {
        spriteRenderer.enabled = true;
        switch (type)
        {
            case BULLET_TYPE.BOW:
                spriteRenderer.sprite = GamePlayResources.Instance.spriteBulletBow;
                break;
            case BULLET_TYPE.BAZOKA_GUN:
                spriteRenderer.sprite = GamePlayResources.Instance.spriteBulletBazoka;
                break;
            default:
                spriteRenderer.sprite = GamePlayResources.Instance.spriteBulletGun;
                break;
        }
    }

    #endregion
}
