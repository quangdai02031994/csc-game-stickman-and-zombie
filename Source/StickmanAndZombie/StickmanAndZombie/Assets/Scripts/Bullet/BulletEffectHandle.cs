﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;
using Spine;
using GameSupport;

public class BulletEffectHandle : MonoBehaviour
{
    [SpineAnimation]
    public string animationNameBullet;

    [SpineAnimation]
    public string animationNameBazoka;

    private SkeletonAnimation skeletonAnimation;
    private MeshRenderer meshRenderer;
    private string startAnimationName;

    #region Behaviour

    void Awake()
    {
        skeletonAnimation = GetComponent<SkeletonAnimation>();
        meshRenderer = GetComponent<MeshRenderer>();
    }

    void Start()
    {
        skeletonAnimation.state.Complete += onCompleteEvent;
        startAnimationName = skeletonAnimation.AnimationName;
        skeletonAnimation.clearStateOnDisable = true;
    }

    void OnDisable()
    {
        skeletonAnimation.AnimationName = startAnimationName;
    }

    void OnDestroy()
    {
        skeletonAnimation.state.Complete -= onCompleteEvent;
    }

    #endregion

    #region Publish Methods

    public void OnSetData(Vector3 startPos, int orderInLayer)
    {
        transform.position = startPos;
        meshRenderer.sortingOrder = orderInLayer + 1;
        this.gameObject.SetActive(true);
    }

    public void OnPlay(BULLET_TYPE type)
    {
        switch (type)
        {
            case BULLET_TYPE.BOW:
                skeletonAnimation.state.SetAnimation(0, animationNameBullet, false);
                break;
            case BULLET_TYPE.GUN:
                skeletonAnimation.state.SetAnimation(0, animationNameBullet, false);
                break;
            case BULLET_TYPE.AWM_GUN:
                skeletonAnimation.state.SetAnimation(0, animationNameBullet, false);
                break;
            case BULLET_TYPE.BAZOKA_GUN:
                skeletonAnimation.state.SetAnimation(0, animationNameBazoka, false);
                break;
//            case BULLET_TYPE.NUCLEAR_GUN:
//                skeletonAnimation.state.SetAnimation(0, animationNameBazoka, false);
                break;
            default:
                Debug.LogError("Arm bullet not found");
                this.gameObject.SetActive(false);
                break;
        }
    }

    #endregion

    #region Private Methods

    private void onCompleteEvent(TrackEntry trackEntry)
    {
        if (trackEntry.Animation.Name == "")
        {
            Debug.LogError("Animation name Null");
        }
        this.gameObject.SetActive(false);
    }

    #endregion


}
