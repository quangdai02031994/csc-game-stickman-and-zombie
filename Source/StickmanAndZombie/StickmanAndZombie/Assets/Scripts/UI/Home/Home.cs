﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using GameSupport;
using System;

public class Home :MonoBehaviour
{

    private ModelSword modelSword;

    public static float OnCalculateFillAmount(float value, float max)
    {
        return value / (float)max;
    }

    public static bool CheckEquipStatus()
    {
        int[] _idEquipped = GameManager.Instance.OnGetPlayerArmEquiped();
        if (_idEquipped[1] < 0 || _idEquipped[2] < 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }


    public static int OnGetMaxDamage()
    {
        return 380;
    }

    public static int OnGetMaxAmmo()
    {
        return 570;
    }

    public static string IntFormatThousand(float numToFormat)
    {
        return numToFormat.ToString("#,##0");
    }

    public static string  FloatFormatThousand(float numToFormat)
    {
        return numToFormat.ToString("#,##0");
    }

    public static string FloatFormatThousandPoint(float numToFormat)
    {
        return numToFormat.ToString("F1");
    }
}
