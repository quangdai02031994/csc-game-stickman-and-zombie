﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using GameSupport;

public class HomeUIHandle:MonoBehaviour
{

    public static HomeUIHandle Instance{ get; private set; }

    public PlayerUIAnimation playerUI;
    public Button btnPlayerLight;

    [Header("Rect Transform")]

    public RectTransform[] background;

    public RectTransform[] uiTransform;

    public RectTransform[] buttonArray;

    public RectTransform statusPanel;
	
    public RectTransform itemPanel;
	
    public Text goldText;

    [Header("Image")]
    public Image circleIcon, lineIcon, roomStickman;

    public Image playerLight, playerLight1;

    public Image shadowFocus, playerBox;

    public Image playLight;

    public Image upgradeBar;

    [Header("Button")]

    public Button backButton, playButton;

    public UpgradeStickmanHandle upgradeStickmanHandle;
    public UpgradeWeaponHandle upgradeWeaponHandle;
    public Sprite onDisableBGSprite, onDisableUpgradeSprite;

    public PLAYER_ARMS playerArmEquip = PLAYER_ARMS.SWORD;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }

        Input.multiTouchEnabled = false;
    }

    void Start()
    {
        StartScene();
        btnPlayerLight.onClick.AddListener(onClickButtonPlayerAnimation);
    }

    void StartScene()
    {
        OnRefreshGold();
        upgradeWeaponHandle.InitAll();
        upgradeStickmanHandle.InitAll();
        StartCoroutine(OnShowHome());
        StartCoroutine(OnShowUI());
    }

    IEnumerator Init()
    {
        yield return new WaitForSeconds(1f);
        circleIcon.rectTransform.DORotate(Vector3.zero, 0.5f);
        roomStickman.DOFade(255f, 3f);
        shadowFocus.gameObject.SetActive(true);
        roomStickman.gameObject.SetActive(true);
        OnRefreshGold();
    }

    /// <summary>
    /// Hieu ung background.
    /// </summary>
    IEnumerator OnShowHome()
    {
        background[0].DOAnchorPos(Vector3.zero, 0.2f);
        background[1].DOAnchorPos(new Vector3(-110f, 0f, 0f), 0.2f);
        background[2].DOAnchorPos(Vector3.zero, 0.2f);
        background[3].DOAnchorPos(new Vector3(0f, 110f, 0f), 0.2f);
        background[4].DOAnchorPos(Vector3.zero, 0.2f);
        roomStickman.gameObject.SetActive(true);
        roomStickman.DOFade(255, 0.5f);
        circleIcon.rectTransform.DORotate(Vector3.zero, 1f);
        yield return new WaitForSeconds(0.5f);
        OnShowPlayerLight();
    }

    /// <summary>
    /// Hieu ung playerlight
    /// </summary>
    void OnShowPlayerLight()
    {
        roomStickman.gameObject.SetActive(true);
        playerLight.gameObject.SetActive(true);
        playerLight1.gameObject.SetActive(true);
        playerLight.rectTransform.DOSizeDelta(new Vector2(538, 840), 0.3f);
        playerBox.DOFade(225f, 2f);
        playerUI.OnShow();
    }

    /// <summary>
    /// Light effect nut Play
    /// </summary>
    IEnumerator OnPlayButtonLightEffect()
    {
        Vector3 lightRectTransform;
        yield return new WaitForSeconds(1f);
        lightRectTransform = playLight.rectTransform.anchoredPosition;
        if (lightRectTransform.x < 0f)
        {
            playLight.gameObject.SetActive(true);
            playLight.rectTransform.DOAnchorPosX(280f, 1.5f);
        }
        else
        {
            playLight.gameObject.SetActive(false);
            playLight.rectTransform.anchoredPosition = new Vector2(-280f, 0f);
        }
    }

    IEnumerator OnShowUI()
    {
        shadowFocus.gameObject.SetActive(true);
        shadowFocus.DOFade(255f, 5f);
        uiTransform[0].DOAnchorPosY(0f, 0.5f);
        uiTransform[1].DOAnchorPos(new Vector3(0f, -13f, 0f), 0.5f);
        uiTransform[2].DOAnchorPos(new Vector3(19f, 4f, 0f), 0.5f);
        uiTransform[3].DOAnchorPosX(0f, 0.5f);
        uiTransform[4].DOAnchorPos(new Vector3(-52f, 7f, 0f), 0.5f);
        uiTransform[5].DOAnchorPos(new Vector3(-45f, 22f, 0f), 0.5f);
        yield return new WaitForSeconds(1.0f);
        uiTransform[5].DOAnchorPos(new Vector3(-32f, 22f, 0f), 0.5f);
    }

    public void OnPlayAnimationUseNewWeapon(PLAYER_ARMS current, PLAYER_ARMS next)
    {
        if (current == next)
            return;
        playerUI.OnUseNewArm(current, next);
    }

    public void OnClickEquipSword()
    {
        OnPlayAnimationUseNewWeapon(playerArmEquip, PLAYER_ARMS.SWORD);
        playerArmEquip = PLAYER_ARMS.SWORD;
    }

    public void OnClickEquipWeapon1()
    {
        int[] ID = GameManager.Instance.OnGetPlayerArmEquiped();
        if (ID[1] > 0)
        {
            PLAYER_ARMS arm = (PLAYER_ARMS)ID[1];
            OnPlayAnimationUseNewWeapon(playerArmEquip, arm);
            playerArmEquip = arm;
        }
    }

    public void OnClickEquipWeapon2()
    {
        int[] ID = GameManager.Instance.OnGetPlayerArmEquiped();
        if (ID[2] > 0)
        {
            PLAYER_ARMS arm = (PLAYER_ARMS)ID[2];
            OnPlayAnimationUseNewWeapon(playerArmEquip, arm);
            playerArmEquip = arm;
        }
    }

    public void OnRefreshGold()
    {
        goldText.text = Home.IntFormatThousand(GameManager.Instance.OnGetGold());
    }

    public void OnClickItemButton()
    {
        Vector3 originPos;
        GameObject currentlySelectButton;
        currentlySelectButton = EventSystem.current.currentSelectedGameObject;
        originPos = currentlySelectButton.GetComponent<RectTransform>().anchoredPosition;
        for (int i = 0; i < buttonArray.Length; i++)
        {
            buttonArray[i].DOAnchorPosX(-0.9f, 0.2f);
            buttonArray[i].gameObject.transform.GetChild(0).gameObject.SetActive(false);
        }
        if (originPos.x < 15f)
        {
            currentlySelectButton.GetComponent<RectTransform>().DOAnchorPosX(15f, 0.2f);
            currentlySelectButton.transform.GetChild(0).gameObject.gameObject.SetActive(true);
        }
        else
        {
            currentlySelectButton.GetComponent<RectTransform>().DOAnchorPosX(-0.9f, 0.2f);
            currentlySelectButton.transform.GetChild(0).gameObject.SetActive(false);
        }
    }

    public void OnCircleIconEffect(float angle, float duration)
    {
        circleIcon.rectTransform.DORotate(new Vector3(0f, 0f, angle), duration);
    }

    public void OnClickGunButton()
    {
        playerUI.OnUseNewArm(playerArmEquip, PLAYER_ARMS.SWORD);
        upgradeWeaponHandle.OnShow();
    }

    public void OnHide()
    {
        OnCircleIconEffect(180f, 0.5f);
        //di chuyen status panel ra man hinh va hide.
        uiTransform[4].DOAnchorPos(new Vector3(673f, 7f, 0f), 1f).OnComplete(() => uiTransform[4].gameObject.SetActive(false));
        //di chuyen button panel ra man hinh va hide.
        uiTransform[0].DOAnchorPosY(-476f, 1f).OnComplete(() => uiTransform[0].gameObject.SetActive(false));
        playerLight1.DOFade(0f, 0.5f);
        roomStickman.rectTransform.DOAnchorPosX(-400f, 0.5f);
        backButton.gameObject.SetActive(true);

    }

    /// <summary>
    /// Reset Item Panel
    /// </summary>
    public void ResetItemButton()
    {
        for (int i = 0; i < buttonArray.Length; i++)
        {
            buttonArray[i].GetComponent<RectTransform>().DOAnchorPosX(-0.9f, 0.5f);
            buttonArray[i].transform.GetChild(0).gameObject.SetActive(false);
        }
    }

    /// <summary>
    /// Khi click vao nut stickman
    /// </summary>
    public void OnClickStickmanButton()
    {
        upgradeStickmanHandle.OnShow();
    }

    /// <summary>
    /// Khi click vao nut back
    /// </summary>
    public void OnClickBackButton()
    {
        if (upgradeStickmanHandle.isOpening)
        {
            upgradeStickmanHandle.OnHide();
        }
        else
        {
            upgradeWeaponHandle.OnHide();
        }
    }

    public void OnPlayAnimation(PLAYER_ARMS arm)
    {
        playerUI.OnPlayAnimation(arm);
    }

    public void OnRefreshAll()
    {
        OnRefreshGold();
        upgradeStickmanHandle.InitAll();
    }

    public void OnPointDown(Button button)
    {
        if (button.enabled == true)
        {
            button.transform.localScale = new Vector3(0.9f, 0.9f, 0.9f);
        }
    }

    public void OnPointUp(Button button)
    {
        if (button.enabled == true)
        {
            button.transform.localScale = Vector3.one;
        }
    }

    /// <summary>
    /// Khi click vao nut play
    /// </summary>
    public void OnClickPlayButton()
    {
        Debug.Log("OnClickPlayButton");
        GameManager.Instance.OnFadingSceneAsync(ScenesName.Map);
        playButton.enabled = false;
    }

    private void onClickButtonPlayerAnimation()
    {
        Debug.Log("onClickButtonPlayerAnimation");
        if (!upgradeWeaponHandle.isOpening)
        {
            OnPlayAnimation(playerArmEquip);
        }
        else
        {
            OnPlayAnimation(upgradeWeaponHandle.playerArm);
        }
    }
}