﻿using System;
using UnityEngine;
using UnityEngine.UI;

[Serializable]
public class ModelWeapon
{
    /// <summary>
    /// Hiển thị chữ Requirement
    /// </summary>
    public Text requirementText;

    /// <summary>
    /// Hiển thị strong lv min
    /// </summary>
    public Text strongLVText;

    /// <summary>
    /// Hiển thị icon strong nâng cấp.
    /// </summary>
    public Image iconUpgradeStrong;

    /// <summary>
    /// Hiển thị số vàng mua item.
    /// </summary>
    public Text coinText;

    /// <summary>
    /// Hiển thịị icon vàng
    /// </summary>
    public Image coinIcon;

    /// <summary>
    /// Hiển thị icon đã mua
    /// </summary>
    public Image purchasedIcon;

    /// <summary>
    /// Hiển thị icon đã trang bị
    /// </summary>
    public Image equipIcon;

    public ModelWeapon()
    {

    }
}

