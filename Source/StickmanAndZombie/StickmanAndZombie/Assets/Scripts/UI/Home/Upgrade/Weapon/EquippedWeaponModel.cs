﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

[Serializable]
public class EquippedWeaponModel
{
    public Text weaponNameText;
    public Text damageUIText;
    public Text damageText;
    public Text ammoUI;
    public Text ammoText;
    public Text infoText;
    public Image weaponImage;
}
