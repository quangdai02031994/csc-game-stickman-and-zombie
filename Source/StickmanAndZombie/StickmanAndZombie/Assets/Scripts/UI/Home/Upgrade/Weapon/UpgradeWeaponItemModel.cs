﻿using System;
using UnityEngine;
using UnityEngine.UI;

[Serializable]
public class UpgradeWeaponItemModel
{
    public Text weaponNameText;
    public Text levelDamage;
    public Text damageText;
    public Text damagePlusText;
    public Text damageAfterUpdatedText;
    public Text coinToUpdateDamageText;
    public Button upgradeDamageButton;
    public Image upgradeDamageIcon;
    public Text ammoUIText;
    public Text levelAmmo;
    public Text ammoText;
    public Text ammoPlusText;
    public Text ammoAfterUpdatedText;
    public Text coinToUpdateAmmoText;
    public Button upgradeAmmoButton;
    public Image arrowIcon;
    public Image upgradeAmmoIcon;
    public Sprite upgradeDisableIcon;
    public Sprite upgradeEnableIcon;
    public Sprite upgradeEnableBG;
    public Sprite upgradeDisableBG;
    public Transform mask;
    public Transform damageTransform;
    public Transform ammoTransform;
    public Image damageSlideBar;
    public Image ammoSlideBar;
    public Image damageYellowBar;
    public Image ammoYellowBar;

    public Text txtMaxLvDamage;
    public Text txtMaxLvAmmo;

    public UpgradeWeaponItemModel()
    {
        
    }

    public void OnHideMaxDamage()
    {
        txtMaxLvDamage.gameObject.SetActive(false);
        damageTransform.gameObject.SetActive(true);
        damagePlusText.gameObject.SetActive(true);
    }

    public void OnShowMaxDamage()
    {
        txtMaxLvDamage.gameObject.SetActive(true);
        damageTransform.gameObject.SetActive(false);
        damagePlusText.gameObject.SetActive(false);
    }

    public void OnShowMaxAmmo()
    {
        txtMaxLvAmmo.gameObject.SetActive(true);
        ammoTransform.gameObject.SetActive(false);
        ammoPlusText.gameObject.SetActive(false);
    }

    public void OnHideMaxAmmo()
    {
        txtMaxLvAmmo.gameObject.SetActive(false);
        ammoTransform.gameObject.SetActive(true);
        ammoPlusText.gameObject.SetActive(true);
    }

}
