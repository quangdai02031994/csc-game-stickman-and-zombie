﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System;

public class UpgradeStickmanHandle : MonoBehaviour
{
    [Header("Rect Transform")]

    [SerializeField]
    private RectTransform upgradeInfoPanel;

    [SerializeField]
    private RectTransform statusPanel, itemPanel, buttonPanel;

    [SerializeField]
    private RectTransform borderImage;

    [Header("Transform")]

    [SerializeField]
    private Transform hpParentTransform, spdParentTransform, strParentTransform;

    [Header("Thông tin của stickman")]
    [SerializeField]
    private Text[] stickmanInfoText;

    [SerializeField]
    private Text[] playerLevelText;

    [Header("Button")]

    [SerializeField]
    /// <summary>
    /// Cac nut nang cap thong tin Stickman
    /// </summary>
    private Button[] upgradeStickManButton;
    /// <summary>
    /// nut tro ve
    /// </summary>

    [SerializeField]
    private Text[] txtMaxLvStickmanInfo = new Text[3];

    [SerializeField]
    private Button backButton;

    [SerializeField]
    private Button gunButton;

    [SerializeField]
    private Button stickmanButton;

    [Header("Image")]

    [SerializeField]
    private Image upgradeBar;

    [SerializeField]
    private Image circleIcon, lineIcon, roomStickman;

    [SerializeField]
    private Image playerLight, playerLight1;

    [Header("Sprite")]
    /// <summary>
	/// Disable Sprite
	/// </summary>

    [SerializeField]
    private Sprite onDisableBGSprite;
    [SerializeField]
    private Sprite onDisableUpgradeSprite;
    /// <summary>
    /// Enable Sprite
    /// </summary>
    [SerializeField]
    private Sprite onEnableBGSprite, onEnableUpgradeSprite;
    /// <summary>
    /// Kiem tra man nang cap stickman co duoc mo
    /// </summary>
    public bool isOpening;


    void Start()
    {
        OnSettingButton();
    }

    void OnSettingButton()
    {
        upgradeStickManButton[0].onClick.AddListener(OnClickUpgradeHPButton);
        upgradeStickManButton[1].onClick.AddListener(OnClickUpgradeSpeedButton);
        upgradeStickManButton[2].onClick.AddListener(OnClickUpgradeStrongButton);
    }

    public void InitAll()
    {
        OnSetPlayerLevel();
        onSetStickmanInfoUI();
        InitUpgradeButton();
    }

    public void OnShow(Action action = null)
    {
        isOpening = true;
        OnLockButton();
        Sequence effectSequence = DOTween.Sequence();
        statusPanel.gameObject.SetActive(true);
        backButton.gameObject.SetActive(true);
        upgradeBar.gameObject.SetActive(true);
        effectSequence.Append(statusPanel.DOAnchorPosX(-770f, 0.5f).OnComplete(() => upgradeInfoPanel.DOAnchorPosX(80f, 0.5f)));
        effectSequence.Join(circleIcon.rectTransform.DORotate(new Vector3(0f, 0f, 180f), 1f));
        effectSequence.Join(lineIcon.rectTransform.DOAnchorPosX(-456f, 0.5f).OnComplete(() =>
                {
                    circleIcon.rectTransform.DORotate(Vector3.zero, 1f);
                    upgradeBar.rectTransform.DOAnchorPosX(-70f, 0.5f);
                }));
        effectSequence.Join(itemPanel.DOAnchorPosX(-738f, 0.5f).OnComplete(() => itemPanel.gameObject.SetActive(false)));
        effectSequence.Join(roomStickman.rectTransform.DOAnchorPosX(-600f, 0.5f));
        effectSequence.Join(buttonPanel.DOAnchorPosY(-720f, 0.5f).OnComplete(() => buttonPanel.gameObject.SetActive(false)));
        effectSequence.Join(playerLight1.DOFade(0f, 0.5f).OnComplete(() => playerLight1.gameObject.SetActive(false)));
        effectSequence.Join(circleIcon.rectTransform.DORotate(new Vector3(0f, 0f, 180f), 1f));
        effectSequence.OnComplete(() =>
            { 
                OnUnlockButton();
                if (action != null)
                    action();
            });
        HomeUIHandle.Instance.ResetItemButton();
    }

    private void OnLockButton()
    {
        backButton.enabled = false;
        stickmanButton.enabled = false;
        gunButton.enabled = false;
    }

    private void OnUnlockButton()
    {
        backButton.enabled = true;
        stickmanButton.enabled = true;
        gunButton.enabled = true;
    }

    private void OnLockUpgradeButton()
    {
        for (int i = 0; i < upgradeStickManButton.Length; i++)
        {
            upgradeStickManButton[i].enabled = false;
        }
    }

    private void OnUnlockUpradeButton()
    {
        for (int i = 0; i < upgradeStickManButton.Length; i++)
        {
            upgradeStickManButton[i].enabled = true;
        }
    }

    public void OnHide(Action action = null)
    {
        isOpening = false;
        OnLockButton();
        Sequence effectSequence = DOTween.Sequence();
        circleIcon.rectTransform.DORotate(Vector3.zero, 1f);
        effectSequence.Append(upgradeInfoPanel.DOAnchorPosX(-950f, 0.5f));
        circleIcon.rectTransform.DORotate(new Vector3(0f, 0f, 180f), 1f);
        effectSequence.Join(upgradeBar.rectTransform.DOAnchorPosX(-590f, 0.5f).OnComplete(() =>
                {
                    circleIcon.rectTransform.DORotate(Vector3.zero, 1f);
                    lineIcon.rectTransform.DOAnchorPosX(-213f, 0.5f);
                    itemPanel.DOAnchorPosX(0f, 0.5f);
                    upgradeBar.gameObject.SetActive(false);
                    backButton.gameObject.SetActive(false);
                }));
        effectSequence.Append(statusPanel.DOAnchorPos(new Vector3(-52f, 7f, 0f), 0.5f));
        effectSequence.Join(buttonPanel.DOAnchorPosY(0f, 0.5f));
        effectSequence.Join(roomStickman.rectTransform.DOAnchorPosX(0f, 0.5f).OnComplete(() => playerLight1.DOFade(255f, 0.5f)));
        effectSequence.InsertCallback(0f, delegate
            {
                playerLight1.gameObject.SetActive(true);
                itemPanel.gameObject.SetActive(true);
                buttonPanel.gameObject.SetActive(true);
            });
        effectSequence.OnComplete(() =>
            {
                OnUnlockButton();
                if (action != null)
                    action();
            });
    }

    /// <summary>
    /// Gán thông tin của stickman
    /// </summary>
    private void onSetStickmanInfoUI()
    {

        float _hp = GameManager.Instance.OnGetPlayerHP();
        float _speed = GameManager.Instance.OnGetPlayerSpeed();
        float _strong = GameManager.Instance.OnGetPlayerStrong();

        float _hpUpgrade = GameManager.Instance.OnCalculateUpgradePlayerHP();
        float _speedUpgrade = GameManager.Instance.OnCalculateUpgradePlayerSpeed();
        float _strongUpgrade = GameManager.Instance.OnCalculateUpgradePlayerStrong();

        stickmanInfoText[0].text = _hp.ToString("#,##0");
        stickmanInfoText[1].text = _speed.ToString("F1");
        stickmanInfoText[2].text = _strong.ToString("#,##0");

        stickmanInfoText[3].text = "+ " + (_hpUpgrade - _hp).ToString("#,##0");
        stickmanInfoText[4].text = "+ " + (_speedUpgrade - _speed).ToString("F1");
        stickmanInfoText[5].text = "+ " + (_strongUpgrade - _strong).ToString("#,##0");
       
        stickmanInfoText[6].text = Home.IntFormatThousand(_hpUpgrade);
        stickmanInfoText[7].text = Home.FloatFormatThousandPoint(_speedUpgrade);
        stickmanInfoText[8].text = Home.FloatFormatThousand(_strongUpgrade);

        stickmanInfoText[9].text = Home.IntFormatThousand(GameManager.Instance.OnPriceUpgradePlayerHP());
        stickmanInfoText[10].text = Home.IntFormatThousand(GameManager.Instance.OnPriceUpgradeSpeed());
        stickmanInfoText[11].text = Home.IntFormatThousand(GameManager.Instance.OnPriceUpgradeStrong());

        stickmanInfoText[12].text = Home.IntFormatThousand(GameManager.Instance.OnGetGold());
    }

    public void OnSetPlayerLevel()
    {
        playerLevelText[0].text = GameManager.Instance.OnGetPlayerLevelHP() + 1 + "";
        playerLevelText[1].text = GameManager.Instance.OnGetPlayerLevelSpeed() + 1 + "";
        playerLevelText[2].text = GameManager.Instance.OnGetPlayerLevelStrong() + 1 + "";
    }


    public void OnClickUpgradeHPButton()
    {
        float _hp = GameManager.Instance.OnCalculateUpgradePlayerHP();
        int _priceUpgradeHP = GameManager.Instance.OnCalculateUpgradePlayerHP();
        if (GameManager.Instance.OnUpgradePlayerHP(_priceUpgradeHP, _hp))
        {
            OnLockButton();
            OnLockUpgradeButton();
            Vector3 tempPos = stickmanInfoText[3].transform.position;
            stickmanInfoText[3].transform.SetParent(borderImage.transform);
            stickmanInfoText[3].transform.DOMove(stickmanInfoText[0].transform.position, 0.5f).OnComplete(() =>
                {
                    stickmanInfoText[3].transform.SetParent(hpParentTransform);
                    stickmanInfoText[3].transform.DOMove(tempPos, 0.5f).OnComplete(() =>
                        {
                            InitUpgradeButton();
                            OnUnlockButton();
                        });
                });
            float _newHP = GameManager.Instance.OnGetPlayerHP();
            DOTween.To(() => _hp, x => _hp = x, _newHP, 1).OnUpdate(() => stickmanInfoText[0].text = string.Format("{0:0,0}", _hp));
            OnSetPlayerLevel();
            onSetStickmanInfoUI();
            HomeUIHandle.Instance.OnRefreshGold();
        }
        else
        {
            Debug.Log("Upgrade HP Failed");
        }

    }

    public void OnClickUpgradeSpeedButton()
    {
        float _speed = GameManager.Instance.OnCalculateUpgradePlayerSpeed();
        int _priceUpgradeSpeed = GameManager.Instance.OnPriceUpgradeSpeed();

        if (GameManager.Instance.OnUpgradePlayerSpeed(_priceUpgradeSpeed, _speed))
        {
            OnLockButton();
            OnLockUpgradeButton();
            Vector3 _tempPos = stickmanInfoText[4].transform.position;
            stickmanInfoText[4].transform.SetParent(borderImage.transform);
            stickmanInfoText[4].transform.DOMove(stickmanInfoText[1].transform.position, 0.5f).OnComplete(() =>
                {
                    stickmanInfoText[4].transform.DOMove(_tempPos, 0.5f).OnComplete(() =>
                        {
                            OnUnlockButton();
                            InitUpgradeButton();
                        });
                    stickmanInfoText[4].transform.SetParent(spdParentTransform);
                });
            float newSpeed = GameManager.Instance.OnGetPlayerSpeed();
            DOTween.To(() => _speed, x => _speed = x, newSpeed, 1).OnUpdate(() => stickmanInfoText[1].text = _speed.ToString("F1"));
            HomeUIHandle.Instance.OnRefreshGold();
            OnSetPlayerLevel();
            onSetStickmanInfoUI();
        }
        else
        {
            Debug.Log("Upgrade Speed Failed");
        }
    }

    public void OnClickUpgradeStrongButton()
    {
        float _strong = GameManager.Instance.OnCalculateUpgradePlayerStrong();
        int _priceStrong = GameManager.Instance.OnPriceUpgradeStrong();
        if (GameManager.Instance.OnUpgradePlayerStrong(_priceStrong, _strong))
        {
            OnLockButton();
            OnLockUpgradeButton();
            Vector3 _tempPos = stickmanInfoText[5].transform.position;
            stickmanInfoText[5].transform.SetParent(borderImage.transform);
            stickmanInfoText[5].transform.DOMove(stickmanInfoText[2].transform.position, 0.5f)
                .OnComplete(() =>
                {
                    stickmanInfoText[5].transform.DOMove(_tempPos, 0.5f).OnComplete(() =>
                        {
                            OnUnlockButton();
                            InitUpgradeButton();
                        });
                    stickmanInfoText[5].transform.SetParent(strParentTransform);
                });
            float newStrong = GameManager.Instance.OnGetPlayerStrong();
            DOTween.To(() => _strong, x => _strong = x, newStrong, 1).OnUpdate(() => stickmanInfoText[2].text = string.Format("{0:0,0}", _strong));
            HomeUIHandle.Instance.OnRefreshGold();
            onSetStickmanInfoUI();
            OnSetPlayerLevel();
        }
        else
        {
            Debug.Log("Upgrade Strong Failed");
        }
    }

    public void InitUpgradeButton()
    {
        if (GameManager.Instance.OnGetGold() < GameManager.Instance.OnPriceUpgradePlayerHP() && GameManager.Instance.OnGetPlayerLevelHP() < 29)
        {
            upgradeStickManButton[0].gameObject.GetComponent<Image>().sprite = onDisableBGSprite;
            upgradeStickManButton[0].transform.GetChild(2).GetComponent<Image>().overrideSprite = onDisableUpgradeSprite;
            upgradeStickManButton[0].enabled = false;
        }
        else if (GameManager.Instance.OnGetPlayerLevelHP() >= 29)
        {
            upgradeStickManButton[0].transform.parent.gameObject.SetActive(false);
            txtMaxLvStickmanInfo[0].gameObject.SetActive(true);
        }
        else
        {
            upgradeStickManButton[0].gameObject.GetComponent<Image>().sprite = onEnableBGSprite;
            upgradeStickManButton[0].transform.GetChild(2).GetComponent<Image>().overrideSprite = onEnableUpgradeSprite;
            upgradeStickManButton[0].enabled = true;
        }

        if (GameManager.Instance.OnGetGold() < GameManager.Instance.OnPriceUpgradeSpeed() && GameManager.Instance.OnGetPlayerLevelSpeed() < 29)
        {
            upgradeStickManButton[1].gameObject.GetComponent<Image>().sprite = onDisableBGSprite;
            upgradeStickManButton[1].transform.GetChild(2).GetComponent<Image>().overrideSprite = onDisableUpgradeSprite;
            upgradeStickManButton[1].enabled = false;
        }
        else if (GameManager.Instance.OnGetPlayerLevelSpeed() >= 29)
        {
            upgradeStickManButton[1].transform.parent.gameObject.SetActive(false);
            txtMaxLvStickmanInfo[1].gameObject.SetActive(true);
        }
        else
        {
            upgradeStickManButton[1].gameObject.GetComponent<Image>().sprite = onEnableBGSprite;
            upgradeStickManButton[1].transform.GetChild(2).GetComponent<Image>().overrideSprite = onEnableUpgradeSprite;
            upgradeStickManButton[1].enabled = true;
        }
        if (GameManager.Instance.OnGetGold() < GameManager.Instance.OnPriceUpgradeStrong() && GameManager.Instance.OnGetPlayerLevelStrong() < 29)
        {
            upgradeStickManButton[2].gameObject.GetComponent<Image>().sprite = onDisableBGSprite;
            upgradeStickManButton[2].transform.GetChild(2).GetComponent<Image>().overrideSprite = onDisableUpgradeSprite;
            upgradeStickManButton[2].enabled = false;
        }
        else if (GameManager.Instance.OnGetPlayerLevelStrong() >= 29)
        {
            upgradeStickManButton[2].transform.parent.gameObject.SetActive(false);
            txtMaxLvStickmanInfo[2].gameObject.SetActive(true);
        }
        else
        {
            upgradeStickManButton[2].gameObject.GetComponent<Image>().sprite = onEnableBGSprite;
            upgradeStickManButton[2].transform.GetChild(2).GetComponent<Image>().overrideSprite = onEnableUpgradeSprite;
            upgradeStickManButton[2].enabled = true;
        }
    }
}
