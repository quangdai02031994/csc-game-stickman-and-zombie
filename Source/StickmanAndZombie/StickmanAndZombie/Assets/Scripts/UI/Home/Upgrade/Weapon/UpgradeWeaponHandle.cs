﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.EventSystems;
using GameSupport;

public class UpgradeWeaponHandle : MonoBehaviour
{

    /// <summary>
    /// thanh ammo trong bang nang cap
    /// </summary>
    public RectTransform rectAmmoBar;

    [Header("Arm Sprite")]

    public Sprite spriteIconSword;
    public Sprite spriteIconBow;
    public Sprite spriteIconPistol;
    public Sprite spriteIconDualPistol;
    public Sprite spriteIconAK47;
    public Sprite spriteIconShotgun;
    public Sprite spriteIconGatlingGun;
    public Sprite spriteIconAwmGun;
    public Sprite spriteIconBazoka;
    public Sprite spriteIconFireGun;
    public Sprite spriteIconThunderGun;
    public Sprite spriteIconNuclearGun;

    [Space(10)]

    ///<summary>
    ///Panel mua sung, panel cac nut , panel overlay
    ///</summary>
    [SerializeField]
    private RectTransform weaponPanel, buttonPanel, buttonPanel1;
    [SerializeField]
    private RectTransform statusPanel;
    /// <summary>
    /// thanh bar nang cap sung 
    /// </summary>
    [SerializeField]
    private Image gunBarImage;
    [SerializeField]
    private Button[] listItemButton;
    private RectTransform[] listItemButtonRT;
    [SerializeField]
    private Button backButton;
    [SerializeField]
    private Image lineIcon, roomStickman, playerLigh1, circleIcon;
    public bool isOpening;
    /// <summary>
    /// Thong tin cua vu khi hien thi ben phai
    /// </summary>
    [SerializeField]
    private Text[] weaponInfoText;
    //mang luu nut ca cac loai vu khi
    [SerializeField]
    private Button[] weaponItemButton;
    [SerializeField]
    private Button gunButton;
    private GameObject clickedButton;
    public PLAYER_ARMS playerArm;
    /// <summary>
    /// nut equip button, unquipbutton, upgrade button
    /// </summary>
    [SerializeField]
    private Button equipButton, unequipButton, upgradeButton;
    [SerializeField]
    private Button buyButton, exitButton;
    [SerializeField]
    /// <summary>
	/// Sprite khi disable nut equip
	/// </summary>
	private Sprite onDisableBGSprite, onDisableEquipSprite;

    /// <summary>
    /// Sprite khi enable nut equip
    /// </summary>
    [SerializeField]
    private Sprite onEnableEquipSprite, onEnableBGSprite;
    [SerializeField]
    private Sprite onEnableGreenBGSprite;
    [SerializeField]
    private Sprite onDisableBuySprite, onEnableBuySprite;
    /// <summary>
    /// Equip Icon cua nut Equip
    /// </summary>
    [SerializeField]
    private Image equipImage, buyImage;
    [SerializeField]
    private ModelWeapon[] modelWeapon;
    [SerializeField]
    private RectTransform upgradeWeaponPanel;
    [SerializeField]
    private Image overlayPanel;

    [SerializeField]
    private UpgradeWeaponItemModel upgradeItemModel;

    [SerializeField]
    private Image damageBar, ammoBar, firerateBar;

    [SerializeField]
    private Image upgradeDamageGreenBar, upgradeDamageYellowBar;

    [SerializeField]
    private Image upgradeAmmoGreenBar, upgradeAmmoYellowBar;

    [SerializeField]
    private float timeTween = 0.5f;

    [SerializeField]
    private ScrollRect storeScrollRect;
	
    [Header("EquippedWeaponModel")]
	//model luu thong tin vu khi dang trang bi
    [SerializeField]
    private EquippedWeaponModel[] equippedWeaponModel;

    void Start()
    {
        listItemButtonRT = new RectTransform[3];
        for (int i = 0; i < listItemButton.Length; i++)
        {
            listItemButtonRT[i] = listItemButton[i].GetComponent<RectTransform>();
        }
    }

    /// <summary>
    /// Khoi tao danh sach vu khi
    /// </summary>
    public void InitAll()
    {
        InitWeaponList();
        onSetSwordItemUI();
        onSetEquippedItem();

    }

    /// <summary>
    /// them su kien cho button
    /// </summary>
    private void onSettingButton()
    {
        for (int i = 0; i < weaponItemButton.Length; i++)
        {
            weaponItemButton[i].onClick.AddListener(OnClickWeaponItemButton);
        }

        upgradeButton.onClick.AddListener(OnClickUpgradeButton);
        exitButton.onClick.AddListener(OnClickExitButton);

        buyButton.onClick.AddListener(OnClickBuyButton);
        equipButton.onClick.AddListener(OnClickEquipButton);

        unequipButton.onClick.AddListener(OnClickUnequipButton);
        upgradeItemModel.upgradeDamageButton.onClick.AddListener(OnClickUpgradeDamageButton);
        upgradeItemModel.upgradeAmmoButton.onClick.AddListener(OnClickUpgradeAmmoButton);
    }

    private void onUnSettingButton()
    {
        for (int i = 0; i < weaponItemButton.Length; i++)
        {
            weaponItemButton[i].onClick.RemoveAllListeners();
        }
        upgradeButton.onClick.RemoveAllListeners();
        exitButton.onClick.RemoveAllListeners();
        buyButton.onClick.RemoveAllListeners();
        equipButton.onClick.RemoveAllListeners();
        unequipButton.onClick.RemoveAllListeners();
        upgradeItemModel.upgradeDamageButton.onClick.RemoveAllListeners();
        upgradeItemModel.upgradeAmmoButton.onClick.RemoveAllListeners();
    }

    /// <summary>
    /// Kiem tra trang thai equip
    /// </summary>
    /// <returns><c>true</c>, neu con o trong, <c>false</c> neu ko con o.</returns>
    private bool onCheckEquip()
    {
        int[] _idEquipped = GameManager.Instance.OnGetPlayerArmEquiped();
        if (_idEquipped[1] < 0 || _idEquipped[2] < 0)
        {
            return true;
        }
        return false;
    }

    /// <summary>
    /// An thong tin UI cua Item khi da duoc mua
    /// </summary>
    /// <param name="weapon">Weapon.</param>
    private void onHideWeaponItemUI(ModelWeapon weapon)
    {
        weapon.requirementText.gameObject.SetActive(false);
        weapon.strongLVText.gameObject.SetActive(false);
        weapon.iconUpgradeStrong.gameObject.SetActive(false);
        weapon.coinText.gameObject.SetActive(false);
        weapon.coinIcon.gameObject.SetActive(false);
        weapon.purchasedIcon.gameObject.SetActive(true);
    }

    /// <summary>
    /// Hien thi thong tin Item khi chua mua
    /// </summary>
    /// <param name="weapon">Weapon.</param>
    private void onShowWeaponItemUI(ModelWeapon weapon, GameSupport.PLAYER_ARMS playerArm)
    {
        weapon.requirementText.gameObject.SetActive(true);
        weapon.strongLVText.gameObject.SetActive(true);
        weapon.iconUpgradeStrong.gameObject.SetActive(true);
        weapon.coinText.gameObject.SetActive(true);
        weapon.coinIcon.gameObject.SetActive(true);
        onShowNotPurchasedItem(weapon, playerArm);
    }

    private void onSetDisableEquipButton()
    {
        onSetActiveEquipButton();
        equipButton.enabled = false;
        equipImage.sprite = onDisableEquipSprite;
        equipButton.image.sprite = onDisableBGSprite;
    }

    private void onSetActiveEquipButton()
    {
        equipButton.gameObject.SetActive(true);
        unequipButton.gameObject.SetActive(false);
    }

    private void onSetEnableEquipButton()
    {
        onSetActiveEquipButton();
        equipButton.enabled = true;
        equipImage.sprite = onEnableEquipSprite;
        equipButton.image.sprite = onEnableBGSprite;
        unequipButton.gameObject.SetActive(false);
    }

    private void onSetDisableBuyButton()
    {
        onSetActiveBuyButton();
        buyButton.enabled = false;
        buyImage.sprite = onDisableBuySprite;
        buyButton.image.sprite = onDisableBGSprite;
    }

    private void onSetEnableBuyButton()
    {
        onSetActiveBuyButton();
        buyButton.enabled = true;
        buyImage.sprite = onEnableBuySprite;
        buyButton.image.sprite = onEnableGreenBGSprite;
    }

    private void onSetActiveBuyButton()
    {
        buyButton.gameObject.SetActive(true);
        upgradeButton.gameObject.SetActive(false);
    }

    public void OnSetActiveUpgradeButton()
    {
        upgradeButton.gameObject.SetActive(true);
        buyButton.gameObject.SetActive(false);
    }

    private void onSetEnableUnequipButton()
    {
        unequipButton.gameObject.SetActive(true);
        equipButton.gameObject.SetActive(false);
    }


    private void onSetSwordItemUI()
    {
        weaponInfoText[0].text = Home.FloatFormatThousand(GameManager.Instance.OnGetSwordInfo().damage);
        weaponInfoText[1].text = "None";
//        weaponInfoText[2].text = "10";
        weaponInfoText[3].text = GameManager.Instance.OnGetSwordInfo().name;
        weaponInfoText[4].text = GameManager.Instance.OnGetSwordInfo().description;
        weaponInfoText[5].text = "(lv " + Home.IntFormatThousand(GameManager.Instance.OnGetSwordInfo().levelDamage + 1) + ")";
        weaponInfoText[6].text = "( lv 0 )";
        ammoBar.DOFillAmount(0f, 0.5f);
        onRefreshSwordDamageBar();
    }

    private  void onSetPurchasedItemUI(GameSupport.PLAYER_ARMS playerArm)
    {
        weaponInfoText[0].text = Home.IntFormatThousand(GameManager.Instance.OnGetPlayerGun(playerArm).modelBullet.damage);
        weaponInfoText[1].text = Home.IntFormatThousand(GameManager.Instance.OnGetPlayerGun(playerArm).numberBullet);
//        weaponInfoText[2].text = "10";
        weaponInfoText[3].text = SaveAndLoadData.OnGetModelGunDefault(playerArm).name.ToString();
        weaponInfoText[4].text = SaveAndLoadData.OnGetModelGunDefault(playerArm).description.ToString();
        weaponInfoText[5].text = "(lv " + Home.IntFormatThousand(GameManager.Instance.OnGetPlayerGun(playerArm).levelDamage + 1) + ")";
        weaponInfoText[6].text = "(lv " + Home.IntFormatThousand(GameManager.Instance.OnGetPlayerGun(playerArm).levelNumberBullet + 1) + ")";
        float _newDamage = GameManager.Instance.OnCalculateUpgradeArmDamage(playerArm) + GameManager.Instance.OnGetPlayerGun(playerArm).modelBullet.damage;
        damageBar.DOFillAmount(Home.OnCalculateFillAmount(GameManager.Instance.OnGetPlayerGun(playerArm).modelBullet.damage, Home.OnGetMaxDamage()), 0.5f);
        ammoBar.DOFillAmount(Home.OnCalculateFillAmount(GameManager.Instance.OnGetPlayerGun(playerArm).numberBullet, Home.OnGetMaxAmmo()), 0.5f);
    }

    private void onShowWeaponItemList(ModelWeapon weapon, GameSupport.PLAYER_ARMS playerArm)
    {
        if (playerArm == GameSupport.PLAYER_ARMS.SWORD)
        {
            OnSetActiveUpgradeButton();
        }
        else if (GameManager.Instance.OnArmBought(playerArm) != null)
        {
            onHideWeaponItemUI(weapon);
            if (GameManager.Instance.OnGetPlayerGun(playerArm) != null)
            {
                onSetPurchasedItemUI(playerArm);
                if (GameManager.Instance.OnGetPlayerGun(playerArm).equipState == 1)
                {
                    weapon.equipIcon.gameObject.SetActive(true);
                }
                else if (GameManager.Instance.OnGetPlayerGun(playerArm).equipState == 0)
                {
                    weapon.equipIcon.gameObject.SetActive(false);
                }
            }
        }
        else
        {
            onSetGunDefaultItemUI(playerArm);
            onShowNotPurchasedItem(weapon, playerArm);
        }
    }


    private void onShowNotPurchasedItem(ModelWeapon weapon, GameSupport.PLAYER_ARMS playerArm)
    {
        weapon.strongLVText.text = "Lv" + GameManager.Instance.OnStrongRequire(playerArm);
        weapon.coinText.text = Home.IntFormatThousand(GameManager.Instance.OnPriceNewArm(playerArm));
    }

    /// <summary>
    /// Khoi tao list vu khi
    /// </summary>
    public void InitWeaponList()
    {
        OnSetActiveUpgradeButton();
        onSetDisableEquipButton();
        onShowWeaponItemList(modelWeapon[0], PLAYER_ARMS.SWORD);
        onShowWeaponItemList(modelWeapon[1], PLAYER_ARMS.BOW);
        onShowWeaponItemList(modelWeapon[2], PLAYER_ARMS.PISTOL);
        onShowWeaponItemList(modelWeapon[3], PLAYER_ARMS.DUAL_PISTOL);
        onShowWeaponItemList(modelWeapon[4], PLAYER_ARMS.AK47);
        onShowWeaponItemList(modelWeapon[5], PLAYER_ARMS.SHOTGUN);
        onShowWeaponItemList(modelWeapon[6], PLAYER_ARMS.GATLING_GUN);
        onShowWeaponItemList(modelWeapon[7], PLAYER_ARMS.AWM_GUN);
        onShowWeaponItemList(modelWeapon[8], PLAYER_ARMS.BAZOKA);
        onShowWeaponItemList(modelWeapon[9], PLAYER_ARMS.FIRE_GUN);
        onShowWeaponItemList(modelWeapon[10], PLAYER_ARMS.THUNDER_GUN);
        onShowWeaponItemList(modelWeapon[11], PLAYER_ARMS.NUCLEAR_GUN);
        onSetUpgradeWeaponUI(PLAYER_ARMS.SWORD);
        for (int i = 0; i < weaponItemButton.Length; i++)
        {
            if (i == 0)
            {
                weaponItemButton[i].image.color = GameSupportUI.HexToColor("73a6cd");
            }
            else
            {
                weaponItemButton[i].image.color = GameSupportUI.HexToColor("b49b6a");
            }
        }
    }

    private void onCheckClickedItem(ModelWeapon weapon, GameSupport.PLAYER_ARMS playerArm)
    {

        int index = (int)playerArm;
        for (int i = 0; i < weaponItemButton.Length; i++)
        {
            if (i == index)
            {
                weaponItemButton[i].image.color = GameSupportUI.HexToColor("73a6cd");
            }
            else
            {
                weaponItemButton[i].image.color = GameSupportUI.HexToColor("b49b6a");
            }
        }
        if (playerArm == GameSupport.PLAYER_ARMS.SWORD)
        {
            OnSetActiveUpgradeButton();
            onSetDisableEquipButton();
            onSetUpgradeWeaponUI(playerArm);
        }
        else if (GameManager.Instance.OnArmBought(playerArm) != null)
        {
            OnSetActiveUpgradeButton();
            upgradeButton.gameObject.SetActive(true);
            if (GameManager.Instance.OnGetPlayerGun(playerArm) != null)
            {
                onSetUpgradeWeaponUI(playerArm);
                if (GameManager.Instance.OnGetPlayerGun(playerArm).equipState == 1)
                {
                    onSetDisableEquipButton();
                    onSetEnableUnequipButton();
                }
                else if (GameManager.Instance.OnGetPlayerGun(playerArm).equipState == 0)
                {
                    if (Home.CheckEquipStatus())
                    {
                        onSetEnableEquipButton();
                    }
                    else
                    {
                        onSetDisableEquipButton();
                    }
                }
                else
                {
                    onSetDisableEquipButton();
                }
            }
        }
        else
        {
            onSetActiveBuyButton();
            onSetDisableBuyButton();
            onSetDisableEquipButton();
            if (GameManager.Instance.OnGetGold() >= GameManager.Instance.OnPriceNewArm(playerArm))
            {
                if (GameManager.Instance.OnGetPlayerLevelStrong() >= GameManager.Instance.OnStrongRequire(playerArm) - 1)
                {
                    onSetEnableBuyButton();
                }
            }
        }
    }

    private void onResetScrollView()
    {
        weaponItemButton[0].interactable = false;
        for (int i = 1; i < weaponItemButton.Length; i++)
        {
            weaponItemButton[i].interactable = true;
        }
        onSetSwordItemUI();

        OnSetActiveUpgradeButton();
        onSetDisableEquipButton();

        storeScrollRect.DOVerticalNormalizedPos(1f, 0.5f, true);
    }

    private void onSetGunDefaultItemUI(GameSupport.PLAYER_ARMS playerArm)
    {
        weaponInfoText[0].text = Home.IntFormatThousand(SaveAndLoadData.OnGetModelGunDefault(playerArm).modelBullet.damage);
        weaponInfoText[1].text = Home.IntFormatThousand(SaveAndLoadData.OnGetModelGunDefault(playerArm).numberBullet);
//        weaponInfoText[2].text = "10";
        weaponInfoText[3].text = SaveAndLoadData.OnGetModelGunDefault(playerArm).name.ToString();
        weaponInfoText[4].text = SaveAndLoadData.OnGetModelGunDefault(playerArm).description.ToString();
        weaponInfoText[5].text = "(lv " + Home.IntFormatThousand(SaveAndLoadData.OnGetModelGunDefault(playerArm).levelDamage + 1) + ")"; 
        weaponInfoText[6].text = "(lv " + Home.IntFormatThousand(SaveAndLoadData.OnGetModelGunDefault(playerArm).levelNumberBullet + 1) + ")";
        damageBar.DOFillAmount(Home.OnCalculateFillAmount(SaveAndLoadData.OnGetModelGunDefault(playerArm).modelBullet.damage, Home.OnGetMaxDamage()), 0.5f);
        ammoBar.DOFillAmount(Home.OnCalculateFillAmount(SaveAndLoadData.OnGetModelGunDefault(playerArm).numberBullet, Home.OnGetMaxAmmo()), 0.5f);
    }

    /// <summary>
    /// khi click vao nut upgrade
    /// </summary>
    ///
    private void onSetUpgradeWeaponUI(PLAYER_ARMS playerArm)
    {
        if (playerArm == GameSupport.PLAYER_ARMS.SWORD)
        {
            float _damage = GameManager.Instance.OnGetModelSword().damage;
            float _damageUpgrade = GameManager.Instance.OnCalculateUpgradeSwordDamage();
            float _additionDamage = _damageUpgrade - _damage;
            int _priceUpgradeDamage = GameManager.Instance.OnPriceUpgradeSwordDamage();

            int _lvDamage = GameManager.Instance.OnGetModelSword().levelDamage;
            onRefreshUpgradeSwordDamageBar();
            //weapon name
            upgradeItemModel.weaponNameText.text = GameManager.Instance.OnGetSwordInfo().name + "";
            upgradeItemModel.levelDamage.text = "(Lv " + (GameManager.Instance.OnGetModelSword().levelDamage + 1).ToString() + ")";

            //so damage hien tai
            upgradeItemModel.damageText.text = Home.FloatFormatThousand(_damage);
            //so damage cong them
            upgradeItemModel.damagePlusText.text = "+ " + Home.FloatFormatThousand(_additionDamage);
            //damage sau khi nang cap
            upgradeItemModel.damageAfterUpdatedText.text = Home.FloatFormatThousand(_damageUpgrade);
            //gia de nang cap damage
            upgradeItemModel.coinToUpdateDamageText.text = Home.IntFormatThousand(_priceUpgradeDamage);

            if (_lvDamage >= 9)
            {
                upgradeItemModel.OnShowMaxDamage();
            }
            else
            {
                upgradeItemModel.OnHideMaxDamage();
                if ((GameManager.Instance.OnGetGold() < GameManager.Instance.OnPriceUpgradeSwordDamage()))
                {
                    onDisableUpgradeDamageButton();
                }
                else
                {
                    onEnableUpgradeDamageButton();
                }
            }

            //Ẩn thông tin nâng cấp Ammo của kiếm
            upgradeItemModel.ammoUIText.gameObject.SetActive(false);
            upgradeItemModel.ammoText.gameObject.SetActive(false);
            upgradeItemModel.arrowIcon.gameObject.SetActive(false);
            upgradeItemModel.upgradeAmmoButton.gameObject.SetActive(false);
            upgradeItemModel.ammoPlusText.gameObject.SetActive(false);
            upgradeItemModel.ammoAfterUpdatedText.gameObject.SetActive(false);
            upgradeItemModel.ammoSlideBar.gameObject.SetActive(false);
            upgradeAmmoYellowBar.gameObject.SetActive(false);
            upgradeItemModel.levelAmmo.gameObject.SetActive(false);
            rectAmmoBar.gameObject.SetActive(false);
            upgradeItemModel.txtMaxLvAmmo.gameObject.SetActive(false);

        }
        else
        {
            //Hiện thông tin Ammo của súng
            upgradeItemModel.ammoUIText.gameObject.SetActive(true);
            upgradeItemModel.ammoText.gameObject.SetActive(true);
            upgradeItemModel.arrowIcon.gameObject.SetActive(true);
            upgradeItemModel.upgradeAmmoButton.gameObject.SetActive(true);
            upgradeItemModel.ammoPlusText.gameObject.SetActive(true);
            upgradeItemModel.ammoAfterUpdatedText.gameObject.SetActive(true);
            upgradeItemModel.ammoSlideBar.gameObject.SetActive(true);
            upgradeAmmoYellowBar.gameObject.SetActive(true);
            rectAmmoBar.gameObject.SetActive(true);
            upgradeItemModel.levelAmmo.gameObject.SetActive(true);
            //weapon name
            upgradeItemModel.weaponNameText.text = SaveAndLoadData.OnGetModelGunDefault(playerArm).name.ToString();
            upgradeItemModel.levelDamage.text = "(Lv " + (GameManager.Instance.OnGetPlayerGun(playerArm).levelDamage + 1).ToString() + ")";
            upgradeItemModel.levelAmmo.text = "(Lv " + (GameManager.Instance.OnGetPlayerGun(playerArm).levelNumberBullet + 1).ToString() + ")";
            //so damage hien tai
            onRefreshGoldRequire();
            onRefreshDamageBar();
            onRefreshAmmoBar();

            if (GameManager.Instance.OnGetPlayerGun(playerArm).levelDamage >= 9)
            {
                upgradeItemModel.OnShowMaxDamage();
            }
            else
            {
                upgradeItemModel.OnHideMaxDamage();
                if (GameManager.Instance.OnGetGold() < GameManager.Instance.OnPriceUpgradeArmDamage(playerArm))
                {
                    onDisableUpgradeDamageButton();
                }
                else
                {
                    onEnableUpgradeDamageButton();
                }
            }

            if (GameManager.Instance.OnGetPlayerGun(playerArm).levelNumberBullet >= 9)
            {
                upgradeItemModel.OnShowMaxAmmo();
            }
            else
            {
                upgradeItemModel.OnHideMaxAmmo();
                if (GameManager.Instance.OnGetGold() < GameManager.Instance.OnPriceUpgradeNumberBullet(playerArm))
                {
                    onDisableUpgradeAmmoButton();
                }
                else
                {
                    onEnableUpgradeAmmoButton();
                }
            }
        }
			
    }

    private void onRefreshUpgradeSwordDamageBar()
    {
        float _damageBar = Home.OnCalculateFillAmount(GameManager.Instance.OnGetModelSword().damage, Home.OnGetMaxDamage());
        float _damageYellowBar = Home.OnCalculateFillAmount(GameManager.Instance.OnCalculateUpgradeSwordDamage(), Home.OnGetMaxDamage());
        upgradeItemModel.damageSlideBar.DOFillAmount(_damageBar, timeTween).OnComplete(() =>
            {
                upgradeItemModel.damageYellowBar.DOFillAmount(_damageYellowBar, timeTween);
            });
    }

    private void onRefreshSwordDamageBar()
    {
        float _damageBar = Home.OnCalculateFillAmount((int)GameManager.Instance.OnGetModelSword().damage, Home.OnGetMaxDamage());
        float _damageYellowBar = Home.OnCalculateFillAmount(GameManager.Instance.OnCalculateUpgradeSwordDamage(), Home.OnGetMaxDamage());
        damageBar.DOFillAmount(_damageBar, timeTween);
    }

    private void onRefreshDamageBar()
    {
        float _damageBar = Home.OnCalculateFillAmount(GameManager.Instance.OnGetPlayerGun(playerArm).modelBullet.damage, Home.OnGetMaxDamage());
        float _damageYellowBar = Home.OnCalculateFillAmount(GameManager.Instance.OnCalculateUpgradeArmDamage(playerArm), Home.OnGetMaxDamage());
        upgradeItemModel.damageSlideBar.DOFillAmount(_damageBar, timeTween).OnComplete(() =>
            {
                upgradeItemModel.damageYellowBar.DOFillAmount(_damageYellowBar, timeTween);
            });
    }

    private void onRefreshGoldRequire()
    {

        int _currentDamage = GameManager.Instance.OnGetPlayerGun(playerArm).modelBullet.damage;
        int _upgradeDamage = GameManager.Instance.OnCalculateUpgradeArmDamage(playerArm);
        int _additionalDamage = _upgradeDamage - _currentDamage;
        int _priceUpgradeDamage = GameManager.Instance.OnPriceUpgradeArmDamage(playerArm);
//        Debug.Log("Current damage: " + _currentDamage + " upgrade damage: " + _upgradeDamage + " price: " + _priceUpgradeDamage + " type: " + playerArm.ToString());

        upgradeItemModel.damageText.text = Home.IntFormatThousand(_currentDamage);
        //so damage cong them 
//        int damage = GameManager.Instance.OnCalculateUpgradeArmDamage(playerArm) - GameManager.Instance.OnGetPlayerGun(playerArm).modelBullet.damage;
        upgradeItemModel.damagePlusText.text = "+ " + Home.IntFormatThousand(_additionalDamage);
        //so damage sau khi nang cap
        upgradeItemModel.damageAfterUpdatedText.text = Home.IntFormatThousand(_upgradeDamage);
        //gia de nang cap damage
        upgradeItemModel.coinToUpdateDamageText.text = Home.IntFormatThousand(_priceUpgradeDamage);

        int _currentAmmo = GameManager.Instance.OnGetPlayerGun(playerArm).numberBullet;
        int _upgradeAmmo = GameManager.Instance.OnCalculateUpgradeNumberBullet(playerArm);
        int _additionalAmmo = _upgradeAmmo - _currentAmmo;
        int _priceUpgradeAmmo = GameManager.Instance.OnPriceUpgradeNumberBullet(playerArm);    
//        Debug.Log("Current ammo: " + _currentAmmo + " upgrade Ammo: " + _upgradeAmmo + " price: " + _priceUpgradeAmmo);

        //so luong dan hien tai
        upgradeItemModel.ammoText.text = Home.IntFormatThousand(_currentAmmo);
        //so luong dan cong them
        upgradeItemModel.ammoPlusText.text = "+ " + Home.IntFormatThousand(_additionalAmmo);
        //so dan sau khi nang cap
        upgradeItemModel.ammoAfterUpdatedText.text = Home.IntFormatThousand(_upgradeAmmo);
        //gia de mua dan
        upgradeItemModel.coinToUpdateAmmoText.text = Home.IntFormatThousand(_priceUpgradeAmmo);
        //gia de nang cap damamge
//        upgradeItemModel.coinToUpdateDamageText.text = Home.IntFormatThousand(GameManager.Instance.OnPriceUpgradeArmDamage(playerArm));
    }

    private void onRefreshAmmoBar()
    {
        float _ammoBar = Home.OnCalculateFillAmount(GameManager.Instance.OnGetPlayerGun(playerArm).numberBullet, Home.OnGetMaxAmmo());
        float _ammoYellowBar = Home.OnCalculateFillAmount(GameManager.Instance.OnCalculateUpgradeNumberBullet(playerArm), Home.OnGetMaxAmmo());
        upgradeItemModel.ammoSlideBar.DOFillAmount(_ammoBar, timeTween).OnComplete(() =>
            {
                upgradeItemModel.ammoYellowBar.DOFillAmount(_ammoYellowBar, timeTween);
            });
    }

    /// <summary>
    /// Enable nut upgrade damage
    /// </summary>
    private void onEnableUpgradeDamageButton()
    {
        upgradeItemModel.upgradeDamageButton.enabled = true;
        upgradeItemModel.upgradeDamageButton.image.sprite = upgradeItemModel.upgradeEnableBG;
        upgradeItemModel.upgradeDamageIcon.sprite = upgradeItemModel.upgradeEnableIcon;
    }

    /// <summary>
    /// disable nut upgrade damage
    /// </summary>
    private void onDisableUpgradeDamageButton()
    {
        upgradeItemModel.upgradeDamageButton.enabled = false;
        upgradeItemModel.upgradeDamageButton.image.sprite = upgradeItemModel.upgradeDisableBG;
        upgradeItemModel.upgradeDamageIcon.sprite = upgradeItemModel.upgradeDisableIcon;
    }

    /// <summary>
    /// enable nut nang cap dan
    /// </summary>
    private void onEnableUpgradeAmmoButton()
    {
        upgradeItemModel.upgradeAmmoButton.enabled = true;
        upgradeItemModel.upgradeAmmoButton.image.sprite = upgradeItemModel.upgradeEnableBG;
        upgradeItemModel.upgradeAmmoIcon.sprite = upgradeItemModel.upgradeEnableIcon;
    }

    /// <summary>
    /// disable nut nang cap dan
    /// </summary>
    private void onDisableUpgradeAmmoButton()
    {
        upgradeItemModel.upgradeAmmoButton.enabled = false;
        upgradeItemModel.upgradeAmmoButton.image.sprite = upgradeItemModel.upgradeDisableBG;
        upgradeItemModel.upgradeAmmoIcon.sprite = upgradeItemModel.upgradeDisableIcon;
    }

    /// <summary>
    /// khoa button
    /// </summary>
    private void onLockUpgradeButton()
    {
        upgradeItemModel.upgradeDamageButton.enabled = false;
        upgradeItemModel.upgradeAmmoButton.enabled = false;
        exitButton.enabled = false;
    }

    /// <summary>
    /// enable button
    /// </summary>
    private void onUnlockUpgradeButton()
    {
        upgradeItemModel.upgradeDamageButton.enabled = true;
        upgradeItemModel.upgradeAmmoButton.enabled = true;
        exitButton.enabled = true;
    }

    /// <summary>
    /// khi click vao nang cap damage
    /// </summary>
    public void OnClickUpgradeDamageButton()
    {
        if (playerArm == GameSupport.PLAYER_ARMS.SWORD)
        {
            float _damageSword = GameManager.Instance.OnGetSwordInfo().damage;
            int _priceUpgrade = GameManager.Instance.OnPriceUpgradeSwordDamage();
            float _damageUpgrade = GameManager.Instance.OnCalculateUpgradeSwordDamage();
            if (GameManager.Instance.OnUpgradeSwordDamage(_priceUpgrade, _damageUpgrade))
            {
                onSetUpgradeWeaponUI(playerArm);
                HomeUIHandle.Instance.OnRefreshAll();
                onLockUpgradeButton();
                Vector3 tempPos = upgradeItemModel.damagePlusText.transform.position;
                float _newDamage = GameManager.Instance.OnGetSwordInfo().damage;
                upgradeItemModel.damagePlusText.transform.SetParent(upgradeItemModel.mask);
                upgradeItemModel.damagePlusText.rectTransform.DOMove(upgradeItemModel.damageText.transform.position, 0.5f).OnComplete(() =>
                    {
                        upgradeItemModel.damagePlusText.transform.SetParent(upgradeItemModel.mask);
                        upgradeItemModel.damagePlusText.transform.DOMove(tempPos, 0.5f).OnComplete(() =>
                            {
                                onUnlockUpgradeButton();
                            });
                    });
                DOTween.To(() => _damageSword, x => _damageSword = x, _newDamage, 1).OnUpdate(() => upgradeItemModel.damageText.text = string.Format("{0:0,0}", _damageSword)).
				OnComplete(() =>
                    {
                        upgradeItemModel.damagePlusText.transform.SetParent(upgradeItemModel.mask);
                    });
                onRefreshSwordDamageBar();
                onSetSwordItemUI();
                onSetEquippedItem();
            }
            else
            {
                Debug.Log("Update Damage Sword Failed");
            }
        }
        else
        {
            Debug.Log("OnClickUpgradeDamageButton");
            int _damage = GameManager.Instance.OnGetPlayerGun(playerArm).modelBullet.damage;
            int _damageUpgrade = GameManager.Instance.OnCalculateUpgradeArmDamage(playerArm);
            int _priceUpgrade = GameManager.Instance.OnPriceUpgradeArmDamage(playerArm);

            if (GameManager.Instance.OnUpgradeArmDamage(playerArm, _priceUpgrade, _damageUpgrade))
            {
                onSetUpgradeWeaponUI(playerArm);
                onLockUpgradeButton();
                upgradeItemModel.upgradeDamageButton.enabled = false; 
                HomeUIHandle.Instance.OnRefreshAll();
                Vector3 tempPos = upgradeItemModel.damagePlusText.transform.position;
                int _newDamage = GameManager.Instance.OnGetPlayerGun(playerArm).modelBullet.damage;
                upgradeItemModel.damagePlusText.transform.SetParent(upgradeItemModel.mask);
                upgradeItemModel.damagePlusText.rectTransform.DOMove(upgradeItemModel.damageText.transform.position, 0.5f).OnComplete(() =>
                    {
                        upgradeItemModel.damagePlusText.transform.SetParent(upgradeItemModel.damageTransform);
                        upgradeItemModel.damagePlusText.transform.DOMove(tempPos, 0.5f).OnComplete(() =>
                            {
                                onUnlockUpgradeButton();
                            });
                    });
                DOTween.To(() => _damage, x => _damage = x, _newDamage, 1)
                    .OnUpdate(() =>
                    {
                        upgradeItemModel.damageText.text = _damage.ToString("#,##0");
                    })
                    .OnComplete(() =>
                    {
                        upgradeItemModel.damagePlusText.transform.SetParent(upgradeItemModel.mask);
                    });
//                HomeUIHandle.Instance.OnRefreshAll();
                onSetPurchasedItemUI(playerArm);
                onRefreshDamageBar();
                onSetEquippedItem();
            }
            else
            {
                Debug.Log("Upgrade Damage Failed");
            }
        }
    }

    /// <summary>
    /// khi click vao nut nang cap dan
    /// </summary>
    public void OnClickUpgradeAmmoButton()
    {
        Debug.Log("OnClickUpgradeAmmoButton");
        int _numberBulletAfterUpdated = GameManager.Instance.OnCalculateUpgradeNumberBullet(playerArm);
        int _addNumberBullet = GameManager.Instance.OnCalculateUpgradeNumberBullet(playerArm) - GameManager.Instance.OnGetPlayerGun(playerArm).numberBullet;
        int _numberBullet = GameManager.Instance.OnGetPlayerGun(playerArm).numberBullet;
        int _priceUpgrade = GameManager.Instance.OnPriceUpgradeNumberBullet(playerArm);
        if (GameManager.Instance.OnUpgradeNumberBullet(playerArm, _priceUpgrade, _numberBulletAfterUpdated))
        {
            onSetUpgradeWeaponUI(playerArm);
            upgradeItemModel.upgradeAmmoButton.enabled = false; 
            HomeUIHandle.Instance.OnRefreshAll();
            Vector3 tempPos = upgradeItemModel.ammoPlusText.transform.position;
            upgradeItemModel.ammoPlusText.transform.SetParent(upgradeItemModel.mask);
            upgradeItemModel.ammoPlusText.rectTransform.DOMove(upgradeItemModel.ammoText.transform.position, 0.5f).OnComplete(() =>
                {
                    upgradeItemModel.ammoPlusText.transform.SetParent(upgradeItemModel.ammoTransform);
                    upgradeItemModel.ammoPlusText.transform.DOMove(tempPos, 0.5f).OnComplete(() =>
                        {
                            onUnlockUpgradeButton();
                            onSetUpgradeWeaponUI(playerArm);
                        });
                });
            DOTween.To(() => _numberBullet, x => _numberBullet = x, _numberBulletAfterUpdated, 1)
                .OnUpdate(() =>
                {
                    upgradeItemModel.ammoText.text = _numberBullet.ToString("#,##0");
                })
                .OnComplete(() =>
                {
                    upgradeItemModel.upgradeAmmoButton.enabled = true;
                    upgradeItemModel.ammoPlusText.transform.SetParent(upgradeItemModel.mask);
                });
            onSetPurchasedItemUI(playerArm);
            onRefreshAmmoBar();
            onSetEquippedItem();
        }
        else
        {
            Debug.Log("Upgrade Damage Failed");
        }
    }

    /// <summary>
    /// khi click vao nut nang cap
    /// </summary>
    public void OnClickUpgradeButton()
    {
        Debug.Log("OnClickUpgradeButton");
        upgradeWeaponPanel.gameObject.SetActive(true);
        upgradeWeaponPanel.DOAnchorPos(Vector3.zero, 0.5f);
        overlayPanel.gameObject.SetActive(true);
        overlayPanel.DOFade(0.7f, 0.5f);
    }

    /// <summary>
    /// Khi click vao nut exit
    /// </summary>
    public void OnClickExitButton()
    {
        upgradeWeaponPanel.DOAnchorPosY(1000f, 0.5f).OnComplete(() => upgradeWeaponPanel.gameObject.SetActive(false));
        overlayPanel.DOFade(0f, 0.5f).OnComplete(() =>
            {
                overlayPanel.gameObject.SetActive(false);
            });
    }

    /// <summary>
    /// Khi click mua vu khi
    /// </summary>
    public void OnClickBuyButton()
    {
        if (GameManager.Instance.OnBuyNewArm(playerArm, GameManager.Instance.OnPriceNewArm(playerArm)))
        {
            onSetWeaponStatus();
            onSetPurchasedItemUI(playerArm);
            OnSetActiveUpgradeButton();
            if (onCheckEquip())
            {
                onSetEnableEquipButton();
            }
            onSetUpgradeWeaponUI(playerArm);
            HomeUIHandle.Instance.OnRefreshAll();
        }
        else
        {
            Debug.Log("Buy failed");
        }
    }

    private void onSetWeaponStatus()
    {
        switch (playerArm)
        {
            case GameSupport.PLAYER_ARMS.SWORD:
                onShowWeaponItemList(modelWeapon[0], playerArm);
                break;
            case GameSupport.PLAYER_ARMS.BOW:
                onShowWeaponItemList(modelWeapon[1], playerArm);
                break;
            case GameSupport.PLAYER_ARMS.PISTOL:
                onShowWeaponItemList(modelWeapon[2], playerArm);
                break;
            case GameSupport.PLAYER_ARMS.DUAL_PISTOL:
                onShowWeaponItemList(modelWeapon[3], playerArm);
                break;
            case GameSupport.PLAYER_ARMS.AK47:
                onShowWeaponItemList(modelWeapon[4], playerArm);
                break;
            case GameSupport.PLAYER_ARMS.SHOTGUN:
                onShowWeaponItemList(modelWeapon[5], playerArm);
                break;
            case GameSupport.PLAYER_ARMS.GATLING_GUN:
                onShowWeaponItemList(modelWeapon[6], playerArm);
                break;
            case GameSupport.PLAYER_ARMS.AWM_GUN:
                onShowWeaponItemList(modelWeapon[7], playerArm);
                break;
            case GameSupport.PLAYER_ARMS.BAZOKA:
                onShowWeaponItemList(modelWeapon[8], playerArm);
                break;
            case GameSupport.PLAYER_ARMS.FIRE_GUN:
                onShowWeaponItemList(modelWeapon[9], playerArm);
                break;
            case GameSupport.PLAYER_ARMS.THUNDER_GUN:
                onShowWeaponItemList(modelWeapon[10], playerArm);
                break;
            case PLAYER_ARMS.NUCLEAR_GUN:
                onShowWeaponItemList(modelWeapon[11], playerArm);
                break;
                
        }
    }

    /// <summary>
    /// khi click nut equip
    /// </summary>
    public void OnClickEquipButton()
    {
		
        if (GameManager.Instance.OnEquipArm(playerArm))
        {
            onSetWeaponStatus();
            onSetPurchasedItemUI(playerArm);
            onSetEnableUnequipButton();
            onSetEquippedItem();
        }
        else
        {
            Debug.Log("Equip Failed");
        }
    }

    /// <summary>
    /// Khi click nut unequip
    /// </summary>
    public void OnClickUnequipButton()
    {
        if (GameManager.Instance.OnUnEquipArm(playerArm))
        {
            onSetWeaponStatus();
            onSetPurchasedItemUI(playerArm);
            onSetEquippedItem();
            onSetEnableEquipButton();
        }
    }

    /// <summary>
    /// Xử lý khi click vào button vũ khí trong danh sách vũ khí.
    /// </summary>
    public void OnClickWeaponItemButton()
    {
        if (EventSystem.current.currentSelectedGameObject == null
            || clickedButton == EventSystem.current.currentSelectedGameObject)
            return;
        clickedButton = EventSystem.current.currentSelectedGameObject;
        switch (clickedButton.name)
        {
            case "SWORD":
                HomeUIHandle.Instance.OnPlayAnimationUseNewWeapon(playerArm, PLAYER_ARMS.SWORD);
                playerArm = GameSupport.PLAYER_ARMS.SWORD;
                onSetSwordItemUI();
                onCheckClickedItem(modelWeapon[0], playerArm);
                break;
            case "BOW":
                HomeUIHandle.Instance.OnPlayAnimationUseNewWeapon(playerArm, PLAYER_ARMS.BOW);
                playerArm = GameSupport.PLAYER_ARMS.BOW;
                onShowWeaponItemList(modelWeapon[1], playerArm);
                onCheckClickedItem(modelWeapon[1], playerArm);
                break;
            case "PISTOL":
                HomeUIHandle.Instance.OnPlayAnimationUseNewWeapon(playerArm, PLAYER_ARMS.PISTOL);
                playerArm = GameSupport.PLAYER_ARMS.PISTOL;
                onShowWeaponItemList(modelWeapon[2], playerArm);
                onCheckClickedItem(modelWeapon[2], playerArm);
                break;
            case "DUAL_PISTOL":
                HomeUIHandle.Instance.OnPlayAnimationUseNewWeapon(playerArm, PLAYER_ARMS.DUAL_PISTOL);
                playerArm = GameSupport.PLAYER_ARMS.DUAL_PISTOL;
                onShowWeaponItemList(modelWeapon[3], playerArm);
                onCheckClickedItem(modelWeapon[3], playerArm);
                break;
            case "AK47":
                HomeUIHandle.Instance.OnPlayAnimationUseNewWeapon(playerArm, PLAYER_ARMS.AK47);
                playerArm = GameSupport.PLAYER_ARMS.AK47;
                onShowWeaponItemList(modelWeapon[4], playerArm);
                onCheckClickedItem(modelWeapon[4], playerArm);
                break;
            case "SHOTGUN":
                HomeUIHandle.Instance.OnPlayAnimationUseNewWeapon(playerArm, PLAYER_ARMS.SHOTGUN);
                playerArm = GameSupport.PLAYER_ARMS.SHOTGUN;
                onShowWeaponItemList(modelWeapon[5], playerArm);
                onCheckClickedItem(modelWeapon[5], playerArm);
                break;
            case "GATLING_GUN":
                HomeUIHandle.Instance.OnPlayAnimationUseNewWeapon(playerArm, PLAYER_ARMS.GATLING_GUN);
                playerArm = GameSupport.PLAYER_ARMS.GATLING_GUN;
                onShowWeaponItemList(modelWeapon[6], playerArm);
                onCheckClickedItem(modelWeapon[6], playerArm);
                break;
            case "AWM_GUN":
                HomeUIHandle.Instance.OnPlayAnimationUseNewWeapon(playerArm, PLAYER_ARMS.AWM_GUN);
                playerArm = GameSupport.PLAYER_ARMS.AWM_GUN;
                onShowWeaponItemList(modelWeapon[7], playerArm);
                onCheckClickedItem(modelWeapon[7], playerArm);
                break;
            case "BAZOKA":
                HomeUIHandle.Instance.OnPlayAnimationUseNewWeapon(playerArm, PLAYER_ARMS.BAZOKA);
                playerArm = GameSupport.PLAYER_ARMS.BAZOKA;
                onShowWeaponItemList(modelWeapon[8], playerArm);
                onCheckClickedItem(modelWeapon[8], playerArm);
                break;
            case "FIRE_GUN":
                HomeUIHandle.Instance.OnPlayAnimationUseNewWeapon(playerArm, PLAYER_ARMS.FIRE_GUN);
                playerArm = GameSupport.PLAYER_ARMS.FIRE_GUN;
                onShowWeaponItemList(modelWeapon[9], playerArm);
                onCheckClickedItem(modelWeapon[9], playerArm);
                break;
            case "THUNDER_GUN":
                HomeUIHandle.Instance.OnPlayAnimationUseNewWeapon(playerArm, PLAYER_ARMS.THUNDER_GUN);
                playerArm = GameSupport.PLAYER_ARMS.THUNDER_GUN;
                onShowWeaponItemList(modelWeapon[10], playerArm);
                onCheckClickedItem(modelWeapon[10], playerArm);
                break;
            case "NUCLEAR_GUN":
                HomeUIHandle.Instance.OnPlayAnimationUseNewWeapon(playerArm, PLAYER_ARMS.NUCLEAR_GUN);
                playerArm = GameSupport.PLAYER_ARMS.NUCLEAR_GUN;
                onShowWeaponItemList(modelWeapon[11], playerArm);
                onCheckClickedItem(modelWeapon[11], playerArm);
                break;
            default :
                playerArm = GameSupport.PLAYER_ARMS.SWORD;
                break;
        }
        if (GameManager.Instance.OnGetPlayerGun(playerArm) != null)
        {
            onSetUpgradeWeaponUI(playerArm);
        }
        for (int i = 0; i < weaponItemButton.Length; i++)
        {
            if (clickedButton.name == weaponItemButton[i].name)
            {
                weaponItemButton[i].interactable = false;
                continue;
            }
            weaponItemButton[i].interactable = true;
        }

    }

    private void onLockButton()
    {
        backButton.enabled = false;
        gunButton.enabled = false;
    }

    private void onUnlockButton()
    {
        backButton.enabled = true;
        gunButton.enabled = true;
    }

    private void onSetEquippedItem()
    {
        int[] _idEquipped = GameManager.Instance.OnGetPlayerArmEquiped();
        onSetSwordInfo();
        if (_idEquipped[1] > 0)
        {
            onShowWeaponInfo(equippedWeaponModel[1]);
            Sprite item1Sprite = onGetWeaponSprite(_idEquipped[1]);
            equippedWeaponModel[1].weaponImage.overrideSprite = item1Sprite;
            onSetEquippedWeaponInfo(equippedWeaponModel[1], (PLAYER_ARMS)_idEquipped[1]);
        }
        else
        {
            onHideWeaponInfo(equippedWeaponModel[1]);
        }
        if (_idEquipped[2] > 0)
        {
            onShowWeaponInfo(equippedWeaponModel[2]);
            Sprite item2Sprite = onGetWeaponSprite(_idEquipped[2]);
            equippedWeaponModel[2].weaponImage.overrideSprite = item2Sprite;
            onSetEquippedWeaponInfo(equippedWeaponModel[2], (PLAYER_ARMS)_idEquipped[2]);
        }
        else
        {
            onHideWeaponInfo(equippedWeaponModel[2]);
        }
    }

    private void onHideWeaponInfo(EquippedWeaponModel equippedWeaponModel)
    {
        equippedWeaponModel.weaponImage.gameObject.SetActive(false);
        equippedWeaponModel.weaponNameText.gameObject.SetActive(false);
        equippedWeaponModel.damageText.gameObject.SetActive(false);
        equippedWeaponModel.damageUIText.gameObject.SetActive(false);
        equippedWeaponModel.ammoUI.gameObject.SetActive(false);
        equippedWeaponModel.ammoText.gameObject.SetActive(false);
        equippedWeaponModel.infoText.gameObject.SetActive(false);
//        equippedWeaponModel.firerateUI.gameObject.SetActive(false);
//        equippedWeaponModel.firerateText.gameObject.SetActive(false);
    }

    private void onShowWeaponInfo(EquippedWeaponModel equippedWeaponModel)
    {
        equippedWeaponModel.weaponImage.gameObject.SetActive(true);
        equippedWeaponModel.weaponNameText.gameObject.SetActive(true);
        equippedWeaponModel.damageText.gameObject.SetActive(true);
        equippedWeaponModel.damageUIText.gameObject.SetActive(true);
        equippedWeaponModel.ammoUI.gameObject.SetActive(true);
        equippedWeaponModel.ammoText.gameObject.SetActive(true);
        equippedWeaponModel.infoText.gameObject.SetActive(true);
//        equippedWeaponModel.firerateUI.gameObject.SetActive(true);
//        equippedWeaponModel.firerateText.gameObject.SetActive(true);
    }

    private void onSetEquippedWeaponInfo(EquippedWeaponModel equippedModel, GameSupport.PLAYER_ARMS arm)
    {
//        Debug.Log("Player arm: " + arm);
        if (arm == GameSupport.PLAYER_ARMS.SWORD)
        {
            onSetSwordInfo();
        }
        else
        {
            equippedModel.weaponNameText.text = SaveAndLoadData.OnGetModelGunDefault(arm).name + "";
            equippedModel.damageText.text = Home.IntFormatThousand(GameManager.Instance.OnGetPlayerGun(arm).modelBullet.damage);
            equippedModel.ammoText.text = Home.IntFormatThousand(GameManager.Instance.OnGetPlayerGun(arm).numberBullet);
            equippedModel.infoText.text = SaveAndLoadData.OnGetModelGunDefault(arm).description + "";
//            equippedModel.firerateText.text = "1000";
        }
    }

    private void onSetSwordInfo()
    {
        equippedWeaponModel[0].weaponNameText.text = GameManager.Instance.OnGetModelSword().name + "";
        equippedWeaponModel[0].damageText.text = Home.FloatFormatThousand(GameManager.Instance.OnGetModelSword().damage);
        equippedWeaponModel[0].ammoText.text = "None";
        equippedWeaponModel[0].infoText.text = GameManager.Instance.OnGetModelSword().description + "";
    }

    private Sprite onGetWeaponSprite(int index)
    {
        PLAYER_ARMS type = (PLAYER_ARMS)index;
        switch (type)
        {
            case PLAYER_ARMS.SWORD:
                return spriteIconSword;
            case PLAYER_ARMS.AK47:
                return spriteIconAK47;
            case PLAYER_ARMS.AWM_GUN:
                return spriteIconAwmGun;
            case PLAYER_ARMS.BAZOKA:
                return spriteIconBazoka;
            case PLAYER_ARMS.BOW:
                return spriteIconBow;
            case PLAYER_ARMS.DUAL_PISTOL:
                return spriteIconDualPistol;
            case PLAYER_ARMS.FIRE_GUN:
                return spriteIconFireGun;
            case PLAYER_ARMS.GATLING_GUN:
                return spriteIconGatlingGun;
            case PLAYER_ARMS.NUCLEAR_GUN:
                return spriteIconNuclearGun;
            case PLAYER_ARMS.PISTOL:
                return spriteIconPistol;
            case PLAYER_ARMS.SHOTGUN:
                return spriteIconShotgun;
            case PLAYER_ARMS.THUNDER_GUN:
                return spriteIconThunderGun;
            default:
                return null;
        }
    }

    public void ResetButtonItem()
    {
        for (int i = 0; i < listItemButton.Length; i++)
        {
            listItemButtonRT[i].DOAnchorPosX(-0.9f, 0.5f);
            listItemButton[i].gameObject.transform.GetChild(0).gameObject.SetActive(false);
        }
    }

    #region UIUpgradeWeaponHandle

    /// <summary>
    /// Hien thi man nang cap vu khi
    /// </summary>
    /// <param name="action">Action.</param>
    public  void OnShow(Action action = null)
    {
        isOpening = true;
        onLockButton();
        for (int i = 0; i < listItemButton.Length; i++)
        {
            listItemButton[i].enabled = false;
            listItemButtonRT[i].DOAnchorPosX(-0.9f, 0.5f);
            listItemButton[i].gameObject.transform.GetChild(0).gameObject.SetActive(false);
        }
        Sequence effectSequence = DOTween.Sequence();
        effectSequence.AppendInterval(0.5f);
        backButton.gameObject.SetActive(true);
        circleIcon.rectTransform.DORotate(new Vector3(0f, 0f, 180f), 1f);
        effectSequence.Join(weaponPanel.DOAnchorPosX(220f, 1f));
        effectSequence.Join(playerLigh1.DOFade(0f, 0.5f));
        effectSequence.Join(lineIcon.rectTransform.DOAnchorPosX(-456f, 0.5f).OnComplete(() =>
                {
                    effectSequence.Join(gunBarImage.rectTransform.DOSizeDelta(new Vector2(584f, 120f), 0.5f)); 
                    effectSequence.Join(gunBarImage.rectTransform.DOAnchorPos(new Vector3(-60f, 0f, 0f), 0.5f));
                    effectSequence.Join(circleIcon.rectTransform.DORotate(Vector3.zero, 1f));
                }));
        effectSequence.Join(circleIcon.rectTransform.DORotate(Vector3.zero, 1f));
        effectSequence.Join(buttonPanel1.DOAnchorPosY(0f, 0.5f));
        effectSequence.Join(buttonPanel.DOAnchorPosY(-720f, 0.5f).OnComplete(() => buttonPanel.gameObject.SetActive(false)));
        effectSequence.Join(statusPanel.DOAnchorPosX(1800f, 0.5f).OnComplete(() => statusPanel.gameObject.SetActive(false)));
        effectSequence.Join(roomStickman.rectTransform.DOAnchorPosX(-400f, 0.5f));
        effectSequence.PrependCallback(() =>
            {
                buttonPanel1.gameObject.SetActive(true);
                weaponPanel.gameObject.SetActive(true);
            });
        effectSequence.OnComplete(() =>
            {
                onUnlockButton();
                if (action != null)
                    action();
            });
        onSettingButton();
    }

    /// <summary>
    /// An man nang cap vu khi
    /// </summary>
    public void OnHide(Action action = null)
    {
        HomeUIHandle.Instance.OnPlayAnimationUseNewWeapon(playerArm, PLAYER_ARMS.SWORD);
        playerArm = PLAYER_ARMS.SWORD;
        for (int i = 0; i < weaponItemButton.Length; i++)
        {
            if (i == 0)
            {
                weaponItemButton[i].image.color = GameSupportUI.HexToColor("73a6cd");
            }
            else
            {
                weaponItemButton[i].image.color = GameSupportUI.HexToColor("b49b6a");
            }
        }

        isOpening = false;
        onLockButton();
        for (int i = 0; i < listItemButton.Length; i++)
        {
            listItemButton[i].enabled = true;
        }
        circleIcon.rectTransform.DORotate(new Vector3(0f, 0f, 180f), 1f);
        Sequence effectSequence = DOTween.Sequence();
        buttonPanel.gameObject.SetActive(true);
        statusPanel.gameObject.SetActive(true);
        effectSequence.Append(gunBarImage.rectTransform.DOAnchorPosX(-590f, 0.5f)
			.OnComplete(() =>
                {
                    lineIcon.rectTransform.DOAnchorPosX(-213f, 0.5f);
                    backButton.gameObject.SetActive(false);
                    circleIcon.rectTransform.DORotate(Vector3.zero, 1f);
                }));
        effectSequence.Join(weaponPanel.DOAnchorPosX(1200f, 0.5f).OnComplete(() => weaponPanel.gameObject.SetActive(false)));
        effectSequence.Join(buttonPanel1.DOAnchorPosY(-900f, 1f).OnComplete(() => buttonPanel1.gameObject.SetActive(false)));
        effectSequence.Join(statusPanel.DOAnchorPosX(-52f, 0.5f));
        effectSequence.Join(buttonPanel.DOAnchorPosY(0f, 0.5f));
        effectSequence.Join(circleIcon.rectTransform.DORotate(new Vector3(0f, 0f, 180f), 1f));
        effectSequence.Join(roomStickman.rectTransform.DOAnchorPosX(0f, 0.5f).OnComplete(() =>
                {
                    playerLigh1.DOFade(255f, 0.5f);
                }));
        effectSequence.OnComplete(() =>
            {
                onUnlockButton();
                if (action != null)
                    action();
            });
        onResetScrollView();
        onUnSettingButton();
        onSetUpgradeWeaponUI(GameSupport.PLAYER_ARMS.SWORD);
    }

    #endregion
}
