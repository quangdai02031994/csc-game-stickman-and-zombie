﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;
using GameSupport;
using Spine;

public class PlayerUIAnimation : MonoBehaviour
{
    public SkeletonGraphic skeletonBody;

    public bool isPreviewAttack = false;


    #region Behaviour

    void Start()
    {
        skeletonBody.AnimationState.Start += onStartAnimation;

        skeletonBody.AnimationState.SetAnimation(0, PlayerAnimationName.body_use_sword, false);
        skeletonBody.AnimationState.AddAnimation(0, PlayerAnimationName.body_idle_sword, true, 0);
    }

    void OnDestroy()
    {
        skeletonBody.AnimationState.Start -= onStartAnimation;
    }

    #endregion

    #region Publish Methods

    public void OnUseNewArm(PLAYER_ARMS currentArm, PLAYER_ARMS nextArm)
    {
        onPlayAnimation(onAnimationDropWeapon(currentArm), false);
        onAddAnimation(onAnimationUseWeapon(nextArm), false);
        onAddAnimation(onAnimationIdleWeapon(nextArm), true);
    }

    public void OnShow()
    {
        this.gameObject.SetActive(true);
    }

    public void OnHide()
    {
        this.gameObject.SetActive(false);
    }

    public void OnPlayAnimation(PLAYER_ARMS arm)
    {
        if (isPreviewAttack)
        {
            return;
        }   

        switch (arm)
        {
            case PLAYER_ARMS.AK47:
                onPlayAnimation(PlayerAnimationName.body_hit_ak47, true);
                onAddAnimation(PlayerAnimationName.body_idle_ak47, true, 1);
                break;
            case PLAYER_ARMS.AWM_GUN:
                onPlayAnimation(PlayerAnimationName.body_aim_awm_gun, false);
                onAddAnimation(PlayerAnimationName.body_hit_awm_gun, false, 0.5f);
                onAddAnimation(PlayerAnimationName.body_idle_awm_gun, true);
                break;
            case PLAYER_ARMS.BAZOKA:
                onPlayAnimation(PlayerAnimationName.body_hit_bazoka, false);
                onAddAnimation(PlayerAnimationName.body_idle_bazoka, true);
                break;
            case PLAYER_ARMS.BOW:
                onPlayAnimation(PlayerAnimationName.body_hit_bow, false);
                onAddAnimation(PlayerAnimationName.body_idle_bow, true);
                break;
            case PLAYER_ARMS.DUAL_PISTOL:
                onPlayAnimation(PlayerAnimationName.body_hit_two_pistol_down, false);
                onAddAnimation(PlayerAnimationName.body_hit_two_pistol_up, false);
                onAddAnimation(PlayerAnimationName.body_idle_two_pistol, true);
                break;
            case PLAYER_ARMS.FIRE_GUN:
                onPlayAnimation(PlayerAnimationName.body_hit_firegun, false);
                onAddAnimation(PlayerAnimationName.body_idle_firegun, true);
                break;
            case PLAYER_ARMS.GATLING_GUN:
                onPlayAnimation(PlayerAnimationName.body_hit_gatling_gun, true);
                onAddAnimation(PlayerAnimationName.body_idle_gatling_gun, true, 1);
                break;
            case PLAYER_ARMS.NUCLEAR_GUN:
                onPlayAnimation(PlayerAnimationName.body_hit_nuclear_gun, false);
                onAddAnimation(PlayerAnimationName.body_idle_nuclear_gun, true);
                break;
            case PLAYER_ARMS.PISTOL:
                onPlayAnimation(PlayerAnimationName.body_hit_pistol, false);
                onAddAnimation(PlayerAnimationName.body_idle_pistol, true);
                break;
            case PLAYER_ARMS.SHOTGUN:
                onPlayAnimation(PlayerAnimationName.body_hit_shotgun, false);
                onAddAnimation(PlayerAnimationName.body_idle_shotgun, true);
                break;
            case PLAYER_ARMS.SWORD:
                onPlayAnimation(PlayerAnimationName.body_hit1_sword, false);
                onAddAnimation(PlayerAnimationName.body_hit2_sword, false);
                onAddAnimation(PlayerAnimationName.body_hit3_sword, false);
                onAddAnimation(PlayerAnimationName.body_idle_sword, true);
                break;
            case PLAYER_ARMS.THUNDER_GUN:
                onPlayAnimation(PlayerAnimationName.body_hit_thundergun, false);
                onAddAnimation(PlayerAnimationName.body_idle_thundergun, true);
                break;
        }

    }

    #endregion

    #region Private Methods

    private void onStartAnimation(TrackEntry trackEntry)
    {
        if (trackEntry.Animation.Name.IndexOf("atk") > -1)
        {
            isPreviewAttack = true;
        }
        else if (trackEntry.Animation.Name.IndexOf("aim") > -1)
        {
            isPreviewAttack = true;
        }
        else if (trackEntry.Animation.Name.IndexOf("idle") > -1)
        {
            isPreviewAttack = false;
        }
        else if (trackEntry.Animation.Name.IndexOf("hit") > -1)
        {
            isPreviewAttack = true;
        }
    }


    private void onAddAnimation(string animationName, bool loop = true, float delay = 0)
    {
        skeletonBody.AnimationState.AddAnimation(0, animationName, loop, delay);
    }


    private void onPlayAnimation(string animationName, bool loop = false)
    {
        skeletonBody.AnimationState.SetAnimation(0, animationName, loop);
    }

    private string onAnimationDropWeapon(PLAYER_ARMS arm)
    {
        switch (arm)
        {
            case PLAYER_ARMS.AK47:
                return PlayerAnimationName.body_drop_ak47;
            case PLAYER_ARMS.AWM_GUN:
                return PlayerAnimationName.body_drop_awm_gun;
            case PLAYER_ARMS.BAZOKA:
                return PlayerAnimationName.body_drop_bazoka;
            case PLAYER_ARMS.BOW:
                return PlayerAnimationName.body_drop_bow;
            case PLAYER_ARMS.DUAL_PISTOL:
                return PlayerAnimationName.body_drop_two_pistol;
            case PLAYER_ARMS.FIRE_GUN:
                return PlayerAnimationName.body_drop_firegun;
            case PLAYER_ARMS.GATLING_GUN:
                return PlayerAnimationName.body_drop_gatling_gun;
            case PLAYER_ARMS.NUCLEAR_GUN:
                return PlayerAnimationName.body_drop_nuclear_gun;
            case PLAYER_ARMS.PISTOL:
                return PlayerAnimationName.body_drop_pistol;
            case PLAYER_ARMS.SHOTGUN:
                return PlayerAnimationName.body_drop_shotgun;
            case PLAYER_ARMS.SWORD:
                return PlayerAnimationName.body_drop_sword;
            case PLAYER_ARMS.THUNDER_GUN:
                return PlayerAnimationName.body_drop_thundergun;
            default:
                return PlayerAnimationName.body_drop_sword;
                break;
        }
    }

    private string onAnimationUseWeapon(PLAYER_ARMS arm)
    {
        switch (arm)
        {
            case PLAYER_ARMS.AK47:
                return PlayerAnimationName.body_use_ak47;
            case PLAYER_ARMS.AWM_GUN:
                return PlayerAnimationName.body_use_awm_gun;
            case PLAYER_ARMS.BAZOKA:
                return PlayerAnimationName.body_use_bazoka;
            case PLAYER_ARMS.BOW:
                return PlayerAnimationName.body_use_bow;
            case PLAYER_ARMS.DUAL_PISTOL:
                return PlayerAnimationName.body_use_two_pistol;
            case PLAYER_ARMS.FIRE_GUN:
                return PlayerAnimationName.body_use_firegun;
            case PLAYER_ARMS.GATLING_GUN:
                return PlayerAnimationName.body_use_gatling_gun;
            case PLAYER_ARMS.NUCLEAR_GUN:
                return PlayerAnimationName.body_use_nuclear_gun;
            case PLAYER_ARMS.PISTOL:
                return PlayerAnimationName.body_use_pistol;
            case PLAYER_ARMS.SHOTGUN:
                return PlayerAnimationName.body_use_shotgun;
            case PLAYER_ARMS.SWORD:
                return PlayerAnimationName.body_use_sword;
            case PLAYER_ARMS.THUNDER_GUN:
                return PlayerAnimationName.body_use_thundergun;
            default:
                return PlayerAnimationName.body_use_sword;
                break;
        }
    }

    private string onAnimationIdleWeapon(PLAYER_ARMS arm)
    {
        switch (arm)
        {
            case PLAYER_ARMS.AK47:
                return PlayerAnimationName.body_idle_ak47;
            case PLAYER_ARMS.AWM_GUN:
                return PlayerAnimationName.body_idle_awm_gun;
            case PLAYER_ARMS.BAZOKA:
                return PlayerAnimationName.body_idle_bazoka;
            case PLAYER_ARMS.BOW:
                return PlayerAnimationName.body_idle_bow;
            case PLAYER_ARMS.DUAL_PISTOL:
                return PlayerAnimationName.body_idle_two_pistol;
            case PLAYER_ARMS.FIRE_GUN:
                return PlayerAnimationName.body_idle_firegun;
            case PLAYER_ARMS.GATLING_GUN:
                return PlayerAnimationName.body_idle_gatling_gun;
            case PLAYER_ARMS.NUCLEAR_GUN:
                return PlayerAnimationName.body_idle_nuclear_gun;
            case PLAYER_ARMS.PISTOL:
                return PlayerAnimationName.body_idle_pistol;
            case PLAYER_ARMS.SHOTGUN:
                return PlayerAnimationName.body_idle_shotgun;
            case PLAYER_ARMS.SWORD:
                return PlayerAnimationName.body_idle_sword;
            case PLAYER_ARMS.THUNDER_GUN:
                return PlayerAnimationName.body_idle_thundergun;
            default:
                return PlayerAnimationName.body_idle_sword;
                break;
        }
    }

    //    private string onAnimationAttackWeapon(PLAYER_ARMS arm)
    //    {
    //        switch (arm)
    //        {
    //            case PLAYER_ARMS.AK47:
    //                return PlayerAnimationName.body_hit_ak47;
    //            case PLAYER_ARMS.AWM_GUN:
    //                return PlayerAnimationName.body_hit_awm_gun;
    //            case PLAYER_ARMS.BAZOKA:
    //                return PlayerAnimationName.body_hit_bazoka;
    //            case PLAYER_ARMS.BOW:
    //                return PlayerAnimationName.body_hit_bow;
    //            case PLAYER_ARMS.DUAL_PISTOL:
    //                return PlayerAnimationName.tw
    //            default:
    //                break;
    //        }
    //    }

    #endregion

}
