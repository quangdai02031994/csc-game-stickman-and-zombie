﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using GameSupport;
using UnityEngine.Events;
using DG.Tweening;

public class GamePlayUIHandle : MonoBehaviour
{
    public static GamePlayUIHandle Instance{ get; private set; }

    /// <summary>
    /// Di chuyển player theo chiều x
    /// </summary>
    /// <value>The x move.</value>
    public float xMove    { get { return joyStick.MoveX; } }

    /// <summary>
    /// Di chuyển player theo chiều y
    /// </summary>
    /// <value>The y move.</value>
    public float yMove    { get { return joyStick.MoveY; } }

    public GamePlayUIEffect uiEffectHandle;

    public GamePlayUIPopupHandle popupHandle;

    public GamePlayUIGameOver uiGameOver;

    public UIMultipleKillHandle uiMultipleKillHandle;

    public Text txtMonsterDead;

    [Header("Bottom Left")]
    public VirtualJoyStick joyStick;

    [Header("Bottom Right")]
    public EventTrigger btnAttackLeft;
    public EventTrigger btnAttackRight;
    public EventTrigger btnChangeGun;
    public EventTrigger btnChangeSword;
    public EventTrigger btnItem;
    public EventTrigger btnItem2;
    public Image[] imgItemIcon = new Image[2];
    public Text[] txtItemQuantity = new Text[2];

    [Header("Top Left")]
    public RectTransform rectPlayerHPBar;
    public Image imgPlayerHPBar;
    public Text txtPlayerHP;
    public EventTrigger btnPauseGame;
    public GamePlayUIGunEquiped[] ListUIGunEquiped = new GamePlayUIGunEquiped[2];
    public Image imgIconSpeed;
    public Text txtTimeSpeed;
    public Image imgIconStrong;
    public Text txtTimeStrong;
    public Image imgIconIron;
    public Text txtTimeIron;

    [Header("Top Right")]
    public Text txtGold;
    public ModelItem[] ListItemEquiped;

    [Header("Game Ready")]
    public RectTransform rectUIGameReady;
    public Button btnReady;
    public RectTransform rectImageReady;
    public Image imgBGReady;


    private List<PLAYER_ARMS> ListArmIDEquiped;
    private float minAnchorBarPos = -280;
    private float maxAnchorHPBar = 0;
    private float timeTween = 0.3f;
    private float gold;

    #region Behaviour

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            Input.multiTouchEnabled = true;
        }   
    }

    void Start()
    {
        InitButtonChangeGun();
        InitButtonChangeSword();
        InitButtonPauseGame();
        InitButtonItem();
        InitButtonItem2();
        InitButtonAttackLeft();
        InitButtonAttackRight();

        InitButtonReady();

        onSettingItemEquiped();
        onSettingArmEquiped();
        PlayerController.Instance.playerGameOver += onGameOver;
        gold = 0;
        onUpdateGold();

    }

    void OnDestroy()
    {
        PlayerController.Instance.playerGameOver -= onGameOver;
    }

    void OnApplicationPause(bool isPause)
    {
        if (GameManager.Instance.GameState != GAME_STATE.PLAYING)
            return;
        if (isPause)
        {
            onReleaseAttackLeft();
            onReleaseAttackRight();
            joyStick.OnPoiterUpJoyStick();
            onClickButtonPauseGame();
        }
    }

    void Update()
    {
        if (GameManager.Instance.GameState == GAME_STATE.PLAYING)
        {
            #if UNITY_EDITOR

            if (Input.GetKeyDown(KeyCode.A))
            {
                onPressAttackLeft();

            }
            else if (Input.GetKeyDown(KeyCode.F))
            {
                onPressAttackRight();
            }
            else if (Input.GetKeyDown(KeyCode.Space))
            {
                onClickChangeGun();
            }
            else if (Input.GetKeyDown(KeyCode.M))
            {
                onClickChangeSword();
            }

            if (Input.GetKeyUp(KeyCode.A))
            {
                onReleaseAttackLeft();
            }
            else if (Input.GetKeyUp(KeyCode.F))
            {
                onReleaseAttackRight();
            }

            #endif
            if (PlayerController.Instance.isAttackLeft && !PlayerController.Instance.isAttackRight)
            {
                onAttack(PLAYER_LEG_STATE.ATTACK_LEFT, PLAYER_BODY_STATE.ATTACK_LEFT);
            }
            else if (!PlayerController.Instance.isAttackLeft && PlayerController.Instance.isAttackRight)
            {
                onAttack(PLAYER_LEG_STATE.ATTACK_RIGHT, PLAYER_BODY_STATE.ATTACK_RIGHT);
            }

        }
    }

    #endregion

    #region Publish Methods

    public void OnGameWin(int rank)
    {
        Debug.Log("Game Win Rank: " + rank);
        popupHandle.OnWinGame(rank, gold);
        gold = 0;
        OnHideSalivaUI();

    }

    public void OnShowItemIron(int time)
    {
        imgIconIron.gameObject.SetActive(true);
        txtTimeIron.text = time + "s";
    }

    public void OnHideItemIron()
    {
        imgIconIron.gameObject.SetActive(false);
        txtTimeIron.text = "0s";
    }

    public void OnShowItemStrong(int time)
    {
        imgIconStrong.gameObject.SetActive(true);
        txtTimeStrong.text = time + "s";
    }

    public void OnHideItemStrong()
    {
        imgIconStrong.gameObject.SetActive(false);
        txtTimeStrong.text = "0s";
    }

    public void OnShowItemSpeed(int time)
    {
        imgIconSpeed.gameObject.SetActive(true);
        txtTimeSpeed.text = time + "s";
    }

    public void OnHideItemSpeed()
    {
        imgIconSpeed.gameObject.SetActive(false);
        txtTimeSpeed.text = "0s";
    }

    public void OnRewardGold(int additionalGold)
    {
        Vector3 startPos = CameraHandle.Instance.OnGetPlayerScreenPos();
        GamePlayUIGoldReward.Instance.OnRewardGold(additionalGold, startPos);
        gold += additionalGold;
        onUpdateGold();
    }

    public void OnMultipeKill(int monsterKill)
    {
        if (monsterKill >= 2)
        {
            uiMultipleKillHandle.gameObject.SetActive(true);
            uiMultipleKillHandle.OnPlayAnimation(monsterKill);
        }
    }

    public void OnShowSalivaUI()
    {
        uiEffectHandle.OnShowSalivaUI();
    }

    public void OnHideSalivaUI()
    {
        uiEffectHandle.OnHideSalivaUI();
    }

    public void OnShowBloodUI()
    {
        uiEffectHandle.OnShowBloodUI();
    }

    public void OnAttack(PLAYER_ARMS type)
    {
        switch (type)
        {
            case PLAYER_ARMS.SWORD:
                
                break;
            default:
                ListUIGunEquiped[0].numberBullet--;
                ListUIGunEquiped[0].OnUpdateBullet(ListUIGunEquiped[0].numberBullet);
                break;
        }
    }

    public int OnGetNumberBullet(PLAYER_ARMS type)
    {
        switch (type)
        {
            case PLAYER_ARMS.SWORD:
                PlayerController.Instance.OnHideBulletEmpty();
                return 0;
            default:
                if (ListUIGunEquiped[0].type == type)
                {
                    if (ListUIGunEquiped[0].numberBullet > 0)
                    {
                        PlayerController.Instance.OnHideBulletEmpty();
                    }
                    else
                    {
                        PlayerController.Instance.OnShowBulletEmpty();
                    }
                    return ListUIGunEquiped[0].numberBullet;
                }
                else
                    return 0;
        }
    }

    public void OnUpdatePlayerArm(PLAYER_ARMS arm)
    {
        if (arm != PLAYER_ARMS.SWORD)
        {
            if (arm == ListUIGunEquiped[1].type)
            {
                int numberBullet = ListUIGunEquiped[1].numberBullet;
                int numberBullet2 = ListUIGunEquiped[0].numberBullet;
                PLAYER_ARMS _gun = ListUIGunEquiped[1].type;
                PLAYER_ARMS _gun2 = ListUIGunEquiped[0].type;

                ListUIGunEquiped[0].OnSetData(_gun, numberBullet);
                ListUIGunEquiped[1].OnSetData(_gun2, numberBullet2);
            }
        }
    }

    public void OnUpdateNumberBullet()
    {
        ListUIGunEquiped[0].OnResetBullet();
    }

    public void OnShowReady()
    {
        rectImageReady.anchoredPosition = new Vector2(-1280, rectImageReady.anchoredPosition.y);
        rectUIGameReady.gameObject.SetActive(true);
        rectImageReady.DOMoveX(0, timeTween).SetEase(Ease.OutQuint).OnComplete(() =>
            {
                btnReady.enabled = true;
            });
    }

    public void OnUpdatePlayerHP(float _value, float currentHP, float maxHP)
    {
        if (_value <= 0)
        {
            if (imgPlayerHPBar.rectTransform.anchoredPosition.x != minAnchorBarPos)
            {
                imgPlayerHPBar.rectTransform.DOAnchorPosX(minAnchorBarPos, timeTween);
            }
        }
        else if (_value >= 1)
        {
            if (imgPlayerHPBar.rectTransform.anchoredPosition.x != maxAnchorHPBar)
            {
                imgPlayerHPBar.rectTransform.DOAnchorPosX(maxAnchorHPBar, timeTween);
            }
        }
        else
        {
            float _posX = (1 - _value) * minAnchorBarPos;
            if (imgPlayerHPBar.rectTransform.anchoredPosition.x != _posX)
            {
                imgPlayerHPBar.rectTransform.DOAnchorPosX(_posX, timeTween);
            }
        }
        if (currentHP > 0)
        {
            txtPlayerHP.text = currentHP + "/" + maxHP;
        }
        else if (currentHP > maxHP)
        {
            txtPlayerHP.text = maxHP + "/" + maxHP;
        }
        else
        {
            txtPlayerHP.text = "0/" + maxHP;

        }
        if (_value < 0.3f)
        {
            imgPlayerHPBar.color = GameSupportUI.HexToColor("e87171");
        }
        else if (_value < 0.75f)
        {
            imgPlayerHPBar.color = GameSupportUI.HexToColor("e7e871");
        }
        else
        {
            imgPlayerHPBar.color = GameSupportUI.HexToColor("71e8c2");
        }

    }


    public void OnMonsterDead(int count)
    {
        txtMonsterDead.text = count.ToString();
    }

    public void OnLockControl()
    {
        joyStick.OnLockJoyStick();
        btnAttackLeft.enabled = false;
        btnAttackRight.enabled = false;
        btnChangeGun.enabled = false;
        btnChangeSword.enabled = false;
        btnItem.enabled = false;
        btnItem2.enabled = false;
    }

    public void OnUnLockControl()
    {
        joyStick.OnUnlockJoyStick();
        btnAttackLeft.enabled = true;
        btnAttackRight.enabled = true;
        btnChangeGun.enabled = true;
        btnChangeSword.enabled = true;
        btnItem.enabled = true;
        btnItem2.enabled = true;
    }




    #endregion

    #region Events Trigger

    private void onClickButtonPauseGame()
    {
        Debug.Log("onClickButtonPauseGame");
        if (GameManager.Instance.GameState == GAME_STATE.START)
            return;
        GameManager.Instance.OnSetGameState(GAME_STATE.PAUSE);
        popupHandle.OnPauseGame();
    }

    private void onClickButtonItem2()
    {
        if (ListItemEquiped[1].type != ITEM_TYPE.CARTRIDGE_BOX)
        {
            ListItemEquiped[1].quantity--;
            if (ListItemEquiped[1].quantity > 0)
            {
                txtItemQuantity[1].text = "x" + ListItemEquiped[1].quantity;
            }
            else
            {
                btnItem2.transform.parent.gameObject.SetActive(false);   
            }
            PlayerController.Instance.OnUseItem(ListItemEquiped[1].type);
            GameManager.Instance.OnUseItem(ListItemEquiped[1].type);
        }
        else
        {
            if (PlayerController.Instance.playerArms == PLAYER_ARMS.SWORD)
                return;
            ListItemEquiped[1].quantity--;
            if (ListItemEquiped[1].quantity > 0)
            {
                txtItemQuantity[1].text = "x" + ListItemEquiped[1].quantity;
            }
            else
            {
                btnItem2.transform.parent.gameObject.SetActive(false);   
            }
            PlayerController.Instance.OnUseItem(ListItemEquiped[1].type);
            GameManager.Instance.OnUseItem(ListItemEquiped[1].type);
        }
    }

    private void onClickButtonItem()
    {
        if (ListItemEquiped[0].type != ITEM_TYPE.CARTRIDGE_BOX)
        {
            ListItemEquiped[0].quantity--;
            if (ListItemEquiped[0].quantity > 0)
            {
                txtItemQuantity[0].text = "x" + ListItemEquiped[0].quantity;
            }
            else
            {
                btnItem.transform.parent.gameObject.SetActive(false);   
            }
            PlayerController.Instance.OnUseItem(ListItemEquiped[0].type);
            GameManager.Instance.OnUseItem(ListItemEquiped[0].type);
        }
        else
        {
            if (PlayerController.Instance.playerArms == PLAYER_ARMS.SWORD)
                return;
            ListItemEquiped[0].quantity--;
            if (ListItemEquiped[0].quantity > 0)
            {
                txtItemQuantity[0].text = "x" + ListItemEquiped[0].quantity;
            }
            else
            {
                btnItem.transform.parent.gameObject.SetActive(false);   
            }
            PlayerController.Instance.OnUseItem(ListItemEquiped[0].type);
            GameManager.Instance.OnUseItem(ListItemEquiped[0].type);
        }
    }

    private void onClickButtonChangeGun(PointerEventData data)
    {
        onClickChangeGun();
    }

    private void onClickButtonChangeSword(PointerEventData data)
    {
        onClickChangeSword();
    }

    private void onAttackLeftPress(PointerEventData data)
    {
        if (GameManager.Instance.GameState != GAME_STATE.PLAYING)
            return;
        if (PlayerController.Instance.playerArms == PLAYER_ARMS.SWORD)
        {
            onPressAttackLeft();
        }
        else
        {
            if (ListUIGunEquiped[0].numberBullet > 0)
            {
                onPressAttackLeft();
            }
        }
    }

    private void onAttackRightPress(PointerEventData data)
    {
        if (GameManager.Instance.GameState != GAME_STATE.PLAYING)
            return;
        if (PlayerController.Instance.playerArms == PLAYER_ARMS.SWORD)
        {
            onPressAttackRight();
        }
        else
        {
            if (ListUIGunEquiped[0].numberBullet > 0)
            {
                onPressAttackRight();
            }
        }
    }

    private void onAttackLeftRelease(PointerEventData data)
    {
        onReleaseAttackLeft();
    }

    private void onAttackRightRelease(PointerEventData data)
    {
        onReleaseAttackRight();
    }

    #endregion

    #region Init Event Trigger Methods

    private void InitButtonItem2()
    {
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.PointerClick;
        entry.callback.AddListener((data) =>
            {
                onClickButtonItem2();
            });
        btnItem2.triggers.Add(entry);
    }


    private void InitButtonItem()
    {
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.PointerClick;
        entry.callback.AddListener((data) =>
            {
                onClickButtonItem();
            });
        btnItem.triggers.Add(entry);
    }

    private void InitButtonPauseGame()
    {
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.PointerClick;
        entry.callback.AddListener((data) =>
            {
                onClickButtonPauseGame();
            });
        btnPauseGame.triggers.Add(entry);
    }

    private void InitButtonChangeGun()
    {
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.PointerClick;
        entry.callback.AddListener((data) =>
            {
                onClickButtonChangeGun((PointerEventData)data);
            });
        btnChangeGun.triggers.Add(entry);
    }

    private void InitButtonChangeSword()
    {
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.PointerClick;
        entry.callback.AddListener((data) =>
            {
                onClickButtonChangeSword((PointerEventData)data);
            });
        btnChangeSword.triggers.Add(entry);
    }

    private void InitButtonAttackLeft()
    {
        EventTrigger.Entry entry_pointer_down = new EventTrigger.Entry();
        entry_pointer_down.eventID = EventTriggerType.PointerDown;
        entry_pointer_down.callback.AddListener((data) =>
            {
                onAttackLeftPress((PointerEventData)data);
            });
        btnAttackLeft.triggers.Add(entry_pointer_down);

        EventTrigger.Entry entry_pointer_up = new EventTrigger.Entry();
        entry_pointer_up.eventID = EventTriggerType.PointerUp;
        entry_pointer_up.callback.AddListener((data) =>
            {
                onAttackLeftRelease((PointerEventData)data);
            });
        btnAttackLeft.triggers.Add(entry_pointer_up);

    }

    private void InitButtonReady()
    {
        btnReady.onClick.AddListener(() =>
            {
                onClickButtonReady();
            });
        btnReady.enabled = false;
    }

    private void InitButtonAttackRight()
    {
        EventTrigger.Entry entry_pointer_down = new EventTrigger.Entry();
        entry_pointer_down.eventID = EventTriggerType.PointerDown;
        entry_pointer_down.callback.AddListener((data) =>
            {
                onAttackRightPress((PointerEventData)data);
            });
        btnAttackRight.triggers.Add(entry_pointer_down);

        EventTrigger.Entry entry_pointer_up = new EventTrigger.Entry();
        entry_pointer_up.eventID = EventTriggerType.PointerUp;
        entry_pointer_up.callback.AddListener((data) =>
            {
                onAttackRightRelease((PointerEventData)data);
            });
        btnAttackRight.triggers.Add(entry_pointer_up);

    }

    #endregion

    #region Private Methods

    private void onUpdateGold()
    {
        txtGold.text = gold.ToString("#,##0");
    }

    private void onGameOver()
    {
        uiGameOver.OnShowGameOver();
        OnHideSalivaUI();
    }

    private void onSettingArmEquiped()
    {
        ListArmIDEquiped = GameManager.Instance.OnGetListArmEquiped();
        switch (ListArmIDEquiped.Count)
        {
            case 1:
                ListUIGunEquiped[0].OnSetData(PLAYER_ARMS.SWORD, 0);
                ListUIGunEquiped[1].OnSetData(PLAYER_ARMS.SWORD, 0);
                btnChangeGun.transform.parent.gameObject.SetActive(false);
                btnChangeSword.transform.parent.gameObject.SetActive(false);
                break;
            case 2:
                ListUIGunEquiped[0].OnSetData(ListArmIDEquiped[1], GameManager.Instance.OnGetPlayerGun(ListArmIDEquiped[1]).numberBullet);
                ListUIGunEquiped[1].OnSetData(PLAYER_ARMS.SWORD, 0);
                btnChangeGun.transform.parent.gameObject.SetActive(true);
                btnChangeSword.transform.parent.gameObject.SetActive(true);
                break;
            case 3:
                ListUIGunEquiped[0].OnSetData(ListArmIDEquiped[1], GameManager.Instance.OnGetPlayerGun(ListArmIDEquiped[1]).numberBullet);
                ListUIGunEquiped[1].OnSetData(ListArmIDEquiped[2], GameManager.Instance.OnGetPlayerGun(ListArmIDEquiped[2]).numberBullet);
                btnChangeGun.transform.parent.gameObject.SetActive(true);
                btnChangeSword.transform.parent.gameObject.SetActive(true);
                break;
        }
    }

    private void onSettingItemEquiped()
    {
        ListItemEquiped = new ModelItem[2];
        ModelItem modelItem = GameManager.Instance.OnGetModelItems(0);
        ModelItem modelItem2 = GameManager.Instance.OnGetModelItems(1);
        if (modelItem.type == ITEM_TYPE.NONE && modelItem2.type != ITEM_TYPE.NONE)
        {
            onSetDataItemEquiped(0, modelItem2);
            ListItemEquiped[0] = modelItem2;

            onSetDataItemEquiped(1, modelItem);
            ListItemEquiped[1] = modelItem;
        }
        else
        {
            onSetDataItemEquiped(0, modelItem);
            onSetDataItemEquiped(1, modelItem2);
            ListItemEquiped[0] = modelItem;
            ListItemEquiped[1] = modelItem2;
        }
    }

    private void onAttack(PLAYER_LEG_STATE legState, PLAYER_BODY_STATE bodyState)
    {
        if (GameManager.Instance.GameState != GAME_STATE.PLAYING)
            return;
        if (PlayerController.Instance.playerBodyState != bodyState)
        {
            PlayerController.Instance.playerBodyState = bodyState;
            PlayerController.Instance.SetAnimationBody(bodyState);
            PlayerController.Instance.playerLegState = legState;
        }
    }

    private void onPressAttackLeft()
    {
        btnAttackLeft.transform.localScale = Vector3.one * 1.2f;
        switch (PlayerController.Instance.playerArms)
        {
            case PLAYER_ARMS.SWORD:
                if (!PlayerController.Instance.isAttacking)
                    PlayerController.Instance.isAttackLeft = true;
                if (PlayerController.Instance.playerBodyState == PLAYER_BODY_STATE.ATTACK_LEFT)
                {
                    PlayerController.Instance.countAttackLeft++;
                }
                break;
            case PLAYER_ARMS.PISTOL:
                if (!PlayerController.Instance.isAttacking)
                {
                    PlayerController.Instance.isAttackLeft = true;
                }
                if (PlayerController.Instance.playerBodyState == PLAYER_BODY_STATE.ATTACK_LEFT)
                {
                    PlayerController.Instance.countAttackLeft++;
                }
                break;
            case PLAYER_ARMS.BOW:
                if (!PlayerController.Instance.isAttacking)
                {
                    PlayerController.Instance.isAttackLeft = true;
                }
                break;
            case PLAYER_ARMS.AK47:
                if (!PlayerController.Instance.isAttacking)
                {
                    PlayerController.Instance.isAttackLeft = true;
                }
                break;
            case PLAYER_ARMS.FIRE_GUN:
                if (!PlayerController.Instance.isAttacking)
                {
                    PlayerController.Instance.isAttackLeft = true;
                }
                break;
            case PLAYER_ARMS.THUNDER_GUN:
                if (!PlayerController.Instance.isAttacking)
                {
                    PlayerController.Instance.isAttackLeft = true;
                }
                break;
            case PLAYER_ARMS.GATLING_GUN:
                if (!PlayerController.Instance.isAttacking)
                {
                    PlayerController.Instance.isAttackLeft = true;
                }
                break;
            case PLAYER_ARMS.DUAL_PISTOL:
                if (!PlayerController.Instance.isAttacking)
                {
                    PlayerController.Instance.isAttackLeft = true;
                }
                if (PlayerController.Instance.playerBodyState == PLAYER_BODY_STATE.ATTACK_LEFT)
                {
                    PlayerController.Instance.countAttackLeft++;
                }
                break;
            case PLAYER_ARMS.SHOTGUN:
                if (!PlayerController.Instance.isAttacking)
                {
                    PlayerController.Instance.isAttackLeft = true;
                }
                break;
            case PLAYER_ARMS.AWM_GUN:
                if (!PlayerController.Instance.isAttacking)
                {
                    PlayerController.Instance.isAttackLeft = true;
                }
                break;
            case PLAYER_ARMS.BAZOKA:
                if (!PlayerController.Instance.isAttacking)
                {
                    PlayerController.Instance.isAttackLeft = true;
                }
                break;
            case PLAYER_ARMS.NUCLEAR_GUN:
                if (!PlayerController.Instance.isAttacking)
                    PlayerController.Instance.isAttackLeft = true;
                if (PlayerController.Instance.playerBodyState == PLAYER_BODY_STATE.ATTACK_LEFT)
                {
                    PlayerController.Instance.countAttackLeft++;
                }
                break;
        }
    }

    private void onPressAttackRight()
    {
        btnAttackRight.transform.localScale = Vector3.one * 1.2f;
        switch (PlayerController.Instance.playerArms)
        {
            case PLAYER_ARMS.SWORD:
                if (!PlayerController.Instance.isAttacking)
                    PlayerController.Instance.isAttackRight = true;
                if (PlayerController.Instance.playerBodyState == PLAYER_BODY_STATE.ATTACK_RIGHT)
                {
                    PlayerController.Instance.countAttackRight++;
                }
                break;
            case PLAYER_ARMS.PISTOL:
                if (!PlayerController.Instance.isAttacking)
                {
                    PlayerController.Instance.isAttackRight = true;
                }
                if (PlayerController.Instance.playerBodyState == PLAYER_BODY_STATE.ATTACK_RIGHT)
                {
                    PlayerController.Instance.countAttackRight++;
                }
                break;
            case PLAYER_ARMS.BOW:
                if (!PlayerController.Instance.isAttacking)
                {
                    PlayerController.Instance.isAttackRight = true;
                }
                break;
            case PLAYER_ARMS.AK47:
                if (!PlayerController.Instance.isAttacking)
                {
                    PlayerController.Instance.isAttackRight = true;
                }
                break;
            case PLAYER_ARMS.FIRE_GUN:
                if (!PlayerController.Instance.isAttacking)
                {
                    PlayerController.Instance.isAttackRight = true;
                }
                break;
            case PLAYER_ARMS.THUNDER_GUN:
                if (!PlayerController.Instance.isAttacking)
                {
                    PlayerController.Instance.isAttackRight = true;
                }
                break;
            case PLAYER_ARMS.GATLING_GUN:
                if (!PlayerController.Instance.isAttacking)
                {
                    PlayerController.Instance.isAttackRight = true;
                }
                break;
            case PLAYER_ARMS.DUAL_PISTOL:
                if (!PlayerController.Instance.isAttacking)
                {
                    PlayerController.Instance.isAttackRight = true;
                }
                if (PlayerController.Instance.playerBodyState == PLAYER_BODY_STATE.ATTACK_RIGHT)
                {
                    PlayerController.Instance.countAttackRight++;
                }
                break;
            case PLAYER_ARMS.SHOTGUN:
                if (!PlayerController.Instance.isAttacking)
                {
                    PlayerController.Instance.isAttackRight = true;
                }
                break;
            case PLAYER_ARMS.AWM_GUN:
                if (!PlayerController.Instance.isAttacking)
                {
                    PlayerController.Instance.isAttackRight = true;
                }
                break;
            case PLAYER_ARMS.BAZOKA:
                if (!PlayerController.Instance.isAttacking)
                {
                    PlayerController.Instance.isAttackRight = true;
                }
                break;
            case PLAYER_ARMS.NUCLEAR_GUN:
                if (!PlayerController.Instance.isAttacking)
                    PlayerController.Instance.isAttackRight = true;
                if (PlayerController.Instance.playerBodyState == PLAYER_BODY_STATE.ATTACK_RIGHT)
                {
                    PlayerController.Instance.countAttackRight++;
                }
                break;
        }
    }

    private void onReleaseAttackLeft()
    {
        if (GameManager.Instance.GameState != GAME_STATE.PLAYING)
            return;
        btnAttackLeft.transform.localScale = Vector3.one;
        switch (PlayerController.Instance.playerArms)
        {
            case PLAYER_ARMS.AK47:
                PlayerController.Instance.isAttackLeft = false;
                break;
            case PLAYER_ARMS.GATLING_GUN:
                PlayerController.Instance.isAttackLeft = false;
                break;
            case PLAYER_ARMS.AWM_GUN:
                PlayerController.Instance.OnShootingAwmGun();
                break;
            case PLAYER_ARMS.FIRE_GUN:
                PlayerController.Instance.isAttackLeft = false;
                break;
//            case PLAYER_ARMS.THUNDER_GUN:
//                PlayerController.Instance.isAttackLeft = false;
//                break;
        }
    }

    private void onReleaseAttackRight()
    {
        if (GameManager.Instance.GameState != GAME_STATE.PLAYING)
            return;
        btnAttackRight.transform.localScale = Vector3.one;
        switch (PlayerController.Instance.playerArms)
        {
            case PLAYER_ARMS.AK47:
                PlayerController.Instance.isAttackRight = false;
                break;
            case PLAYER_ARMS.GATLING_GUN:
                PlayerController.Instance.isAttackRight = false;
                break;
            case PLAYER_ARMS.AWM_GUN:
                PlayerController.Instance.OnShootingAwmGun();
                break;
            case PLAYER_ARMS.FIRE_GUN:
                PlayerController.Instance.isAttackRight = false;
                break;
//            case PLAYER_ARMS.THUNDER_GUN:
//                PlayerController.Instance.isAttackRight = false;
//                break;
        }

    }

    private void onClickButtonReady()
    {
        Debug.Log("onClickButtonReady");
        btnReady.enabled = false;
        Vector2 _sizeBG = imgBGReady.rectTransform.sizeDelta;
        DOTween.To(() => imgBGReady.rectTransform.sizeDelta, x => imgBGReady.rectTransform.sizeDelta = x, new Vector2(_sizeBG.x, 0), timeTween);
        rectImageReady.DOMoveX(1280, timeTween).SetEase(Ease.InQuint).OnComplete(() =>
            {
                rectImageReady.anchoredPosition = new Vector2(-1280, rectImageReady.anchoredPosition.y);
                imgBGReady.rectTransform.sizeDelta = _sizeBG;
                rectUIGameReady.gameObject.SetActive(false);
                GameManager.Instance.OnSetGameState(GAME_STATE.PLAYING);
            });
    }

    private void onClickChangeGun()
    {
        if (GameManager.Instance.GameState != GAME_STATE.PLAYING)
            return;
        switch (ListArmIDEquiped.Count)
        {
            case 2:
                PlayerController.Instance.OnChangePlayerArms(ListArmIDEquiped[1]);
                break;
            case 3:
                if (PlayerController.Instance.playerArms == PLAYER_ARMS.SWORD)
                {
                    PlayerController.Instance.OnChangePlayerArms(ListArmIDEquiped[1]);
                }
                else if (PlayerController.Instance.playerArms == ListArmIDEquiped[1])
                {
                    PlayerController.Instance.OnChangePlayerArms(ListArmIDEquiped[2]);
                }
                else
                {
                    PlayerController.Instance.OnChangePlayerArms(ListArmIDEquiped[1]);
                }
                break;
        }
    }



    private void onClickChangeSword()
    {
        if (GameManager.Instance.GameState != GAME_STATE.PLAYING)
            return;
        PlayerController.Instance.OnChangePlayerArms(PLAYER_ARMS.SWORD);    
//        countPlayerArm = 0;
    }

    private void onSetDataItemEquiped(int index, ModelItem _item)
    {
        if (_item.type == ITEM_TYPE.NONE)
        {
            if (index == 0)
            {
                btnItem.transform.parent.gameObject.SetActive(false);
            }
            else
            {
                btnItem2.transform.parent.gameObject.SetActive(false);
            }
            return;
        }
        else
        {
            if (index == 0)
                btnItem.transform.parent.gameObject.SetActive(true);
            else
                btnItem2.transform.parent.gameObject.SetActive(true);
            switch (_item.type)
            {
                case ITEM_TYPE.BLOOD_BOX:
                    if (_item.quantity > 0)
                    {
                        txtItemQuantity[index].transform.parent.gameObject.SetActive(true);
                        txtItemQuantity[index].text = "x" + _item.quantity;
                    }
                    else
                    {
                        txtItemQuantity[index].transform.parent.gameObject.SetActive(false);
                    }
                    imgItemIcon[index].sprite = GamePlayResources.Instance.spriteItemBloodBox;
                    break;
                case ITEM_TYPE.CARTRIDGE_BOX:
                    if (_item.quantity > 0)
                    {
                        txtItemQuantity[index].transform.parent.gameObject.SetActive(true);
                        txtItemQuantity[index].text = "x" + _item.quantity;
                    }
                    else
                    {
                        txtItemQuantity[index].transform.parent.gameObject.SetActive(false);
                    }
                    imgItemIcon[index].sprite = GamePlayResources.Instance.spriteItemCartridgeBox;
                    break;
                case ITEM_TYPE.POWER_DRINK:
                    if (_item.quantity > 0)
                    {
                        txtItemQuantity[index].transform.parent.gameObject.SetActive(true);
                        txtItemQuantity[index].text = "x" + _item.quantity;
                    }
                    else
                    {
                        txtItemQuantity[index].transform.parent.gameObject.SetActive(false);
                    }
                    imgItemIcon[index].sprite = GamePlayResources.Instance.spriteItemPowerDrink;
                    break;
                case ITEM_TYPE.SHOE_SPEED:
                    if (_item.quantity > 0)
                    {
                        txtItemQuantity[index].transform.parent.gameObject.SetActive(true);
                        txtItemQuantity[index].text = "x" + _item.quantity;
                    }
                    else
                    {
                        txtItemQuantity[index].transform.parent.gameObject.SetActive(false);
                    }
                    imgItemIcon[index].sprite = GamePlayResources.Instance.spriteItemShoeSpeed;
                    break;
            }
        }
    }



    #endregion
}
