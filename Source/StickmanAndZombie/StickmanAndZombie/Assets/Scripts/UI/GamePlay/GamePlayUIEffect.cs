﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class GamePlayUIEffect : MonoBehaviour
{
    [SerializeField]
    private Image[] listImageBloodItem = new Image[3];
    [SerializeField]
    private Sprite[] listSpriteBloodItem = new Sprite[2];


    [SerializeField]
    private Image imgEffectBlood;

    [SerializeField]
    private Image imgEffectSaliva;

    private IEnumerator salivaShow;

    private bool isPlayingBlood{ get { return listImageBloodItem[0].gameObject.activeSelf ||
        listImageBloodItem[1].gameObject.activeSelf || listImageBloodItem[2].gameObject.activeSelf; } }

    private bool isPlayingSaliva = false;

    private float timeTween = 0.6f;
    private float minRandomY = 0;
    private float maxRandomY = 330;
    private float minRandomX = -650;
    private float maxRandonX = 650;

    public void OnShowBloodUI()
    {
        if (!isPlayingBlood)
        {
            this.gameObject.SetActive(true);
            imgEffectBlood.gameObject.SetActive(true);
            imgEffectBlood.color = new Color(1, 0, 0, 0);
            imgEffectBlood.DOFade(1, timeTween);
            for (int i = 0; i < listImageBloodItem.Length; i++)
            {
                if (!listImageBloodItem[i].gameObject.activeSelf)
                {
                    listImageBloodItem[i].color = Color.red;
                    listImageBloodItem[i].transform.localScale = Vector3.zero;
                    listImageBloodItem[i].gameObject.SetActive(true);
                    onScaleItem(listImageBloodItem[i]);
                    break;
                }
            }
        }

    }

    public void OnHideBloodUI()
    {
        imgEffectBlood.DOFade(0, timeTween).OnComplete(() =>
            {
                if (!isPlayingBlood)
                {
                    imgEffectBlood.gameObject.SetActive(false);
                    if (!isPlayingSaliva)
                    {
                        this.gameObject.SetActive(false);
                    }
                }
            });
    }

    public void OnShowSalivaUI()
    {
        if (!isPlayingSaliva)
        {
            gameObject.SetActive(true);
            salivaShow = onShowEffectSaliva();
            StartCoroutine(salivaShow);

        }
    }

    public void OnHideSalivaUI()
    {
        if (isPlayingSaliva)
        {
            StopCoroutine(salivaShow);
            isPlayingSaliva = false;
            imgEffectSaliva.gameObject.SetActive(false);
            if (!isPlayingBlood)
            {
                this.gameObject.SetActive(false);
            }
        }
    }

    private IEnumerator onShowEffectSaliva()
    {
        isPlayingSaliva = true;
        imgEffectSaliva.gameObject.SetActive(true);
        imgEffectSaliva.color = Color.green;
        yield return new WaitForSeconds(4.8f);
        imgEffectSaliva.DOFade(0, 0.2f).OnComplete(() =>
            {
                isPlayingSaliva = false;
                PlayerController.Instance.isSaliva = false;
                imgEffectSaliva.gameObject.SetActive(false);
                if (!isPlayingBlood)
                {
                    this.gameObject.SetActive(false);
                }
            });
    }

    private void onScaleItem(Image item)
    {
        Vector3 _randomPos = new Vector3(Random.Range(minRandomX, maxRandonX), Random.Range(minRandomY, maxRandomY));
        item.rectTransform.anchoredPosition = _randomPos;
        int random = Random.Range(0, 2);
        if (random == 0)
        {
            item.sprite = listSpriteBloodItem[0];
        }
        else
        {
            item.sprite = listSpriteBloodItem[1];
        }
        item.transform.DOScale(Vector3.one, timeTween).SetEase(Ease.OutElastic).OnComplete(() =>
            {
                item.gameObject.SetActive(false);
                if (!isPlayingBlood)
                {
                    OnHideBloodUI();
                }
            });
    }

}
