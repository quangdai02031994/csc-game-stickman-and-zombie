﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class GamePlayUIGoldReward : MonoBehaviour
{
    public static GamePlayUIGoldReward Instance{ get; private set; }

    public GameObject prefabTextReward;

    public List<Text> ListTextReward;

    private float timeTween = 0.5f;

    #region Behaviour

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
    }

    private void Start()
    {
        ListTextReward = new List<Text>();
        PlayerController.Instance.playerGameOver += onGameOver;
        PlayerController.Instance.playerGameWin += onGameWin;
    }

    private void OnDestroy()
    {
        PlayerController.Instance.playerGameOver -= onGameOver;
        PlayerController.Instance.playerGameWin += onGameWin;
    }


    #endregion

    #region Publish Methods

    public void OnRewardGold(int value, Vector2 startPos)
    {
        Text _gold = onGetGoldReward(value, startPos);
        _gold.gameObject.SetActive(true);
        _gold.rectTransform.DOAnchorPosY(startPos.y + 200, timeTween).OnComplete(() =>
            {
                _gold.gameObject.SetActive(false);
            });
    }

    #endregion

    #region Private Methods

    private Text onGetGoldReward(int value, Vector2 startPos)
    {
        if (ListTextReward.Count == 0)
        {
            return onCreateGoldReward(value, startPos);
        }
        else
        {
            for (int i = 0; i < ListTextReward.Count; i++)
            {
                if (!ListTextReward[i].gameObject.activeSelf)
                {
                    ListTextReward[i].text = "+ " + value;
                    ListTextReward[i].transform.position = startPos;
                    return ListTextReward[i];
                }
            }
            return onCreateGoldReward(value, startPos);
        }
    }

    private Text onCreateGoldReward(int value, Vector2 startPos)
    {
        GameObject _obj = Instantiate(prefabTextReward, Vector3.zero, Quaternion.identity, this.transform)as GameObject;
        _obj.transform.localScale = Vector3.one;
        Text _txtObj = _obj.GetComponent<Text>();
        _txtObj.text = "+ " + value;
        _txtObj.transform.position = startPos;
        ListTextReward.Add(_txtObj);
        return _txtObj;
    }

    private void onGameOver()
    {
        if (transform.childCount > 0)
        {
            ListTextReward.Clear();
            foreach (Transform item in this.transform)
            {
                Destroy(item.gameObject);
            }
        }
    }

    private void onGameWin()
    {
        if (transform.childCount > 0)
        {
            ListTextReward.Clear();
            foreach (Transform item in this.transform)
            {
                Destroy(item.gameObject);
            }
        }
    }

    #endregion

}
