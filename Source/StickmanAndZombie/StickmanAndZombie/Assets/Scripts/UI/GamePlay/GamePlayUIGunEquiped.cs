﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GameSupport;

public class GamePlayUIGunEquiped : MonoBehaviour
{
    public bool IsEquiped = false;

    public PLAYER_ARMS type;

    public int numberBullet;

    public Image imgIconGun;

    public Text txtGunAmmob;

    public void OnSetData(PLAYER_ARMS type, int numberBullet)
    {
        if (type == PLAYER_ARMS.SWORD)
        {
            this.IsEquiped = false;
            onSetDataArmEquiped(type);
        }
        else
        {
            this.IsEquiped = true;
            this.numberBullet = numberBullet;
            this.type = type;
            onSetDataArmEquiped(type);
        }
    }

    public void OnResetBullet()
    {
        if (type != PLAYER_ARMS.SWORD)
        {
            numberBullet = GameManager.Instance.OnGetPlayerGun(type).numberBullet;
            txtGunAmmob.text = "x" + numberBullet;
        }
    }

    public void OnUpdateBullet(int numberBullet)
    {
        if (numberBullet > 0)
        {
            txtGunAmmob.text = "x" + numberBullet.ToString();
        }
        else
        {
            txtGunAmmob.text = "x0";
        }
    }

    private void onSetDataArmEquiped(PLAYER_ARMS type)
    {
        if (type == PLAYER_ARMS.SWORD)
        {
            imgIconGun.gameObject.SetActive(false);
            txtGunAmmob.transform.parent.gameObject.SetActive(false);
        }
        else
        {
            imgIconGun.gameObject.SetActive(true);
            txtGunAmmob.transform.parent.gameObject.SetActive(true);
            if (numberBullet > 0)
                txtGunAmmob.text = "x" + numberBullet;
            else
                txtGunAmmob.text = "x0";
            switch (type)
            {
                case PLAYER_ARMS.AK47:
                    imgIconGun.sprite = GamePlayResources.Instance.spriteAK47;
                    break;
                case PLAYER_ARMS.AWM_GUN:
                    imgIconGun.sprite = GamePlayResources.Instance.spriteAwmGun;
                    break;
                case PLAYER_ARMS.BAZOKA:
                    imgIconGun.sprite = GamePlayResources.Instance.spriteArmBazoka;
                    break;
                case PLAYER_ARMS.BOW:
                    imgIconGun.sprite = GamePlayResources.Instance.spriteArmBow;
                    break;
                case PLAYER_ARMS.FIRE_GUN:
                    imgIconGun.sprite = GamePlayResources.Instance.spriteArmFireGun;
                    break;
                case PLAYER_ARMS.GATLING_GUN:
                    imgIconGun.sprite = GamePlayResources.Instance.spriteArmGaltingGun;
                    break;
                case PLAYER_ARMS.PISTOL:
                    imgIconGun.sprite = GamePlayResources.Instance.spritePistol;
                    break;
                case PLAYER_ARMS.SHOTGUN:
                    imgIconGun.sprite = GamePlayResources.Instance.spriteArmShotGun;
                    break;
                case PLAYER_ARMS.THUNDER_GUN:
                    imgIconGun.sprite = GamePlayResources.Instance.spriteArmThunderGun;
                    break;
                case PLAYER_ARMS.DUAL_PISTOL:
                    imgIconGun.sprite = GamePlayResources.Instance.spriteTwoPistol;
                    break;
                case PLAYER_ARMS.NUCLEAR_GUN:
                    imgIconGun.sprite = GamePlayResources.Instance.spriteNuclearGun;
                    break;
            }
        }
    }

}
