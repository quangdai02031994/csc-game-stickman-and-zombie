﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using GameSupport;
using UnityEngine.UI;
using UnityEngine.Events;
using DG.Tweening;

public class GamePlayUIPopupHandle : MonoBehaviour
{
    [Header("Game Win")]
    public GameObject objPanelGameWin;
    public Image imgTitle;
    public Image[] listStar = new Image[3];
    public Text txtCoinReward;
    public Text txtCoinBonus;
    public Text txtTotalCoin;

    public EventTrigger btnGameWinHome;
    public EventTrigger btnGameWinRestart;
    public EventTrigger btnGameWinNext;
    public EventTrigger btnWatchVideos;

    [Header("Game Pause")]
    public Image imgGamePause;
    public EventTrigger btnExit;
    public EventTrigger btnResum;


    private float timeTween = 0.5f;

    #region Behaviour

    void Start()
    {
        InitButtonAction(btnExit, () =>
            {
                onClickButtonExit();
            });
        InitButtonAction(btnResum, () =>
            {
                onClickButtonResum();
            });
        InitButtonAction(btnGameWinHome, () =>
            {
                onClickButtonGameWinHome();
            });
        InitButtonAction(btnGameWinNext, () =>
            {
                onClickButtonGameWinNext();
            });
        InitButtonAction(btnGameWinRestart, () =>
            {
                onClickButtonGameWinRestart();
            });
        InitButtonAction(btnWatchVideos, () =>
            {
                onClickButtonWatchVideo();
            });
    }

    #endregion

    #region Public Methods

    public void OnPauseGame()
    {
        Time.timeScale = 0;
        imgGamePause.gameObject.SetActive(true);
        this.gameObject.SetActive(true);    
    }

    public void OnWinGame(int rank, float gold)
    {
        int rewardGold = (int)gold;
        int bonusGold = 0;
        switch (rank)
        {
            case 1:
                listStar[0].sprite = GamePlayResources.Instance.spriteStar;
                listStar[1].sprite = GamePlayResources.Instance.spriteStarDisable;
                listStar[2].sprite = GamePlayResources.Instance.spriteStarDisable;
                break;
            case 2:
                bonusGold = (int)(gold * 0.5f);
                listStar[0].sprite = GamePlayResources.Instance.spriteStar;
                listStar[1].sprite = GamePlayResources.Instance.spriteStar;
                listStar[2].sprite = GamePlayResources.Instance.spriteStarDisable;
                break;
            case 3:
                bonusGold = (int)gold;
                listStar[0].sprite = GamePlayResources.Instance.spriteStar;
                listStar[1].sprite = GamePlayResources.Instance.spriteStar;
                listStar[2].sprite = GamePlayResources.Instance.spriteStar;
                break;
            default:
                listStar[0].sprite = GamePlayResources.Instance.spriteStarDisable;
                listStar[1].sprite = GamePlayResources.Instance.spriteStarDisable;
                listStar[2].sprite = GamePlayResources.Instance.spriteStarDisable;
                bonusGold = 0;
                break;
        }
        txtCoinReward.text = rewardGold.ToString("#,##0");
        txtCoinBonus.text = bonusGold.ToString("#,##0");
        txtTotalCoin.text = (rewardGold + bonusGold).ToString("#,##0");
        Time.timeScale = 0;
//        imgTitle.transform.localScale = Vector3.zero;
        objPanelGameWin.SetActive(true);
        this.gameObject.SetActive(true);
//        imgTitle.transform.DOScale(Vector3.one, timeTween).SetEase(Ease.OutElastic);            
    }

    #endregion

    #region Private Methods

    private void onLockButtons()
    {
        btnExit.enabled = false;
        btnGameWinHome.enabled = false;
        btnGameWinNext.enabled = false;
        btnGameWinRestart.enabled = false;
        btnResum.enabled = false;
        btnWatchVideos.enabled = false;
    }

    private void onUnLockButtons()
    {
        btnExit.enabled = true;
        btnGameWinHome.enabled = true;
        btnGameWinNext.enabled = true;
        btnGameWinRestart.enabled = true;
        btnResum.enabled = true;
        btnWatchVideos.enabled = true;
    }


    private void onClickButtonResum()
    {
        Time.timeScale = 1;
        GameManager.Instance.OnSetGameState(GAME_STATE.PLAYING);
        imgGamePause.gameObject.SetActive(false);
        this.gameObject.SetActive(false);
    }

    private void onClickButtonExit()
    {
        Debug.Log("onClickButtonExit");
        btnExit.enabled = false;
        btnResum.enabled = false;
        Time.timeScale = 1;
        GameManager.Instance.OnLoadingSceneAsync(ScenesName.Map);
    }

    private void onClickButtonWatchVideo()
    {
        Debug.Log("onClickButtonWatchVideo");            
    }

    private void onClickButtonGameWinRestart()
    {
        Debug.Log("onClickButtonGameWinRestart");
    }

    private void onClickButtonGameWinNext()
    {
        Debug.Log("onClickButtonGameWinNext");
        Time.timeScale = 1;
        GameManager.Instance.OnSetGameState(GAME_STATE.START);
        GameManager.Instance.OnLoadingSceneAsync(ScenesName.Map);
    }

    private void onClickButtonGameWinHome()
    {
        Debug.Log("onClickButtonHome");
        Time.timeScale = 1;
        GameManager.Instance.OnSetGameState(GAME_STATE.START);
        GameManager.Instance.OnLoadingSceneAsync(ScenesName.Home);
    }

    private void InitButtonAction(EventTrigger trigger, UnityAction action)
    {
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.PointerClick;
        entry.callback.AddListener((data) =>
            {
                action();
            });
        trigger.triggers.Add(entry);
    }

    #endregion

}
