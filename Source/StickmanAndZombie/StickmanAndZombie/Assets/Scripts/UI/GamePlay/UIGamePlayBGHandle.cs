﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSupport;
using UnityEngine.UI;

public class UIGamePlayBGHandle : MonoBehaviour
{
    public Sprite[] spriteBG;

    public Image imgBG1;
    public Image imgBG2;

    private void Awake()
    {
        switch (GameManager.Instance.OnGetModelLevel().name)
        {
            case MAP_TYPES.GRAY_1:
                Debug.Log("Looad BG Map gray 1");
                break;
            case MAP_TYPES.YELLOW_2:
                Debug.Log("Looad BG Map Yellow 2");
                break;
            case MAP_TYPES.GREEN_3:
                Debug.Log("Looad BG Map Green 3");
                break;
            case MAP_TYPES.WHILE_4:
                Debug.Log("Looad BG Map While 4");
                break;
            case MAP_TYPES.RED_5:
                Debug.Log("Looad BG Map Red 5");
                break;
        }
    }
}
