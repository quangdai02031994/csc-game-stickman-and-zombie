﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;
using Spine;

public class UIGoldItemHandle : MonoBehaviour
{

    [SpineAnimation]
    public string animationNameRotate;

    [SpineAnimation]
    public string animationNameAppear;

    [SpineAnimation]
    public string animationNameCollect;

    private Vector3 originCast = new Vector3(0, -0.8f, 0);

    public LayerMask layerCast;

    private float radius = 0.25f;

    private SkeletonAnimation skeletonAnimation;
    private MeshRenderer meshRenderer;
    private int gold;
    private bool isCollected = false;

    #region Behaviour


    private void Awake()
    {
        skeletonAnimation = GetComponent<SkeletonAnimation>();
        meshRenderer = GetComponent<MeshRenderer>();
    }

    private void OnDisable()
    {
        isCollected = false;
        skeletonAnimation.AnimationName = animationNameAppear;
    }

    private void Update()
    {
        if (isCollected)
            return;
        RaycastHit2D _hit = Physics2D.CircleCast(transform.TransformPoint(originCast), radius, Vector2.zero, radius, layerCast);
        if (_hit.collider != null && !isCollected)
        {
            GamePlayUIHandle.Instance.OnRewardGold(gold);
            skeletonAnimation.AnimationState.SetAnimation(0, animationNameCollect, false);
            isCollected = true;
        }
    }

    private void Start()
    {
        skeletonAnimation.AnimationState.Complete += onCompleteSpineAnimation;
    }

    private void OnDestroy()
    {
        skeletonAnimation.AnimationState.Complete -= onCompleteSpineAnimation;
    }

    #if UNITY_EDITOR

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.TransformPoint(originCast), radius);
    }
    #endif

    #endregion

    #region Publish Methods



    public void OnSetData(float gold, int orderInLayer)
    {
        this.gameObject.SetActive(true);
        this.gold = Mathf.RoundToInt(gold);
        meshRenderer.sortingOrder = orderInLayer;
        skeletonAnimation.AnimationState.SetAnimation(0, animationNameAppear, false);
        skeletonAnimation.AnimationState.AddAnimation(0, animationNameRotate, true, 0);
    }

    #endregion

    private void onCompleteSpineAnimation(TrackEntry trackEntry)
    {
        if (trackEntry.Animation.Name == animationNameCollect)
        {
            this.gameObject.SetActive(false);
        }
        
    }

}
