﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using GameSupport;

public class GamePlayUIGameOver : MonoBehaviour
{

    public Button btnTapToContinue;

    public Image imgTitle;
    public Image imgTapToContinue;

    private float timeTween = 0.5f;


    void Start()
    {
        btnTapToContinue.onClick.AddListener(() =>
            {
                onClickButtonContinue();
            });
    }


    public void OnShowGameOver()
    {
        this.gameObject.SetActive(true);
        btnTapToContinue.enabled = false;
        imgTitle.transform.localScale = Vector3.zero;
        imgTitle.gameObject.SetActive(true);
        imgTitle.transform.DOScale(Vector3.one, timeTween).SetEase(Ease.OutElastic).OnComplete(() =>
            {
                imgTapToContinue.gameObject.SetActive(true);
                btnTapToContinue.enabled = true;
            });

    }

    private void onClickButtonContinue()
    {
        Debug.Log("onClickButtonContinue");
        GameManager.Instance.OnLoadingSceneAsync(ScenesName.Map);
    }
}
