﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using GameSupport;


public class VirtualJoyStick : MonoBehaviour,IDragHandler,IPointerUpHandler,IPointerDownHandler
{
    private Image bgImg;
    private Image joinstickImg;
    public Vector3 inputVector;

    private Vector3 defaultStickBGScale = new Vector3(0.7f, 0.7f, 0.7f);

    private float xMove;

    private float yMove;

    /// <summary>
    /// Chuẩn hóa tọa độ X của joystick bằng góc với trục Vector3.right
    /// </summary>
    /// <value>The move x.</value>
    public float MoveX
    {
        get
        {
            float angle = Vector3.Angle(inputVector, Vector3.right);
            xMove = Mathf.Cos(angle * Mathf.PI / 180);
            return xMove;
        }
    }

    /// <summary>
    /// Chuẩn hóa tọa độ Y của joystick bằng góc theo Vector3.up
    /// </summary>
    /// <value>The move y.</value>
    public float MoveY
    {
        get
        {
            float angle = Vector3.Angle(inputVector, Vector3.up);
            yMove = Mathf.Cos(angle * Mathf.PI / 180);
            return yMove;
        }
    }


    #region Behaviour

    private void Start()
    {
        bgImg = GetComponent<Image>();
        joinstickImg = transform.GetChild(0).GetComponent<Image>();
        bgImg.rectTransform.localScale = defaultStickBGScale;
    }


    #endregion

    #region Interface Implements



    public virtual void OnDrag(PointerEventData ped)
    {
        if (GameManager.Instance.GameState != GAME_STATE.PLAYING)
            return;
        bgImg.rectTransform.localScale = Vector3.one;
        Vector2 pos;
        if (RectTransformUtility.ScreenPointToLocalPointInRectangle(bgImg.rectTransform, ped.position, ped.pressEventCamera, out pos))
        {
            pos.x = (pos.x / bgImg.rectTransform.sizeDelta.x);
            pos.y = (pos.y / bgImg.rectTransform.sizeDelta.y);
    
            inputVector = new Vector3(pos.x * 2, pos.y * 2);
            inputVector = (inputVector.magnitude > 1) ? inputVector.normalized : inputVector;
    
            joinstickImg.rectTransform.anchoredPosition =
                    new Vector3(inputVector.x * (bgImg.rectTransform.sizeDelta.x / 3), inputVector.y * (bgImg.rectTransform.sizeDelta.y / 3));
        }
    
        if (inputVector.x <= 0 && !PlayerController.Instance.isMinOrMax)
        {
            switch (PlayerController.Instance.playerArms)
            {
                case PLAYER_ARMS.SWORD:

                    if (PlayerController.Instance.playerLegState != PLAYER_LEG_STATE.RUN_LEFT && !PlayerController.Instance.isAttacking)
                    {
                        PlayerController.Instance.playerLegState = PLAYER_LEG_STATE.RUN_LEFT;
                        PlayerController.Instance.SetAnimationLeg(PLAYER_LEG_STATE.RUN_LEFT);
                    }
                        
                    if (PlayerController.Instance.playerBodyState == PLAYER_BODY_STATE.IDE_RIGHT)
                    {
                        PlayerController.Instance.playerBodyState = PLAYER_BODY_STATE.IDE_LEFT;
                        PlayerController.Instance.SetAnimationBody(PLAYER_BODY_STATE.IDE_LEFT);
                    }

                    break;
                default:
                    if (PlayerController.Instance.playerLegState != PLAYER_LEG_STATE.RUN_LEFT)
                    {
                        PlayerController.Instance.playerLegState = PLAYER_LEG_STATE.RUN_LEFT;
                        PlayerController.Instance.SetAnimationLeg(PLAYER_LEG_STATE.RUN_LEFT);
                    }

                    if (PlayerController.Instance.playerBodyState == PLAYER_BODY_STATE.IDE_RIGHT)
                    {
                        PlayerController.Instance.playerBodyState = PLAYER_BODY_STATE.IDE_LEFT;
                        PlayerController.Instance.SetAnimationBody(PLAYER_BODY_STATE.IDE_LEFT);
                    }
                    break;
            }
        }
        else if (inputVector.x > 0 && !PlayerController.Instance.isMinOrMax)
        {
            switch (PlayerController.Instance.playerArms)
            {
                case PLAYER_ARMS.SWORD:
                    if (PlayerController.Instance.playerLegState != PLAYER_LEG_STATE.RUN_RIGHT && !PlayerController.Instance.isAttacking)
                    {
                        PlayerController.Instance.playerLegState = PLAYER_LEG_STATE.RUN_RIGHT;
                        PlayerController.Instance.SetAnimationLeg(PLAYER_LEG_STATE.RUN_RIGHT);
                    }

                    if (PlayerController.Instance.playerBodyState == PLAYER_BODY_STATE.IDE_LEFT)
                    {
                        PlayerController.Instance.playerBodyState = PLAYER_BODY_STATE.IDE_RIGHT;
                        PlayerController.Instance.SetAnimationBody(PLAYER_BODY_STATE.IDE_RIGHT);
                    }

                    break;
                default:
                    if (PlayerController.Instance.playerLegState != PLAYER_LEG_STATE.RUN_RIGHT)
                    {
                        PlayerController.Instance.playerLegState = PLAYER_LEG_STATE.RUN_RIGHT;
                        PlayerController.Instance.SetAnimationLeg(PLAYER_LEG_STATE.RUN_RIGHT);
                    }

                    if (PlayerController.Instance.playerBodyState == PLAYER_BODY_STATE.IDE_LEFT)
                    {
                        PlayerController.Instance.playerBodyState = PLAYER_BODY_STATE.IDE_RIGHT;
                        PlayerController.Instance.SetAnimationBody(PLAYER_BODY_STATE.IDE_RIGHT);
                    }

                    break;
            }
        }

    }


    public virtual void OnPointerDown(PointerEventData  ped)
    {
        OnDrag(ped);
    }

    public virtual void OnPointerUp(PointerEventData ped)
    {
        if (ped != null)
        {
            xMove = yMove = 0;
            inputVector = Vector3.zero;
            bgImg.rectTransform.localScale = defaultStickBGScale;
            joinstickImg.rectTransform.localPosition = Vector3.zero;
        }
        if (PlayerController.Instance.playerLegState == PLAYER_LEG_STATE.RUN_LEFT)
        {
            PlayerController.Instance.playerLegState = PLAYER_LEG_STATE.IDE_LEFT;
            PlayerController.Instance.SetAnimationLeg(PLAYER_LEG_STATE.IDE_LEFT);
            if (PlayerController.Instance.playerBodyState == PLAYER_BODY_STATE.IDE_RIGHT)
            {
                PlayerController.Instance.playerBodyState = PLAYER_BODY_STATE.IDE_LEFT;
                PlayerController.Instance.SetAnimationBody(PLAYER_BODY_STATE.IDE_LEFT);
            }
        }
        else if (PlayerController.Instance.playerLegState == PLAYER_LEG_STATE.RUN_RIGHT)
        {
            PlayerController.Instance.playerLegState = PLAYER_LEG_STATE.IDE_RIGHT;
            PlayerController.Instance.SetAnimationLeg(PLAYER_LEG_STATE.IDE_RIGHT);
            if (PlayerController.Instance.playerBodyState == PLAYER_BODY_STATE.IDE_LEFT)
            {
                PlayerController.Instance.playerBodyState = PLAYER_BODY_STATE.IDE_RIGHT;
                PlayerController.Instance.SetAnimationBody(PLAYER_BODY_STATE.IDE_RIGHT);
            }
        }
    }

    #endregion

    #region Publish Methods



    public void OnLockJoyStick()
    {
//        xMove = yMove = 0;
//        inputVector = Vector3.zero;
//        bgImg.rectTransform.localScale = defaultStickBGScale;
//        joinstickImg.rectTransform.localPosition = Vector3.zero;
        OnPointerUp(null);
        onDisableJoyStick();
    }

    public void OnUnlockJoyStick()
    {
        onEnableJoyStick();
    }

    public void OnPoiterUpJoyStick()
    {
        OnPointerUp(null);
    }

    #endregion

    #region Private Methods

    private void onDisableJoyStick()
    {
        this.enabled = false;
    }

    private void onEnableJoyStick()
    {
        this.enabled = true;
    }

    #endregion

}
