﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;
using Spine;

public class UIMultipleKillHandle : MonoBehaviour
{
    [SpineAnimation]
    public string doubleKill;

    [SpineAnimation]
    public string tripleKill;

    [SpineAnimation]
    public string multipleKill;

    private SkeletonGraphic skeletonGraphics;

    #region Behaviour

    void Awake()
    {
        skeletonGraphics = GetComponent<SkeletonGraphic>();
    }

    void Start()
    {
        skeletonGraphics.AnimationState.Complete += onCompleteAnimation;
    }

    #endregion

    #region Publish Methods

    public void OnPlayAnimation(int numberMonster)
    {
        if (numberMonster < 2)
            return;
        switch (numberMonster)
        {
            case 2:
                skeletonGraphics.AnimationState.SetAnimation(0, doubleKill, false);
                break;
            case 3:
                skeletonGraphics.AnimationState.SetAnimation(0, tripleKill, false);
                break;
            default:
                skeletonGraphics.AnimationState.SetAnimation(0, multipleKill, false);
                break;
        }
    }

    #endregion

    #region Spine Event

    private void onCompleteAnimation(TrackEntry trackEntry)
    {
        this.gameObject.SetActive(false);
    }

    #endregion
}
