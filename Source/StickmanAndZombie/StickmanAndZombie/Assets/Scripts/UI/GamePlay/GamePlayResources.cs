﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GamePlayResources : MonoBehaviour
{

    public static GamePlayResources Instance{ get; private set; }

    public GameObject objGold;
    public GameObject objTextRewardGold;


    [Header("Effect")]
    public GameObject objEffectBlood;
    public GameObject objEffectCartouche;
    public GameObject objEffectDead;
    public GameObject objEffectExplosion;
    public GameObject objEffectSaliva;
    public GameObject objEffectBurning;

    [Header("Monster Bullet")]
    public GameObject objBulletStone;
    public GameObject objBulletSpear;

    [Header("Bullet Resource")]
    public Sprite spriteBulletGun;
    public Sprite spriteBulletBow;
    public Sprite spriteBulletBazoka;

    [Header("Items Icon")]
    public Sprite spriteItemCartridgeBox;
    public Sprite spriteItemShoeSpeed;
    public Sprite spriteItemPowerDrink;
    public Sprite spriteItemBloodBox;
    public Sprite spriteItemNone;

    [Header("Arm Icon")]
    public Sprite spriteArmBow;
    public Sprite spriteArmFireGun;
    public Sprite spriteArmThunderGun;
    public Sprite spriteArmShotGun;
    public Sprite spriteArmGaltingGun;
    public Sprite spriteArmBazoka;
    public Sprite spriteAwmGun;
    public Sprite spriteTwoPistol;
    public Sprite spritePistol;
    public Sprite spriteAK47;
    public Sprite spriteNuclearGun;

    [Header("Icon Star")]
    public Sprite spriteStar;
    public Sprite spriteStarDisable;

    #region Behaviour

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    #endregion

}
