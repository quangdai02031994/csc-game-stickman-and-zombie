﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using GameSupport;

[RequireComponent(typeof(EventTrigger))]
public class UIRelease : MonoBehaviour
{
    void Start()
    {
        EventTrigger trigger = GetComponent<EventTrigger>();
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.PointerUp;
        entry.callback.AddListener((data) =>
            {
                OnPointerUp((PointerEventData)data);
            });
        trigger.triggers.Add(entry);
    }


    private void OnPointerUp(PointerEventData data)
    {
//        GameConsole.Log("Pointer Up");
        transform.localScale = Vector3.one;
    }

}
