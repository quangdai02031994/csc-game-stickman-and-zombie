﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.EventSystems;
using UnityEngine.Events;


public class UIUpgradePlayerHandle : MonoBehaviour
{
    public RectTransform rectPanelUpgradePlayer;
    public RectTransform rectPanelContent;

    [Header("HP")]
    public Text txtHPValue;
    public Text txtHPLevel;
    public RectTransform rectHPObject;
    public Text txtHPAdd;
    public Text txtHPUpgrade;
    public Text txtPriceUpgradeHP;
    public Text txtMaxLevelHP;
    public EventTrigger btnUpgradeHP;
    public Image imgButtonUpgradeHP;
    public Image imgTextUpgradeHP;


    [Header("Speed")]
    public Text txtSpeedValue;
    public Text txtSpeedLevel;
    public RectTransform rectSpeedObject;
    public Text txtSpeedAdd;
    public Text txtSpeedUpgrade;
    public Text txtPriceUpgradeSpeed;
    public Text txtMaxLevelSpeed;
    public EventTrigger btnUpgradeSpeed;
    public Image imgButtonUpgradeSpeed;
    public Image imgTextUpgradeSpeed;

    [Header("Strong")]
    public Text txtStrongValue;
    public Text txtStrongLevel;
    public RectTransform rectStrongObject;
    public Text txtStrongAdd;
    public Text txtStrongUpgrade;
    public Text txtPriceUpgradeStrong;
    public Text txtMaxLevelStrong;
    public EventTrigger btnUpgradeStrong;
    public Image imgButtonUpgradeStrong;
    public Image imgTextUpgradeStrong;

    private Vector3 startPosPanel = new Vector3(400, 0, 0);
    private Vector3 endPosPanel = new Vector3(-910, 0, 0);

    private Vector2 startContenPanel = new Vector2(-720, 0);
    private Vector2 endContenPanel = new Vector2(0, 0);

    private float timeTween = 0.5f;

    #region Behaviour

    private void Start()
    {
        onInitButtons(btnUpgradeHP, () =>
            {
                onClickButtonUpgradeHP();
            });
        onInitButtons(btnUpgradeSpeed, () =>
            {
                onClickButtonUpgradeSpeed();
            });
        onInitButtons(btnUpgradeStrong, () =>
            {
                onClickButtonUpgradeStrong();
            });
    }

    #endregion

    #region Publish Methods

    public void  OnShow()
    {
        rectPanelUpgradePlayer.anchoredPosition = startPosPanel;
        rectPanelUpgradePlayer.gameObject.SetActive(true);
        onSetDataHP();
        onSetDataSpeed();
        onSetDataStrong();
        OnLockButtons();
        rectPanelUpgradePlayer.DOAnchorPos(endPosPanel, timeTween)
            .OnComplete(() =>
            {
                rectPanelContent.anchoredPosition = startContenPanel;
                rectPanelContent.DOAnchorPos(endContenPanel, timeTween / 2).OnComplete(() =>
                    {
                        OnUnLockButtons();
                    });
            });
        onSetDataHP();
        onSetDataSpeed();
        onSetDataStrong();
    }

    public void OnHide()
    {
        OnLockButtons();
        rectPanelContent.DOAnchorPos(startContenPanel, timeTween / 2).OnComplete(() =>
            {
                rectPanelUpgradePlayer.DOAnchorPos(startPosPanel, timeTween)
                    .OnComplete(() =>
                    {
                        OnUnLockButtons();
                        rectPanelUpgradePlayer.gameObject.SetActive(false);
                    });
            });
    }

    public void OnLockButtons()
    {
        btnUpgradeHP.enabled = false;
        btnUpgradeSpeed.enabled = false;
        btnUpgradeStrong.enabled = false;
    }

    public void OnUnLockButtons()
    {
        btnUpgradeHP.enabled = true;
        btnUpgradeSpeed.enabled = true;
        btnUpgradeStrong.enabled = true;
    }

    #endregion


    #region Private Methods

    private void onClickButtonUpgradeHP()
    {
        Debug.Log("onClickButtonUpgradeHP");

        int currentPriceHP = GameManager.Instance.OnPriceUpgradePlayerHP();
        int currentHP = (int)GameManager.Instance.OnGetPlayerHP();
        int currentHPUpgrade = GameManager.Instance.OnCalculateUpgradePlayerHP();
        int currentAddHP = currentHPUpgrade - currentHP;

        if (GameManager.Instance.OnUpgradePlayerHP(currentPriceHP, GameManager.Instance.OnCalculateUpgradePlayerHP()))
        {
            OnLockButtons();
            int levelHP = GameManager.Instance.OnGetPlayerLevelHP();
            txtHPLevel.text = (levelHP + 1).ToString();
            UIMapHandle.Instance.OnUpdateGold();
            onSetDataSpeed();
            onSetDataStrong();
            if (levelHP < 9)
            {
                txtMaxLevelHP.gameObject.SetActive(false);
                rectHPObject.gameObject.SetActive(true);

                int nextHP = (int)GameManager.Instance.OnGetPlayerHP();
                int nextHPUpgrade = GameManager.Instance.OnCalculateUpgradePlayerHP();
                int nextPriceHPUpgrade = GameManager.Instance.OnPriceUpgradePlayerHP();

                if (GameManager.Instance.OnGetGold() < nextPriceHPUpgrade)
                {
                    imgButtonUpgradeHP.sprite = ResourceMapHandle.Instance.spriteButtonGray;
                    imgTextUpgradeHP.sprite = ResourceMapHandle.Instance.spriteTextUpgradeDisable;
                }
                else
                {
                    imgButtonUpgradeHP.sprite = ResourceMapHandle.Instance.spriteButtonGreen;
                    imgTextUpgradeHP.sprite = ResourceMapHandle.Instance.spriteTextUpgradeEnable;
                }

                DOTween.To(() => currentHP, x => currentHP = x, nextHP, timeTween)
                    .OnUpdate(() =>
                    {
                        txtHPValue.text = currentHP.ToString("#,##0");
                    });
                DOTween.To(() => currentHPUpgrade, x => currentHPUpgrade = x, nextHPUpgrade, timeTween)
                    .OnUpdate(() =>
                    {
                        txtHPUpgrade.text = currentHPUpgrade.ToString("#,##0");
                    });
                DOTween.To(() => currentPriceHP, x => currentPriceHP = x, nextPriceHPUpgrade, timeTween)
                    .OnUpdate(() =>
                    {
                        txtPriceUpgradeHP.text = currentPriceHP.ToString("#,##0");
                    })
                    .OnComplete(() =>
                    {
                        OnUnLockButtons();
                    });
                txtHPAdd.rectTransform.DOAnchorPosX(txtHPAdd.rectTransform.anchoredPosition.x - 200, timeTween / 2).OnComplete(() =>
                    {
                        txtHPAdd.text = "+ " + (nextHPUpgrade - nextHP).ToString("#,##0");
                        txtHPAdd.rectTransform.DOAnchorPosX(txtHPAdd.rectTransform.anchoredPosition.x + 200, timeTween / 2);
                    });
            }
            else
            {
                txtMaxLevelHP.gameObject.SetActive(true);
                rectHPObject.gameObject.SetActive(false);
                OnUnLockButtons();
            }
        }
    }

    private void onClickButtonUpgradeSpeed()
    {
        Debug.Log("onClickButtonUpgradeSpeed");
        float currentSpeed = GameManager.Instance.OnGetPlayerSpeed();
        float currentSpeedUpgrade = GameManager.Instance.OnCalculateUpgradePlayerSpeed();
        int currentPriceUpgradeSpeed = GameManager.Instance.OnPriceUpgradeSpeed();

        if (GameManager.Instance.OnUpgradePlayerSpeed(currentPriceUpgradeSpeed, currentSpeedUpgrade))
        {
            OnLockButtons();
            int levelSpeed = GameManager.Instance.OnGetPlayerLevelSpeed();
            txtSpeedLevel.text = (levelSpeed + 1).ToString();

            UIMapHandle.Instance.OnUpdateGold();
            onSetDataHP();
            onSetDataStrong();

            if (levelSpeed < 9)
            {
                txtMaxLevelSpeed.gameObject.SetActive(false);
                rectSpeedObject.gameObject.SetActive(true);

                float nextSpeed = GameManager.Instance.OnGetPlayerSpeed();
                float nextSpeedUpgrade = GameManager.Instance.OnCalculateUpgradePlayerSpeed();
                int nextPriceUpgradeSpeed = GameManager.Instance.OnPriceUpgradeSpeed();
                if (GameManager.Instance.OnGetGold() < nextPriceUpgradeSpeed)
                {
                    imgButtonUpgradeSpeed.sprite = ResourceMapHandle.Instance.spriteButtonGray;
                    imgTextUpgradeSpeed.sprite = ResourceMapHandle.Instance.spriteTextUpgradeDisable;
                }
                else
                {
                    imgButtonUpgradeSpeed.sprite = ResourceMapHandle.Instance.spriteButtonGreen;
                    imgTextUpgradeSpeed.sprite = ResourceMapHandle.Instance.spriteTextUpgradeEnable;
                }

                DOTween.To(() => currentSpeed, x => currentSpeed = x, nextSpeed, timeTween)
                    .OnUpdate(() =>
                    {
                        txtSpeedValue.text = currentSpeed.ToString("F1");
                    });
                DOTween.To(() => currentSpeedUpgrade, x => currentSpeedUpgrade = x, nextSpeedUpgrade, timeTween)
                    .OnUpdate(() =>
                    {
                        txtSpeedUpgrade.text = currentSpeedUpgrade.ToString("F1");
                    });
                DOTween.To(() => currentPriceUpgradeSpeed, x => currentPriceUpgradeSpeed = x, nextPriceUpgradeSpeed, timeTween)
                    .OnUpdate(() =>
                    {
                        txtPriceUpgradeSpeed.text = currentPriceUpgradeSpeed.ToString("#,##0");
                    })
                    .OnComplete(() =>
                    {
                        OnUnLockButtons();
                    });
                txtSpeedAdd.rectTransform.DOAnchorPosX(txtSpeedAdd.rectTransform.anchoredPosition.x - 200, timeTween / 2)
                    .OnComplete(() =>
                    {
                        txtSpeedAdd.text = "+ " + (nextSpeedUpgrade - nextSpeed).ToString("F1");
                        txtSpeedAdd.rectTransform.DOAnchorPosX(txtSpeedAdd.rectTransform.anchoredPosition.x + 200, timeTween / 2);
                    });
                
            }
            else
            {
                txtMaxLevelSpeed.gameObject.SetActive(true);
                rectSpeedObject.gameObject.SetActive(false);
                OnUnLockButtons();
            }
        }
    }

    private void onClickButtonUpgradeStrong()
    {
        Debug.Log("onClickButtonUpgradeStrong");

        float currentStrong = GameManager.Instance.OnGetPlayerStrong();
        float currentStrongUpgrade = GameManager.Instance.OnCalculateUpgradePlayerStrong();
        int currentPriceUpgradeStrong = GameManager.Instance.OnPriceUpgradeStrong();

        if (GameManager.Instance.OnUpgradePlayerStrong(currentPriceUpgradeStrong, currentStrong))
        {
            OnLockButtons();

            int levelStrong = GameManager.Instance.OnGetPlayerLevelStrong();
            txtStrongLevel.text = (levelStrong + 1).ToString();

            UIMapHandle.Instance.OnUpdateGold();
            onSetDataHP();
            onSetDataSpeed();

            if (levelStrong < 9)
            {
                txtMaxLevelStrong.gameObject.SetActive(false);
                rectStrongObject.gameObject.SetActive(true);

                float nextStrong = GameManager.Instance.OnGetPlayerStrong();
                float nextStrongUpgrade = GameManager.Instance.OnCalculateUpgradePlayerStrong();
                int nextPriceUpgradeStrong = GameManager.Instance.OnPriceUpgradeStrong();

                if (GameManager.Instance.OnGetGold() < nextPriceUpgradeStrong)
                {
                    imgButtonUpgradeStrong.sprite = ResourceMapHandle.Instance.spriteButtonGray;
                    imgTextUpgradeStrong.sprite = ResourceMapHandle.Instance.spriteTextUpgradeDisable;
                }
                else
                {
                    imgButtonUpgradeStrong.sprite = ResourceMapHandle.Instance.spriteButtonGreen;
                    imgTextUpgradeStrong.sprite = ResourceMapHandle.Instance.spriteTextUpgradeEnable;
                }

                DOTween.To(() => currentStrong, x => currentStrong = x, nextStrong, timeTween)
                    .OnUpdate(() =>
                    {
                        txtStrongValue.text = currentStrong.ToString("#,##0");
                    });
                DOTween.To(() => currentStrongUpgrade, x => currentStrongUpgrade = x, nextStrongUpgrade, timeTween)
                    .OnUpdate(() =>
                    {
                        txtStrongUpgrade.text = currentStrongUpgrade.ToString("#,##0");
                    });
                DOTween.To(() => currentPriceUpgradeStrong, x => currentPriceUpgradeStrong = x, nextPriceUpgradeStrong, timeTween)
                    .OnUpdate(() =>
                    {
                        txtPriceUpgradeStrong.text = currentPriceUpgradeStrong.ToString("#,##0");
                    })
                    .OnComplete(() =>
                    {
                        OnUnLockButtons();
                    });
                txtStrongAdd.rectTransform.DOAnchorPosX(txtStrongAdd.rectTransform.anchoredPosition.x - 200, timeTween / 2)
                    .OnComplete(() =>
                    {
                        txtStrongAdd.text = "+ " + (nextStrongUpgrade - nextStrong).ToString("#,##0");
                        txtStrongAdd.rectTransform.DOAnchorPosX(txtStrongAdd.rectTransform.anchoredPosition.x + 200, timeTween / 2);
                    });
            }
            else
            {
                txtMaxLevelStrong.gameObject.SetActive(true);
                rectStrongObject.gameObject.SetActive(false);
                OnUnLockButtons();
            }
        }
    }

    private void onInitButtons(EventTrigger trigger, UnityAction evt)
    {
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.PointerClick;
        entry.callback.AddListener((data) =>
            {
                evt();
            });
        trigger.triggers.Add(entry);
    }

    private void onSetDataHP()
    {

        float hp = GameManager.Instance.OnGetPlayerHP();
        float hpUpgrade = GameManager.Instance.OnCalculateUpgradePlayerHP();
        int hpAdd = (int)(hpUpgrade - hp);
        int levelHP = GameManager.Instance.OnGetPlayerLevelHP() + 1;

        txtHPAdd.text = "+ " + hpAdd.ToString();
        txtHPUpgrade.text = hpUpgrade.ToString("#,##0");

        txtHPLevel.text = levelHP.ToString();
        txtHPValue.text = hp.ToString("#,##0");

        if (levelHP >= 9)
        {
            txtMaxLevelHP.gameObject.SetActive(true);
            rectHPObject.gameObject.SetActive(false);
        }
        else
        {
            txtMaxLevelHP.gameObject.SetActive(false);
            rectHPObject.gameObject.SetActive(true);

            int goldHP = GameManager.Instance.OnPriceUpgradePlayerHP();
            txtPriceUpgradeHP.text = goldHP.ToString("#,##0");

            if (GameManager.Instance.OnGetGold() < goldHP)
            {
                imgButtonUpgradeHP.sprite = ResourceMapHandle.Instance.spriteButtonGray;
                imgTextUpgradeHP.sprite = ResourceMapHandle.Instance.spriteTextUpgradeDisable;
            }
            else
            {
                imgButtonUpgradeHP.sprite = ResourceMapHandle.Instance.spriteButtonGreen;
                imgTextUpgradeHP.sprite = ResourceMapHandle.Instance.spriteTextUpgradeEnable;
            }

        }

    }

    private void onSetDataSpeed()
    {
        float speed = GameManager.Instance.OnGetPlayerSpeed();
        float speedUpgrade = GameManager.Instance.OnCalculateUpgradePlayerSpeed();
        float speedAdd = speedUpgrade - speed;
        int levelSpeed = GameManager.Instance.OnGetPlayerLevelSpeed() + 1;

        txtSpeedAdd.text = "+ " + speedAdd.ToString("F1");
        txtSpeedUpgrade.text = speedUpgrade.ToString("F1");

        txtSpeedLevel.text = levelSpeed.ToString();
        txtSpeedValue.text = speed.ToString("F1");

        if (levelSpeed >= 9)
        {
            txtMaxLevelSpeed.gameObject.SetActive(true);
            rectSpeedObject.gameObject.SetActive(false);
        }
        else
        {
            txtMaxLevelSpeed.gameObject.SetActive(false);
            rectSpeedObject.gameObject.SetActive(true);

            int goldSpeed = GameManager.Instance.OnPriceUpgradeSpeed();
            txtPriceUpgradeSpeed.text = goldSpeed.ToString("#,##0");
            if (GameManager.Instance.OnGetGold() < goldSpeed)
            {
                imgButtonUpgradeSpeed.sprite = ResourceMapHandle.Instance.spriteButtonGray;
                imgTextUpgradeSpeed.sprite = ResourceMapHandle.Instance.spriteTextUpgradeDisable;
            }
            else
            {
                imgButtonUpgradeSpeed.sprite = ResourceMapHandle.Instance.spriteButtonGreen;
                imgTextUpgradeSpeed.sprite = ResourceMapHandle.Instance.spriteTextUpgradeEnable;
            }
        }

    }

    private void onSetDataStrong()
    {
        float strong = GameManager.Instance.OnGetPlayerStrong();
        float strongUpgrade = GameManager.Instance.OnCalculateUpgradePlayerStrong();
        float strongAdd = strongUpgrade - strong;
        int levelStrong = GameManager.Instance.OnGetPlayerLevelStrong() + 1;

        txtStrongAdd.text = "+ " + strongAdd.ToString("F1");
        txtStrongUpgrade.text = strongUpgrade.ToString("F1");

        txtStrongLevel.text = levelStrong.ToString();
        txtStrongValue.text = strong.ToString("F1");

        if (levelStrong >= 9)
        {
            txtMaxLevelStrong.gameObject.SetActive(true);
            rectStrongObject.gameObject.SetActive(false);
        }
        else
        {
            txtMaxLevelStrong.gameObject.SetActive(false);
            rectStrongObject.gameObject.SetActive(true);

            int goldStrong = GameManager.Instance.OnPriceUpgradeStrong();
            txtPriceUpgradeStrong.text = goldStrong.ToString("#,##0");
            if (GameManager.Instance.OnGetGold() < goldStrong)
            {
                imgButtonUpgradeStrong.sprite = ResourceMapHandle.Instance.spriteButtonGray;
                imgTextUpgradeStrong.sprite = ResourceMapHandle.Instance.spriteTextUpgradeDisable;
            }
            else
            {
                imgButtonUpgradeStrong.sprite = ResourceMapHandle.Instance.spriteButtonGreen;
                imgTextUpgradeStrong.sprite = ResourceMapHandle.Instance.spriteTextUpgradeEnable;
            }
        }
    }

    #endregion
}
