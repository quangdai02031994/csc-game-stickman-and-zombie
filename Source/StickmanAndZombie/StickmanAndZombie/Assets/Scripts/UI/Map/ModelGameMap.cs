﻿using System;
using System.Collections.Generic;
using GameSupport;

[Serializable]
public class ModelGameMap
{
    /// <summary>
    /// ID map đang chơi
    /// </summary>
    public MAP_TYPES type;

    /// <summary>
    /// Level đang chơi
    /// </summary>
    public int level;

    /// <summary>
    /// Danh sách các map
    /// </summary>
    public List<ModelMap> listMap;

    public ModelGameMap()
    {
        type = MAP_TYPES.GRAY_1;
        level = 0;
        listMap = new List<ModelMap>();

        ModelMap _modelMap1 = new ModelMap(MAP_TYPES.GRAY_1, "MAP_GRAY", 10);
        ModelMap _modelMap2 = new ModelMap(MAP_TYPES.YELLOW_2, "MAP_YELLOW", 15);
        ModelMap _modelMap3 = new ModelMap(MAP_TYPES.GREEN_3, "MAP_GREEN", 15);
        ModelMap _modelMap4 = new ModelMap(MAP_TYPES.WHILE_4, "MAP_WHILE", 20);
        ModelMap _modelMap5 = new ModelMap(MAP_TYPES.RED_5, "MAP_RED", 20);
        listMap.Add(_modelMap1);
        listMap.Add(_modelMap2);
        listMap.Add(_modelMap3);
        listMap.Add(_modelMap4);
        listMap.Add(_modelMap5);
    }

}
