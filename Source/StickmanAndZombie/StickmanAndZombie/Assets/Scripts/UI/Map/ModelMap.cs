﻿using System.Collections;
using System.Collections.Generic;
using System;
using GameSupport;

[Serializable]
public class ModelMap
{
    /// <summary>
    /// Type map
    /// </summary>
    public MAP_TYPES type;

    /// <summary>
    /// Tên map
    /// </summary>
    public string name;

    /// <summary>
    /// Xếp hạng map
    /// </summary>
    public int rank;

    /// <summary>
    /// Level đang chơi
    /// </summary>
    public int idLevel;

    /// <summary>
    /// Xếp hạng các level
    /// </summary>
    public List<int> levels;


    public ModelMap(MAP_TYPES type, string name, int numberLevel)
    {
        this.type = type;
        this.name = name;
        this.rank = 0;
        this.idLevel = 0;
        this.levels = new List<int>();
        for (int i = 0; i < numberLevel; i++)
        {
            levels.Add(0);
        }
    }

}