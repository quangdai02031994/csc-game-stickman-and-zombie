﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using GameSupport;

public class ShopItemHandle : MonoBehaviour
{

    public GameObject prefabItem;

    public ScrollRect scrollListItem;

    public RectTransform rectListItem;

    public Button btnClose;

    public List<ShopItemModel> ListModelItemShop;

    #region Behaviour

    void Awake()
    {
        btnClose.onClick.AddListener(onClickButtonClose);
    }

    #endregion

    #region Publish Methods


    public void OnShowPanel()
    {
        this.gameObject.SetActive(true);
        if (rectListItem.transform.childCount == 0)
        {
            onCreateDataItems();
        }
    }

    public void OnUpdatePanel()
    {
        if (ListModelItemShop.Count > 0)
        {
            onUpdateDataItem();
        }
    }

    public void OnHidePanel()
    {
        UIMapHandle.Instance.OnUnlockButtons();
        this.gameObject.SetActive(false);
        onClearData();
    }

    #endregion

    #region Private methods

    private void onUpdateDataItem()
    {
        int count = ListModelItemShop.Count;
        if (count > 0)
        {
            for (int i = 0; i < count; i++)
            {
                ModelItem _item = GameManager.Instance.OnCheckItemExist(ListModelItemShop[i].modelItem.type);
                if (_item.type != ITEM_TYPE.NONE)
                {
                    ListModelItemShop[i].OnSetData(_item);
                }
                else
                {
                    ListModelItemShop[i].OnSetData(ListModelItemShop[i].modelItem);
                }
            }
        }
    }

    private void onCreateDataItems()
    {
        ListModelItemShop = new List<ShopItemModel>();
        int _countItem = Enum.GetNames(typeof(ITEM_TYPE)).Length;
        for (int i = 0; i < _countItem; i++)
        {
            ITEM_TYPE _type = (ITEM_TYPE)i;
            if (_type != ITEM_TYPE.NONE)
            {
                ModelItem _item = GameManager.Instance.OnCheckItemExist(_type);
                if (_item.type != ITEM_TYPE.NONE)
                {
                    onCreateItem(_item);
                }
                else
                {
                    onCreateItem(SaveAndLoadData.OnGetModelItemDefault(_type));
                }
            }
            else
            {
                continue;
            }
        }
    }



    private void onClickButtonClose()
    {
        Debug.Log("onClickButtonClose");
        OnHidePanel();
    }

    private void onClearData()
    {
        if (rectListItem.transform.childCount > 0)
        {
            ListModelItemShop.Clear();
            foreach (Transform item in rectListItem.transform)
            {
                Destroy(item.gameObject);
            }
        }
    }

    public void onCreateItem(ModelItem modelItem)
    {
        GameObject _obj = Instantiate(prefabItem, Vector3.zero, Quaternion.identity, rectListItem.transform);
        ShopItemModel _model = _obj.GetComponent<ShopItemModel>();
        _model.OnSetData(modelItem);
        ListModelItemShop.Add(_model);
    }

    #endregion
}
