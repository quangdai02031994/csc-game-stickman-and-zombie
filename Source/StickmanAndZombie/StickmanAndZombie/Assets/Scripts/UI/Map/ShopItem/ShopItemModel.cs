﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GameSupport;

public class ShopItemModel : MonoBehaviour
{
    public ModelItem modelItem;

    [SerializeField]
    private Image imgItemIcon;

    [SerializeField]
    private Image imgUsed;

    [SerializeField]
    private Text txtQuantity;

    [SerializeField]
    private Text txtItemName;

    [SerializeField]
    private Text txtItemDescription;

    [SerializeField]
    private Button btnBuy;

    [SerializeField]
    private Button btnUse;

    [SerializeField]
    private Image imgTextBuy;

    [SerializeField]
    private Image imgButtonBuy;

    [SerializeField]
    private Image imgUse_UnUse;

    [SerializeField]
    private Image imgButtonUse_UnUse;

    [SerializeField]
    private Text txtGoldValue;

    private int gold = 0;

    #region Behaviour

    void Awake()
    {
        btnBuy.onClick.AddListener(onClickButtonBuyItem);    
        btnUse.onClick.AddListener(onClickButtonUseItem);
    }


    #endregion

    #region Publish Methods

    public void OnSetData(ModelItem _model)
    {
        this.modelItem = _model;
        this.name = _model.type.ToString();
        txtItemName.text = modelItem.name;
        txtItemDescription.text = modelItem.description;
        if (modelItem.state != -1)
        {
            gold = modelItem.gold + modelItem.gold / 2 * modelItem.quantity;
        }
        else
        {
            gold = modelItem.gold + modelItem.gold / 2 * (modelItem.quantity - 1);
        }

        Debug.Log("Type: " + _model.type + " Quantity: " + modelItem.quantity);
        txtGoldValue.text = gold.ToString("#,##0");

        if (GameManager.Instance.OnGetGold() < gold)
        {
            btnBuy.enabled = false;
            imgButtonBuy.sprite = ResourceMapHandle.Instance.spriteButtonGray;
            imgTextBuy.sprite = ResourceMapHandle.Instance.spriteTextBuyDisable;
        }
        else
        {
            btnBuy.enabled = true;
            imgButtonBuy.sprite = ResourceMapHandle.Instance.spriteButtonGreen;
            imgTextBuy.sprite = ResourceMapHandle.Instance.spriteTextBuyEnable;
        }

        if (modelItem.state == 1)
        {
            imgUsed.gameObject.SetActive(true);
            if (modelItem.quantity != 0)
            {
                txtQuantity.gameObject.SetActive(true);
                txtQuantity.text = "x" + modelItem.quantity;
            }
            else
            {
                txtQuantity.gameObject.SetActive(false);
            }
            imgUse_UnUse.sprite = ResourceMapHandle.Instance.spriteTextUnUse;

            imgButtonUse_UnUse.sprite = ResourceMapHandle.Instance.spriteButtonBlue;
        }
        else if (modelItem.state == 0)
        {
            imgUsed.gameObject.SetActive(false);
            if (modelItem.quantity != 0)
            {
                txtQuantity.gameObject.SetActive(true);
                txtQuantity.text = "x" + modelItem.quantity;
            }
            else
            {
                txtQuantity.gameObject.SetActive(false);
            }
            imgUse_UnUse.sprite = ResourceMapHandle.Instance.spriteTextUse;
            imgButtonUse_UnUse.sprite = ResourceMapHandle.Instance.spriteButtonBlue;
        }
        else
        {
            imgUsed.gameObject.SetActive(false);
            txtQuantity.gameObject.SetActive(false);
            imgUse_UnUse.sprite = ResourceMapHandle.Instance.spriteTextUseDisable;
            imgButtonUse_UnUse.sprite = ResourceMapHandle.Instance.spriteButtonGray;
        }

        switch (_model.type)
        {
            case ITEM_TYPE.BLOOD_BOX:
                imgItemIcon.sprite = ResourceMapHandle.Instance.spriteItemBloodBox;
                break;
            case ITEM_TYPE.CARTRIDGE_BOX:
                imgItemIcon.sprite = ResourceMapHandle.Instance.spriteItemCartridgeBox;
                break;
            case ITEM_TYPE.POWER_DRINK:
                imgItemIcon.sprite = ResourceMapHandle.Instance.spriteItemPowerDrink;
                break;
            case ITEM_TYPE.SHOE_SPEED:
                imgItemIcon.sprite = ResourceMapHandle.Instance.spriteItemShoeSpeed;
                break;
            default:
                imgItemIcon.sprite = ResourceMapHandle.Instance.spriteItemNone;
                break;
        }
        imgButtonBuy.SetNativeSize();
        imgButtonUse_UnUse.SetNativeSize();
        imgUse_UnUse.SetNativeSize();
        imgItemIcon.SetNativeSize();
        imgTextBuy.SetNativeSize();
        imgUsed.SetNativeSize();
    }


    #endregion

    #region Private Methods

    private void onClickButtonBuyItem()
    {
        bool isSuccess = GameManager.Instance.OnBuyNewItem(modelItem.type, gold);
        if (isSuccess)
        {
            UIMapHandle.Instance.OnUpdateGold();
            UIMapHandle.Instance.OnBuyItemSuccess();
        }
    }

    private void onClickButtonUseItem()
    {
        if (modelItem.state == -1)
        {
            return;
        }
        else if (modelItem.state == 0)
        {
            bool isSuccess = GameManager.Instance.OnEquipItem(modelItem.type);
            if (isSuccess)
            {
                UIMapHandle.Instance.OnUseItemSuccess();
            }
        }
        else if (modelItem.state == 1)
        {
            bool isSuccess = GameManager.Instance.OnUnEquipedItem(modelItem.type);
            if (isSuccess)
            {
                UIMapHandle.Instance.OnUnUseItemSuccess();
            }
        }
    }

    #endregion

}
