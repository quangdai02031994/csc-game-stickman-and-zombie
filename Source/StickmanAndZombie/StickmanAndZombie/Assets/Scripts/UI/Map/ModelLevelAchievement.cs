﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModelLevelAchievement : MonoBehaviour
{
    public SpriteRenderer numberRenderer;
    public SpriteRenderer[] starRenderer;

    public void OnSetData(Vector3 pos, int rank, int level)
    {
        this.gameObject.SetActive(true);
        transform.position = pos;
        numberRenderer.sprite = ResourceMapHandle.Instance.numbers[level];
        switch (rank)
        {
            case 1:
                starRenderer[0].sprite = ResourceMapHandle.Instance.spriteStar;
                starRenderer[1].sprite = ResourceMapHandle.Instance.spriteStarDisable;
                starRenderer[2].sprite = ResourceMapHandle.Instance.spriteStarDisable;
                break;
            case 2:
                starRenderer[0].sprite = ResourceMapHandle.Instance.spriteStar;
                starRenderer[1].sprite = ResourceMapHandle.Instance.spriteStar;
                starRenderer[2].sprite = ResourceMapHandle.Instance.spriteStarDisable;
                break;
            case 3:
                starRenderer[0].sprite = ResourceMapHandle.Instance.spriteStar;
                starRenderer[1].sprite = ResourceMapHandle.Instance.spriteStar;
                starRenderer[2].sprite = ResourceMapHandle.Instance.spriteStar;
                break;
            default:
                starRenderer[0].sprite = ResourceMapHandle.Instance.spriteStarDisable;
                starRenderer[1].sprite = ResourceMapHandle.Instance.spriteStarDisable;
                starRenderer[2].sprite = ResourceMapHandle.Instance.spriteStarDisable;
                break;
        }
    }

    public void OnHide()
    {
        this.gameObject.SetActive(false);
    }
}
