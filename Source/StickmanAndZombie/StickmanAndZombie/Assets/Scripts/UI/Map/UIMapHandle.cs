﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GameSupport;
using DG.Tweening;
using System;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using Spine.Unity;

public enum PANEL
{
    MAP,
    UPGRADE_PLAYER,
    UPGRADE_WEAPON
}


public class UIMapHandle : MonoBehaviour
{
    public static UIMapHandle Instance{ get; private set; }

    public MAP_TYPES mapSelected = MAP_TYPES.NONE;

    public PANEL mapPanel = PANEL.MAP;

    public ShopItemHandle shopItemHandle;

    public UIUpgradePlayerHandle upgradePlayerHandle;

    public PlayerUIAnimation playerAnimation;

    [Header("Objects Child")]
    public MapHandle mapHandle;

    public GameObject objUIMapBG;

    public GameObject objUIMapGold;

    public GameObject objUIMapRoot;

    public Camera cameraMap;

    [Header("Bottom Left")]
    public RectTransform rectButtonsPlayerGun;

    public EventTrigger btnGun;

    public EventTrigger btnPlayer;

    public RectTransform rectButtonsUpgradeWeapon;

    public EventTrigger btnEquipBuy;

    public EventTrigger btnUnEquipExit;

    public Image imgButtonEquipBuy;

    public Image imgButtonUnEquipExit;

    public Image imgButtonTitleEquipBuy;

    public Image imgButtonTitleUnEquipExit;
  
    [Header("Bottom Right")]
    public Button btnStartGame;

    [Header("Top Left")]
    public Button btnBack;

    public Image imgTitle;

    public RectTransform rectBGMission;

    public RectTransform rectButtonStart;

    public RectTransform rectBGShop;

    public RectTransform rectBGTitle;

    public RectTransform rectIconSetting;
    
    [Header("Top Right")]
    public Text txtGold;

    public Button btnAddGold;

    [Header("Left")]

    public RectTransform rectPanelPlayer;

    public Image imgPlayerBox;

    public Image imgPlayerLight;

    public Button btnPlayerLightCenter;

    public SkeletonGraphic skeletonGraphicsPlayer;

    public Button btnShop;

    public Image[] imgItemIcon = new Image[2];

    public Text[] txtItemQuantity = new Text[2];

    public Button btnCloseMission;

    private Vector3 startBGShopPos;
    private Vector3 endBGShopPos;

    private Vector3 startButtonStartPos;
    private Vector3 endButtonStartPos;

    private Vector3 startBGMissionPos;
    private Vector3 endBGMissionPos;

    private Vector2 endRoomUpgradePlayer = new Vector2(450, 0);
    private Vector2 endRoomUpgradeWeaponPos;
    private Vector2 startRoomPos = new Vector2(-920, 0);

    private Vector2 startButtonsGunPlayer = new Vector2(420, -256);
    private Vector2 endButtonsGunPlayer = new Vector2(420, 50);


    private float timeTween = 1;

    #region Behaviour

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            Input.multiTouchEnabled = false;
        }

        onSettingButtons();
        OnUpdateGold();
    }

    void Start()
    {
        DOTween.To(() => rectIconSetting.transform.localEulerAngles, x => rectIconSetting.transform.localEulerAngles = x, Vector3.zero, timeTween * 2);
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0) && mapHandle.enableSelectMap)
        {
            RaycastHit2D _hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
            if (_hit.collider != null)
            {
                int _index = mapHandle.OnCheckMapSelect(_hit.collider);
                MAP_TYPES _type = (MAP_TYPES)_index;
                if (_type != mapSelected)
                {
                    mapSelected = _type;
                    Vector3 _target = mapHandle.onSelectMap(mapSelected);
                    if (_target != Vector3.zero)
                    {
                        onZoomInMap(mapSelected, _target);
                    }
                }
            }
        }
        else if (Input.GetMouseButtonDown(0) && mapHandle.enableSelectLevel)
        {
            RaycastHit2D _hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
            if (_hit.collider != null)
            {
                mapHandle.OnClickLevel(_hit.collider);
            }
        }

    }

    #endregion



    #region Publish Methods

    public void OnUpdateGold()
    {
        txtGold.text = GameManager.Instance.OnGetGold().ToString("#,##0");
    }

    public void OnBuyItemSuccess()
    {
        shopItemHandle.OnUpdatePanel();
        onRefreshItemUse();
    }

    public void OnUseItemSuccess()
    {
        shopItemHandle.OnUpdatePanel();
        onRefreshItemUse();
    }

    public void OnUnUseItemSuccess()
    {
        shopItemHandle.OnUpdatePanel();
        onRefreshItemUse();
    }

    public void OnUnlockButtons()
    {
        btnAddGold.enabled = true;
        btnBack.enabled = true;
        btnGun.enabled = true;
        btnPlayer.enabled = true;
        btnStartGame.enabled = true;
        btnCloseMission.enabled = true;
        btnShop.enabled = true;
    }

    #endregion

    #region Buttons Handle

    private void onClickButtonStart()
    {
        Debug.Log("onClickButtonStart");            
        if (mapSelected != MAP_TYPES.NONE
            && GameManager.Instance.OnGetModelLevel() != null
            && GameManager.Instance.OnGetModelLevel().ListMonster.Count > 0)
        {
            GameAnalys.Instance.OnSetData(mapSelected, GameManager.Instance.OnGetModelLevel().id);
            GameManager.Instance.OnLoadingSceneAsync(ScenesName.GamePlay);
            onHideScene();
        }
    }

    private void onClickButtonPlayer()
    {
        Debug.Log("onClickButtonPlayer");
    }

    private void onClickButtonShop()
    {
        Debug.Log("onClickButtonShop"); 
        onLockButtons();
        shopItemHandle.OnShowPanel();
    }

    private void onClickButtonCloseMission()
    {
        if (rectBGMission.gameObject.activeSelf)
        {
            onLockButtons();
            rectBGMission.DOAnchorPos(startBGMissionPos, timeTween).OnComplete(() =>
                {
                    OnUnlockButtons();
                    rectBGMission.gameObject.SetActive(false);
                });
        }
    }

    private void onClickButtonBack()
    {
        switch (mapPanel)
        {
            case PANEL.MAP:
                if (mapSelected == MAP_TYPES.NONE)
                {
                    GameManager.Instance.OnFadingSceneAsync(ScenesName.Home);
                }
                else
                {
                    onZoomOutMap(() =>
                        {
                            mapSelected = MAP_TYPES.NONE;
                        });
                }
                break;
            case PANEL.UPGRADE_PLAYER:
                onHideUpgradePlayer();
                break;
            case PANEL.UPGRADE_WEAPON:

                break;
        }
    }

    private void onClickButtonAddGold()
    {
        Debug.Log("onClickButtonAddGold");
    }

    private void onClickButtonUpgradeGun()
    {
        Debug.Log("onclickButtonUpgradeGun");
    }

    private void onClickButtonUpgradePlayer()
    {
        Debug.Log("onClickButtonUpgradePlayer");
        onShowUpgradePlayer();
    }

    #endregion


    #region Private Methods

    private void onPlayAnimation(PLAYER_ARMS arm)
    {
        playerAnimation.OnPlayAnimation(arm);
    }

    private void onShowUpgradePlayer()
    {
        mapPanel = PANEL.UPGRADE_PLAYER;
        onLockButtons();

        onChangeTitleState();

        mapHandle.OnHideMaps();

        rectPanelPlayer.anchoredPosition = startRoomPos;
        rectPanelPlayer.gameObject.SetActive(true);
        rectPanelPlayer.DOAnchorPos(endRoomUpgradePlayer, timeTween / 2).OnComplete(() =>
            {
                imgPlayerBox.gameObject.SetActive(true);
                Color color = imgPlayerBox.color;
                color.a = 0;
                imgPlayerBox.color = color;
                imgPlayerBox.DOFade(1, timeTween / 2);
                
                imgPlayerLight.gameObject.SetActive(true);
                Color color2 = imgPlayerLight.color;
                color2.a = 0;
                imgPlayerLight.color = color2;
                imgPlayerLight.DOFade(1, timeTween / 2).OnComplete(() =>
                    {
                        OnUnlockButtons();
                        btnPlayerLightCenter.gameObject.SetActive(true);
                        skeletonGraphicsPlayer.gameObject.SetActive(true);
                        onPlayAnimation(PLAYER_ARMS.SWORD);
                    });
            });
        rectButtonsPlayerGun.DOAnchorPos(startButtonsGunPlayer, timeTween / 2).OnComplete(() =>
            {
                rectButtonsPlayerGun.gameObject.SetActive(false);
            });
        
        upgradePlayerHandle.OnShow();
    }

    private void onHideUpgradePlayer()
    {
        onLockButtons();
        onChangeTitleState();
        rectPanelPlayer.DOAnchorPos(startRoomPos, timeTween).OnComplete(() =>
            {
                Color color = imgPlayerBox.color;
                color.a = 0;
                imgPlayerBox.color = color;
                imgPlayerBox.gameObject.SetActive(false);

                Color color2 = imgPlayerLight.color;
                color2.a = 0;
                imgPlayerLight.gameObject.SetActive(false);

                btnPlayerLightCenter.gameObject.SetActive(false);
                skeletonGraphicsPlayer.gameObject.SetActive(false);
                rectPanelPlayer.gameObject.SetActive(false);
            });
        rectButtonsPlayerGun.gameObject.SetActive(true);
        rectButtonsPlayerGun.DOAnchorPos(endButtonsGunPlayer, timeTween).OnComplete(() =>
            {
                OnUnlockButtons();
                mapPanel = PANEL.MAP;
            });
        upgradePlayerHandle.OnHide();
        mapHandle.OnShowMap();
    }

    private void onChangeTitleState(Sprite spriteTitle = null)
    {
        rectBGTitle.DOAnchorPosX(-500, timeTween / 2)
            .OnComplete(() =>
            {
                rectBGTitle.DOAnchorPosX(0, timeTween / 2);
                if (spriteTitle != null)
                {
                    imgTitle.sprite = spriteTitle;
                    imgTitle.SetNativeSize();
                }
            });
        
        DOTween.To(() => rectIconSetting.transform.localEulerAngles, x => rectIconSetting.transform.localEulerAngles = x, new Vector3(0, 0, 180), timeTween / 2)
            .OnComplete(() =>
            {
                DOTween.To(() => rectIconSetting.transform.localEulerAngles, x => rectIconSetting.transform.localEulerAngles = x, new Vector3(0, 0, 0), timeTween / 2);
            });
    }

    private void onHideScene()
    {
        objUIMapBG.SetActive(false);
        objUIMapGold.SetActive(false);
        objUIMapRoot.SetActive(false);
        mapHandle.gameObject.SetActive(false);
    }

    private void onRefreshItemUse()
    {
        ModelItem item = GameManager.Instance.OnGetModelItems(0);
        ModelItem _item1 = GameManager.Instance.OnGetModelItems(1);
        onSetDataItemEquiped(0, item);
        onSetDataItemEquiped(1, _item1);
    }

    private void onSetDataItemEquiped(int index, ModelItem _item)
    {
        switch (_item.type)
        {
            case ITEM_TYPE.BLOOD_BOX:
                if (_item.quantity > 0)
                {
                    txtItemQuantity[index].gameObject.SetActive(true);
                    txtItemQuantity[index].text = "x" + _item.quantity;
                }
                else
                {
                    txtItemQuantity[index].gameObject.SetActive(false);
                }
                imgItemIcon[index].sprite = ResourceMapHandle.Instance.spriteItemBloodBox;
                break;
            case ITEM_TYPE.CARTRIDGE_BOX:
                if (_item.quantity > 0)
                {
                    txtItemQuantity[index].gameObject.SetActive(true);
                    txtItemQuantity[index].text = "x" + _item.quantity;
                }
                else
                {
                    txtItemQuantity[index].gameObject.SetActive(false);
                }
                imgItemIcon[index].sprite = ResourceMapHandle.Instance.spriteItemCartridgeBox;
                break;
            case ITEM_TYPE.POWER_DRINK:
                if (_item.quantity > 0)
                {
                    txtItemQuantity[index].gameObject.SetActive(true);
                    txtItemQuantity[index].text = "x" + _item.quantity;
                }
                else
                {
                    txtItemQuantity[index].gameObject.SetActive(false);
                }
                imgItemIcon[index].sprite = ResourceMapHandle.Instance.spriteItemPowerDrink;
                break;
            case ITEM_TYPE.SHOE_SPEED:
                if (_item.quantity > 0)
                {
                    txtItemQuantity[index].gameObject.SetActive(true);
                    txtItemQuantity[index].text = "x" + _item.quantity;
                }
                else
                {
                    txtItemQuantity[index].gameObject.SetActive(false);
                }
                imgItemIcon[index].sprite = ResourceMapHandle.Instance.spriteItemShoeSpeed;
                break;
            default:
                imgItemIcon[index].sprite = ResourceMapHandle.Instance.spriteItemNone;
                txtItemQuantity[index].gameObject.SetActive(false);
                break;
        }
    }

    private void onLockButtons()
    {
        btnAddGold.enabled = false;
        btnBack.enabled = false;
        btnGun.enabled = false;
        btnPlayer.enabled = false;
        btnStartGame.enabled = false;
        btnCloseMission.enabled = false;
        btnShop.enabled = false;
    }




    private void onSettingButtons()
    {
        btnAddGold.onClick.AddListener(onClickButtonAddGold);
        btnBack.onClick.AddListener(onClickButtonBack);
        onRegisButtonEvent(btnGun, () =>
            {
                onClickButtonUpgradeGun();
            });
        onRegisButtonEvent(btnPlayer, () =>
            {
                onClickButtonUpgradePlayer();
            });

        btnStartGame.onClick.AddListener(onClickButtonStart);
        btnCloseMission.onClick.AddListener(onClickButtonCloseMission);
        btnShop.onClick.AddListener(onClickButtonShop);

        btnPlayerLightCenter.onClick.AddListener(onClickButtonPlayer);

        endBGMissionPos = rectBGMission.anchoredPosition;
        startBGMissionPos = new Vector3(endBGMissionPos.x - 600, endBGMissionPos.y, endBGMissionPos.z);

        endButtonStartPos = rectButtonStart.anchoredPosition;
        startButtonStartPos = new Vector3(endButtonStartPos.x + 600, endButtonStartPos.y, endButtonStartPos.z);

        endBGShopPos = rectBGShop.anchoredPosition;
        startBGShopPos = new Vector3(endBGShopPos.x - 600, endBGShopPos.y, endBGShopPos.z);


        rectButtonStart.anchoredPosition = startButtonStartPos;
        rectBGMission.anchoredPosition = startBGMissionPos;
        rectBGShop.anchoredPosition = startBGShopPos;


        rectBGShop.gameObject.SetActive(false);
        rectButtonStart.gameObject.SetActive(false);
        rectBGMission.gameObject.SetActive(false);

    }

    private void onZoomInMap(MAP_TYPES type, Vector3 targetPos, Action actComplete = null)
    {
        onLockButtons();
        onChangeTitleState();
        cameraMap.transform.DOMove(new Vector3(targetPos.x, targetPos.y, cameraMap.transform.position.z), timeTween);
        cameraMap.DOOrthoSize(3.5f, timeTween)
            .OnComplete(() =>
            {
                OnUnlockButtons();
                mapHandle.OnZoomInMap(type);
                if (actComplete != null)
                    actComplete();
            });

        if (!rectBGMission.gameObject.activeSelf)
        {
            rectBGMission.gameObject.SetActive(true);
            rectBGMission.anchoredPosition = startBGMissionPos;
            rectBGMission.DOAnchorPos(endBGMissionPos, timeTween);
        }

        rectButtonsPlayerGun.DOAnchorPos(startButtonsGunPlayer, timeTween).OnComplete(() =>
            {
                rectButtonsPlayerGun.gameObject.SetActive(false);
            });

        if (!rectBGShop.gameObject.activeSelf)
        {
            rectBGShop.gameObject.SetActive(true);
            rectBGShop.anchoredPosition = startBGShopPos;
            rectBGShop.DOAnchorPos(endBGShopPos, timeTween);
            onRefreshItemUse();
        }

        if (!rectButtonStart.gameObject.activeSelf)
        {
            rectButtonStart.gameObject.SetActive(true);
            rectButtonStart.anchoredPosition = startButtonStartPos;
            rectButtonStart.DOAnchorPos(endButtonStartPos, timeTween);
        }
    }


    private void onZoomOutMap(Action actComplete = null)
    {
        onLockButtons();

        onChangeTitleState();

        rectButtonsPlayerGun.gameObject.SetActive(true);
        rectButtonsPlayerGun.DOAnchorPos(endButtonsGunPlayer, timeTween);
        cameraMap.transform.DOMove(new Vector3(0, 0, cameraMap.transform.position.z), timeTween)
            .OnComplete(() =>
            {
                mapHandle.OnZoomOutMap();
                OnUnlockButtons();
                if (actComplete != null)
                    actComplete();
            });
        cameraMap.DOOrthoSize(5, timeTween);

        if (rectBGMission.gameObject.activeSelf)
        {
            rectBGMission.DOAnchorPos(startBGMissionPos, timeTween).OnComplete(() =>
                {
                    rectBGMission.gameObject.SetActive(false);
                });
        }
        if (rectButtonStart.gameObject.activeSelf)
        {
            rectButtonStart.DOAnchorPos(startButtonStartPos, timeTween).OnComplete(() =>
                {
                    rectButtonStart.gameObject.SetActive(false);
                });
        }

        if (rectBGShop.gameObject.activeSelf)
        {
            rectBGShop.DOAnchorPos(startBGShopPos, timeTween).OnComplete(() =>
                {
                    rectBGShop.gameObject.SetActive(false);
                });
        }
      
    }

    private void onRegisButtonEvent(EventTrigger button, UnityAction action)
    {
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.callback.AddListener((data) =>
            {
                action();
            });
        entry.eventID = EventTriggerType.PointerClick;
        button.triggers.Add(entry);
    }

    #endregion


}
