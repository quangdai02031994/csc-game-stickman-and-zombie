﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourceMapHandle : MonoBehaviour
{
    public static ResourceMapHandle Instance{ get; private set; }

    public GameObject prefabLevelAchievement;
    public Sprite[] numbers;

    [Space(10)]
    public Sprite spriteStar;
    public Sprite spriteStarDisable;

    [Space(10)]
    public Sprite spriteItemCartridgeBox;
    public Sprite spriteItemShoeSpeed;
    public Sprite spriteItemPowerDrink;
    public Sprite spriteItemBloodBox;
    public Sprite spriteItemNone;


    [Space(10)]
    public Sprite spriteButtonBlue;
    public Sprite spriteButtonGray;
    public Sprite spriteButtonGreen;

    [Space(10)]
    public Sprite spriteTextBuyDisable;
    public Sprite spriteTextBuyEnable;
    public Sprite spriteTextUseDisable;
    public Sprite spriteTextUse;
    public Sprite spriteTextUnUse;
    public Sprite spriteTextUpgradeDisable;
    public Sprite spriteTextUpgradeEnable;


    #region Behaviour

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    #endregion

}
