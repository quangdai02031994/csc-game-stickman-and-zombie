﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSupport;
using DG.Tweening;

public class MapHandle : MonoBehaviour
{
    public bool enableSelectMap    { get; private set; }

    public bool enableSelectLevel{ get; private set; }

    [Header("Model Map")]
    public ModelMap modelMap;

    [Header("Map Achievement List")]
    public Transform transformAchievement;
    private List<ModelLevelAchievement> ListLevelAchievement;

    [Header("Flag")]
    public Transform transformFlag;
    public Transform transformArrowSelect;
    public SpriteRenderer[] starRenderer = new SpriteRenderer[3];
    public SpriteRenderer numberFlagRenderer;

    [Header("Maps")]
    public List<Collider2D> listMap;
    
    public List<Transform> listMapDisable;
    
    public List<Transform> listMapHighlight;

    public List<Transform> listMapLevel;

    [Header("Map Levels")]
    public List<Collider2D> listLevelMap1;

    public List<Collider2D> listLevelMap2;

    public List<Collider2D> listLevelMap3;

    public List<Collider2D> listLevelMap4;

    public List<Collider2D> listLevelMap5;

    private float timeTween = 0.5f;

    #region Behaviour

    private void Awake()
    {
        OnUnLockMap();
        OnLockSelectLevel();
        onLoadDataMap(GameManager.Instance.OnGetModelGameMap().type);
    }

    private void Start()
    {
        ListLevelAchievement = new List<ModelLevelAchievement>();
        for (int i = 0; i < 20; i++)
        {
            onCreateLevelAchievement(Vector3.zero, 0, 0);
        }
    }

    #endregion

    #region Publish Methods

    public void OnShowMap()
    {
        transform.localPosition = new Vector3(0, 10, 0);
        transform.gameObject.SetActive(true);
        OnLockMap();
        transform.DOLocalMoveY(0, timeTween).OnComplete(() =>
            {
                OnUnLockMap();
            });
    }

    public void OnHideMaps()
    {
        OnLockMap();
        transform.DOLocalMoveY(10, timeTween).OnComplete(() =>
            {
                OnUnLockMap();
                this.gameObject.SetActive(false);
            });
    }

    public void OnClickLevel(Collider2D level)
    {
        if (modelMap == null)
            return;
        MAP_TYPES type = (MAP_TYPES)modelMap.type;
        switch (type)
        {
            case MAP_TYPES.GRAY_1:
                onCreateLevel(type, listLevelMap1.IndexOf(level));
                break;
            case MAP_TYPES.YELLOW_2:
                onCreateLevel(type, listLevelMap2.IndexOf(level));
                break;
            case MAP_TYPES.GREEN_3:
                onCreateLevel(type, listLevelMap3.IndexOf(level));
                break;
            case MAP_TYPES.WHILE_4:
                onCreateLevel(type, listLevelMap4.IndexOf(level));
                break;
            case MAP_TYPES.RED_5:
                onCreateLevel(type, listLevelMap5.IndexOf(level));
                break;
        }
        Vector3 posArrow = new Vector3(level.transform.position.x, level.transform.position.y + 0.5f, level.transform.position.z);
        if (transformArrowSelect.gameObject.activeSelf)
        {
            if (posArrow != transformArrowSelect.position)
            {
                transformArrowSelect.DOMove(posArrow, timeTween)
                    .OnStart(() =>
                    {
                        OnLockSelectLevel();
                    }).OnComplete(() =>
                    {
                        OnUnLockSelectLevel();
                    });
            }
        }
        else
        {
            transformArrowSelect.gameObject.SetActive(true);
            transformArrowSelect.position = posArrow;
        }
    }

    public int OnCheckMapSelect(Collider2D collider)
    {
        
        if (listMap.Count == 0)
            return 0;
        for (int i = 0; i < listMap.Count; i++)
        {
            if (listMap[i] == collider)
            {
                return (i + 1);
            }
        }
        return 0;
    }

    public Vector3 onSelectMap(MAP_TYPES type)
    {
        Vector3 targetPos = Vector3.zero;
        if (type == MAP_TYPES.NONE)
            return targetPos;
        int _index = (int)type - 1;
        targetPos = listMap[_index].transform.position;
        listMapHighlight[(int)type - 1].gameObject.SetActive(true);
        OnLockMap();
        return targetPos;
    }

    public void OnZoomOutMap()
    {
        MAP_TYPES _type = (MAP_TYPES)modelMap.type;
        modelMap = null;
        GameManager.Instance.OnResetLevel();
        onHideMapLevel(_type);
        OnUnLockMap();
        listMap[(int)_type - 1].enabled = true;
        listMapHighlight[(int)_type - 1].gameObject.SetActive(false);
    }

    public void OnZoomInMap(MAP_TYPES type)
    {
        Debug.Log("Zoomin map: " + type + " index: " + (int)type);
        modelMap = GameManager.Instance.OnGetModelMap(type);
        onShowMapLevel(type, modelMap.idLevel);
        listMap[(int)type - 1].enabled = false;
    }

    public void OnLockMap()
    {
        enableSelectMap = false;
    }

    public void OnUnLockMap()
    {
        enableSelectMap = true;
    }

    public void OnLockSelectLevel()
    {
        enableSelectLevel = false;
    }

    public void OnUnLockSelectLevel()
    {
        enableSelectLevel = true;
    }

    #endregion

    #region Private Methods

    private void onLoadDataMap(MAP_TYPES type)
    {
        Debug.Log("Current Map: " + type);
        int index = (int)type - 1;
        for (int i = 0; i < listMap.Count; i++)
        {
            if (i <= index)
            {
                listMap[i].enabled = true;
                listMapDisable[i].gameObject.SetActive(false);
                listMapLevel[i].gameObject.SetActive(false);
            }
            else
            {
                listMap[i].enabled = false;
                listMapDisable[i].gameObject.SetActive(true);
                listMapLevel[i].gameObject.SetActive(false);
            }
        }
    }

    private void onCreateLevel(MAP_TYPES type, int idLevel)
    {
        Debug.Log("Create level: " + idLevel + " in map: " + type);
        ModelLevel _modelLevel = new ModelLevel();
        _modelLevel.id = idLevel;
        _modelLevel.countMonster = 15;
        _modelLevel.idMonster = -1;
        _modelLevel.name = type;
        _modelLevel.rank = 0;
        _modelLevel.timePass = 0;
        _modelLevel.type = LEVEL_TYPE.COUNT_MONSTER;
        onCreateListMonster(_modelLevel.countMonster, _modelLevel.ListMonster);
        GameManager.Instance.OnSetLevel(_modelLevel);
    }

    private void onCreateListMonster(int countMonster, List<ModelMonster> listMonster)
    {
        for (int i = 0; i < countMonster; i++)
        {
            listMonster.Add(SaveAndLoadData.OnGetModelMonsterHand());
        }
    }

    private void onShowMapLevel(MAP_TYPES type, int indexMap)
    {
        Debug.Log("Show level map: " + type + " level: " + indexMap);
        listMapLevel[(int)type - 1].gameObject.SetActive(true);
        Vector3 posFlag = Vector3.zero;
        switch (type)
        {
            case MAP_TYPES.GRAY_1:
                for (int i = 0; i < listLevelMap1.Count; i++)
                {
                    if (i <= indexMap)
                    {
                        listLevelMap1[i].gameObject.SetActive(true);
                        if (i < indexMap)
                        {
                            ListLevelAchievement[i].OnSetData(listLevelMap1[i].transform.position, modelMap.levels[i], i);
                        }
                    }
                    else
                    {
                        listLevelMap1[i].gameObject.SetActive(false);
                    }
                }
                break;
            case MAP_TYPES.YELLOW_2:
                for (int i = 0; i < listLevelMap2.Count; i++)
                {
                    if (i <= indexMap)
                    {
                        listLevelMap2[i].gameObject.SetActive(true);
                        if (i < indexMap)
                        {
                            ListLevelAchievement[i].OnSetData(listLevelMap2[i].transform.position, modelMap.levels[i], i);
                        }
                    }
                    else
                    {
                        listLevelMap2[i].gameObject.SetActive(false);
                    }
                }
                break;
            case MAP_TYPES.GREEN_3:
                for (int i = 0; i < listLevelMap3.Count; i++)
                {
                    if (i <= indexMap)
                    {
                        listLevelMap3[i].gameObject.SetActive(true);
                        if (i < indexMap)
                        {
                            ListLevelAchievement[i].OnSetData(listLevelMap3[i].transform.position, modelMap.levels[i], i);
                        }
                    }
                    else
                    {
                        listLevelMap3[i].gameObject.SetActive(false);
                    }
                }
                break;
            case MAP_TYPES.WHILE_4:
                for (int i = 0; i < listLevelMap4.Count; i++)
                {
                    if (i <= indexMap)
                    {
                        listLevelMap4[i].gameObject.SetActive(true);
                        if (i < indexMap)
                        {
                            ListLevelAchievement[i].OnSetData(listLevelMap4[i].transform.position, modelMap.levels[i], i);
                        }
                    }
                    else
                    {
                        listLevelMap4[i].gameObject.SetActive(false);
                    }
                }
                break;
            case MAP_TYPES.RED_5:
                for (int i = 0; i < listLevelMap5.Count; i++)
                {
                    if (i <= indexMap)
                    {
                        listLevelMap5[i].gameObject.SetActive(true);
                        if (i < indexMap)
                        {
                            ListLevelAchievement[i].OnSetData(listLevelMap5[i].transform.position, modelMap.levels[i], i);
                        }
                    }
                    else
                    {
                        listLevelMap5[i].gameObject.SetActive(false);
                    }
                }
                break;
        }
        onShowFlagLevel(type, indexMap);
    }

    private void onShowFlagLevel(MAP_TYPES type, int indexLevel)
    {
        if (indexLevel == modelMap.levels.Count)
        {
            Debug.Log("Map clear");
        }
        else
        {
            Vector3 posFlag = Vector3.zero;
            int rank = modelMap.levels[indexLevel];
            numberFlagRenderer.sprite = ResourceMapHandle.Instance.numbers[indexLevel];
            switch (type)
            {
                case MAP_TYPES.GRAY_1:
                    posFlag = listLevelMap1[indexLevel].transform.position;
                    break;
                case MAP_TYPES.YELLOW_2:
                    posFlag = listLevelMap2[indexLevel].transform.position;
                    break;
                case MAP_TYPES.GREEN_3:
                    posFlag = listLevelMap3[indexLevel].transform.position;
                    break;
                case MAP_TYPES.WHILE_4:
                    posFlag = listLevelMap4[indexLevel].transform.position;
                    break;
                case MAP_TYPES.RED_5:
                    posFlag = listLevelMap5[indexLevel].transform.position;
                    break;
            }
            transformFlag.gameObject.SetActive(true);
            transformFlag.position = posFlag;
            switch (rank)
            {
                case 1:
                    starRenderer[0].sprite = ResourceMapHandle.Instance.spriteStar;
                    starRenderer[1].sprite = ResourceMapHandle.Instance.spriteStarDisable;
                    starRenderer[2].sprite = ResourceMapHandle.Instance.spriteStarDisable;
                    break;
                case 2:
                    starRenderer[0].sprite = ResourceMapHandle.Instance.spriteStar;
                    starRenderer[1].sprite = ResourceMapHandle.Instance.spriteStar;
                    starRenderer[2].sprite = ResourceMapHandle.Instance.spriteStarDisable;
                    break;
                case 3:
                    starRenderer[0].sprite = ResourceMapHandle.Instance.spriteStar;
                    starRenderer[1].sprite = ResourceMapHandle.Instance.spriteStar;
                    starRenderer[2].sprite = ResourceMapHandle.Instance.spriteStar;
                    break;
                default:
                    starRenderer[0].sprite = ResourceMapHandle.Instance.spriteStarDisable;
                    starRenderer[1].sprite = ResourceMapHandle.Instance.spriteStarDisable;
                    starRenderer[2].sprite = ResourceMapHandle.Instance.spriteStarDisable;
                    break;
            }
        }
        OnUnLockSelectLevel();
    }

    private void onHideMapLevel(MAP_TYPES type)
    {
        switch (type)
        {
            case MAP_TYPES.GRAY_1:
                for (int i = 0; i < listLevelMap1.Count; i++)
                {
                    listLevelMap1[i].gameObject.SetActive(false);
                }
                break;
            case MAP_TYPES.YELLOW_2:
                for (int i = 0; i < listLevelMap2.Count; i++)
                {
                    listLevelMap2[i].gameObject.SetActive(false);
                }
                break;
            case MAP_TYPES.GREEN_3:
                for (int i = 0; i < listLevelMap3.Count; i++)
                {
                    listLevelMap3[i].gameObject.SetActive(false);
                }
                break;
            case MAP_TYPES.WHILE_4:
                for (int i = 0; i < listLevelMap4.Count; i++)
                {
                    listLevelMap4[i].gameObject.SetActive(false);
                }
                break;
            case MAP_TYPES.RED_5:
                for (int i = 0; i < listLevelMap5.Count; i++)
                {
                    listLevelMap5[i].gameObject.SetActive(false);
                }
                break;
            default:
                break;
        }
        listMapLevel[(int)type - 1].gameObject.SetActive(false);
        transformFlag.gameObject.SetActive(false);
        transformArrowSelect.gameObject.SetActive(false);
        OnLockSelectLevel();
        for (int i = 0; i < ListLevelAchievement.Count; i++)
        {
            ListLevelAchievement[i].OnHide();
        }
    }

    private void onCreateLevelAchievement(Vector3 startPos, int rank, int number, bool active = false)
    {
        GameObject obj = Instantiate(ResourceMapHandle.Instance.prefabLevelAchievement, Vector3.zero, Quaternion.identity, transformAchievement) as GameObject;
        obj.transform.position = startPos;
        obj.name = "itemAchievement";
        obj.transform.localScale = Vector3.one;
        ModelLevelAchievement _modelLevel = obj.GetComponent<ModelLevelAchievement>();
        ListLevelAchievement.Add(_modelLevel);
        _modelLevel.gameObject.SetActive(active);
    }

    #endregion


}
