﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using GameSupport;

[RequireComponent(typeof(EventTrigger))]
public class UIPress : MonoBehaviour
{
    void Start()
    {
        EventTrigger trigger = GetComponent<EventTrigger>();
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.PointerDown;
        entry.callback.AddListener((data) =>
            {
                OnPointerDown((PointerEventData)data);
            });
        trigger.triggers.Add(entry);
    }

    private void OnPointerDown(PointerEventData data)
    {
        transform.localScale = Vector3.one * 1.2f;
    }

}
