﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GameSupport;

public class CameraHandle : MonoBehaviour
{

    public static CameraHandle Instance{ get; private set; }

    [SerializeField]
    private RectTransform rectBG;

    [SerializeField]
    private Transform playerTarget;

    public Vector3 centerPositon{ get { return mainCamera.ScreenToWorldPoint(new Vector3(Screen.width / 2, Screen.height / 2)); } }


    /// <summary>
    /// Chiều ngang của screen bên phải
    /// </summary>
    /// <value>The max camera x.</value>
    public float maxCameraX
    {
        get
        { 
            Vector3 topRight = mainCamera.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height));
            return topRight.x;
        }
    }

    /// <summary>
    /// Chiều ngang của screen bên trái
    /// </summary>
    /// <value>The minimum camera x.</value>
    public float minCameraX
    {
        get
        { 
            Vector3 topLeft = mainCamera.ScreenToWorldPoint(new Vector3(0, Screen.height));
            return topLeft.x;
        }
    }

    /// <summary>
    /// Chiều ngang của BG bên phải
    /// </summary>
    /// <value>The max background position x.</value>
    public float maxBGPositionX
    {
        get
        { 
            Vector3[] corner = new Vector3[4];
            rectBG.GetWorldCorners(corner);
            return corner[2].x;
        }
    }

    /// <summary>
    /// Chiều ngang của screen bên trái
    /// </summary>
    /// <value>The minimum background position x.</value>
    public float minBGPositionX
    {
        get
        { 
            Vector3[] corner = new Vector3[4];
            rectBG.GetWorldCorners(corner);
            return corner[0].x;
        }
    }

    public float maxY{ get; private set; }

    public float minY{ get; private set; }

    private Camera mainCamera;

    private float widthScreen;

    private Vector3 targetPos;

    private Vector3 offset;

    private float interpVelocity;

    #region Behaviour

    private  void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        mainCamera = GetComponent<Camera>();

        Vector3 topRight = mainCamera.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height));
        maxY = topRight.y;
        minY = -topRight.y;
        widthScreen = Mathf.Abs(topRight.x);
    }

    private void Start()
    {
        targetPos = transform.position;
    }

    private void Update()
    {
        if (GameManager.Instance.GameState == GAME_STATE.PLAYING)
        {
            
            Vector3 posNoZ = transform.position;
            posNoZ.z = playerTarget.transform.position.z;
            Vector3 targetDirection = (playerTarget.transform.position - posNoZ);
            interpVelocity = targetDirection.magnitude * 5f;
            targetPos = transform.position + (targetDirection.normalized * interpVelocity * Time.deltaTime); 
            targetPos = new Vector3(targetPos.x, 0, targetPos.z);
            if (targetPos.x < maxBGPositionX - widthScreen && targetPos.x > minBGPositionX + widthScreen)
            {
                transform.position = Vector3.Lerp(transform.position, targetPos, 0.25f);
            }
            else
            {
                if (targetPos.x > 0)
                {
                    targetPos = new Vector3(maxBGPositionX - widthScreen, 0, targetPos.z);
                    transform.position = Vector3.Lerp(transform.position, targetPos, 0.25f);
                }
                else
                {
                    targetPos = new Vector3(minBGPositionX + widthScreen, 0, targetPos.z);
                    transform.position = Vector3.Lerp(transform.position, targetPos, 0.25f);
                }
            }
        }

    }

    #endregion

    #region Publish Methods

    public Vector3 OnGetPlayerScreenPos()
    {
        if (playerTarget != null)
        {
            return playerTarget.position;
        }
        else
        {
            return Vector3.zero;
        }
    }

    #endregion

}

