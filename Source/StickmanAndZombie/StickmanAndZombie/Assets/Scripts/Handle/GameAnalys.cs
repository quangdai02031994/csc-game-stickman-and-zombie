﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSupport;

public class GameAnalys : MonoBehaviour
{
    public static GameAnalys Instance{ get; private set; }


    public MAP_TYPES mapType;
    public int levelID;
    public int rank = 0;

    #region Behaviour

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    void Start()
    {
        OnResetData();
    }

    #endregion

    #region Publish Methods

    public void OnSetData(MAP_TYPES type, int levelID)
    {
        this.mapType = type;
        this.levelID = levelID;
    }

    public void OnResetData()
    {
        mapType = MAP_TYPES.NONE;
        levelID = -1;
        rank = 0;
    }

    public void OnGameWin(float currentHP, float maxHP)
    {
        Debug.Log("Current HP: " + currentHP + "- max HP: " + maxHP);
        float percent = currentHP / maxHP;
        int _rank = 0;
        if (percent <= 0)
        {
            _rank = 0;
        }
        else if (percent < 0.5f)
        {
            _rank = 1;
        }
        else if (percent < 0.8f)
        {
            _rank = 2;
        }
        else
        {
            _rank = 3;
        }
        this.rank = _rank;
        GameManager.Instance.OnGameWin(mapType, levelID, _rank);
//        GamePlayUIHandle.Instance.OnGameWin(rank);
    }

    #endregion

}
