﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using GameSupport;
using System;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance{ get; private set; }
    //        public GAME_STATE GameState{ get; private set; }
    public GAME_STATE GameState;

    [SerializeField]
    private ModelPlayer modelPlayer;

    [SerializeField]
    private ModelGameData modelGameData;

    [SerializeField]
    private ModelSword modelSword;

    [SerializeField]
    private ModelPlayerArm modelPlayerArm;

    [SerializeField]
    private ModelPlayerItem modelPlayerItem;

    [SerializeField]
    private ModelLevel modelLevel;

    [SerializeField]
    private ModelGameMap modelGameMap;

    #region Behaviour

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(this.gameObject);
            this.gameObject.name = "Game Manager";
            Debug.Log(Application.persistentDataPath + Path.DirectorySeparatorChar + "Data");
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    void Start()
    {
        onGetDataPlayerLocal();
        onGetDataGameLocal();
        onGetDataGunLocal();
        GameState = GAME_STATE.START;
        modelLevel = null;
    }

    #endregion


    #region Public Methods

    /// <summary>
    /// Luu thong tin khi win game
    /// </summary>
    /// <param name="type">Type.</param>
    /// <param name="levelID">Level I.</param>
    /// <param name="rank">Rank.</param>
    public void OnGameWin(MAP_TYPES type, int levelID, int rank)
    {
        int mapID = (int)type - 1;

        if (modelGameMap.listMap[mapID].levels[levelID] < rank)
        {
            modelGameMap.listMap[mapID].levels[levelID] = rank;
        }

        if (levelID >= modelGameMap.listMap[mapID].idLevel)
        {
            modelGameMap.listMap[mapID].idLevel++;
            if (modelGameMap.listMap[mapID].idLevel == modelGameMap.listMap[mapID].levels.Count)
            {
                int _mapType = (int)modelGameMap.type;
                _mapType++;
                if (_mapType > (int)MAP_TYPES.RED_5)
                {
                    modelGameMap.type = MAP_TYPES.RED_5;
                    modelGameMap.level = modelGameMap.listMap[4].levels.Count - 1;
                }
                else
                {
                    modelGameMap.type = (MAP_TYPES)_mapType;
                    modelGameMap.level = 0;
                }
            }
            else
            {
                modelGameMap.level = modelGameMap.listMap[mapID].idLevel;
            }
        }
        SaveAndLoadData.SaveDataMap(modelGameMap);
    }

    /// <summary>
    /// Tao model level
    /// </summary>
    /// <param name="_model">Model.</param>
    public void OnSetLevel(ModelLevel _model)
    {
        if (this.modelLevel == null || this.modelLevel.name != _model.name)
        {
            this.modelLevel = _model;
        }
    }

    /// <summary>
    /// Lay thong tin model level
    /// </summary>
    public ModelLevel OnGetModelLevel()
    {
        return modelLevel;
    }

    /// <summary>
    /// Reset model level
    /// </summary>
    public void OnResetLevel()
    {
        modelLevel = null;
    }


    /// <summary>
    /// Lấy thông tin dữ liệu game map
    /// </summary>
    public ModelGameMap OnGetModelGameMap()
    {
        return modelGameMap;
    }

    /// <summary>
    /// Lay thong tin map theo index, tra ve null neu index fail
    /// </summary>
    /// <param name="indexMap">Index map.</param>
    public ModelMap OnGetModelMap(MAP_TYPES type)
    {
//        Debug.Log("Map: " + indexMap);
        if (modelGameMap.listMap.Count == 0)
        {
            return null;
        }
        else
        {
            switch (type)
            {
                case MAP_TYPES.GRAY_1:
                    return modelGameMap.listMap[0];
                case MAP_TYPES.YELLOW_2:
                    return modelGameMap.listMap[1];
                case MAP_TYPES.GREEN_3:
                    return modelGameMap.listMap[2];
                case MAP_TYPES.WHILE_4:
                    return modelGameMap.listMap[3];
                case MAP_TYPES.RED_5:
                    return modelGameMap.listMap[4];
                default:
                    return null;
            }
        }
    }

    /// <summary>
    /// Su dung item
    /// </summary>
    /// <param name="type">Type.</param>
    public void OnUseItem(ITEM_TYPE type)
    {
        if (type == modelPlayerItem.itemEquip.type)
        {
            if (modelPlayerItem.itemEquip.quantity <= 0)
            {
                int index = modelPlayerItem.ListItemBoughtID.IndexOf(type);
                modelPlayerItem.itemEquip = new ModelItem();
                modelPlayerItem.ListItemBoughtID.RemoveAt(index);
                modelPlayerItem.ListItemBoughtModel.RemoveAt(index);
            }
        }
        else if (type == modelPlayerItem.itemEquip2.type)
        {
            if (modelPlayerItem.itemEquip2.quantity <= 0)
            {
                int index = modelPlayerItem.ListItemBoughtID.IndexOf(type);
                modelPlayerItem.itemEquip2 = new ModelItem();
                modelPlayerItem.ListItemBoughtID.RemoveAt(index);
                modelPlayerItem.ListItemBoughtModel.RemoveAt(index);
            }
        }
        SaveAndLoadData.SavePlayerItemData(modelPlayerItem);
    }

    /// <summary>
    /// Lấy thông tin kiếm katara
    /// </summary>
    public ModelSword OnGetModelSword()
    {
        return modelSword;
    }

    /// <summary>
    /// Lấy thông tin đạn của súng mà player đang mua, null nếu chưa mua
    /// </summary>
    /// <param name="arm">Arm.</param>
    public ModelBullet OnGetModelBullet(PLAYER_ARMS arm)
    {
        if (modelPlayerArm.ListGunType.Contains(arm))
        {
            return modelPlayerArm.ListModelGun[modelPlayerArm.ListGunType.IndexOf(arm)].modelBullet;
        }
        else
        {
            return null;
        }
    }

    /// <summary>
    /// Trang bị item
    /// </summary>
    /// <param name="type">Type.</param>
    public bool OnEquipItem(ITEM_TYPE type)
    {
        if (modelPlayerItem.ListItemBoughtID.Count == 0)
        {
            return false;
        }
        else
        {
            if (modelPlayerItem.itemEquip.type == ITEM_TYPE.NONE || modelPlayerItem.itemEquip2.type == ITEM_TYPE.NONE)
            {
                if (modelPlayerItem.ListItemBoughtID.Contains(type))
                {
                    if (modelPlayerItem.ListItemBoughtModel[modelPlayerItem.ListItemBoughtID.IndexOf(type)].state == 0)
                    {
                        modelPlayerItem.ListItemBoughtModel[modelPlayerItem.ListItemBoughtID.IndexOf(type)].state = 1;
                        if (modelPlayerItem.itemEquip.type == ITEM_TYPE.NONE)
                        {
                            modelPlayerItem.itemEquip = modelPlayerItem.ListItemBoughtModel[modelPlayerItem.ListItemBoughtID.IndexOf(type)];
                        }
                        else if (modelPlayerItem.itemEquip2.type == ITEM_TYPE.NONE)
                        {
                            modelPlayerItem.itemEquip2 = modelPlayerItem.ListItemBoughtModel[modelPlayerItem.ListItemBoughtID.IndexOf(type)];
                        }
                        SaveAndLoadData.SavePlayerItemData(modelPlayerItem);
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }

        }
    }

    /// <summary>
    /// Bỏ trang bị Itemm
    /// </summary>
    /// <param name="type">Type.</param>
    public bool OnUnEquipedItem(ITEM_TYPE type)
    {
        if (modelPlayerItem.ListItemBoughtID.Count == 0)
        {
            return false;
        }
        else
        {
            if (modelPlayerItem.ListItemBoughtID.Contains(type))
            {
                if (modelPlayerItem.ListItemBoughtModel[modelPlayerItem.ListItemBoughtID.IndexOf(type)].state == 1)
                {
                    modelPlayerItem.ListItemBoughtModel[modelPlayerItem.ListItemBoughtID.IndexOf(type)].state = 0;
                    if (modelPlayerItem.itemEquip.type == type)
                    {
                        modelPlayerItem.itemEquip = new ModelItem();
                    }
                    else if (modelPlayerItem.itemEquip2.type == type)
                    {
                        modelPlayerItem.itemEquip2 = new ModelItem();
                    }
                    SaveAndLoadData.SavePlayerItemData(modelPlayerItem);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
    }

    /// <summary>
    /// Kiểm tra item đã mua chưa, nếu chưa mua trả về item có type là None
    /// </summary>
    /// <param name="type">Type.</param>
    public ModelItem OnCheckItemExist(ITEM_TYPE type)
    {
        if (modelPlayerItem.ListItemBoughtModel.Count == 0)
        {
            return new ModelItem();
        }
        else
        {
            if (modelPlayerItem.ListItemBoughtID.Contains(type))
            {
                return modelPlayerItem.ListItemBoughtModel[modelPlayerItem.ListItemBoughtID.IndexOf(type)];
            }
            else
            {
                return new ModelItem();
            }
        }
    }

    /// <summary>
    /// Mua Item mới
    /// </summary>
    /// <param name="type">Loại item.</param>
    /// <param name="quantity">số lượng.</param>
    /// <param name="goldRequire">Lượng vàng yêu cầu.</param>
    public bool OnBuyNewItem(ITEM_TYPE type, int goldRequire)
    {
        if (modelGameData.gold < goldRequire)
        {
            return false;
        }
        else
        {
            ModelItem _item = OnCheckItemExist(type);
            if (_item.type != ITEM_TYPE.NONE)
            {
                _item.quantity += 1;
                modelGameData.gold -= goldRequire;
                SaveAndLoadData.SavePlayerItemData(modelPlayerItem);
                SaveAndLoadData.SaveGameData(modelGameData);
                return true;
            }
            else
            {
                ModelItem item = SaveAndLoadData.OnGetModelItemDefault(type);
                item.state = 0;
                item.quantity = 1;
                modelGameData.gold -= goldRequire;
                modelPlayerItem.ListItemBoughtModel.Add(item);
                modelPlayerItem.ListItemBoughtID.Add(type);
                SaveAndLoadData.SaveGameData(modelGameData);
                SaveAndLoadData.SavePlayerItemData(modelPlayerItem);
                return true;
            }
        }
    }

    /// <summary>
    /// Lay so luong item dang co trong tui
    /// </summary>
    /// <param name="type">Type.</param>
    public int OnGetPlayerItemQuantity(ITEM_TYPE type)
    {
        if (modelPlayerItem.ListItemBoughtModel.Count == 0)
        {
            return 0;
        }
        else
        {
            if (modelPlayerItem.ListItemBoughtID.Contains(type))
            {
                return modelPlayerItem.ListItemBoughtModel[modelPlayerItem.ListItemBoughtID.IndexOf(type)].quantity;
            }
            else
            {
                return 0;
            }
        }
    }


    /// <summary>
    /// Lay thong tin item duoc trang bi
    /// </summary>
    /// <param name="index">Vi tri trong tui do .</param>
    public ModelItem OnGetModelItems(int index)
    {
        if (index == 0)
        {
            return modelPlayerItem.itemEquip;
        }
        else
        {
            return modelPlayerItem.itemEquip2;    
        }

    }

    /// <summary>
    /// Lấy Id của vũ khí mà player đã trang bị
    /// </summary>
    public int[] OnGetPlayerArmEquiped()
    {
        return modelPlayerArm.IDArmEquiped;
    }

    public List<PLAYER_ARMS> OnGetListArmEquiped()
    {
        List<PLAYER_ARMS> _listArmEquiped = new List<PLAYER_ARMS>();
        _listArmEquiped.Add(PLAYER_ARMS.SWORD);
        for (int i = 0; i < modelPlayerArm.ListModelGun.Count; i++)
        {
            if (modelPlayerArm.ListModelGun[i].equipState == 1)
            {
                if (!_listArmEquiped.Contains(modelPlayerArm.ListGunType[i]))
                {
                    _listArmEquiped.Add(modelPlayerArm.ListGunType[i]);
                }
            }
        }
        return _listArmEquiped;
    }


    public void OnLoadingSceneAsync(String sceneName, Action actComplete = null)
    {
        SceneLoadingHandle.Instance.OnLoadingSceneAsync(sceneName, actComplete);
    }

    public void OnFadingSceneAsync(string sceneName, Action onComplete = null)
    {
        SceneLoadingHandle.Instance.OnFadingSceneAsync(sceneName, onComplete);
    }

    /// <summary>
    /// Trạng thái game
    /// </summary>
    /// <param name="state">State.</param>
    public void OnSetGameState(GAME_STATE state)
    {
        if (state != GameState)
        {
            GameState = state;
        }
    }


    /// <summary>
    /// Strong tối thiểu để mua súng
    /// </summary>
    /// <param name="arm">Loại súng.</param>
    public int OnStrongRequire(PLAYER_ARMS arm)
    {
        switch (arm)
        {
            case PLAYER_ARMS.BOW:
                return 1;
            case PLAYER_ARMS.PISTOL:
                return 2;
            case PLAYER_ARMS.DUAL_PISTOL:
                return 4;
            case PLAYER_ARMS.AK47:
                return 6;
            case PLAYER_ARMS.SHOTGUN:
                return 8;
            case PLAYER_ARMS.GATLING_GUN:
                return 10;
            case PLAYER_ARMS.AWM_GUN:
                return 10;
            case PLAYER_ARMS.BAZOKA:
                return 15;  
            case PLAYER_ARMS.FIRE_GUN:
                return 16;
            case PLAYER_ARMS.THUNDER_GUN:
                return 20;
            case PLAYER_ARMS.NUCLEAR_GUN:
                return 20;
                
            default:
                return -1;
        }
    }


    /// <summary>
    /// Kiem tra vu khi da mua chua, tra ve null neu chua mua
    /// </summary>
    /// <param name="arm">Loai sung.</param>
    public ModelGun OnArmBought(PLAYER_ARMS arm)
    {
        switch (arm)
        {
            case PLAYER_ARMS.SWORD:
                return null;
            default:
                if (modelPlayerArm.ListGunType.Contains(arm))
                    return modelPlayerArm.ListModelGun[modelPlayerArm.ListGunType.IndexOf(arm)];
                else
                    return null;
        }
    }


    /// <summary>
    /// Bỏ trang bị súng cho stickman
    /// </summary>
    /// <param name="arm">Loại súng.</param>
    /// <param name="indexTarget">Vị trí 1 hoặc 2.</param>
    public bool OnUnEquipArm(PLAYER_ARMS arm)
    {
        switch (arm)
        {
            case PLAYER_ARMS.SWORD:
                return false;
            default:
                if (modelPlayerArm.IDArmEquiped[1] == (int)arm)
                {
                    modelPlayerArm.ListModelGun[modelPlayerArm.ListGunType.IndexOf(arm)].equipState = 0;
                    modelPlayerArm.IDArmEquiped[1] = -1;
                    SaveAndLoadData.SaveDataPlayerGun(modelPlayerArm);
                    return true;
                }
                else if (modelPlayerArm.IDArmEquiped[2] == (int)arm)
                {
                    modelPlayerArm.ListModelGun[modelPlayerArm.ListGunType.IndexOf(arm)].equipState = 0;
                    modelPlayerArm.IDArmEquiped[2] = -1;
                    SaveAndLoadData.SaveDataPlayerGun(modelPlayerArm);
                    return true;
                }
                else
                {
                    return false;
                }
        }
    }


    /// <summary>
    /// Trang bị súng cho player, trả về true nếu thành công
    /// </summary>
    /// <param name="arm">Loại súng.</param>
    public bool OnEquipArm(PLAYER_ARMS arm)
    {
        Debug.Log("Equip: " + arm);
        switch (arm)
        {
            case PLAYER_ARMS.SWORD:
                return false;
            default:
                if (modelPlayerArm.IDArmEquiped[1] == -1)
                {
                    modelPlayerArm.ListModelGun[modelPlayerArm.ListGunType.IndexOf(arm)].equipState = 1;
                    modelPlayerArm.IDArmEquiped[1] = (int)arm;
                    SaveAndLoadData.SaveDataPlayerGun(modelPlayerArm);
                    return true;
                }
                else if (modelPlayerArm.IDArmEquiped[2] == -1)
                {
                    modelPlayerArm.ListModelGun[modelPlayerArm.ListGunType.IndexOf(arm)].equipState = 1;
                    modelPlayerArm.IDArmEquiped[2] = (int)arm;
                    SaveAndLoadData.SaveDataPlayerGun(modelPlayerArm);
                    return true;
                }
                else
                {
                    return false;
                }
        }
    }

    /// <summary>
    /// Tính toán số lượng vàng cần mua súng
    /// </summary>
    /// <param name="arm">Loại súng.</param>
    public int OnPriceNewArm(PLAYER_ARMS arm)
    {
        switch (arm)
        {
            case PLAYER_ARMS.BOW:
                return 500;
            case PLAYER_ARMS.PISTOL:
                return 1000;
            case PLAYER_ARMS.DUAL_PISTOL:
                return 2000;
            case PLAYER_ARMS.AK47:
                return 10000;
            case PLAYER_ARMS.SHOTGUN:
                return 15000;
            case PLAYER_ARMS.GATLING_GUN:
                return 30000;
            case PLAYER_ARMS.AWM_GUN:
                return 35000;
            case PLAYER_ARMS.BAZOKA:
                return 40000;
            case PLAYER_ARMS.FIRE_GUN:
                return 60000;
            case PLAYER_ARMS.THUNDER_GUN:
                return 100000;
            case PLAYER_ARMS.NUCLEAR_GUN:
                return 200000;
            default:
                return -1;
        }
    }


    /// <summary>
    /// Mua súng
    /// </summary>
    /// <param name="arm"> Loại súng.</param>
    /// <param name="goldRequire">Giá vàng yêu cầu.</param>
    public bool OnBuyNewArm(PLAYER_ARMS arm, int goldRequire)
    {
        if (modelPlayerArm.ListGunType.Contains(arm) || modelGameData.gold < goldRequire)
        {
            return false;
        }
        else
        {
            if (onStrongRequire(arm))
            {
                ModelGun _modelGun = SaveAndLoadData.OnGetModelGunDefault(arm);
                if (_modelGun != null)
                {
                    modelGameData.gold -= goldRequire;
                    modelPlayerArm.ListGunType.Add(arm);
                    _modelGun.equipState = 0;
                    modelPlayerArm.ListModelGun.Add(_modelGun);
                    SaveAndLoadData.SaveGameData(modelGameData);
                    SaveAndLoadData.SaveDataPlayerGun(modelPlayerArm);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
    }

    /// <summary>
    /// Nâng cấp damage cho súng, trả về true nếu thành công
    /// </summary>
    /// <param name="arm">Tên loại súng.</param>
    /// <param name="goldRequire">Vàng yêu cầu dùng để nâng cấp.</param>
    /// <param name="damageBullet">Giá trị damage sau khi nâng cấp.</param>
    public bool OnUpgradeArmDamage(PLAYER_ARMS arm, int goldRequire, int damageBullet)
    {
        if (modelPlayerArm.ListGunType.Count == 0 || !modelPlayerArm.ListGunType.Contains(arm)
            || modelGameData.gold < goldRequire)
        {
            return false;
        }
        else
        {
            modelGameData.gold -= goldRequire;
            modelPlayerArm.ListModelGun[modelPlayerArm.ListGunType.IndexOf(arm)].modelBullet.damage = damageBullet;
            modelPlayerArm.ListModelGun[modelPlayerArm.ListGunType.IndexOf(arm)].levelDamage++;
            SaveAndLoadData.SaveGameData(modelGameData);
            SaveAndLoadData.SaveDataPlayerGun(modelPlayerArm);
            return true;
        }
    }

    /// <summary>
    /// Lượng vàng cần để nâng cấp, giá trị -1 là fail
    /// </summary>
    /// <param name="arm">Loại súng.</param>
    public int OnPriceUpgradeArmDamage(PLAYER_ARMS arm)
    {
        if (modelPlayerArm.ListGunType.Count == 0 || !modelPlayerArm.ListGunType.Contains(arm))
        {
            return -1;
        }
        else
        {
            return onPriceUpgradeArmDamage(arm);
        }
    }

    /// <summary>
    /// Tính toán số lượng damage của vũ khí sau khi nâng cấp
    /// </summary>
    /// <param name="arm">Loại súng.</param>
    public int OnCalculateUpgradeArmDamage(PLAYER_ARMS arm)
    {
        if (modelPlayerArm.ListGunType.Count == 0 || !modelPlayerArm.ListGunType.Contains(arm))
        {
            return -1;
        }
        else
        {
            return onCalculateUpgradeArmDamage(arm);
        }
    }

    /// <summary>
    /// Nâng cấp số lượng đạn, trả về true nếu thành công
    /// </summary>
    /// <param name="arm">Loại súng.</param>
    /// <param name="goldRequire">Vàng cần nâng cấp.</param>
    /// <param name="numberBullet">Số lượng đạn sau khi nâng cấp.</param>
    public bool OnUpgradeNumberBullet(PLAYER_ARMS arm, int goldRequire, int numberBullet)
    {
        //        Debug.LogError("Number Bullet: " + numberBullet);
        if (modelPlayerArm.ListGunType.Count == 0 || !modelPlayerArm.ListGunType.Contains(arm) || modelGameData.gold < goldRequire)
        {
            return false;
        }
        else
        {
            modelGameData.gold -= goldRequire;
            modelPlayerArm.ListModelGun[modelPlayerArm.ListGunType.IndexOf(arm)].levelNumberBullet++;
            modelPlayerArm.ListModelGun[modelPlayerArm.ListGunType.IndexOf(arm)].numberBullet = numberBullet;
            SaveAndLoadData.SaveGameData(modelGameData);
            SaveAndLoadData.SaveDataPlayerGun(modelPlayerArm);
            return true;
        }
        
    }

    /// <summary>
    /// Tính toán số lượng đạn sau khi nâng cấp
    /// </summary>
    /// <param name="arm">Loại súng.</param>
    public int OnCalculateUpgradeNumberBullet(PLAYER_ARMS arm)
    {
        if (modelPlayerArm.ListGunType.Count == 0 || !modelPlayerArm.ListGunType.Contains(arm))
        {
            return-1;
        }
        else
        {
            return onCalculateUpgradeNumberBullet(arm);
        }
    }

    /// <summary>
    /// Tính toán số lượng vàng cần để nâng cấp đạn
    /// </summary>
    /// <param name="arm">Loại súng.</param>
    public int OnPriceUpgradeNumberBullet(PLAYER_ARMS arm)
    {
        if (modelPlayerArm.ListGunType.Count == 0 || !modelPlayerArm.ListGunType.Contains(arm))
        {
            return -1;   
        }
        else
        {
            return onPriceUpgradeNumberBullet(arm);
        }
    }

    /// <summary>
    /// Lấy thông tin của súng player đang có, trả về null nếu không có
    /// </summary>
    /// <param name="type">Loại súng.</param>
    public ModelGun OnGetPlayerGun(PLAYER_ARMS type)
    {
        if (modelPlayerArm.ListGunType.Count == 0)
        {
            return null;
        }
        else
        {
            if (modelPlayerArm.ListGunType.Contains(type))
            {
                return modelPlayerArm.ListModelGun[modelPlayerArm.ListGunType.IndexOf(type)];
            }
            else
            {
                return null;
            }
        }
        
    }

    public bool OnUpgradeSwordDamage(int goldRequire, float damage)
    {
        if (modelGameData.gold < goldRequire)
        {
            Debug.LogError("Not enought gold");
            return false;
        }
        else
        {
            modelGameData.gold -= goldRequire;
            modelSword.damage = damage;
            modelSword.levelDamage++;
            SaveAndLoadData.SaveGameData(modelGameData);
            SaveAndLoadData.SaveSwordData(modelSword);
            return true;
        }
    }

    public int OnPriceUpgradeSwordDamage()
    {
        return 100 + 5 * (modelSword.levelDamage + 1);
    }

    public float OnCalculateUpgradeSwordDamage()
    {
        if (modelSword.levelDamage < 9)
        {
            return 100 + (modelSword.levelDamage + 1) * 10;
        }
        else
        {
            return 0;
        }
    }


    /// <summary>
    /// Lấy thông tin của kiếm
    /// </summary>
    public ModelSword OnGetSwordInfo()
    {
        return modelSword;
    }

    /// <summary>
    /// Lấy thông tin vàng player đang có
    /// </summary>
    public int OnGetGold()
    {
        return modelGameData.gold;
    }

    /// <summary>
    /// Lấy thông tin HP của player
    /// </summary>
    public float OnGetPlayerHP()
    {
        return modelPlayer.hp;
    }

    /// <summary>
    /// Lấy thông tin speed player
    /// </summary>
    public float OnGetPlayerSpeed()
    {
        return modelPlayer.speed;
    }


    /// <summary>
    /// Lấy thông tin strong player
    /// </summary>
    public float OnGetPlayerStrong()
    {
        return modelPlayer.strong;
    }

    /// <summary>
    /// Lay level strong cua player
    /// </summary>
    public int OnGetPlayerLevelStrong()
    {
        return modelPlayer.levelStrong;
    }

    /// <summary>
    /// Lay level HP cua player
    /// </summary>
    public int OnGetPlayerLevelHP()
    {
        return modelPlayer.levelHP;
    }

    /// <summary>
    /// Lay level speed cua player
    /// </summary>
    public int OnGetPlayerLevelSpeed()
    {
        return modelPlayer.levelSpeed;
    }

    /// <summary>
    /// Tính toán cấp player
    /// </summary>
    public int OnCalculateUpgradePlayerHP()
    {
        return 100 + (modelPlayer.levelHP + 1) * 100;
    }

    /// <summary>
    /// Tính toán lượng vàng nâng cấp HP
    /// </summary>
    public int OnPriceUpgradePlayerHP()
    {
        return 100 + 500 * (modelPlayer.levelHP + 1);
    }

    /// <summary>
    /// Tính toán nâng cấp speed
    /// </summary>
    public float OnCalculateUpgradePlayerSpeed()
    {
        return 2 + (modelPlayer.levelSpeed + 1) * 0.1f;
    }

    /// <summary>
    /// Tính toán lượng vàng nâng cấp speed
    /// </summary>
    public int OnPriceUpgradeSpeed()
    {
        return 100 + 1000 * (modelPlayer.levelSpeed + 1);
    }

    /// <summary>
    /// Tính toán nâng cấp trong player
    /// </summary>
    public float OnCalculateUpgradePlayerStrong()
    {
        return 10 + (modelPlayer.levelStrong + 1) * 10;
    }

    /// <summary>
    /// Tính toán lượng vàng nâng cấp strong
    /// </summary>
    public int OnPriceUpgradeStrong()
    {
        return 100 + 1500 * (modelPlayer.levelStrong + 1);
    }

    /// <summary>
    /// Nâng cấp HP player, trả về true nếu thành công
    /// </summary>
    /// <param name="goldRequire">Gold require.</param>
    /// <param name="playerHP">Player H.</param>
    public bool OnUpgradePlayerHP(int goldRequire, float playerHP)
    {
        if (modelGameData.gold < goldRequire)
        {
            return false;
        }
        else
        {
            modelGameData.gold -= goldRequire;
            modelPlayer.hp = playerHP;
            modelPlayer.levelHP++;
            SaveAndLoadData.SavePlayerData(modelPlayer);
            SaveAndLoadData.SaveGameData(modelGameData);
            return true;
        }
    }

    /// <summary>
    /// Nâng cấp tốc độ của player, trả về true nếu thành công
    /// </summary>
    /// <param name="goldRequire">Lượng vàng yêu cầu.</param>
    /// <param name="playerSpeed">Tốc độ sau khi nâng cấp.</param>
    public bool OnUpgradePlayerSpeed(int goldRequire, float playerSpeed)
    {
        if (modelGameData.gold < goldRequire)
        {
            return false;
        }
        else
        {
            modelGameData.gold -= goldRequire;
            modelPlayer.speed = playerSpeed;
            modelPlayer.levelSpeed++;
            SaveAndLoadData.SavePlayerData(modelPlayer);
            SaveAndLoadData.SaveGameData(modelGameData);
            return true;
        }
    }

    /// <summary>
    /// Nâng cấp strong của player, trả về true nếu thành công
    /// </summary>
    /// <param name="goldRequire">Vàng cần nâng cấp.</param>
    /// <param name="playerStrong">Strong player sau nâng cấp.</param>
    public bool OnUpgradePlayerStrong(int goldRequire, float playerStrong)
    {
//        Debug.LogError("Gold require: " + goldRequire);
        if (modelGameData.gold < goldRequire)
        {
            return false;
        }
        else
        {
            modelGameData.gold -= goldRequire;
            modelPlayer.strong = playerStrong;
            modelPlayer.levelStrong++;
            SaveAndLoadData.SavePlayerData(modelPlayer);
            SaveAndLoadData.SaveGameData(modelGameData);
            return true;
        }
    }


    #endregion

    #region Private Methods

    private bool onStrongRequire(PLAYER_ARMS type)
    {
        switch (type)
        {
            case PLAYER_ARMS.BOW:
                return true;
            case PLAYER_ARMS.PISTOL:
                if (modelPlayer.strong >= 1)
                    return true;
                else
                    return false;
            case PLAYER_ARMS.DUAL_PISTOL:
                if (modelPlayer.strong >= 3)
                    return true;
                else
                    return false;
            case PLAYER_ARMS.AK47:
                if (modelPlayer.strong >= 5)
                    return true;
                else
                    return false;
            case PLAYER_ARMS.SHOTGUN:
                if (modelPlayer.strong >= 7)
                    return true;
                else
                    return false;
            case PLAYER_ARMS.GATLING_GUN:
                if (modelPlayer.strong >= 9)
                    return true;
                else
                    return false;
            case PLAYER_ARMS.AWM_GUN:
                if (modelPlayer.strong >= 9)
                    return true;
                else
                    return false;
            case PLAYER_ARMS.BAZOKA:
                if (modelPlayer.strong >= 14)
                    return true;
                else
                    return false;
            case PLAYER_ARMS.FIRE_GUN:
                if (modelPlayer.strong >= 15)
                    return true;
                else
                    return false;
            case PLAYER_ARMS.THUNDER_GUN:
                if (modelPlayer.strong >= 19)
                    return true;
                else
                    return false;
            case PLAYER_ARMS.NUCLEAR_GUN:
                if (modelPlayer.strong >= 19)
                    return true;
                else
                    return false;
            default:
                return false;
        }
    }

    private void onGetDataPlayerLocal()
    {
        modelPlayer = SaveAndLoadData.LoadPlayerData();
    }

    private void onGetDataGameLocal()
    {
        modelGameData = SaveAndLoadData.LoadGameData();
        modelSword = SaveAndLoadData.LoadSwordData();
        modelPlayerItem = SaveAndLoadData.LoadPlayerItemData();
        modelGameMap = SaveAndLoadData.LoadDataGameMap();
    }

    private void onGetDataGunLocal()
    {
        modelPlayerArm = SaveAndLoadData.LoadDataPlayerGun();
    }

    private int onCalculateUpgradeNumberBullet(PLAYER_ARMS arm)
    {
        ModelGun _modelArms = modelPlayerArm.ListModelGun[modelPlayerArm.ListGunType.IndexOf(arm)];
        if (_modelArms.levelNumberBullet >= 9)
            return 0;
        switch (arm)
        {
            case PLAYER_ARMS.BOW:
                return 50 + (_modelArms.levelNumberBullet + 1) * 10;
            case PLAYER_ARMS.PISTOL:
                return 100 + (_modelArms.levelNumberBullet + 1) * 10;
            case PLAYER_ARMS.DUAL_PISTOL:
                return 150 + (_modelArms.levelNumberBullet + 1) * 15;
            case PLAYER_ARMS.AK47:
                return 200 + (_modelArms.levelNumberBullet + 1) * 20;
            case PLAYER_ARMS.SHOTGUN:
                return 50 + (_modelArms.levelNumberBullet + 1) * 5;
            case PLAYER_ARMS.GATLING_GUN:
                return 300 + (_modelArms.levelNumberBullet + 1) * 30;
            case PLAYER_ARMS.AWM_GUN:
                return 50 + (_modelArms.levelNumberBullet + 1) * 5;
            case PLAYER_ARMS.BAZOKA:
                return 30 + (_modelArms.levelNumberBullet + 1) * 3;
            case PLAYER_ARMS.FIRE_GUN:
                return 30 + (_modelArms.levelNumberBullet + 1) * 3;
            case PLAYER_ARMS.THUNDER_GUN:
                return 50 + (_modelArms.levelNumberBullet + 1) * 5;
            case PLAYER_ARMS.NUCLEAR_GUN:
                return 50 + (_modelArms.levelNumberBullet + 1) * 5;
            default:
                return-1;
        }
    }

    private int onPriceUpgradeNumberBullet(PLAYER_ARMS arm)
    {
        ModelGun _modelArms = modelPlayerArm.ListModelGun[modelPlayerArm.ListGunType.IndexOf(arm)];
        switch (arm)
        {
            case PLAYER_ARMS.BOW:
                return 100 + 5 * (_modelArms.levelNumberBullet + 1);
            case PLAYER_ARMS.PISTOL:
                return 150 + 5 * (_modelArms.levelNumberBullet + 1);
            case PLAYER_ARMS.DUAL_PISTOL:
                return 150 + 5 * (_modelArms.levelNumberBullet + 1);
            case PLAYER_ARMS.AK47:
                return 200 + 5 * (_modelArms.levelNumberBullet + 1);
            case PLAYER_ARMS.SHOTGUN:
                return 200 + 5 * (_modelArms.levelNumberBullet + 1);
            case PLAYER_ARMS.GATLING_GUN:
                return 250 + 10 * (_modelArms.levelNumberBullet + 1);
            case PLAYER_ARMS.AWM_GUN:
                return 250 + 10 * (_modelArms.levelNumberBullet + 1);
            case PLAYER_ARMS.BAZOKA:
                return 300 + 20 * (_modelArms.levelNumberBullet + 1);
            case PLAYER_ARMS.FIRE_GUN:
                return 300 + 20 * (_modelArms.levelNumberBullet + 1);
            case PLAYER_ARMS.THUNDER_GUN:
                return 400 + 30 * (_modelArms.levelNumberBullet + 1);
            case PLAYER_ARMS.NUCLEAR_GUN:
                return 400 + 30 * (_modelArms.levelNumberBullet + 1);
            default:
                return -1;
        }
    }

    private int onCalculateUpgradeArmDamage(PLAYER_ARMS arm)
    {
        ModelGun _modelArms = modelPlayerArm.ListModelGun[modelPlayerArm.ListGunType.IndexOf(arm)];
        if (_modelArms.levelDamage >= 9)
            return 0;
        switch (arm)
        {
            case PLAYER_ARMS.BOW:
                return 50 + 10 * (_modelArms.levelDamage + 1);
            case PLAYER_ARMS.PISTOL:
                return 150 + 10 * (_modelArms.levelDamage + 1);
            case PLAYER_ARMS.DUAL_PISTOL:
                return 150 + 15 * (_modelArms.levelDamage + 1);
            case PLAYER_ARMS.AK47:
                return 150 + 15 * (_modelArms.levelDamage + 1);
            case PLAYER_ARMS.SHOTGUN:
                return 50 + 15 * (_modelArms.levelDamage + 1);
            case PLAYER_ARMS.GATLING_GUN:
                return 150 + 15 * (_modelArms.levelDamage + 1);
            case PLAYER_ARMS.AWM_GUN:
                return 150 + 15 * (_modelArms.levelDamage + 1);
            case PLAYER_ARMS.BAZOKA:
                return 100 + 20 * (_modelArms.levelDamage + 1);
            case PLAYER_ARMS.FIRE_GUN:
                return 20 + 5 * (_modelArms.levelDamage + 1);
            case PLAYER_ARMS.THUNDER_GUN:
                return 20 + 5 * (_modelArms.levelDamage + 1);
            case PLAYER_ARMS.NUCLEAR_GUN:
                return 200 + 20 * (_modelArms.levelDamage + 1);
            default:
                return -1;
        }
    }

    private int onPriceUpgradeArmDamage(PLAYER_ARMS arm)
    {
        ModelGun _modelArms = modelPlayerArm.ListModelGun[modelPlayerArm.ListGunType.IndexOf(arm)];
        switch (arm)
        {
            case PLAYER_ARMS.BOW:
                return 100 + 5 * (_modelArms.levelDamage + 1);
            case PLAYER_ARMS.PISTOL:
                return 150 + 5 * (_modelArms.levelDamage + 1);
            case PLAYER_ARMS.DUAL_PISTOL:
                return 150 + 5 * (_modelArms.levelDamage + 1);
            case PLAYER_ARMS.AK47:
                return 200 + 5 * (_modelArms.levelDamage + 1);
            case PLAYER_ARMS.SHOTGUN:
                return 200 + 5 * (_modelArms.levelDamage + 1);
            case PLAYER_ARMS.GATLING_GUN:
                return 250 + 10 * (_modelArms.levelDamage + 1);
            case PLAYER_ARMS.AWM_GUN:
                return 250 + 10 * (_modelArms.levelDamage + 1);
            case PLAYER_ARMS.BAZOKA:
                return 300 + 20 * (_modelArms.levelDamage + 1);
            case PLAYER_ARMS.FIRE_GUN:
                return 300 + 20 * (_modelArms.levelDamage + 1);
            case PLAYER_ARMS.THUNDER_GUN:
                return 400 + 30 * (_modelArms.levelDamage + 1);
            case PLAYER_ARMS.NUCLEAR_GUN:
                return 400 + 30 * (_modelArms.levelDamage + 1);
            default:
                return -1;
        }
    }


    #endregion
}
