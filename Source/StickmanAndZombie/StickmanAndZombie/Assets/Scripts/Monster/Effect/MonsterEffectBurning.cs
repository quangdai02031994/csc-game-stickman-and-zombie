﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterEffectBurning : MonoBehaviour
{

    //Particle System
    public ParticleSystem particleFire;
    public ParticleSystem particleSmoke;
    public ParticleSystem particleSpackle;

    [Space(10)]
    //Renderer
    public Renderer renderFire;
    public Renderer renderSmoke;
    public Renderer renderSpackle;



    #region Behaviour

    void Awake()
    {
        onSettingParticle();
    }

    #endregion

    #region Publish Methods

    public void OnSetData(int orderInLayer, float localScale)
    {
        this.gameObject.SetActive(true);
//        onSettingSize(localScale);
        onSettingLayer(orderInLayer);
    }

    public void OnPlay()
    {
        particleFire.Play();
        particleSmoke.Play();
        particleSpackle.Play();
    }

    public void OnStop()
    {
        particleFire.Stop();
        particleSmoke.Stop();
        particleSpackle.Stop();
        this.gameObject.SetActive(false);
    }

    #endregion

    #region Private Methods

    private void onSettingLayer(int orderInLayer)
    {
        renderFire.sortingOrder = orderInLayer + 2;
        renderSmoke.sortingOrder = orderInLayer - 1;
        renderSpackle.sortingOrder = orderInLayer + 2;
    }


    private void onSettingParticle()
    {
        var _mainFire = particleFire.main;
        _mainFire.playOnAwake = false;
        _mainFire.loop = true;

        var _mainSmoke = particleSmoke.main;
        _mainSmoke.playOnAwake = false;
        _mainSmoke.loop = true;

        var _mainSpackle = particleSpackle.main;
        _mainSpackle.playOnAwake = false;
        _mainSpackle.loop = true;

    }

    #endregion

}
