﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;
using Spine;

public class MonsterSalivaEffect : MonoBehaviour
{
    public LayerMask layerCast;

    [SpineAnimation]
    public string animationName;

    [SpineEvent]
    public string spineEventName;


    public Transform localLeft;
    public Transform localRight;

    private bool isAttackLeft;
    private bool isCatching = false;

    private SkeletonAnimation skeletonAnimation;
    private MeshRenderer meshRenderer;

    private Vector2 sizeBoxCast = new Vector2(1.3f, 0.3f);
    private RaycastHit2D hitObject;

    #region Behaviour

    void Awake()
    {
        skeletonAnimation = GetComponent<SkeletonAnimation>();
        meshRenderer = GetComponent<MeshRenderer>();
    }

    void Start()
    {
        skeletonAnimation.AnimationState.Complete += onCompleteEvent;   
        skeletonAnimation.AnimationState.Event += onEventHandle;
    }

    void OnDestroy()
    {
        skeletonAnimation.AnimationState.Complete -= onCompleteEvent;
        skeletonAnimation.AnimationState.Event -= onEventHandle;
    }

    void OnDisable()
    {
        hitObject = new RaycastHit2D();
        isCatching = false;
    }

    void Update()
    {
        if (!isCatching)
            return;
        
        if (isAttackLeft)
        {
            RaycastHit2D _hit = Physics2D.BoxCast(localLeft.position, sizeBoxCast, 0, Vector2.zero, 0, layerCast);
            if (_hit.collider != null && _hit != hitObject
                && !PlayerController.Instance.isSaliva)
//                && Mathf.Abs(_hit.transform.position.y - transform.position.y) < 0.75f)
            {
//                Debug.Log(Mathf.Abs(_hit.transform.position.y - transform.position.y));
                PlayerController.Instance.OnSaliva();
                hitObject = _hit;
            }
        }
        else
        {
            RaycastHit2D _hit = Physics2D.BoxCast(localRight.position, sizeBoxCast, 0, Vector2.zero, 0, layerCast);
            if (_hit.collider != null && _hit != hitObject
                && !PlayerController.Instance.isSaliva)
//                && Mathf.Abs(_hit.transform.position.y - transform.position.y) < 0.75f)
            {
//                Debug.Log(Mathf.Abs(_hit.transform.position.y - transform.position.y));
                PlayerController.Instance.OnSaliva();
                hitObject = _hit;
            }
        }
    }

    #if UNITY_EDITOR
    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        if (!isAttackLeft)
        {
            Gizmos.DrawWireCube(localRight.position, sizeBoxCast);
        }
        else
        {
            Gizmos.DrawWireCube(localLeft.position, sizeBoxCast);
        }
    }
    #endif

    #endregion

    #region Private Methods

    private void onEventHandle(TrackEntry trackEntry, Spine.Event evt)
    {
        if (evt.Data.Name == spineEventName && !isCatching)
        {
            isCatching = true;
        }
    }

    private void onCompleteEvent(TrackEntry trackEntry)
    {
        if (trackEntry.Animation.Name == animationName)
        {
            this.gameObject.SetActive(false);
        }
    }

    #endregion

    #region Publish Methods

    public void OnSetData(Vector3 startPos, int orderInLayer, Vector3 localScale)
    {
        this.gameObject.SetActive(true);
        this.transform.position = startPos;
        this.transform.localScale = localScale;
        meshRenderer.sortingOrder = orderInLayer;
    }

    public void OnPlay(bool isLeft)
    {
        isAttackLeft = isLeft;
        if (isLeft)
        {
            skeletonAnimation.state.SetAnimation(0, animationName, false);
            skeletonAnimation.skeleton.FlipX = true;
        }
        else
        {
            skeletonAnimation.state.SetAnimation(0, animationName, false);
            skeletonAnimation.skeleton.FlipX = false;
        }
    }

    #endregion



}
