﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;
using Spine;

public class MonsterDeadEffect : MonoBehaviour
{
    [SpineAnimation]
    public string spineAnimationName;

    private SkeletonAnimation skeletonAnimation;
    private MeshRenderer meshRenderer;
    private string startAnimationName;

    #region Behaviour

    void Awake()
    {
        skeletonAnimation = GetComponent<SkeletonAnimation>();
        meshRenderer = GetComponent<MeshRenderer>();
    }

    void Start()
    {
        skeletonAnimation.state.Complete += onCompleteEvent;
        startAnimationName = skeletonAnimation.AnimationName;
        skeletonAnimation.clearStateOnDisable = true;
    }

    void OnDisable()
    {
        skeletonAnimation.AnimationName = startAnimationName;
    }

    void OnDestroy()
    {
        skeletonAnimation.state.Complete -= onCompleteEvent;
    }

    #endregion

    #region Publish Methods

    public void OnSetData(Vector3 startPos, int orderInLayer)
    {
        transform.position = startPos;
        meshRenderer.sortingOrder = 10;
        this.gameObject.SetActive(true);
    }

    public void OnPlay(bool isRight)
    {
        skeletonAnimation.state.SetAnimation(0, spineAnimationName, false);
        skeletonAnimation.skeleton.FlipX = isRight;
    }

    #endregion


    #region Private methods

    private void onCompleteEvent(TrackEntry trackEntry)
    {
        if (trackEntry.Animation.Name == spineAnimationName)
        {
            StartCoroutine(onAutoDisable());
        }
    }

    private IEnumerator onAutoDisable()
    {
        yield return new WaitForEndOfFrame();
        gameObject.SetActive(false);
    }

    #endregion
}
