﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSupport;
using Spine;

public class MonsterSalivaController : MonsterHandle
{
    private const string BoneSalivaMonsterAttack = "bone2";

    private MonsterSalivaEffect effectBullet;

    #region Behaviour

    protected override void Awake()
    {
        base.Awake();
    }

    protected override void Start()
    {
        base.Start();
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        effectBullet = null;
        skeletonAnimation.AnimationName = MonsterAnimationName.saliva_appear;
    }

    //    void Update()
    //    {
    //        if (GameManager.Instance.GameState == GAME_STATE.PLAYING)
    //        {
    //            onBurningMonster();
    //            onCheckMonsterState();
    //        }
    //
    //    }
    void Update()
    {
        switch (GameManager.Instance.GameState)
        {
            case GAME_STATE.PLAYING:
                onBurningMonster();
                onCheckMonsterState();
                break;
            case GAME_STATE.GAMEOVER:
                if (skeleton.FlipX && monsterState != MONSTER_STATE.IDLE_LEFT)
                {
                    onSetMonsterAnimation(MONSTER_STATE.IDLE_LEFT, true);
                }
                else if (!skeleton.FlipX && monsterState != MONSTER_STATE.IDLE_RIGHT)
                {
                    onSetMonsterAnimation(MONSTER_STATE.IDLE_RIGHT, true);
                }
                break;
        }
    }



    #endregion

    #region Spine Events

    protected override void onCompleteEvent(Spine.TrackEntry trackEntry)
    {
//        Debug.Log("Complete: " + trackEntry.Animation.Name + " state: " + monsterState);
        switch (trackEntry.Animation.Name)
        {
            case MonsterAnimationName.saliva_appear:
                onEnableCollider();
                if (monsterState == MONSTER_STATE.APPEAR_LEFT)
                {
                    onSetMonsterAnimation(MONSTER_STATE.WALK_LEFT, true);
                }
                else if (monsterState == MONSTER_STATE.APPEAR_RIGHT)
                {
                    onSetMonsterAnimation(MONSTER_STATE.WALK_RIGHT, true);
                }
                break;
            case MonsterAnimationName.saliva_attack:
                if (monsterState == MONSTER_STATE.ATTACK_LEFT)
                {
                    onSetMonsterAnimation(MONSTER_STATE.IDLE_LEFT, true);
                }
                else if (monsterState == MONSTER_STATE.ATTACK_RIGHT)
                {
                    onSetMonsterAnimation(MONSTER_STATE.IDLE_RIGHT, true);
                }
                timeCountFind = 0;
                break;
            case MonsterAnimationName.saliva_hit:
                timeCountFind = modelMonster.timeBeginFind;
                if (transform.position.x > playerTransform.position.x)
                {
                    onSetMonsterAnimation(MONSTER_STATE.WALK_RIGHT, true);
                }
                else
                {
                    onSetMonsterAnimation(MONSTER_STATE.WALK_LEFT, true);
                }
                break;
            case MonsterAnimationName.saliva_hit_thunder:
                timeCountFind = modelMonster.timeBeginFind;
                if (transform.position.x > playerTransform.position.x)
                {
                    onSetMonsterAnimation(MONSTER_STATE.WALK_RIGHT, true);
                }
                else
                {
                    onSetMonsterAnimation(MONSTER_STATE.WALK_LEFT, true);
                }
                break;
            case MonsterAnimationName.saliva_dead:
                onDead();
                break;
            case MonsterAnimationName.saliva_dead_thunder:
                onDead();
                break;
            case MonsterAnimationName.saliva_dead_fire:
                onDead();
                break;
        }
    }

    protected override void onStartEvent(Spine.TrackEntry trackEntry)
    {
        switch (trackEntry.Animation.Name)
        {
            case MonsterAnimationName.saliva_dead:
                ListMonsterHandle.Instance.OnCheckCombo();
                break;
            case MonsterAnimationName.saliva_dead_fire:
                ListMonsterHandle.Instance.OnCheckCombo();
                break;
            case MonsterAnimationName.saliva_dead_thunder:
                ListMonsterHandle.Instance.OnCheckCombo();
                break;
        }
    }

    protected override void onHandleEvent(Spine.TrackEntry trackEntry, Spine.Event evt)
    {
//        Debug.Log(trackEntry.Animation.Name + " Event name: " + evt.Data.Name);
        switch (evt.Data.Name)
        {
            case SpineEventAttack:
                Bone bone = skeleton.FindBone(BoneSalivaMonsterAttack);
                Vector3 startPos = transform.TransformPoint(new Vector3(bone.WorldX, bone.WorldY, 0));
                if (monsterState == MONSTER_STATE.ATTACK_LEFT)
                {
                    onPlayEffectAttack(startPos);
                    effectBullet.OnPlay(true);
                }
                else if (monsterState == MONSTER_STATE.ATTACK_RIGHT)
                {
                    onPlayEffectAttack(startPos);
                    effectBullet.OnPlay(false);
                }
                break;
            case SpineEventOnDead:
                onSpawGold(ListMonsterHandle.Instance.isCombo);
                break;
        }
    }


    #endregion

    #region Publish methods

    public override void OnHit(int hp, bool fromRight)
    {
        base.OnHit(hp, fromRight);
    }

    public override void OnSetData(ModelMonster model, Vector3 startPos)
    {
        base.OnSetData(model, startPos);
        timeCountFind = model.timeBeginFind;
    }

    #endregion

    #region Private and Protected Methods

    protected override void onCheckMonsterState()
    {
        switch (monsterState)
        {
            case MONSTER_STATE.WALK_LEFT:
                onMoveLeft();
                onReScale();
                onFindTarget();
                break;
            case MONSTER_STATE.WALK_RIGHT:
                onMoveRight();
                onReScale();
                onFindTarget();
                break;
            case MONSTER_STATE.IDLE_LEFT:
                onCheckPlayerPosition();
                break;
            case MONSTER_STATE.IDLE_RIGHT:
                onCheckPlayerPosition();
                break;
        }
    }

    protected override IEnumerator onAppearMonster()
    {
        yield return new WaitForEndOfFrame();
        if (monsterState == MONSTER_STATE.APPEAR_LEFT)
        {
            skeleton.FlipX = true;
            onPlayAnimation(MonsterAnimationName.saliva_appear, true);
        }
        else if (monsterState == MONSTER_STATE.APPEAR_RIGHT)
        {
            skeleton.FlipX = false;
            onPlayAnimation(MonsterAnimationName.saliva_appear, false);
        }
    }

    protected override void onFoundStickMan()
    {
//        Debug.Log("Found stickman: " + monsterState);
        if (transform.position.x < playerTransform.position.x)
        {
            if (monsterState != MONSTER_STATE.ATTACK_RIGHT)
            {
                onSetMonsterAnimation(MONSTER_STATE.ATTACK_RIGHT, false);
            }
        }
        else if (transform.position.x > playerTransform.position.x)
        {
            if (monsterState != MONSTER_STATE.ATTACK_LEFT)
            {
                onSetMonsterAnimation(MONSTER_STATE.ATTACK_LEFT, false);
            }
        }
    }

    protected override void onCheckPlayerPosition()
    {
        if (Vector3.Distance(transform.position, playerTransform.position) > modelMonster.distance)
        {
            if (transform.position.x < playerTransform.position.x)
            {
                if (monsterState == MONSTER_STATE.WALK_LEFT && timeCountFind < modelMonster.timeBeginFind)
                    return;
                onSetMonsterAnimation(MONSTER_STATE.WALK_RIGHT, true);
            }
            else
            {
                if (monsterState == MONSTER_STATE.WALK_RIGHT && timeCountFind < modelMonster.timeBeginFind)
                    return;
                onSetMonsterAnimation(MONSTER_STATE.WALK_LEFT, true);
            }
        }
        else
        {
            if (Mathf.Abs(transform.position.y - playerTransform.position.y) < modelMonster.deltaY
                && Mathf.Abs(transform.position.x - playerTransform.position.x) > (modelMonster.deltaX - 0.5f)
                && transform.position.x < CameraHandle.Instance.maxCameraX
                && transform.position.x > CameraHandle.Instance.minCameraX)
            {
                onFoundStickMan();
            }
            else
            {
                onNotFoundStickMan();
            }
        }
    }

    protected override void onMoveLeft()
    {
        transform.Translate(Vector3.left * speedMove * Time.deltaTime);
        onCheckMonsterPositon();
    }

    protected override void onMoveRight()
    {
        transform.Translate(Vector3.right * speedMove * Time.deltaTime);
        onCheckMonsterPositon();
    }

    protected override MonsterBulletHandler onCreateBullet(Vector3 startPos, ModelBullet model)
    {
        return base.onCreateBullet(startPos, model);
    }

    protected override void onSetMonsterAnimation(MONSTER_STATE state, bool loop)
    {
        monsterState = state;
        switch (state)
        {
            case MONSTER_STATE.WALK_LEFT:
                if (modelMonster.speed < 1f)
                {
                    onPlayAnimation(MonsterAnimationName.saliva_walk, true, loop);
                }
                else
                {
                    onPlayAnimation(MonsterAnimationName.saliva_run, true, loop);
                }
                break;
            case MONSTER_STATE.WALK_RIGHT:
                if (modelMonster.speed < 1f)
                {
                    onPlayAnimation(MonsterAnimationName.saliva_walk, false, loop);
                }
                else
                {
                    onPlayAnimation(MonsterAnimationName.saliva_run, false, loop);
                }
                break;
            case MONSTER_STATE.ATTACK_RIGHT:
                onPlayAnimation(MonsterAnimationName.saliva_attack, false, loop);
                break;
            case MONSTER_STATE.ATTACK_LEFT:
                onPlayAnimation(MonsterAnimationName.saliva_attack, true, loop);
                break;
            case MONSTER_STATE.IDLE_LEFT:
                onPlayAnimation(MonsterAnimationName.hand_idle, true, loop);
                break;
            case MONSTER_STATE.IDLE_RIGHT:
                onPlayAnimation(MonsterAnimationName.saliva_idle, false, loop);
                break;
            case MONSTER_STATE.HIT_LEFT:
                onPlayAnimation(MonsterAnimationName.saliva_hit, true, loop);
                timeCountAttack = 0;
                break;
            case MONSTER_STATE.HIT_RIGHT:
                onPlayAnimation(MonsterAnimationName.saliva_hit, false, loop);
                timeCountAttack = 0;
                break;
            case MONSTER_STATE.HIT_LEFT_THUNDER:
                onPlayAnimation(MonsterAnimationName.saliva_hit_thunder, true, loop);
                timeCountAttack = 0;
                break;
            case MONSTER_STATE.HIT_RIGHT_THUNDER:
                onPlayAnimation(MonsterAnimationName.saliva_hit_thunder, false, loop);
                timeCountAttack = 0;
                break;
            case MONSTER_STATE.DEAD:
                onPlayAnimation(MonsterAnimationName.saliva_dead, skeleton.FlipX, loop);
//                ListMonsterHandle.Instance.OnCheckCombo();   
                break;
            case MONSTER_STATE.DEAD_THUNDER:
                onPlayAnimation(MonsterAnimationName.saliva_dead_thunder, skeleton.FlipX, loop);
                break;
            case MONSTER_STATE.DEAD_FIRE:
                onPlayAnimation(MonsterAnimationName.saliva_dead_fire, skeleton.FlipX, loop);
                break;
            default:
                Debug.LogError("No state select: " + state);
                break;
        }
    }

    protected override void onNotFoundStickMan()
    {
//        Debug.Log("Not found stickman");
        if (monsterState == MONSTER_STATE.IDLE_LEFT)
        {
            onSetMonsterAnimation(MONSTER_STATE.WALK_LEFT, true);
        }
        else if (monsterState == MONSTER_STATE.IDLE_RIGHT)
        {
            onSetMonsterAnimation(MONSTER_STATE.WALK_RIGHT, true);
        }
        else
        {
            timeCountFind = modelMonster.timeBeginFind;
        }
    }

    private void onPlayEffectAttack(Vector3 startPos)
    {
        effectBullet = EffectManager.Instance.OnGetEffectSaliva(startPos);
        effectBullet.OnSetData(startPos, meshRenderer.sortingOrder, transform.localScale);
    }

    #endregion




}
