﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSupport;
using Spine;

public class MonsterSpearController : MonsterHandle
{

    private Vector3 startPosAttackRight = new Vector3(0.65f, 0.6f, 0);
    private Vector3 startPosAttackLeft = new Vector3(-0.65f, 0.6f, 0);


    #region Behaviour

    protected override void Awake()
    {
        base.Awake();
    }

    protected override void Start()
    {
        base.Start();
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        skeletonAnimation.AnimationName = MonsterAnimationName.spear_appear;
    }

    //    void Update()
    //    {
    //        if (GameManager.Instance.GameState == GAME_STATE.PLAYING)
    //        {
    //            onBurningMonster();
    //            onCheckMonsterState();
    //        }
    //    }
    void Update()
    {
        switch (GameManager.Instance.GameState)
        {
            case GAME_STATE.PLAYING:
                onBurningMonster();
                onCheckMonsterState();
                break;
            case GAME_STATE.GAMEOVER:
                if (skeleton.FlipX && monsterState != MONSTER_STATE.IDLE_LEFT)
                {
                    onSetMonsterAnimation(MONSTER_STATE.IDLE_LEFT, true);
                }
                else if (!skeleton.FlipX && monsterState != MONSTER_STATE.IDLE_RIGHT)
                {
                    onSetMonsterAnimation(MONSTER_STATE.IDLE_RIGHT, true);
                }
                break;
        }
    }


    #endregion

    #region Spine Events

    protected override void onCompleteEvent(Spine.TrackEntry trackEntry)
    {
//        Debug.Log("Animation Complete: " + trackEntry.Animation.Name + " state: " + monsterState);
        switch (trackEntry.Animation.Name)
        {
            case MonsterAnimationName.spear_appear:
                onEnableCollider();
                if (monsterState == MONSTER_STATE.APPEAR_LEFT)
                {
                    onSetMonsterAnimation(MONSTER_STATE.WALK_LEFT, true);
                }
                else if (monsterState == MONSTER_STATE.APPEAR_RIGHT)
                {
                    onSetMonsterAnimation(MONSTER_STATE.WALK_RIGHT, true);
                }
                break;
            case MonsterAnimationName.spear_hit:
                if (transform.position.x < playerTransform.position.x)
                {
                    onSetMonsterAnimation(MONSTER_STATE.WALK_RIGHT, true);
                }
                else
                {
                    onSetMonsterAnimation(MONSTER_STATE.WALK_LEFT, true);
                }
                timeCountFind = modelMonster.timeBeginFind;
                timeCountAttack = modelMonster.timeDelayAttack;
                break;
            case MonsterAnimationName.spear_attack:
                if (monsterState == MONSTER_STATE.ATTACK_LEFT)
                {
                    onSetMonsterAnimation(MONSTER_STATE.IDLE_LEFT, true);
                }
                else
                {
                    onSetMonsterAnimation(MONSTER_STATE.IDLE_RIGHT, true);
                }
                break;
            case MonsterAnimationName.spear_idle:
                if (transform.position.x < playerTransform.position.x)
                {
                    onSetMonsterAnimation(MONSTER_STATE.WALK_RIGHT, true);
                }
                else
                {
                    onSetMonsterAnimation(MONSTER_STATE.WALK_LEFT, true);
                }
                timeCountFind = modelMonster.timeBeginFind;
                timeCountAttack = modelMonster.timeDelayAttack;
                break;
            case MonsterAnimationName.spear_hit_thunder:
                if (transform.position.x < playerTransform.position.x)
                {
                    onSetMonsterAnimation(MONSTER_STATE.WALK_RIGHT, true);
                }
                else
                {
                    onSetMonsterAnimation(MONSTER_STATE.WALK_LEFT, true);
                }
                timeCountFind = modelMonster.timeBeginFind;
                timeCountAttack = modelMonster.timeDelayAttack;
                break;
            case MonsterAnimationName.spear_dead:
                onDead();
                break;
            case MonsterAnimationName.spear_dead_thunder:
                onDead();
                break;
            case MonsterAnimationName.spear_dead_fire:
                onDead();
                break;

        }
    }

    protected override void onStartEvent(Spine.TrackEntry trackEntry)
    {
        switch (trackEntry.Animation.Name)
        {
            case MonsterAnimationName.spear_dead:
                ListMonsterHandle.Instance.OnCheckCombo();
                break;
            case MonsterAnimationName.spear_dead_fire:
                ListMonsterHandle.Instance.OnCheckCombo();
                break;
            case MonsterAnimationName.spear_dead_thunder:
                ListMonsterHandle.Instance.OnCheckCombo();
                break;
        }
    }

    protected override void onHandleEvent(Spine.TrackEntry trackEntry, Spine.Event evt)
    {
//        Debug.Log(trackEntry.TrackIndex + " animation name: " + evt.Data.Name);
        Vector3 startPos;
        switch (evt.Data.Name)
        {
            case SpineEventAttack:
                if (monsterState == MONSTER_STATE.ATTACK_LEFT)
                {
                    startPos = transform.TransformPoint(startPosAttackLeft);
                    MonsterBulletHandler bullet = onCreateBullet(startPos, onCreateModelBullet());
                    bullet.ShootingBullet(false);
                }
                else if (monsterState == MONSTER_STATE.ATTACK_RIGHT)
                {
                    startPos = transform.TransformPoint(startPosAttackRight);
                    MonsterBulletHandler bullet = onCreateBullet(startPos, onCreateModelBullet());
                    bullet.ShootingBullet(true);
                }
                break;
            case SpineEventOnDead:
                onSpawGold(ListMonsterHandle.Instance.isCombo);
                break;
        }
    }

    #endregion

    #region Publish Methods

    public override void OnHit(int hp, bool fromRight)
    {
        base.OnHit(hp, fromRight);
    }

    public override void OnSetData(ModelMonster model, Vector3 startPos)
    {
        base.OnSetData(model, startPos);
    }

    #endregion

    #region Private and Protected Methods

    protected override void onNotFoundStickMan()
    {
        if (monsterState == MONSTER_STATE.IDLE_LEFT)
        {
            onSetMonsterAnimation(MONSTER_STATE.WALK_LEFT, true);
        }
        else if (monsterState == MONSTER_STATE.IDLE_RIGHT)
        {
            onSetMonsterAnimation(MONSTER_STATE.WALK_RIGHT, true);
        }
        else
        {
            timeCountFind = modelMonster.timeBeginFind;
        }
    }

    protected override IEnumerator onAppearMonster()
    {
        yield return new WaitForEndOfFrame();
        if (monsterState == MONSTER_STATE.APPEAR_LEFT)
        {
            skeleton.FlipX = true;
            onPlayAnimation(MonsterAnimationName.spear_appear, true);
        }
        else if (monsterState == MONSTER_STATE.APPEAR_RIGHT)
        {
            skeleton.FlipX = false;
            onPlayAnimation(MonsterAnimationName.spear_appear, false);
        }
        timeCountFind = modelMonster.timeBeginFind;
    }

    protected override void onCheckPlayerPosition()
    {
        if (Vector3.Distance(transform.position, playerTransform.position) > modelMonster.distance)
        {
            if (transform.position.x <= playerTransform.position.x)
            {
                if (monsterState == MONSTER_STATE.WALK_LEFT && timeCountFind < modelMonster.timeBeginFind)
                    return;
                onSetMonsterAnimation(MONSTER_STATE.WALK_RIGHT, true);
            }
            else if (transform.position.x > playerTransform.position.x)
            {
                if (monsterState == MONSTER_STATE.WALK_RIGHT && timeCountFind < modelMonster.timeBeginFind)
                    return;
                onSetMonsterAnimation(MONSTER_STATE.WALK_LEFT, true);
            }
        }
        else
        {
            if (Mathf.Abs(transform.position.y - playerTransform.position.y) < modelMonster.deltaY
                && Mathf.Abs(transform.position.x - playerTransform.position.x) > (modelMonster.deltaX - 0.5f)
                && transform.position.x < CameraHandle.Instance.maxCameraX
                && transform.position.x > CameraHandle.Instance.minCameraX)
            {
                onFoundStickMan();
            }
            else
            {
                onNotFoundStickMan();
            }
        }
    }

    protected override ModelBullet onCreateModelBullet()
    {
        ModelBullet model = new ModelBullet();
        model.damage = 100;
        model.distance = 20;
        model.speed = 10;
        return model;
    }

    protected override MonsterBulletHandler onCreateBullet(Vector3 startPos, ModelBullet model)
    {
        if (bulletHandler == null)
        {
            GameObject obj = Instantiate(GamePlayResources.Instance.objBulletSpear, startPos, Quaternion.identity, this.transform) as GameObject;
            obj.name = "bullet_spear";
            bulletHandler = obj.GetComponent<MonsterBulletHandler>();
            bulletHandler.OnSetBullet(startPos, model, BULLET_TYPE.SPEAR);

        }
        else
        {
            bulletHandler.gameObject.name = "bullet_spear";
            bulletHandler.OnSetBullet(startPos, model, BULLET_TYPE.SPEAR);
        }
        return bulletHandler;
    }

    protected override void onFoundStickMan()
    {
        if (transform.position.x < playerTransform.position.x && timeCountAttack > modelMonster.timeDelayAttack)
        {
            if (monsterState != MONSTER_STATE.ATTACK_RIGHT)
            {
                onSetMonsterAnimation(MONSTER_STATE.ATTACK_RIGHT, false);
            }
        }
        else if (transform.position.x > playerTransform.position.x && timeCountAttack > modelMonster.timeDelayAttack)
        {
            if (monsterState != MONSTER_STATE.ATTACK_LEFT)
            {
                onSetMonsterAnimation(MONSTER_STATE.ATTACK_LEFT, false);
            }
        }
    }



    protected override void onMoveLeft()
    {
        transform.Translate(Vector3.left * speedMove * Time.deltaTime);
        onCheckMonsterPositon();
    }

    protected override void onMoveRight()
    {
        transform.Translate(Vector3.right * speedMove * Time.deltaTime);
        onCheckMonsterPositon();
    }

    protected override void onCheckMonsterState()
    {
        switch (monsterState)
        {
            case MONSTER_STATE.WALK_LEFT:
                onMoveLeft();
                onReScale();
                onFindTarget();
                timeCountAttack += Time.deltaTime;
                break;
            case MONSTER_STATE.WALK_RIGHT:
                onMoveRight();
                onReScale();
                onFindTarget();
                timeCountAttack += Time.deltaTime;
                break;
            case MONSTER_STATE.IDLE_LEFT:
                timeCountAttack += Time.deltaTime;
                if (timeCountAttack > modelMonster.timeDelayAttack)
                {
                    onCheckPlayerPosition();
                    timeCountAttack = 0;
                }
                break;
            case MONSTER_STATE.IDLE_RIGHT:
                timeCountAttack += Time.deltaTime;
                if (timeCountAttack > modelMonster.timeDelayAttack)
                {
                    onCheckPlayerPosition();
                    timeCountAttack = 0;
                }
                break;
        }    
    }

    protected override void onSetMonsterAnimation(MONSTER_STATE state, bool loop)
    {
        monsterState = state;
        switch (state)
        {
            case MONSTER_STATE.WALK_LEFT:
                if (modelMonster.speed < 0.6f)
                {
                    onPlayAnimation(MonsterAnimationName.spear_walk, true, loop);
                }
                else
                {
                    onPlayAnimation(MonsterAnimationName.spear_run, true, loop);
                }
                break;
            case MONSTER_STATE.WALK_RIGHT:
                if (modelMonster.speed < 0.6f)
                {
                    onPlayAnimation(MonsterAnimationName.spear_walk, false, loop);    
                }
                else
                {
                    onPlayAnimation(MonsterAnimationName.spear_run, false, loop);
                }

                break;
            case MONSTER_STATE.HIT_LEFT:
                onPlayAnimation(MonsterAnimationName.spear_hit, true, loop);
                break;
            case MONSTER_STATE.HIT_RIGHT:
                onPlayAnimation(MonsterAnimationName.spear_hit, false, loop);
                break;
            case MONSTER_STATE.ATTACK_LEFT:
                onPlayAnimation(MonsterAnimationName.spear_attack, true, loop);
                break;
            case MONSTER_STATE.ATTACK_RIGHT:
                onPlayAnimation(MonsterAnimationName.spear_attack, false, loop);
                break;
            case MONSTER_STATE.IDLE_LEFT:
                onPlayAnimation(MonsterAnimationName.spear_idle, true, loop);
                break;
            case MONSTER_STATE.IDLE_RIGHT:
                onPlayAnimation(MonsterAnimationName.spear_idle, false, loop);
                break;
            case MONSTER_STATE.DEAD:
                onPlayAnimation(MonsterAnimationName.spear_dead, skeleton.FlipX, false);
//                ListMonsterHandle.Instance.OnCheckCombo();   
                break;
            case MONSTER_STATE.DEAD_THUNDER:
                onPlayAnimation(MonsterAnimationName.spear_dead_thunder, skeleton.FlipX, false);
                break;
            case MONSTER_STATE.HIT_LEFT_THUNDER:
                onPlayAnimation(MonsterAnimationName.spear_hit_thunder, true, loop);
                break;
            case MONSTER_STATE.HIT_RIGHT_THUNDER:
                onPlayAnimation(MonsterAnimationName.spear_hit_thunder, false, loop);
                break;
            case MONSTER_STATE.DEAD_FIRE:
                onPlayAnimation(MonsterAnimationName.spear_dead_fire, skeleton.FlipX, false);
                break;
        }
    }

    #endregion

}
