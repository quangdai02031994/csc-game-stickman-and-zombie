﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSupport;

public class MonsterBraveController : MonsterHandle
{
    public LayerMask layerCast;
    private float sizeBullet = 1.5f;


    #region Behaviour

    #if UNITY_EDITOR
    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, sizeBullet);
    }
    #endif

    protected override void Awake()
    {
        base.Awake();
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        skeletonAnimation.AnimationName = MonsterAnimationName.brave_appear;
    }

    protected override void Start()
    {
        base.Start();
    }

    void Update()
    {
        switch (GameManager.Instance.GameState)
        {
            case GAME_STATE.PLAYING:
                onBurningMonster();
                onCheckMonsterState();
                break;
            case GAME_STATE.GAMEOVER:
                if (skeleton.FlipX && monsterState != MONSTER_STATE.IDLE_LEFT)
                {
                    onSetMonsterAnimation(MONSTER_STATE.IDLE_LEFT, true);
                }
                else if (!skeleton.FlipX && monsterState != MONSTER_STATE.IDLE_RIGHT)
                {
                    onSetMonsterAnimation(MONSTER_STATE.IDLE_RIGHT, true);
                }
                break;
        }
    }

    #endregion

    #region Spine Events

    protected override void onStartEvent(Spine.TrackEntry trackEntry)
    {
        switch (trackEntry.Animation.Name)
        {
            case MonsterAnimationName.brave_attack:
                onDisableCollider();
                onDisableBurning();
                break;
            case MonsterAnimationName.brave_dead:
                ListMonsterHandle.Instance.OnCheckCombo();
                break;
            case MonsterAnimationName.brave_dead_fire:
                ListMonsterHandle.Instance.OnCheckCombo();
                break;
            case MonsterAnimationName.brave_dead_thunder:
                ListMonsterHandle.Instance.OnCheckCombo();
                break;
        }
    }

    protected override void onHandleEvent(Spine.TrackEntry trackEntry, Spine.Event evt)
    {
        switch (evt.Data.Name)
        {
            case SpineEventAttack:
                onKillPlayer();
                break;
            case SpineEventOnDead:
                Debug.Log(trackEntry.Animation.Name);
                onSpawGold(ListMonsterHandle.Instance.isCombo);
                break;
        }
    }

    protected override void onCompleteEvent(Spine.TrackEntry trackEntry)
    {
//        Debug.Log("Animation Complete: " + trackEntry.Animation.Name + " state: " + monsterState);
        switch (trackEntry.Animation.Name)
        {
            case MonsterAnimationName.brave_appear:
                onEnableCollider();
                if (monsterState == MONSTER_STATE.APPEAR_LEFT)
                {
                    onSetMonsterAnimation(MONSTER_STATE.WALK_LEFT, true);
                }
                else if (monsterState == MONSTER_STATE.APPEAR_RIGHT)
                {
                    onSetMonsterAnimation(MONSTER_STATE.WALK_RIGHT, true);
                }
                break;
            case MonsterAnimationName.brave_hit:
                if (skeleton.FlipX)
                {
                    onSetMonsterAnimation(MONSTER_STATE.WALK_LEFT, true);
                }
                else
                {
                    onSetMonsterAnimation(MONSTER_STATE.WALK_RIGHT, true);
                }
                break;
            case MonsterAnimationName.brave_hit_thunder:
                if (skeleton.FlipX)
                {
                    onSetMonsterAnimation(MONSTER_STATE.WALK_LEFT, true);
                }
                else
                {
                    onSetMonsterAnimation(MONSTER_STATE.WALK_RIGHT, true);
                }
                break;
            case MonsterAnimationName.brave_attack:
                onDead(true);
                break;
            case MonsterAnimationName.brave_dead:
                onDead();
                break;
            case MonsterAnimationName.brave_dead_fire:
                onDead();
                break;
            case MonsterAnimationName.brave_dead_thunder:
                onDead();
                break;
        }
    }

    #endregion

    #region Publish Methods

    public override void OnHit(int hp, bool fromRight)
    {
        base.OnHit(hp, fromRight);
    }

    public override void OnHitNuclearGun(int hp, bool fromRight)
    {
        base.OnHitNuclearGun(hp, fromRight);
    }

    public override void OnSetData(ModelMonster model, Vector3 startPos)
    {
        base.OnSetData(model, startPos);
    }

    #endregion

    #region Private and Protected Methods

    protected override void onCheckMonsterState()
    {
        switch (monsterState)
        {
            case MONSTER_STATE.WALK_LEFT:
                onMoveLeft();
                onReScale();
                onFindTarget();
                if (timeCountFind < modelMonster.timeBeginFind)
                {
                    timeCountFind += Time.deltaTime;
                }
                break;
            case MONSTER_STATE.WALK_RIGHT:
                onMoveRight();
                onReScale();
                onFindTarget();
                if (timeCountFind < modelMonster.timeBeginFind)
                {
                    timeCountFind += Time.deltaTime;
                }
                break;
            case MONSTER_STATE.IDLE_LEFT:
                timeCountAttack += Time.deltaTime;
                if (timeCountAttack > modelMonster.timeDelayAttack)
                {
                    onCheckPlayerPosition();
                    timeCountAttack = 0;
                }
                break;
            case MONSTER_STATE.IDLE_RIGHT:
                timeCountAttack += Time.deltaTime;
                if (timeCountAttack > modelMonster.timeDelayAttack)
                {
                    onCheckPlayerPosition();
                    timeCountAttack = 0;
                }
                break;
        }

    }

    protected override void onSetMonsterAnimation(MONSTER_STATE state, bool loop)
    {
        monsterState = state;
        switch (state)
        {
            case MONSTER_STATE.WALK_LEFT:
                onPlayAnimation(MonsterAnimationName.brave_walk, true, loop);
                break;
            case MONSTER_STATE.WALK_RIGHT:
                onPlayAnimation(MonsterAnimationName.brave_walk, false, loop);
                break;
            case MONSTER_STATE.HIT_RIGHT:
                onPlayAnimation(MonsterAnimationName.brave_hit, false, loop);
                break;
            case MONSTER_STATE.HIT_LEFT:
                onPlayAnimation(MonsterAnimationName.brave_hit, true, loop);
                break;
            case MONSTER_STATE.ATTACK_LEFT:
                onPlayAnimation(MonsterAnimationName.brave_attack, true, loop);
                break;
            case MONSTER_STATE.ATTACK_RIGHT:
                onPlayAnimation(MonsterAnimationName.brave_attack, false, loop);
                break;
            case MONSTER_STATE.HIT_LEFT_THUNDER:
                onPlayAnimation(MonsterAnimationName.brave_hit_thunder, true, loop);
                break;
            case MONSTER_STATE.HIT_RIGHT_THUNDER:
                onPlayAnimation(MonsterAnimationName.brave_hit_thunder, false, loop);
                break;
            case MONSTER_STATE.DEAD:
                onPlayAnimation(MonsterAnimationName.brave_dead, skeleton.FlipX, false);
//                ListMonsterHandle.Instance.OnCheckCombo();   
                break;
            case MONSTER_STATE.DEAD_FIRE:
                onPlayAnimation(MonsterAnimationName.brave_dead_fire, skeleton.FlipX, loop);
                break;
            case MONSTER_STATE.DEAD_THUNDER:
                onPlayAnimation(MonsterAnimationName.brave_dead_thunder, skeleton.FlipX, loop);
                break;
            case MONSTER_STATE.IDLE_LEFT:
                onPlayAnimation(MonsterAnimationName.brave_idle, true, loop);
                break;
            case MONSTER_STATE.IDLE_RIGHT:
                onPlayAnimation(MonsterAnimationName.brave_idle, false, loop);
                break;
            default:
                Debug.LogError("No state select: " + state);
                break;
        }
    }

    protected override IEnumerator onAppearMonster()
    {
        yield return new WaitForEndOfFrame();
        if (monsterState == MONSTER_STATE.APPEAR_LEFT)
        {
            skeleton.FlipX = true;
            onPlayAnimation(MonsterAnimationName.brave_appear, true);
        }
        else if (monsterState == MONSTER_STATE.APPEAR_RIGHT)
        {
            skeleton.FlipX = false;
            onPlayAnimation(MonsterAnimationName.brave_appear, false);
        }
        timeCountFind = modelMonster.timeBeginFind;
    }

    protected override void onMoveLeft()
    {
        if (Mathf.Abs(transform.position.x - playerTransform.position.x) > modelMonster.deltaX)
        {
            transform.Translate(Vector3.left * speedMove * Time.deltaTime);
        }
        else
        {
            if (Mathf.Abs(transform.position.y - playerTransform.position.y) > modelMonster.deltaY && timeCountFind < modelMonster.timeBeginFind)
            {
                transform.Translate(Vector3.left * speedMove * Time.deltaTime);
            }
        }
        onCheckMonsterPositon();
    }

    protected override void onMoveRight()
    {
        if (Mathf.Abs(transform.position.x - playerTransform.position.x) > modelMonster.deltaX)
        {
            transform.Translate(Vector3.right * speedMove * Time.deltaTime);
        }
        else
        {
            if (Mathf.Abs(transform.position.y - playerTransform.position.y) > modelMonster.deltaY && timeCountFind < modelMonster.timeBeginFind)
            {
                transform.Translate(Vector3.right * speedMove * Time.deltaTime);
            }
        }
        onCheckMonsterPositon();
    }

    protected override void onCheckPlayerPosition()
    {
        if (Vector3.Distance(transform.position, playerTransform.position) > modelMonster.distance)
        {
            if (transform.position.x <= playerTransform.position.x)
            {
                if (monsterState == MONSTER_STATE.WALK_LEFT && timeCountFind < modelMonster.timeBeginFind)
                    return;
                onSetMonsterAnimation(MONSTER_STATE.WALK_RIGHT, true);
            }
            else if (transform.position.x > playerTransform.position.x)
            {
                if (monsterState == MONSTER_STATE.WALK_RIGHT && timeCountFind < modelMonster.timeBeginFind)
                    return;
                onSetMonsterAnimation(MONSTER_STATE.WALK_LEFT, true);
            }
        }
        else
        {
            if (Mathf.Abs(transform.position.y - playerTransform.position.y) < modelMonster.deltaY)
            {
                onFoundStickMan();
            }
            else
            {
                onNotFoundStickMan();
            }
        }
    }

    protected override void onFoundStickMan()
    {
        if (transform.position.x < playerTransform.position.x)
        {
            if (monsterState != MONSTER_STATE.ATTACK_RIGHT)
            {
                onSetMonsterAnimation(MONSTER_STATE.ATTACK_RIGHT, false);
            }
        }
        else if (transform.position.x > playerTransform.position.x)
        {
            if (monsterState != MONSTER_STATE.ATTACK_LEFT)
            {
                onSetMonsterAnimation(MONSTER_STATE.ATTACK_LEFT, false);
            }
        }
    }

    protected override void onNotFoundStickMan()
    {
        if (monsterState == MONSTER_STATE.IDLE_LEFT)
        {
            onSetMonsterAnimation(MONSTER_STATE.WALK_LEFT, true);
            timeCountFind = 0;
        }
        else if (monsterState == MONSTER_STATE.IDLE_RIGHT)
        {
            onSetMonsterAnimation(MONSTER_STATE.WALK_RIGHT, true);
            timeCountFind = 0;
        }
        else
        {
            timeCountFind = modelMonster.timeBeginFind;
        }
    }

    private void onKillPlayer()
    {
        RaycastHit2D hit = Physics2D.CircleCast(transform.position, sizeBullet, Vector2.zero, sizeBullet, layerCast);
        if (hit.collider != null)
        {
            PlayerController.Instance.OnHit(250);
        }
    }

    #endregion



}
