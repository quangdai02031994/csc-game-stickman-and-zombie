﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;
using Spine;
using GameSupport;

public class MonsterHandle : MonoBehaviour
{
    public ModelMonster modelMonster;
    public ModelGun modelFireGun;
    public MONSTER_STATE monsterState = MONSTER_STATE.APPEAR_RIGHT;
    public bool isBurning = false;

    protected Transform playerTransform;
    protected SkeletonAnimation skeletonAnimation;
    protected Skeleton skeleton;
    protected Spine.AnimationState animationState;

    protected float timeCountAttack = 0;
    protected float timeCountFind = 0;


    protected const string SpineEventAttack = "onatk";
    protected const string SpineEventOnDead = "ondie";
    protected MonsterBulletHandler bulletHandler = null;

    protected MeshRenderer meshRenderer;
    protected float speedMove;
    protected float countBurning;
    protected float countDelayBurner = 1;

    private BoxCollider2D boxCollider2D;

    private MonsterEffectBurning effectBurning = null;

    private float maxHP;

    private float maxVertical;
    private float minVertical;
    private float heightPositonY;
    private const int MAX_RENDER = 1000;


    #region Behaviour

    protected virtual void  Awake()
    {
        boxCollider2D = GetComponent<BoxCollider2D>();
        meshRenderer = GetComponentInChildren<MeshRenderer>();
        skeletonAnimation = GetComponentInChildren<SkeletonAnimation>();
        playerTransform = (FindObjectOfType(typeof(PlayerController)) as PlayerController).transform;

    }

    void OnEnable()
    {
        if (skeleton != null)
        {
            if (skeleton.Skin.Name != modelMonster.skinName && modelMonster.skinName != "")
            {
                skeleton.SetSkin(modelMonster.skinName);
            }
        }
    }

    protected virtual void Start()
    {
        
        animationState = skeletonAnimation.state;
        skeleton = skeletonAnimation.skeleton;
        animationState.Start += onStartEvent;
        animationState.Complete += onCompleteEvent;
        animationState.Event += onHandleEvent;

        modelFireGun = GameManager.Instance.OnGetPlayerGun(PLAYER_ARMS.FIRE_GUN);
        skeleton.SetSkin(modelMonster.skinName);
        maxVertical = CameraHandle.Instance.maxY - 2.8f;
        minVertical = CameraHandle.Instance.minY + 2.5f;
        heightPositonY = Mathf.Abs(minVertical - maxVertical);
        onReScale();
    }


    protected virtual void OnDisable()
    {
        monsterState = MONSTER_STATE.APPEAR_RIGHT;
        timeCountAttack = 0;
        timeCountFind = 0;
        transform.localScale = Vector3.one;
        isBurning = false;
        countBurning = 0;
        countDelayBurner = 1;
    }



    #endregion

    #region Spine Event

    protected virtual void onStartEvent(TrackEntry trackEntry)
    {
    }


    protected virtual void onCompleteEvent(TrackEntry trackEntry)
    {
    }

    protected virtual void onHandleEvent(TrackEntry trackEntry, Spine.Event evt)
    {
    }

    #endregion

    #region Publish Methods

    public void OnAppearMonster()
    {
        if (transform.position.x > playerTransform.position.x)
        {
            monsterState = MONSTER_STATE.APPEAR_LEFT;
        }
        else
        {
            monsterState = MONSTER_STATE.APPEAR_RIGHT;
        }
        StartCoroutine(onAppearMonster());
    }

    public virtual void OnSetData(ModelMonster model, Vector3 startPos)
    {
        this.modelMonster = model;
        this.name = model.name + "_" + model.skinName;
        this.maxHP = modelMonster.hp;
        transform.position = startPos;
        if (heightPositonY != 0)
        {
            onReScale();
        }
        gameObject.SetActive(true);
    }

    public virtual void OnHit(int hp, bool fromRight)
    {
        if (monsterState == MONSTER_STATE.DEAD || monsterState == MONSTER_STATE.DEAD_FIRE || monsterState == MONSTER_STATE.DEAD_THUNDER)
            return;
        modelMonster.hp -= hp;
        if (modelMonster.hp > 0)
        {
            if (skeleton.FlipX)
            {
                onSetMonsterAnimation(MONSTER_STATE.HIT_LEFT, false);
            }
            else
            {
                onSetMonsterAnimation(MONSTER_STATE.HIT_RIGHT, false);
            }
            MonsterBloodEffect _effectBlood = EffectManager.Instance.OnGetEffectBlood();
            _effectBlood.OnSetData(transform.position, meshRenderer.sortingOrder);
            _effectBlood.OnPlay(fromRight);
        }
        else
        {
            onSetMonsterAnimation(MONSTER_STATE.DEAD, false);
            MonsterDeadEffect _effectDead = EffectManager.Instance.OnGetEffectDead();
            _effectDead.OnSetData(transform.position, meshRenderer.sortingOrder);
            _effectDead.OnPlay(fromRight);
        }
    }


    public virtual void OnHitNuclearGun(int hp, bool fromRight)
    {
        if (monsterState == MONSTER_STATE.DEAD || monsterState == MONSTER_STATE.DEAD_FIRE || monsterState == MONSTER_STATE.DEAD_THUNDER)
            return;
        modelMonster.hp -= hp;
        if (modelMonster.hp > 0)
        {
            if (skeleton.FlipX)
            {
                onSetMonsterAnimation(MONSTER_STATE.HIT_LEFT, false);
            }
            else
            {
                onSetMonsterAnimation(MONSTER_STATE.HIT_RIGHT, false);
            }
            MonsterBloodEffect _effectBlood = EffectManager.Instance.OnGetEffectBlood();
            _effectBlood.OnSetData(transform.position, meshRenderer.sortingOrder);
            _effectBlood.OnPlay(fromRight);
        }
        else
        {
            onSetMonsterAnimation(MONSTER_STATE.DEAD_FIRE, false);
//            ListMonsterHandle.Instance.OnCheckCombo();
        }
    }

    public void OnBurning(int hp)
    {
        if (monsterState == MONSTER_STATE.DEAD || monsterState == MONSTER_STATE.DEAD_FIRE || monsterState == MONSTER_STATE.DEAD_THUNDER)
            return;
        if (!isBurning)
        {
            if (effectBurning == null)
            {
                onCreateEffectBuring();
            }
            effectBurning.OnSetData(meshRenderer.sortingOrder, transform.localScale.x);
            effectBurning.OnPlay();
            isBurning = true;
        }
    }

    public void OnElectricShocking(int hp, bool isGenerate = false)
    {
        if (monsterState == MONSTER_STATE.DEAD || monsterState == MONSTER_STATE.DEAD_FIRE || monsterState == MONSTER_STATE.DEAD_THUNDER)
            return;
        if (skeletonAnimation.AnimationName == MonsterAnimationName.brave_attack)
            return;
        modelMonster.hp -= hp;
        if (modelMonster.hp > 0)
        {
            if (skeleton.FlipX)
            {
                onSetMonsterAnimation(MONSTER_STATE.HIT_LEFT_THUNDER, false);
            }
            else
            {
                onSetMonsterAnimation(MONSTER_STATE.HIT_RIGHT_THUNDER, false);
            }
        }
        else
        {
            onSetMonsterAnimation(MONSTER_STATE.DEAD_THUNDER, false);
        }

        if (isGenerate)
        {
            MonsterHandle[] _listHandler = Object.FindObjectsOfType<MonsterHandle>();
            for (int i = 0; i < _listHandler.Length; i++)
            {
                if (_listHandler[i] != this
                    && _listHandler[i].monsterState != MONSTER_STATE.APPEAR_LEFT
                    && _listHandler[i].monsterState != MONSTER_STATE.APPEAR_RIGHT
                    && _listHandler[i].monsterState != MONSTER_STATE.DEAD
                    && _listHandler[i].monsterState != MONSTER_STATE.DEAD_FIRE
                    && _listHandler[i].monsterState != MONSTER_STATE.DEAD_THUNDER
                    && _listHandler[i].modelMonster.hp > 0)
                {
                    if (_listHandler[i].transform.position.x < CameraHandle.Instance.maxCameraX
                        && _listHandler[i].transform.position.x > CameraHandle.Instance.minCameraX)
                    {
                        _listHandler[i].OnElectricShocking(hp);
                        LightningBoltHandle _handler = LightningBoltManager.Instance.OnCreateLightning(transform.position, _listHandler[i].transform.position, this.meshRenderer.sortingOrder);
                        _handler.OnPlay();
                    }
                }
            }
        }
    }

    #endregion

    #region Private Methods

    protected void onSpawGold(bool isCombo)
    {
        if (isCombo)
        {
            onGetGold(1.5f);
        }
        else
        {
            onGetGold();
        }
    }

    protected void onDead(bool isAutoDead = false)
    {
        ListMonsterHandle.Instance.OnMonsterDead();
        this.gameObject.SetActive(false);
    }

    protected virtual void onCheckMonsterState()
    {
    }

    protected void onBurningMonster()
    {
        if (isBurning)
        {
            speedMove = modelMonster.speed * 0.5f;
            countBurning += Time.deltaTime;
            if (countBurning > 5)
            {
                onDisableBurning();
                countBurning = 0;
                countDelayBurner = 1;
            }
            else
            {
                countDelayBurner += Time.deltaTime;
                if (countDelayBurner >= 1)
                {
                    if (modelFireGun != null)
                    {
                        modelMonster.hp -= modelFireGun.modelBullet.damage;
                        if (modelMonster.hp <= 0)
                        {
                            onSetMonsterAnimation(MONSTER_STATE.DEAD_FIRE, false);
                            onDisableBurning();
                        }
                    }
                    countDelayBurner = 0;
                }
            }
        }
        else
        {
            speedMove = modelMonster.speed;
        }
    }

    protected virtual void onSetMonsterAnimation(MONSTER_STATE state, bool loop)
    {
    }

    protected virtual IEnumerator onAppearMonster()
    {
        yield return null;
    }

    protected void onPlayAnimation(string animationName, bool FlipX, bool loop = false)
    {
        if (skeletonAnimation.AnimationName != animationName)
        {
            animationState.SetAnimation(0, animationName, loop);
        }
        skeleton.FlipX = FlipX;
    }

    protected virtual void onCheckPlayerPosition()
    {
    }

    protected virtual void onMoveLeft()
    {
        
    }

    protected virtual void onMoveRight()
    {
        
    }

    protected void onMoveUp()
    {
        if (transform.position.y < maxVertical)
        {
            transform.Translate(Vector3.up * speedMove / 2 * Time.deltaTime);
            onCheckMonsterPositon();
        }
    }

    protected void onMoveDown()
    {
        if (transform.position.y > minVertical)
        {
            transform.Translate(Vector3.down * speedMove / 2 * Time.deltaTime);
            onCheckMonsterPositon();
        }
    }

    protected void onReScale()
    {
        float delta = Mathf.Abs(transform.localPosition.y - minVertical) / heightPositonY;
        transform.localScale = Vector3.one - new Vector3(0.4f, 0.4f, 0.4f) * delta;
        meshRenderer.sortingOrder = MAX_RENDER - (int)(delta * 900);
    }

    protected void onCheckMonsterPositon()
    {
        if (transform.localPosition.x > CameraHandle.Instance.maxCameraX + 2)
        {
            transform.localPosition = new Vector3(CameraHandle.Instance.maxCameraX + 2, transform.localPosition.y);
            if (monsterState == MONSTER_STATE.WALK_RIGHT)
            {
                onSetMonsterAnimation(MONSTER_STATE.WALK_LEFT, true);
            }
        }
        else if (transform.localPosition.x < CameraHandle.Instance.minCameraX - 2)
        {
            transform.localPosition = new Vector3(CameraHandle.Instance.minCameraX - 2, transform.localPosition.y);
            if (monsterState == MONSTER_STATE.WALK_LEFT)
            {
                onSetMonsterAnimation(MONSTER_STATE.WALK_RIGHT, true);
            }
        }

        if (transform.localPosition.y > maxVertical)
        {
            transform.localPosition = new Vector3(transform.localPosition.x, maxVertical);
        }
        else if (transform.localPosition.y < minVertical)
        {
            transform.localPosition = new Vector3(transform.localPosition.x, minVertical);
        }
    }

    protected void onEnableCollider()
    {
        boxCollider2D.enabled = true;
    }

    protected void onDisableCollider()
    {
        boxCollider2D.enabled = false;
    }

    protected void onFindTarget()
    {
        if (timeCountFind >= modelMonster.timeBeginFind)
        {
            if (transform.position.y > playerTransform.position.y && Mathf.Abs(transform.position.y - playerTransform.position.y) > modelMonster.deltaY)
            {
                onMoveDown();
            }
            else if (transform.position.y < playerTransform.position.y && Mathf.Abs(transform.position.y - playerTransform.position.y) > modelMonster.deltaY)
            {
                onMoveUp();
            }
        }
        else
        {
//            timeCountFind += Time.deltaTime;
        }
        onCheckPlayerPosition();
    }

    protected virtual void onFoundStickMan()
    {
    }

    protected virtual void onNotFoundStickMan()
    {
    }

    protected virtual MonsterBulletHandler onCreateBullet(Vector3 startPos, ModelBullet model)
    {
        return new MonsterBulletHandler();
    }

    protected virtual ModelBullet onCreateModelBullet()
    {
        return new ModelBullet();
    }

    protected void onCreateEffectBuring()
    {
        if (effectBurning == null)
        {
            GameObject _obj = Instantiate(GamePlayResources.Instance.objEffectBurning, Vector3.zero, Quaternion.identity, this.transform) as GameObject;
            _obj.name = "effect_burning";
            _obj.transform.localPosition = new Vector3(0, -0.5f, 0);
            _obj.transform.localEulerAngles = new Vector3(-90, 0, 0);
            effectBurning = _obj.GetComponent<MonsterEffectBurning>();
        }
    }

    protected void onDisableBurning()
    {
        if (isBurning)
        {
            isBurning = false;
            countDelayBurner = 1;
        }
        if (effectBurning != null && effectBurning.gameObject.activeSelf)
        {
            effectBurning.OnStop();
        }
    }

    private void onGetGold(float value = 1)
    {
        UIGoldItemHandle _item = EffectManager.Instance.OnGetGoldItemReward(transform.position);
        _item.transform.position = transform.position;
        if (transform.position.x > CameraHandle.Instance.maxBGPositionX - 2)
        {
            _item.transform.position = new Vector3(CameraHandle.Instance.maxBGPositionX - 2, _item.transform.position.y, _item.transform.position.z);
        }
        else if (_item.transform.position.x < CameraHandle.Instance.minBGPositionX + 2)
        {
            _item.transform.position = new Vector3(CameraHandle.Instance.minBGPositionX + 2, _item.transform.position.y, _item.transform.position.z);
        }
        _item.transform.localScale = transform.localScale;
        if (maxHP < 100)
        {
            _item.OnSetData(2 * value, meshRenderer.sortingOrder);
        }
        else if (maxHP < 150)
        {
            _item.OnSetData(2 * value, meshRenderer.sortingOrder);
        }
        else if (maxHP < 200)
        {
            _item.OnSetData(2 * value, meshRenderer.sortingOrder);
        }
        else if (maxHP < 300)
        {
            _item.OnSetData(2 * value, meshRenderer.sortingOrder);
        }
        else
        {
            _item.OnSetData(2 * value, meshRenderer.sortingOrder);
        }
    }

    #endregion


}
