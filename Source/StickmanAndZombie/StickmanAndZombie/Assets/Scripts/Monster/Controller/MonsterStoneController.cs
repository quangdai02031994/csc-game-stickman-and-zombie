﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSupport;
using Spine;

public class MonsterStoneController : MonsterHandle
{
    private const string BoneStoneMonsterAttack = "bone16";

    #region Behaviour

    protected override void Awake()
    {
        base.Awake();
    }

    protected override void Start()
    {
        base.Start();
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        skeletonAnimation.AnimationName = MonsterAnimationName.stone_appear;
    }

    //    void Update()
    //    {
    //
    //        if (GameManager.Instance.GameState == GAME_STATE.PLAYING)
    //        {
    //            onBurningMonster();
    //            onCheckMonsterState();
    //        }
    //    }
    void Update()
    {
        switch (GameManager.Instance.GameState)
        {
            case GAME_STATE.PLAYING:
                onBurningMonster();
                onCheckMonsterState();
                break;
            case GAME_STATE.GAMEOVER:
                if (skeleton.FlipX && monsterState != MONSTER_STATE.IDLE_LEFT)
                {
                    onSetMonsterAnimation(MONSTER_STATE.IDLE_LEFT, true);
                }
                else if (!skeleton.FlipX && monsterState != MONSTER_STATE.IDLE_RIGHT)
                {
                    onSetMonsterAnimation(MONSTER_STATE.IDLE_RIGHT, true);
                }
                break;
        }
    }


    #endregion

    #region Spine Events

    protected override void onCompleteEvent(Spine.TrackEntry trackEntry)
    {
//        Debug.Log("Animation Complete: " + trackEntry.Animation.Name);
        switch (trackEntry.Animation.Name)
        {
            case MonsterAnimationName.stone_appear:
                onEnableCollider();
                if (monsterState == MONSTER_STATE.APPEAR_LEFT)
                {
                    onSetMonsterAnimation(MONSTER_STATE.WALK_LEFT, true);
                }
                else if (monsterState == MONSTER_STATE.APPEAR_RIGHT)
                {
                    onSetMonsterAnimation(MONSTER_STATE.WALK_RIGHT, true);
                }
                break;
            case MonsterAnimationName.stone_attack:
                if (monsterState == MONSTER_STATE.ATTACK_LEFT)
                {
                    onSetMonsterAnimation(MONSTER_STATE.IDLE_LEFT, true);
                }
                else
                {
                    onSetMonsterAnimation(MONSTER_STATE.IDLE_RIGHT, true);
                }
                break;
            case MonsterAnimationName.stone_hit:
                if (transform.position.x < playerTransform.position.x)
                {
                    onSetMonsterAnimation(MONSTER_STATE.WALK_RIGHT, true);
                }
                else
                {
                    onSetMonsterAnimation(MONSTER_STATE.WALK_LEFT, true);
                }
                timeCountFind = modelMonster.timeBeginFind;
                timeCountAttack = modelMonster.timeDelayAttack;
                break;
            case MonsterAnimationName.stone_idle:
                if (monsterState == MONSTER_STATE.IDLE_LEFT)
                {
                    onSetMonsterAnimation(MONSTER_STATE.WALK_RIGHT, true);
                }
                else if (monsterState == MONSTER_STATE.IDLE_RIGHT)
                {
                    onSetMonsterAnimation(MONSTER_STATE.WALK_LEFT, true);
                }
                break;
            
            case MonsterAnimationName.stone_hit_thunder:
                if (transform.position.x < playerTransform.position.x)
                {
                    onSetMonsterAnimation(MONSTER_STATE.WALK_RIGHT, true);
                }
                else
                {
                    onSetMonsterAnimation(MONSTER_STATE.WALK_LEFT, true);
                }
                timeCountFind = modelMonster.timeBeginFind;
                timeCountAttack = modelMonster.timeDelayAttack;
                break;
            case MonsterAnimationName.stone_dead:
                onDead();
                break;
            case MonsterAnimationName.stone_dead_thunder:
                onDead();
                break;
            case MonsterAnimationName.stone_dead_fire:
                onDead();
                break;
        }
    }

    protected override void onStartEvent(Spine.TrackEntry trackEntry)
    {
        switch (trackEntry.Animation.Name)
        {
            case MonsterAnimationName.stone_dead:
                ListMonsterHandle.Instance.OnCheckCombo();
//                onSpawGold(ListMonsterHandle.Instance.isCombo);
                break;
            case MonsterAnimationName.stone_dead_fire:
                ListMonsterHandle.Instance.OnCheckCombo();
//                onSpawGold(ListMonsterHandle.Instance.isCombo);
                break;
            case MonsterAnimationName.stone_dead_thunder:
                ListMonsterHandle.Instance.OnCheckCombo();
//                onSpawGold(ListMonsterHandle.Instance.isCombo);
                break;
        }   
    }

    protected override void onHandleEvent(Spine.TrackEntry trackEntry, Spine.Event evt)
    {
//        Debug.Log(trackEntry.Animation.Name + " Event name: " + evt.Data.Name);
        Vector3 startPos;
        switch (evt.Data.Name)
        {
            case SpineEventAttack:
                if (monsterState == MONSTER_STATE.ATTACK_LEFT)
                {
                    Bone bone = skeleton.FindBone(BoneStoneMonsterAttack);
                    startPos = skeletonAnimation.transform.TransformPoint(new Vector3(bone.WorldX, bone.WorldY, 0f));
                    MonsterBulletHandler bullet = onCreateBullet(startPos, onCreateModelBullet());
                    bullet.ShootingBullet(false);
                }
                else if (monsterState == MONSTER_STATE.ATTACK_RIGHT)
                {
                    Bone bone = skeleton.FindBone(BoneStoneMonsterAttack);
                    startPos = skeletonAnimation.transform.TransformPoint(new Vector3(bone.WorldX, bone.WorldY, 0f));
                    MonsterBulletHandler bullet = onCreateBullet(startPos, onCreateModelBullet());
                    bullet.ShootingBullet(true);
                }
                break;
            case SpineEventOnDead:
                onSpawGold(ListMonsterHandle.Instance.isCombo);
                break;
                
        }
    }

    #endregion

    #region Publish Methods

    public override void OnHit(int hp, bool fromRight)
    {
        base.OnHit(hp, fromRight);
    }

    public override void OnSetData(ModelMonster model, Vector3 startPos)
    {
        base.OnSetData(model, startPos);
    }


    #endregion

    #region Private and Proteced Methods

    protected override void onCheckMonsterState()
    {
        switch (monsterState)
        {
            case MONSTER_STATE.WALK_LEFT:
                onMoveLeft();
                onReScale();
                onFindTarget();
                timeCountAttack += Time.deltaTime;
                break;
            case MONSTER_STATE.WALK_RIGHT:
                onMoveRight();
                onReScale();
                onFindTarget();
                timeCountAttack += Time.deltaTime;
                break;
            case MONSTER_STATE.IDLE_LEFT:
                timeCountAttack += Time.deltaTime;
                if (timeCountAttack > modelMonster.timeDelayAttack)
                {
                    onCheckPlayerPosition();
                    timeCountAttack = 0;
                }
                break;
            case MONSTER_STATE.IDLE_RIGHT:
                timeCountAttack += Time.deltaTime;
                if (timeCountAttack > modelMonster.timeDelayAttack)
                {
                    onCheckPlayerPosition();
                    timeCountAttack = 0;
                }
                break;
        }    
    }

    protected override void onSetMonsterAnimation(MONSTER_STATE state, bool loop)
    {
        monsterState = state;
        switch (state)
        {
            case MONSTER_STATE.WALK_LEFT:
                if (modelMonster.speed < 0.6f)
                {
                    onPlayAnimation(MonsterAnimationName.stone_walk, true, loop);
                }
                else
                {
                    onPlayAnimation(MonsterAnimationName.stone_run, true, loop);
                }
                break;
            case MONSTER_STATE.WALK_RIGHT:
                if (modelMonster.speed < 0.6f)
                {
                    onPlayAnimation(MonsterAnimationName.stone_walk, false, loop);
                }
                else
                {
                    onPlayAnimation(MonsterAnimationName.stone_run, false, loop);
                }
                break;
            case MONSTER_STATE.IDLE_LEFT:
                onPlayAnimation(MonsterAnimationName.stone_idle, true, loop);
                break;
            case MONSTER_STATE.IDLE_RIGHT:
                onPlayAnimation(MonsterAnimationName.stone_idle, false, loop);
                break;
            case MONSTER_STATE.ATTACK_LEFT:
                onPlayAnimation(MonsterAnimationName.stone_attack, true, loop);
                break;
            case MONSTER_STATE.ATTACK_RIGHT:
                onPlayAnimation(MonsterAnimationName.stone_attack, false, loop);
                break;
            case MONSTER_STATE.HIT_LEFT:
                onPlayAnimation(MonsterAnimationName.stone_hit, true, loop);
                break;
            case MONSTER_STATE.HIT_RIGHT:
                onPlayAnimation(MonsterAnimationName.stone_hit, false, loop);
                break;
            case MONSTER_STATE.DEAD:
                onPlayAnimation(MonsterAnimationName.stone_dead, skeleton.FlipX, loop);
                break;
            case MONSTER_STATE.DEAD_THUNDER:
                onPlayAnimation(MonsterAnimationName.stone_dead_thunder, skeleton.FlipX, loop);
                break;
            case MONSTER_STATE.HIT_LEFT_THUNDER:
                onPlayAnimation(MonsterAnimationName.stone_hit_thunder, true, loop);
                break;
            case MONSTER_STATE.HIT_RIGHT_THUNDER:
                onPlayAnimation(MonsterAnimationName.stone_hit_thunder, false, loop);
                break;
            case MONSTER_STATE.DEAD_FIRE:
                onPlayAnimation(MonsterAnimationName.stone_dead_fire, skeleton.FlipX, loop);
                break;
            default:
                Debug.LogError("No state: " + state);
                break;
        }
    }

    protected override IEnumerator onAppearMonster()
    {
        yield return new WaitForEndOfFrame();
        if (monsterState == MONSTER_STATE.APPEAR_LEFT)
        {
            skeleton.FlipX = true;
            onPlayAnimation(MonsterAnimationName.stone_appear, true);
        }
        else if (monsterState == MONSTER_STATE.APPEAR_RIGHT)
        {
            skeleton.FlipX = false;
            onPlayAnimation(MonsterAnimationName.stone_appear, false);
        }
    }


    protected override void onMoveLeft()
    {
        transform.Translate(Vector3.left * speedMove * Time.deltaTime);
        onCheckMonsterPositon();
    }

    protected override void onMoveRight()
    {
        transform.Translate(Vector3.right * speedMove * Time.deltaTime);
        onCheckMonsterPositon();
    }

    protected override void onCheckPlayerPosition()
    {
        if (Vector3.Distance(transform.position, playerTransform.position) > modelMonster.distance)
        {
            if (transform.position.x <= playerTransform.position.x)
            {
                if (monsterState == MONSTER_STATE.WALK_LEFT && timeCountFind < modelMonster.timeBeginFind)
                    return;
                onSetMonsterAnimation(MONSTER_STATE.WALK_RIGHT, true);
            }
            else if (transform.position.x > playerTransform.position.x)
            {
                if (monsterState == MONSTER_STATE.WALK_RIGHT && timeCountFind < modelMonster.timeBeginFind)
                    return;
                onSetMonsterAnimation(MONSTER_STATE.WALK_LEFT, true);
            }
        }
        else
        {
            if (Mathf.Abs(transform.position.y - playerTransform.position.y) < modelMonster.deltaY
                && Mathf.Abs(transform.position.x - playerTransform.position.x) > (modelMonster.deltaX - 0.5f)
                && transform.position.x < CameraHandle.Instance.maxCameraX
                && transform.position.x > CameraHandle.Instance.minCameraX)
            {
                onFoundStickMan();
            }
            else
            {
                onNotFoundStickMan();
            }
        }
    }

    protected override void onNotFoundStickMan()
    {
        if (monsterState == MONSTER_STATE.IDLE_LEFT)
        {
            onSetMonsterAnimation(MONSTER_STATE.WALK_LEFT, true);
            timeCountFind = 0;
        }
        else if (monsterState == MONSTER_STATE.IDLE_RIGHT)
        {
            onSetMonsterAnimation(MONSTER_STATE.WALK_RIGHT, true);
            timeCountFind = 0;
        }
        else
        {
            timeCountFind = modelMonster.timeBeginFind;
        }
    }

    protected override ModelBullet onCreateModelBullet()
    {
        ModelBullet model = new ModelBullet();
        model.damage = 50;
        model.distance = 10;
        model.speed = 10;
        return model;
    }

    protected override MonsterBulletHandler onCreateBullet(Vector3 startPos, ModelBullet model)
    {
        if (bulletHandler == null)
        {
            GameObject obj = Instantiate(GamePlayResources.Instance.objBulletStone, startPos, Quaternion.identity, this.transform) as GameObject;
            obj.name = "bullet_stone";
            bulletHandler = obj.GetComponent<MonsterBulletHandler>();
            bulletHandler.OnSetBullet(startPos, model, BULLET_TYPE.STONE);

        }
        else
        {
            bulletHandler.gameObject.name = "bullet_stone";
            bulletHandler.OnSetBullet(startPos, model, BULLET_TYPE.STONE);
        }
        return bulletHandler;
    }

    protected override void onFoundStickMan()
    {
//        Debug.Log("Found stickman: " + monsterState);
        if (transform.position.x < playerTransform.position.x
            && timeCountAttack > modelMonster.timeDelayAttack)
        {
            if (monsterState != MONSTER_STATE.ATTACK_RIGHT)
            {
                onSetMonsterAnimation(MONSTER_STATE.ATTACK_RIGHT, false);
            }
        }
        else if (transform.position.x > playerTransform.position.x
                 && timeCountAttack > modelMonster.timeDelayAttack)
        {
            if (monsterState != MONSTER_STATE.ATTACK_LEFT)
            {
                onSetMonsterAnimation(MONSTER_STATE.ATTACK_LEFT, false);
            }
        }
    }

    #endregion

}
