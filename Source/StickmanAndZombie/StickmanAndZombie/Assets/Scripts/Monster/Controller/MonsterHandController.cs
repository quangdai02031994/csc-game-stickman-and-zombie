﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSupport;
using Spine;

public class MonsterHandController : MonsterHandle
{

    #region Behaviour

    protected override void Awake()
    {
        base.Awake();
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        skeletonAnimation.AnimationName = MonsterAnimationName.hand_appear;
    }


    protected override void Start()
    {
        base.Start();
    }

    void Update()
    {
        switch (GameManager.Instance.GameState)
        {
            case GAME_STATE.PLAYING:
                onBurningMonster();
                onCheckMonsterState();
                break;
            case GAME_STATE.GAMEOVER:
                if (skeleton.FlipX && monsterState != MONSTER_STATE.IDLE_LEFT)
                {
                    onSetMonsterAnimation(MONSTER_STATE.IDLE_LEFT, true);
                }
                else if (!skeleton.FlipX && monsterState != MONSTER_STATE.IDLE_RIGHT)
                {
                    onSetMonsterAnimation(MONSTER_STATE.IDLE_RIGHT, true);
                }
                break;
        }
    }


    #endregion

    #region Publish Methods

    public override void OnHit(int hp, bool fromRight)
    {
        base.OnHit(hp, fromRight);
    }

    public override void OnSetData(ModelMonster model, Vector3 startPos)
    {
        base.OnSetData(model, startPos);
        timeCountFind = model.timeBeginFind;
    }

    #endregion

    #region Spine Events


    protected override void onCompleteEvent(Spine.TrackEntry trackEntry)
    {
//        Debug.Log("Animation Complete: " + trackEntry.Animation.Name + " state: " + monsterState);
        switch (trackEntry.Animation.Name)
        {
            case MonsterAnimationName.hand_appear:
                onEnableCollider();
                if (monsterState == MONSTER_STATE.APPEAR_LEFT)
                {
                    onSetMonsterAnimation(MONSTER_STATE.WALK_LEFT, true);
                }
                else if (monsterState == MONSTER_STATE.APPEAR_RIGHT)
                {
                    onSetMonsterAnimation(MONSTER_STATE.WALK_RIGHT, true);
                }
                break;
            case MonsterAnimationName.hand_attack:
                if (monsterState == MONSTER_STATE.ATTACK_LEFT)
                {
                    onSetMonsterAnimation(MONSTER_STATE.IDLE_LEFT, true);
                }
                else if (monsterState == MONSTER_STATE.ATTACK_RIGHT)
                {
                    onSetMonsterAnimation(MONSTER_STATE.IDLE_RIGHT, true);
                }
                break;
            case MonsterAnimationName.hand_attack1:
                if (monsterState == MONSTER_STATE.ATTACK_LEFT)
                {
                    onSetMonsterAnimation(MONSTER_STATE.IDLE_LEFT, true);
                }
                else if (monsterState == MONSTER_STATE.ATTACK_RIGHT)
                {
                    onSetMonsterAnimation(MONSTER_STATE.IDLE_RIGHT, true);
                }
                break;
            case MonsterAnimationName.hand_hit:
                if (transform.position.x > playerTransform.position.x)
                {
                    onSetMonsterAnimation(MONSTER_STATE.WALK_LEFT, true);
                }
                else
                {
                    onSetMonsterAnimation(MONSTER_STATE.WALK_RIGHT, true);
                }
                break;
            case MonsterAnimationName.hand_hit_thunder:
                if (transform.position.x > playerTransform.position.x)
                {
                    onSetMonsterAnimation(MONSTER_STATE.WALK_LEFT, true);
                }
                else
                {
                    onSetMonsterAnimation(MONSTER_STATE.WALK_RIGHT, true);
                }
                break;
            case MonsterAnimationName.hand_dead:
                onDead();
                break;
            case MonsterAnimationName.hand_dead_thunder:
                onDead();
                break;
            case MonsterAnimationName.hand_dead_fire:
                onDead();
                break;

        }
    }

    protected override void onStartEvent(Spine.TrackEntry trackEntry)
    {
        switch (trackEntry.Animation.Name)
        {
            case MonsterAnimationName.hand_dead:
                ListMonsterHandle.Instance.OnCheckCombo();
                break;
            case MonsterAnimationName.hand_dead_fire:
                ListMonsterHandle.Instance.OnCheckCombo();
                break;
            case MonsterAnimationName.hand_dead_thunder:
                ListMonsterHandle.Instance.OnCheckCombo();
                break;
        }
    }

    protected override void onHandleEvent(TrackEntry trackEntry, Spine.Event evt)
    {
//        Debug.Log(trackEntry.Animation.Name + " Event name: " + evt.Data.Name);
        switch (evt.Data.Name)
        {
            case SpineEventAttack:
                onCheckAttackPlayer();
                break;
            case SpineEventOnDead:
                onSpawGold(ListMonsterHandle.Instance.isCombo);
                break;
        }
    }


    #endregion

    #region Private and Protected Methods

    private void onCheckAttackPlayer()
    {
        if (Vector3.Distance(transform.position, playerTransform.position) < modelMonster.distance)
        {
            PlayerController.Instance.OnHit(50);
        }
    }

    protected override void onCheckMonsterState()
    {
        switch (monsterState)
        {
            case MONSTER_STATE.WALK_LEFT:
                onMoveLeft();
                onReScale();
                onFindTarget();
                if (timeCountFind < modelMonster.timeBeginFind)
                    timeCountFind += Time.deltaTime;
                break;
            case MONSTER_STATE.WALK_RIGHT:
                onMoveRight();
                onReScale();
                onFindTarget();
                if (timeCountFind < modelMonster.timeBeginFind)
                    timeCountFind += Time.deltaTime;
                break;
            case MONSTER_STATE.IDLE_LEFT:
                timeCountAttack += Time.deltaTime;
                if (timeCountAttack > modelMonster.timeDelayAttack)
                {
                    onCheckPlayerPosition();
                    timeCountAttack = 0;
                }
                break;
            case MONSTER_STATE.IDLE_RIGHT:
                timeCountAttack += Time.deltaTime;
                if (timeCountAttack > modelMonster.timeDelayAttack)
                {
                    onCheckPlayerPosition();
                    timeCountAttack = 0;
                }
                break;
        }
    }

    protected override void onNotFoundStickMan()
    {
        if (monsterState == MONSTER_STATE.IDLE_LEFT)
        {
            onSetMonsterAnimation(MONSTER_STATE.WALK_LEFT, true);
            timeCountFind = 0;
        }
        else if (monsterState == MONSTER_STATE.IDLE_RIGHT)
        {
            onSetMonsterAnimation(MONSTER_STATE.WALK_RIGHT, true);
            timeCountFind = 0;
        }
        else
        {
            timeCountFind = modelMonster.timeBeginFind;
        }
    }

    protected override void onFoundStickMan()
    {
        if (transform.position.x < playerTransform.position.x)
        {
            if (monsterState != MONSTER_STATE.ATTACK_RIGHT)
            {
                onSetMonsterAnimation(MONSTER_STATE.ATTACK_RIGHT, false);
            }
        }
        else if (transform.position.x > playerTransform.position.x)
        {
            if (monsterState != MONSTER_STATE.ATTACK_LEFT)
            {
                onSetMonsterAnimation(MONSTER_STATE.ATTACK_LEFT, false);
            }
        }
    }


    protected override void onMoveRight()
    {
        if (Mathf.Abs(transform.position.x - playerTransform.position.x) > modelMonster.deltaX)
        {
            transform.Translate(Vector3.right * speedMove * Time.deltaTime);
        }
        else
        {
            if (Mathf.Abs(transform.position.y - playerTransform.position.y) > modelMonster.deltaY
                && timeCountFind < modelMonster.timeBeginFind)
            {
                transform.Translate(Vector3.right * speedMove * Time.deltaTime);
            }
        }
        onCheckMonsterPositon();
    }

    protected override void onMoveLeft()
    {
        if (Mathf.Abs(transform.position.x - playerTransform.position.x) > modelMonster.deltaX)
        {
            transform.Translate(Vector3.left * speedMove * Time.deltaTime);
        }
        else
        {
            if (Mathf.Abs(transform.position.y - playerTransform.position.y) > modelMonster.deltaY
                && timeCountFind < modelMonster.timeBeginFind)
            {
                transform.Translate(Vector3.left * speedMove * Time.deltaTime);
            }
        }
        onCheckMonsterPositon();
    }

    protected override void onSetMonsterAnimation(MONSTER_STATE state, bool loop)
    {
        monsterState = state;
        switch (state)
        {
            case MONSTER_STATE.WALK_LEFT:
                if (modelMonster.speed < 0.6f)
                {
                    onPlayAnimation(MonsterAnimationName.hand_walk, true, loop);
                }
                else
                {
                    onPlayAnimation(MonsterAnimationName.hand_run, true, loop);
                }
                break;
            case MONSTER_STATE.WALK_RIGHT:
                if (modelMonster.speed < 0.6f)
                {
                    onPlayAnimation(MonsterAnimationName.hand_walk, false, loop);
                }
                else
                {
                    onPlayAnimation(MonsterAnimationName.hand_run, false, loop);
                }
                break;
            case MONSTER_STATE.IDLE_LEFT:
                onPlayAnimation(MonsterAnimationName.hand_idle, true, loop);
                break;
            case MONSTER_STATE.IDLE_RIGHT:
                onPlayAnimation(MonsterAnimationName.hand_idle, false, loop);
                break;
            case MONSTER_STATE.ATTACK_LEFT:
                int ran = Random.Range(0, 2);
                if (ran == 0)
                {
                    onPlayAnimation(MonsterAnimationName.hand_attack, true, loop);
                }
                else
                {
                    onPlayAnimation(MonsterAnimationName.hand_attack1, true, loop);
                }
                break;
            case MONSTER_STATE.ATTACK_RIGHT:
                int rand_2 = Random.Range(0, 2);
                if (rand_2 == 0)
                {
                    onPlayAnimation(MonsterAnimationName.hand_attack, false, loop);
                }
                else
                {
                    onPlayAnimation(MonsterAnimationName.hand_attack1, false, loop);
                }
                break;
            case MONSTER_STATE.HIT_LEFT:
                onPlayAnimation(MonsterAnimationName.hand_hit, true, loop);
                timeCountAttack = 0;
                break;
            case MONSTER_STATE.HIT_RIGHT:
                onPlayAnimation(MonsterAnimationName.hand_hit, false, loop);
                timeCountAttack = 0;
                break;
            case MONSTER_STATE.DEAD:
                onPlayAnimation(MonsterAnimationName.hand_dead, skeleton.FlipX, loop);
                break;
            case MONSTER_STATE.HIT_LEFT_THUNDER:
                onPlayAnimation(MonsterAnimationName.hand_hit_thunder, true, loop);
                timeCountAttack = 0;
                break;
            case MONSTER_STATE.HIT_RIGHT_THUNDER:
                onPlayAnimation(MonsterAnimationName.hand_hit_thunder, false, loop);
                timeCountAttack = 0;
                break;
            case MONSTER_STATE.DEAD_THUNDER:
                onPlayAnimation(MonsterAnimationName.hand_dead_thunder, skeleton.FlipX, loop);
                break;
            case MONSTER_STATE.DEAD_FIRE:
                onPlayAnimation(MonsterAnimationName.hand_dead_fire, skeleton.FlipX, loop);
                break;
            default:
                Debug.LogError("No state select: " + state);
                break;
        }
    }

    protected override void onCheckPlayerPosition()
    {
        if (Vector3.Distance(transform.position, playerTransform.position) > modelMonster.distance)
        {
            if (transform.position.x <= playerTransform.position.x)
            {
                if (monsterState == MONSTER_STATE.WALK_LEFT && timeCountFind < modelMonster.timeBeginFind)
                    return;
                onSetMonsterAnimation(MONSTER_STATE.WALK_RIGHT, true);
            }
            else if (transform.position.x > playerTransform.position.x)
            {
                if (monsterState == MONSTER_STATE.WALK_RIGHT && timeCountFind < modelMonster.timeBeginFind)
                    return;
                onSetMonsterAnimation(MONSTER_STATE.WALK_LEFT, true);
            }
        }
        else
        {
            if (Mathf.Abs(transform.position.y - playerTransform.position.y) < modelMonster.deltaY)
            {
                onFoundStickMan();
            }
            else
            {
                onNotFoundStickMan();
            }
        }
    }

    protected override IEnumerator onAppearMonster()
    {
        yield return new WaitForEndOfFrame();
        if (monsterState == MONSTER_STATE.APPEAR_LEFT)
        {
            skeleton.FlipX = true;
            onPlayAnimation(MonsterAnimationName.hand_appear, true);
        }
        else if (monsterState == MONSTER_STATE.APPEAR_RIGHT)
        {
            skeleton.FlipX = false;
            onPlayAnimation(MonsterAnimationName.hand_appear, false);
        }
    }

    #endregion
}
