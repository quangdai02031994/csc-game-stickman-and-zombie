﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSupport;

public class ListMonsterHandle : MonoBehaviour
{
    public static ListMonsterHandle Instance{ get; private set; }

    public GameObject prefabMonsterHand;
    public GameObject prefabMonsterStone;
    public GameObject prefabMonsterSaliva;
    public GameObject prefabMonsterSpear;
    public GameObject prefabMonsterBrave;

    public bool isCombo = false;

    private int countMonster;
    private int monsterDead;
    private const float TIME_SPAW = 5;
    private const int MAX_MONSTER = 10;
    private Dictionary<MONSTER_TYPE,List<MonsterHandle>> ListMonster = new Dictionary<MONSTER_TYPE, List<MonsterHandle>>();

    private float countTimeSpaw = 0;

    private float timeCombo = 0.3f;
    private int countCombo;
    private ModelLevel modelLevel;

    #region Behaviour

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    void Start()
    {
        countTimeSpaw = TIME_SPAW;
        PlayerController.Instance.playerGameOver += onGameOver;
        PlayerController.Instance.playerGameWin += onGameWin;
        modelLevel = GameManager.Instance.OnGetModelLevel();

        onInitMonster();

    }

    void OnDestroy()
    {
        PlayerController.Instance.playerGameOver -= onGameOver;
        PlayerController.Instance.playerGameWin -= onGameWin;
    }

    void Update()
    {
        if (GameManager.Instance.GameState == GAME_STATE.PLAYING)
        {
            if (GameManager.Instance.OnGetModelLevel() == null)
                return;
//            switch (GameManager.Instance.OnGetModelLevel().type)
//            {
//                default:
//                    break;
//            }
            countTimeSpaw += Time.deltaTime;
            if (countTimeSpaw > TIME_SPAW && countMonster < MAX_MONSTER && modelLevel.ListMonster.Count > 0)
            {
                onSpawMonster();
            }
            if (timeCombo <= 0)
            {
                timeCombo = 0;
                countCombo = 0;
            }
            else
            {
                timeCombo -= Time.deltaTime;
            }
        }
    }


    #endregion

    #region Publish Methods

    public void OnCheckCombo()
    {
        countCombo++;
        if (timeCombo == 0)
        {
            timeCombo = 0.3f;
        }
        else
        {
            GamePlayUIHandle.Instance.OnMultipeKill(countCombo);
        }
        if (countCombo < 2)
        {
            isCombo = false;
        }
        else
        {
            isCombo = true;
        }
    }

    public void OnMonsterDead()
    {
        countMonster--;
        if (countMonster < 0)
        {
            countMonster = 0;
        }
        monsterDead++;
        GamePlayUIHandle.Instance.OnMonsterDead(monsterDead);
        if (monsterDead == modelLevel.countMonster)
        {
            StartCoroutine(PlayerController.Instance.OnGameWin());
        }
        else
        {
            if (countMonster < MAX_MONSTER && modelLevel.ListMonster.Count > 0)
            {
                onSpawMonster();
            }
        }
    }

    #endregion


    #region Private Methods

    private void onInitMonster()
    {
        for (int i = 0; i < 5; i++)
        {
            ModelMonster _model = onGetModelMonsterHand();
            onCreateMonster(Vector3.zero, _model, false);
        }

        for (int i = 0; i < 5; i++)
        {
            ModelMonster _model = onGetModelMonsterBrave();
            onCreateMonster(Vector3.zero, _model, false);
        }

        for (int i = 0; i < 5; i++)
        {
            ModelMonster _model = onGetModelMonsterSaliva();
            onCreateMonster(Vector3.zero, _model, false);
        }
        for (int i = 0; i < 5; i++)
        {
            ModelMonster _model = onGetModelMonsterSpear();
            onCreateMonster(Vector3.zero, _model, false);
        }
        for (int i = 0; i < 5; i++)
        {
            ModelMonster _model = onGetModelMonsterStone();
            onCreateMonster(Vector3.zero, _model, false);
        }
    }

    private void onGameWin()
    {
        GameManager.Instance.OnSetGameState(GAME_STATE.WIN);
        modelLevel = null;
        GameManager.Instance.OnResetLevel();
        ListMonster.Clear();
        foreach (Transform item in this.transform)
        {
            Destroy(item.gameObject);
        }
    }

    private void onGameOver()
    {
        modelLevel = null;
        GameManager.Instance.OnResetLevel();
        ListMonster.Clear();
        foreach (Transform item in this.transform)
        {
            Destroy(item.gameObject);
        }
    }

    private ModelMonster onGetModelMonster()
    {
//        int ran = Random.Range(0, 5);
//        if (ran == 0)
//        {
//            return onGetModelMonsterBrave();
//        }
//        else if (ran == 1)
//        {
//            return onGetModelMonsterHand();
//        }
//        else if (ran == 2)
//        {
//            return onGetModelMonsterSaliva();
//        }
//        else if (ran == 3)
//        {
//            return onGetModelMonsterSpear();
//        }
//        else
//        {
//            return onGetModelMonsterStone();
//        }


        int ran = Random.Range(0, modelLevel.ListMonster.Count);
        ModelMonster model = modelLevel.ListMonster[ran];
        modelLevel.ListMonster.RemoveAt(ran);
        return model;
    }

    private ModelMonster onGetModelMonsterSaliva()
    {
        ModelMonster model = new ModelMonster();
        model.type = MONSTER_TYPE.SALIVA;
        model.deltaY = Random.Range(0.1f, 0.3f);
        model.distance = Random.Range(3f, 4f);
        model.deltaX = model.distance;
        model.hp = Random.Range(50, 300);
        model.name = "Zombie_HP: " + model.hp + " " + model.type;
        model.speed = Random.Range(0.5f, 1.5f);
        model.timeBeginFind = Random.Range(5, 10);
        model.timeDelayAttack = Random.Range(0.5f, 1.0f);
        model.skinName = onGetSkin();
        return model;
    }


    private ModelMonster onGetModelMonsterHand()
    {
        ModelMonster model = new ModelMonster();
        model.type = MONSTER_TYPE.HAND;
        model.deltaY = Random.Range(0.1f, 0.2f);
        model.distance = Random.Range(0.2f, 0.35f);
        model.deltaX = Random.Range(0.2f, 0.35f);
        model.hp = Random.Range(100, 300);
        model.name = "Zombie_HP: " + model.hp + " " + model.type;
        model.speed = Random.Range(0.5f, 1);
        model.timeBeginFind = Random.Range(5, 8);
        model.timeDelayAttack = Random.Range(1.0f, 3.0f);
        model.skinName = onGetSkin();
        return model;
    }

    private ModelMonster onGetModelMonsterStone()
    {
        ModelMonster model = new ModelMonster();
        model.type = MONSTER_TYPE.STONE;
        model.deltaY = 0.2f;
        model.distance = 4;
        model.deltaX = 4;
        model.hp = Random.Range(50, 300);
        model.name = "Zombie_HP: " + model.hp + " " + model.type;
        model.speed = Random.Range(0.5f, 1.5f);
        model.timeBeginFind = Random.Range(5, 10);
        model.timeDelayAttack = Random.Range(3, 7);
        model.skinName = onGetSkin();
        return model;
    }

    private  ModelMonster onGetModelMonsterSpear()
    {
        ModelMonster model = new ModelMonster();
        model.type = MONSTER_TYPE.SPEAR;
        model.deltaY = 0.2f;
        model.distance = 4;
        model.deltaX = 4;
        model.hp = Random.Range(50, 300);
        model.name = "Zombie_HP: " + model.hp + " " + model.type;
        model.speed = Random.Range(1f, 1.5f);
        model.timeBeginFind = Random.Range(10, 15);
        model.timeDelayAttack = Random.Range(3, 7);
        model.skinName = onGetSkin();
        return model;
    }

    private ModelMonster onGetModelMonsterBrave()
    {
        ModelMonster model = new ModelMonster();
        model.type = MONSTER_TYPE.BRAVE;
        model.deltaY = 0.25f;
        model.distance = 1;
        model.deltaX = 0.75f;
        model.hp = Random.Range(150, 300);
        model.name = "Zombie_HP: " + model.hp + " " + model.type;
        model.speed = Random.Range(1.5f, 2f);
        model.timeBeginFind = Random.Range(2, 5);
        model.timeDelayAttack = Random.Range(1.0f, 3.0f);
        model.skinName = MonsterSkin.skin_default;
        return model;
    }

    private void onSpawMonster()
    {
        countMonster++;
        countTimeSpaw = 0;
        if (ListMonster.Count == 0)
        {
            ModelMonster model = onGetModelMonster();
            onCreateMonster(onGetStartPosition(), model);
        }
        else
        {
            ModelMonster model = onGetModelMonster();
            Vector3 startPos = onGetStartPosition();

            if (ListMonster.ContainsKey(model.type))
            {
                if (ListMonster[model.type].Count == 0)
                {
                    onCreateMonster(startPos, model);
                    return;
                }
                else
                {
                    for (int i = 0; i < ListMonster[model.type].Count; i++)
                    {
                        if (!ListMonster[model.type][i].gameObject.activeSelf)
                        {
                            if (startPos.x < PlayerController.Instance.transform.position.x)
                            {
                                ListMonster[model.type][i].OnSetData(model, startPos);
                            }
                            else
                            {
                                ListMonster[model.type][i].OnSetData(model, startPos);
                            }
                            ListMonster[model.type][i].OnAppearMonster();
                            return;
                        }
                    }
                    onCreateMonster(startPos, model);
                }
            }
            else
            {
                onCreateMonster(startPos, model);
            }
        }
    }


    private void onCreateMonster(Vector3 startPos, ModelMonster model, bool active = true)
    {
        GameObject prefabs = onGetPrefabMonster(model.type);
        if (prefabs != null)
        {
            GameObject obj = Instantiate(prefabs, startPos, Quaternion.identity, this.transform) as GameObject;
            MonsterHandle handler = obj.GetComponent<MonsterHandle>();
            handler.OnSetData(model, startPos);
            handler.OnAppearMonster();
            if (ListMonster.ContainsKey(model.type))
            {
                ListMonster[model.type].Add(handler);
            }
            else
            {
                List<MonsterHandle> listHandle = new List<MonsterHandle>();
                listHandle.Add(handler);
                ListMonster.Add(model.type, listHandle);
            }
            obj.SetActive(active);
        }
        else
        {
            Debug.LogError("Prefabs not found");
        }
    }

    private string onGetSkin()
    {
        int rand = Random.Range(0, 4);
        switch (rand)
        {
            case 0:
                return MonsterSkin.skin_z1;
            case 1:
                return MonsterSkin.skin_z2;
            case 2:
                return MonsterSkin.skin_z3;
            case 3:
                return MonsterSkin.skin_z4;
            default:
                return MonsterSkin.skin_z1;
        }
    }

    private GameObject onGetPrefabMonster(MONSTER_TYPE type)
    {
        
        switch (type)
        {
            case MONSTER_TYPE.HAND:
                return prefabMonsterHand;
            case MONSTER_TYPE.SPEAR:
                return prefabMonsterSpear;
            case MONSTER_TYPE.SALIVA:
                return prefabMonsterSaliva;
            case MONSTER_TYPE.STONE:
                return prefabMonsterStone;
            case MONSTER_TYPE.BRAVE:
                return prefabMonsterBrave;
            default:
                return null;
        }
    }


    private Vector3 onGetStartPosition()
    {
        float randX = Random.Range(CameraHandle.Instance.minCameraX + 1, CameraHandle.Instance.maxCameraX - 1);
        float randY = Random.Range(CameraHandle.Instance.minY + 2.5f, CameraHandle.Instance.maxY - 2.8f);
        return new Vector3(randX, randY, 0);
    }

    #endregion


}
