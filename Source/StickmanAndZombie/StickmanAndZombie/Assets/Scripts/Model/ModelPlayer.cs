﻿using System;


[Serializable]
public class ModelPlayer:BaseModel
{
    /// <summary>
    /// Strong cua Player
    /// </summary>
    public float strong;

    /// <summary>
    /// Level nang cap HP cua Player
    /// </summary>
    public int levelHP;

    /// <summary>
    /// Level nang cap Speed cua Player
    /// </summary>
    public int levelSpeed;

    /// <summary>
    /// Level nang cap Strong cua Player
    /// </summary>
    public int levelStrong;

    /// <summary>
    /// Gia tri speed thay doi khi trung doc
    /// </summary>
    public float deltaSpeedSaliva;

    /// <summary>
    /// Gia tri speed thay doi khi su dung item
    /// </summary>
    public float deltaSpeedItem;

    /// <summary>
    /// Gia tri strong thay doi khi su dung item
    /// </summary>
    public float deltaStrong;


    public ModelPlayer()
    {
        levelHP = 0;
        levelSpeed = 0;
        levelStrong = 0;
        deltaSpeedSaliva = 0;
        deltaStrong = 0;
        deltaSpeedItem = 0;
    }
}