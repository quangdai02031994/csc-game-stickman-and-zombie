﻿using System;
using System.Collections.Generic;
using GameSupport;

[Serializable]
public class ModelPlayerArm
{
    /// <summary>
    /// Danh sách súng đã mua
    /// </summary>
    public List<ModelGun> ListModelGun;

    /// <summary>
    /// Danh sách loại súng đã mua
    /// </summary>
    public List<PLAYER_ARMS> ListGunType;


    /// <summary>
    /// Danh sách ID vũ khí đã trang bị, -1 là chưa trang bị
    /// </summary>
    public int[] IDArmEquiped;

    public ModelPlayerArm()
    {
        ListModelGun = new List<ModelGun>();
        ListGunType = new List<PLAYER_ARMS>();

        IDArmEquiped = new int[3];
        IDArmEquiped[0] = (int)PLAYER_ARMS.SWORD;
        IDArmEquiped[1] = -1;
        IDArmEquiped[2] = -1;
    }
}