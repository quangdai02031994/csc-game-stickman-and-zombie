﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSupport;
using System;

[Serializable]
public class ModelPlayerItem
{
    /// <summary>
    /// Item trang bị ở vị trí số 1
    /// </summary>
    public ModelItem itemEquip;

    /// <summary>
    /// Item trang bị ở vị trí số 2
    /// </summary>
    public ModelItem itemEquip2;

    /// <summary>
    /// Danh sách các id item đã mua
    /// </summary>
    public List<ITEM_TYPE> ListItemBoughtID;

    /// <summary>
    /// Danh sách các item đã mua
    /// </summary>
    public List<ModelItem> ListItemBoughtModel;


    public ModelPlayerItem()
    {
        ListItemBoughtID = new List<ITEM_TYPE>();
        ListItemBoughtModel = new List<ModelItem>();
        itemEquip = new ModelItem();
        itemEquip2 = new ModelItem();
    }
}
