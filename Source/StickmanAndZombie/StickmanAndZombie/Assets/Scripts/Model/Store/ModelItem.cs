﻿using System;
using GameSupport;

[Serializable]
public class ModelItem
{

    public ITEM_TYPE type;

    /// <summary>
    /// Tên của item
    /// </summary>
    public string name;

    /// <summary>
    /// Giá của item bằng vàng
    /// </summary>
    public int gold;

    /// <summary>
    /// Mô tả của item
    /// </summary>
    public string description;

    /// <summary>
    /// Trạng thái của item, -1-không thể sd, 0- có thể sd, 1-đang sử dụng
    /// </summary>
    public int state;

    /// <summary>
    /// Số lượng của item trong túi
    /// </summary>
    public int quantity;

    public ModelItem()
    {
        name = "";
        gold = 0;
        description = "";
        state = -1;
        quantity = 0;
        type = ITEM_TYPE.NONE;
    }


    public ModelItem(ITEM_TYPE type, string name, int gold, string description, int state = -1)
    {
        this.name = name;
        this.gold = gold;
        this.description = description;
        this.state = state;
        quantity = 1;
        this.type = type;

    }

}
