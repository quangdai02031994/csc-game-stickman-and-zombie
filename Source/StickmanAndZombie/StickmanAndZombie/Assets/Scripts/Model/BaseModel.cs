﻿
using System;

[Serializable]
public class BaseModel
{
    /// <summary>
    /// Tên
    /// </summary>
    public string name;

    /// <summary>
    /// Máu
    /// </summary>
    public float hp;

    /// <summary>
    /// Tốc độ (nếu là quái thì tốc độ di chuyển dọc bằng 25% tốc độ di chuyển ngang)
    /// </summary>
    public float speed;

}