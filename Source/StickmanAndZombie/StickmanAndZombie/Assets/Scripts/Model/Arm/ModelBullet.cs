﻿using System;

[Serializable]
public class ModelBullet
{
    /// <summary>
    /// tầm xa của đạn
    /// </summary>
    public float distance;

    /// <summary>
    /// Số damage mỗi viên đạn
    /// </summary>
    public int damage;

    /// <summary>
    /// Tốc độ bay của đạn
    /// </summary>
    public int speed;

    /// <summary>
    /// Số mục tiêu tối đa đánh trúng
    /// </summary>
    public int countHit;

    public ModelBullet()
    {
        distance = 20;
        damage = 50;
        speed = 25;
        countHit = 1;
    }

    /// <summary>
    /// Ham khoi tao model bullet
    /// </summary>
    /// <param name="distance">Khoang cach.</param>
    /// <param name="damage">Damage.</param>
    /// <param name="speed">Toc do.</param>
    /// <param name="countHit">So luong zombie xuyen qua.</param>
    public ModelBullet(float distance, int damage, int speed, int countHit)
    {
        this.distance = distance;
        this.damage = damage;
        this.speed = speed;
        this.countHit = countHit;
    }

    public void CopyData(ModelBullet model)
    {
        this.distance = model.distance;
        this.damage = model.damage;
        this.countHit = model.countHit;
        this.speed = model.speed;
    }


}