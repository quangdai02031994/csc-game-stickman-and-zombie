﻿using System;

[Serializable]
public class ModelArm
{
    /// <summary>
    /// Tên vũ khí
    /// </summary>
    public string name;

    /// <summary>
    /// Level nâng cấp damage
    /// </summary>
    public int levelDamage;

    /// <summary>
    /// Mô tả vũ khí
    /// </summary>
    public string description;

    public ModelArm()
    {
        levelDamage = 0;
    }

}