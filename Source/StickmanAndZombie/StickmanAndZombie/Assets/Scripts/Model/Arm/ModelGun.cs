﻿using System;

[Serializable]
public class ModelGun:ModelArm
{
    /// <summary>
    /// Thông số của đạn
    /// </summary>
    public ModelBullet modelBullet;

    /// <summary>
    /// Số lượng đạn
    /// </summary>
    public int numberBullet;

    /// <summary>
    /// Level nâng cấp số lượng đạn
    /// </summary>
    public int levelNumberBullet;

    /// <summary>
    /// Trạng thái súng được trang bị hay không,
    /// 1- đã trang bị,
    /// 0- có thể trang bị,
    /// -1 không thể trang bị
    /// </summary>
    public int equipState;

    public ModelGun()
    {
        modelBullet = new ModelBullet();
        levelNumberBullet = 0;
        levelDamage = 0;
        equipState = -1;
        numberBullet = 0;
    }

}
