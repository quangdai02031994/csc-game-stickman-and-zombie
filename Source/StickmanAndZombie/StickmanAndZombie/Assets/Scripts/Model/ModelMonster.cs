﻿using GameSupport;
using System;

[Serializable]
public class ModelMonster : BaseModel
{
    /// <summary>
    /// Kiểu quái
    /// </summary>
    public MONSTER_TYPE type;

    /// <summary>
    /// Thời điểm bắt đầu tìm kiếm player
    /// </summary>
    public float timeBeginFind;

    /// <summary>
    /// Khoảng cách tấn công
    /// </summary>
    public float distance;

    /// <summary>
    /// Chênh lệch chiều dọc
    /// </summary>
    public float deltaY;

    /// <summary>
    /// Chênh lệch chiều ngang
    /// </summary>
    public float deltaX;

    /// <summary>
    /// thời gian giữa mỗi turn đánh
    /// </summary>
    public float timeDelayAttack;

    /// <summary>
    /// Tên của skin
    /// </summary>
    public string skinName;

}
