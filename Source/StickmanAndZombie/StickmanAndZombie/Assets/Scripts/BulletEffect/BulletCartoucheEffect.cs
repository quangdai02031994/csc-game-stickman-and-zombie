﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;
using Spine;

public class BulletCartoucheEffect : MonoBehaviour
{

    [SpineAnimation]
    public string[] animationName = new string[4];

    private MeshRenderer meshRenderer;
    private SkeletonAnimation skeletonAnimation;


    #region Behaviour

    void Awake()
    {
        meshRenderer = GetComponent<MeshRenderer>();
        skeletonAnimation = GetComponent<SkeletonAnimation>();
    }

    void Start()
    {
        skeletonAnimation.state.Complete += onCompleteEvent;
    }

    void OnDestroy()
    {
        skeletonAnimation.state.Complete -= onCompleteEvent;
    }

    #endregion

    #region Publish Methods

    public void OnSetData(Vector3 startPos, int orderInLayer)
    {
        transform.position = startPos;
        meshRenderer.sortingOrder = orderInLayer;
        this.gameObject.SetActive(true);
    }

    public void OnPlay(bool isRight)
    {
        int rand = Random.Range(0, animationName.Length);
        skeletonAnimation.state.SetAnimation(0, animationName[rand], false);
        skeletonAnimation.skeleton.FlipX = isRight;
    }

    #endregion

    #region Private Methods

    private void onCompleteEvent(TrackEntry trackEntry)
    {
        if (trackEntry.Animation.Name != "")
        {
            this.gameObject.SetActive(false);
        }
    }

    #endregion
}
