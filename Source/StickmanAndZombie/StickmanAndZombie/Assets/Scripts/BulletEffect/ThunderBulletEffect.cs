﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThunderBulletEffect : BulletEffect
{
    [Header("Particle System")]
    public ParticleSystem particleElectricBolt;
    public ParticleSystem particleHit;
    public ParticleSystem particleElectricity;
    public ParticleSystem particleElectricitySmall;
    public ParticleSystem particleSparkles;

    [Header("Renderer")]
    public Renderer renderElectricBolt;
    public Renderer renderHit;
    public Renderer renderElectricity;
    public Renderer renderElectricitySmall;
    public Renderer renderSparkles;

    public bool isDischarging{ get { return particleElectricBolt.isPlaying; } }



    #region Behaviour

    protected override void Start()
    {
        base.Start();
        onSetParticlePlayOnAwake(false, particleHit, particleSparkles, particleElectricitySmall, particleElectricity, particleElectricBolt);
        onSetParticlePlayLoop(true, particleElectricBolt, particleElectricity, particleElectricitySmall, particleHit, particleSparkles);
    }

  
    #endregion

    #region Publish

    public override void OnAttack(bool isLeft, Vector3 startPos)
    {
        base.OnAttack(isLeft, startPos);
        if (isLeft)
        {
            transform.localEulerAngles = new Vector3(0, 90, 0);
        }
        else
        {
            transform.localEulerAngles = new Vector3(0, -90, 0);
        }
        onPlay();
        onRelayerParticle();
    }

    public override void OnSetData(ModelBullet model)
    {
        base.OnSetData(model);
    }

    #endregion

    #region Protected and Private Methods

    protected override void onPlay()
    {
        particleHit.Play();
        particleSparkles.Play();
        particleElectricitySmall.Play();
        particleElectricity.Play();
        particleElectricBolt.Play();
    }

    protected override void onRelayerParticle()
    {
        float delta = Mathf.Abs(transform.position.y - minVertical) / heightPositonY;
        int layerOrder = MAX_RENDER - (int)(delta * 900);
        renderElectricBolt.sortingOrder = layerOrder;
        renderElectricity.sortingOrder = layerOrder + 1;
        renderElectricitySmall.sortingOrder = layerOrder + 1;
        renderHit.sortingOrder = layerOrder + 1;
        renderSparkles.sortingOrder = layerOrder + 1;
    }

    #endregion

}
