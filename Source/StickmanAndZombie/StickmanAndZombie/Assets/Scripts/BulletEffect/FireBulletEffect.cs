﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSupport;

public class FireBulletEffect : BulletEffect
{
    public ParticleSystem particleFire;
    public ParticleSystem particleSmoke;

    public bool isBurning{ get { return particleFire.isPlaying; } }

    private Renderer renderFire;
    private Renderer renderSmoke;


    #region Behaviour

    void Awake()
    {
        renderFire = GetComponent<Renderer>();
        renderSmoke = particleSmoke.GetComponent<Renderer>();
    }

    protected override void Start()
    {
        base.Start();
        onSetParticlePlayOnAwake(false, particleFire, particleSmoke);
        onSetParticlePlayLoop(true, particleFire, particleSmoke);
    }

    #endregion

    #region Publish Methods

    public override void OnSetData(ModelBullet model)
    {
        base.OnSetData(model);
    }

    public override void OnAttack(bool isLeft, Vector3 startPos)
    {
        base.OnAttack(isLeft, startPos);
        if (isLeft)
        {
            transform.localEulerAngles = new Vector3(0, -90, 0);
        }
        else
        {
            transform.localEulerAngles = new Vector3(0, 90, 0);
        }
        onPlay();
        onRelayerParticle();
    }

    public override void OnCancelAttack()
    {
        particleFire.Stop();
        particleSmoke.Stop();
    }

    #endregion

    #region Protected and Private Methods

    protected override void onPlay()
    {
        particleFire.Play();
        particleSmoke.Play();
    }

    protected override void onRelayerParticle()
    {
        float delta = Mathf.Abs(transform.position.y - minVertical) / heightPositonY;
        int layerOrder = MAX_RENDER - (int)(delta * 900);
        renderFire.sortingOrder = layerOrder;
        renderSmoke.sortingOrder = layerOrder - 1;
    }


    #endregion

}
