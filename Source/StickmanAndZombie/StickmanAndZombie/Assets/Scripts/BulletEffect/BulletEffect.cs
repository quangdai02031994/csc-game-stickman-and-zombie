﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSupport;

public class BulletEffect : MonoBehaviour
{

    public ModelBullet modelBullet;

    protected const int MAX_RENDER = 1000;
    protected float maxVertical;
    protected float minVertical;
    protected float heightPositonY;

    protected virtual void Start()
    {
        maxVertical = CameraHandle.Instance.maxY - 2.8f;
        minVertical = CameraHandle.Instance.minY + 2.5f;
        heightPositonY = Mathf.Abs(minVertical - maxVertical);
    }

    public virtual void OnSetData(ModelBullet model)
    {
        this.modelBullet = model;
    }

    public virtual void OnAttack(bool isLeft, Vector3 startPos)
    {
        transform.position = startPos;
    }

    protected virtual void onRelayerParticle()
    {
        
    }

    protected virtual void onPlay()
    {
        
    }

    public virtual void OnCancelAttack()
    {
        
    }

    protected void onSetParticlePlayOnAwake(bool isPlayOnAwake, params ParticleSystem[] listParticle)
    {
        for (int i = 0; i < listParticle.Length; i++)
        {
            var main = listParticle[i].main;
            main.playOnAwake = isPlayOnAwake;
        }
    }

    protected void onSetParticlePlayLoop(bool isLooping, params ParticleSystem[] listParticles)
    {
        for (int i = 0; i < listParticles.Length; i++)
        {
            var main = listParticles[i].main;
            main.loop = isLooping;
        }
    }

}
