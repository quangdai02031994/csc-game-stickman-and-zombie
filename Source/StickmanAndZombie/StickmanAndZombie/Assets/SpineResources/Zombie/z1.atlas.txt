
z1.png
format: RGBA8888
filter: Linear,Linear
repeat: none
aces
  rotate: true
  xy: 1231, 386
  size: 20, 49
  orig: 20, 49
  offset: 0, 0
  index: -1
arm1
  rotate: false
  xy: 1148, 6
  size: 22, 30
  orig: 22, 30
  offset: 0, 0
  index: -1
arm2
  rotate: true
  xy: 1063, 2
  size: 37, 28
  orig: 37, 28
  offset: 0, 0
  index: -1
arm3
  rotate: false
  xy: 1172, 7
  size: 20, 29
  orig: 20, 29
  offset: 0, 0
  index: -1
arm3_L
  rotate: false
  xy: 1122, 6
  size: 24, 33
  orig: 24, 33
  offset: 0, 0
  index: -1
atk1
  rotate: true
  xy: 2, 4
  size: 215, 310
  orig: 215, 310
  offset: 0, 0
  index: -1
atk2
  rotate: false
  xy: 668, 222
  size: 184, 299
  orig: 215, 310
  offset: 17, 0
  index: -1
atk3
  rotate: false
  xy: 854, 255
  size: 151, 266
  orig: 215, 310
  offset: 51, 0
  index: -1
atk4
  rotate: false
  xy: 1007, 328
  size: 91, 193
  orig: 215, 310
  offset: 89, 0
  index: -1
blood
  rotate: false
  xy: 854, 222
  size: 119, 31
  orig: 119, 31
  offset: 0, 0
  index: -1
blood_tia1
  rotate: false
  xy: 1228, 258
  size: 71, 126
  orig: 86, 146
  offset: 14, 0
  index: -1
blood_tia2
  rotate: true
  xy: 314, 2
  size: 85, 146
  orig: 86, 146
  offset: 0, 0
  index: -1
blood_tia3
  rotate: true
  xy: 462, 3
  size: 84, 133
  orig: 86, 146
  offset: 2, 0
  index: -1
body
  rotate: false
  xy: 749, 6
  size: 26, 68
  orig: 26, 68
  offset: 0, 0
  index: -1
bone_arm1
  rotate: true
  xy: 975, 228
  size: 25, 30
  orig: 25, 30
  offset: 0, 0
  index: -1
bone_arm2
  rotate: true
  xy: 977, 6
  size: 15, 43
  orig: 15, 43
  offset: 0, 0
  index: -1
bone_arm3
  rotate: false
  xy: 1093, 2
  size: 27, 37
  orig: 27, 37
  offset: 0, 0
  index: -1
bone_body
  rotate: false
  xy: 629, 2
  size: 38, 70
  orig: 38, 70
  offset: 0, 0
  index: -1
bone_head
  rotate: false
  xy: 1067, 84
  size: 83, 80
  orig: 83, 80
  offset: 0, 0
  index: -1
bone_leg1
  rotate: true
  xy: 1152, 76
  size: 15, 42
  orig: 15, 42
  offset: 0, 0
  index: -1
bone_leg2
  rotate: false
  xy: 720, 6
  size: 27, 68
  orig: 27, 68
  offset: 0, 0
  index: -1
bone_leg3
  rotate: false
  xy: 1022, 2
  size: 34, 14
  orig: 34, 14
  offset: 0, 0
  index: -1
break1
  rotate: true
  xy: 940, 42
  size: 32, 31
  orig: 32, 31
  offset: 0, 0
  index: -1
break2
  rotate: false
  xy: 795, 15
  size: 37, 57
  orig: 37, 57
  offset: 0, 0
  index: -1
break3
  rotate: true
  xy: 907, 2
  size: 37, 33
  orig: 37, 33
  offset: 0, 0
  index: -1
break4
  rotate: false
  xy: 850, 202
  size: 21, 18
  orig: 21, 18
  offset: 0, 0
  index: -1
break5
  rotate: false
  xy: 1029, 18
  size: 32, 31
  orig: 32, 31
  offset: 0, 0
  index: -1
break6
  rotate: false
  xy: 834, 15
  size: 37, 57
  orig: 37, 57
  offset: 0, 0
  index: -1
break7
  rotate: true
  xy: 942, 2
  size: 37, 33
  orig: 37, 33
  offset: 0, 0
  index: -1
die_fire
  rotate: true
  xy: 1231, 408
  size: 113, 64
  orig: 113, 64
  offset: 0, 0
  index: -1
dress
  rotate: false
  xy: 1100, 166
  size: 125, 109
  orig: 125, 109
  offset: 0, 0
  index: -1
eye
  rotate: true
  xy: 1272, 116
  size: 54, 27
  orig: 54, 27
  offset: 0, 0
  index: -1
eye_hit
  rotate: true
  xy: 1272, 60
  size: 54, 27
  orig: 54, 27
  offset: 0, 0
  index: -1
hair
  rotate: true
  xy: 778, 74
  size: 114, 93
  orig: 114, 93
  offset: 0, 0
  index: -1
hat
  rotate: false
  xy: 1100, 277
  size: 126, 107
  orig: 126, 107
  offset: 0, 0
  index: -1
hat1
  rotate: false
  xy: 1100, 386
  size: 129, 135
  orig: 129, 135
  offset: 0, 0
  index: -1
head_low1
  rotate: true
  xy: 1263, 172
  size: 84, 33
  orig: 84, 35
  offset: 0, 2
  index: -1
head_up1
  rotate: false
  xy: 972, 135
  size: 93, 74
  orig: 93, 74
  offset: 0, 0
  index: -1
hip
  rotate: false
  xy: 1298, 255
  size: 1, 1
  orig: 1, 1
  offset: 0, 0
  index: -1
incom
  rotate: false
  xy: 314, 89
  size: 340, 130
  orig: 340, 130
  offset: 0, 0
  index: -1
leg1
  rotate: false
  xy: 873, 2
  size: 32, 44
  orig: 32, 44
  offset: 0, 0
  index: -1
leg1_L
  rotate: true
  xy: 669, 2
  size: 15, 49
  orig: 15, 49
  offset: 0, 0
  index: -1
leg2
  rotate: false
  xy: 777, 2
  size: 16, 70
  orig: 16, 70
  offset: 0, 0
  index: -1
leg2_L
  rotate: true
  xy: 873, 48
  size: 26, 65
  orig: 26, 65
  offset: 0, 0
  index: -1
leg3
  rotate: false
  xy: 1150, 38
  size: 32, 14
  orig: 32, 14
  offset: 0, 0
  index: -1
luoi
  rotate: true
  xy: 1150, 54
  size: 20, 35
  orig: 20, 35
  offset: 0, 0
  index: -1
mask
  rotate: false
  xy: 873, 130
  size: 97, 90
  orig: 97, 90
  offset: 0, 0
  index: -1
shadow
  rotate: false
  xy: 668, 190
  size: 180, 30
  orig: 180, 30
  offset: 0, 0
  index: -1
shield
  rotate: false
  xy: 669, 19
  size: 49, 55
  orig: 49, 55
  offset: 0, 0
  index: -1
shield1
  rotate: true
  xy: 1240, 82
  size: 23, 30
  orig: 23, 30
  offset: 0, 0
  index: -1
smoke
  rotate: true
  xy: 656, 76
  size: 112, 120
  orig: 112, 120
  offset: 0, 0
  index: -1
stick
  rotate: false
  xy: 873, 76
  size: 98, 52
  orig: 98, 52
  offset: 0, 0
  index: -1
stone
  rotate: true
  xy: 1067, 175
  size: 34, 31
  orig: 34, 31
  offset: 0, 0
  index: -1
thunder_effect
  rotate: false
  xy: 2, 221
  size: 331, 300
  orig: 331, 300
  offset: 0, 0
  index: -1
thunder_effect1
  rotate: false
  xy: 335, 221
  size: 331, 300
  orig: 331, 300
  offset: 0, 0
  index: -1
z1_eye
  rotate: false
  xy: 1067, 41
  size: 81, 41
  orig: 81, 41
  offset: 0, 0
  index: -1
z1_eye_hit
  rotate: true
  xy: 1211, 98
  size: 64, 27
  orig: 64, 27
  offset: 0, 0
  index: -1
z1_head_low1
  rotate: true
  xy: 1227, 164
  size: 92, 34
  orig: 92, 34
  offset: 0, 0
  index: -1
z1_head_up1
  rotate: true
  xy: 1007, 211
  size: 115, 91
  orig: 115, 91
  offset: 0, 0
  index: -1
z3_body
  rotate: false
  xy: 1152, 93
  size: 57, 71
  orig: 57, 71
  offset: 0, 0
  index: -1
z3_eye
  rotate: false
  xy: 977, 23
  size: 50, 26
  orig: 50, 26
  offset: 0, 0
  index: -1
z3_eye_hit
  rotate: true
  xy: 1240, 107
  size: 55, 30
  orig: 55, 30
  offset: 0, 0
  index: -1
z3_head_low1
  rotate: true
  xy: 597, 2
  size: 85, 30
  orig: 85, 30
  offset: 0, 0
  index: -1
z3_head_up1
  rotate: false
  xy: 973, 51
  size: 92, 82
  orig: 92, 82
  offset: 0, 0
  index: -1
